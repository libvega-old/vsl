# VSL

Vega Shader Langauge - A GLSL/HLSL-like shading language utilized by [Vega](https://gitlab.com/libvega/vega).

Vega uses a bind-less/bind-once approach to resources, and a dynamic semantic-based vertex attribute binding system. VSL makes these features first-class in the language, simplifying development of shaders and making them easier to use dynamically within the engine.

A user tutorial for VSL is coming soon to the main [Vega website](https://libvega.dev/), and an overview of the binding model and implementation of VSL can be found in the [project wiki](https://gitlab.com/libvega/vsl/-/wikis/home).

## Acknowledgements

A huge thanks to the following projects for making VSL possible:

* [Premake](https://premake.github.io/) - Project configuration generator.
* [Antlr](https://www.antlr.org/index.html) - Parser/Lexer generator and visitor runtime for VSL grammar.
* [ShaderC](https://github.com/google/shaderc) - GLSL to Vulkan SPIR-V compiler library.

## Legal

VSL is licensed under the [Microsoft Public License (Ms-PL)](https://choosealicense.com/licenses/ms-pl/). This is a permissive OSS license close to the MIT license, but with an extra clause that modifications to the VSL source (*if also made open source*) need to be under a license compatible with the Ms-PL. The authors feel that this encourages code sharing without punishing closed source and commercial projects. For full details, please refer to the full text of the license.

All third-party libraries and projects are utilized within their original licenses, and, where applicible, are rehosted under the original licenses as well. These licenses are availble in at the links above, and are additionally available in the `licenses/` folder in this repo. These third party libraries belong solely to their original authors - the VSL authors make no ownership claims over them.
