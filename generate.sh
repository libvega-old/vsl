#!/bin/bash
#
# Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
# This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
# the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
#

if [ "$(uname)" = "Darwin" ]; then
	echo "ERROR: MacOS builds are not yet supported"
	exit 1
elif [ "$(expr substr $(uname -s) 1 5)" = "Linux" ]; then
	./tools/premake5.linux --file=./vsl.project gmake2
elif [ "$(expr substr $(uname -s) 1 5)" = "MINGW" ]; then
	echo "ERROR: MinGW builds are not supported"
	exit 1
elif [ $"(expr substr $(uname -s) 1 6]" = "Cygwin" ]; then
	echo "ERROR: Cygwin builds are not supported"
	exit 1
else
	echo "ERROR: Unknown build platform $(uname)"
	exit 1
fi
