/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

#include "./Common.hpp"
#include "./Shader.hpp"


namespace vsl
{

// The set of options to control shader compilation.
class CompilerOptions final
{
public:
	CompilerOptions() 
		: outputFile_{ }, saveGLSL_{ false }, saveSpirv_{ false }, saveAsm_{ false }, disableOptimization_{ false }
		, noCompile_{ false }
	{ }

	inline const String& OutputFile() const { return outputFile_; }
	inline void OutputFile(const String& outputFile) { outputFile_ = outputFile; }
	inline bool SaveGLSL() const { return saveGLSL_; }
	inline void SaveGLSL(bool save) { saveGLSL_ = save; }
	inline bool SaveSpirv() const { return saveSpirv_; }
	inline void SaveSpirv(bool save) { saveSpirv_ = save; }
	inline bool SaveAsm() const { return saveAsm_; }
	inline void SaveAsm(bool save) { saveAsm_ = save; }
	inline bool DisableOptimization() const { return disableOptimization_; }
	inline void DisableOptimization(bool disable) { disableOptimization_ = disable; }
	inline bool NoCompile() const { return noCompile_; }
	inline void NoCompile(bool noCompile) { noCompile_ = noCompile; }

private:
	String outputFile_;
	bool saveGLSL_;
	bool saveSpirv_;
	bool saveAsm_;
	bool disableOptimization_;
	bool noCompile_;
}; // class CompilerOptions


// Contains the information about an error that occured during compilation.
class CompilerError final
{
public:
	CompilerError(const String& message, uint32 line = 0, uint32 character = 0, const String& badText = {})
		: message_{ message }, line_{ line }, character_{ character }, badText_{ badText }
	{ }
	CompilerError() : message_{ }, line_{ }, character_{ }, badText_{ } { }

	// The human-readable message describing the error.
	inline const String& Message() const { return message_; }
	// The index of the source line where the error occured.
	inline uint32 Line() const { return line_; }
	// The character index within the source line where the error occured.
	inline uint32 Character() const { return character_; }
	// If retreivable, the source text that produced the error.
	inline const String& BadText() const { return badText_; }

	inline void Message(const String& message) { message_ = message; }
	inline void Line(uint32 line) { line_ = line; }
	inline void Character(uint32 character) { character_ = character; }
	inline void BadText(const String& badText) { badText_ = badText; }

private:
	String message_;
	uint32 line_;
	uint32 character_;
	String badText_;
}; // class CompilerError


// Performs setup and compilation of a single source file into a Shader object.
class Compiler final
{
public:
	//! @brief Construct a new compiler object using the given options.
	//! @param options The compiler options to use with compilations with this object.
	Compiler(const CompilerOptions& options);
	~Compiler();

	//! @brief If the compiler has encountered an error and cannot continue the compilation process.
	inline bool HasError() const { return error_.has_value(); }
	//! @brief The error message, if any, that the compiler has encountered.
	inline const CompilerError* Error() const { return &(error_.value()); }
	//! @brief Same as HasError(), checks if the compiler is valid.
	inline operator bool() const { return !HasError(); }

	//! @brief If the compiler has been given source code to compile (either directly or from a file).
	inline bool HasSource() const { return !sourceText_.empty(); }
	//! @brief If the compiler has been given source code from a file.
	inline bool HasSourceFile() const { return !sourceFile_.empty(); }

	//! @brief Sets the VSL source file to use as the primary source file for shader compilation.
	//! @param path The path to the source file.
	//! @return The compiler instance for call chaining.
	Compiler& SetSourceFile(const String& path);
	//! @brief Sets the given VSL source code as the primary source code for shader compilation.
	//! @param source The VSL source code.
	//! @return The compiler instance for call chaining.
	Compiler& SetSourceCode(const String& source);

	//! @brief Performs the compilation of the source files and returns the final compiled shader.
	//! @return The compiled shader, or \c nullptr if an error occured.
	UPtr<Shader> Compile();

private:
	const CompilerOptions options_;
	std::optional<CompilerError> error_;
	String sourceFile_;
	String sourceText_;

	VSL_NO_COPY(Compiler)
	VSL_NO_MOVE(Compiler)
}; // class Compiler

} // namespace vsl
