/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

 /* Ensure 64-bit */
#if !defined(_M_X64) && !defined(__x86_64__)
#	error VSL must be built as 64-bit.
#endif

/* OS Detection */
#if defined(_WIN32)
#	define VSL_WIN32
#	ifndef NOMINMAX
#		define NOMINMAX
#	endif
#	ifndef WIN32_LEAN_AND_MEAN
#		define WIN32_LEAN_AND_MEAN
#	endif
#elif defined(__APPLE__)
#	error Cannot compile VSL for Apple platforms.
#elif defined(__ANDROID__)
#	error Cannot compile VSL for Android platforms.
#elif defined(__linux__)
	// Technically android is linux too, but that case is caught above
#	define VSL_LINUX
#else
#	error Supported OS not detected - please use Windows or desktop Linux.
#endif // defined(_WIN32)

/* Compiler Detection */
#if defined(_MSC_VER)
#	define VSL_MSVC
#elif defined(__MINGW32__)
#	error Cannot use MinGW to compile VSL.
#elif defined(__clang__)
#	define VSL_CLANG
#elif defined(__GNUC__)
#	define VSL_GCC
#else
#	error Unsupported compiler detected - please use MSVC, GNU GCC, or Clang.
#endif // defined(_MSC_VER)

/* Import/Export Macros */
// VSL is only buildable statically, so we don't worry about import/export descriptors

/* Library Version */
#define VSL_VERSION_MAJOR 0
#define VSL_VERSION_MINOR 1
#define VSL_VERSION_PATCH 0
//! @brief Creates a 32-bit packed integer version from the major, minor, and patch components.
#define VSL_MAKE_VERSION(_maj,_min,_pat) (((_maj)<<22)|((_min)<<12)|(_pat))
//! @brief The 32-bit packed integer version of the library.
#define VSL_VERSION VSL_MAKE_VERSION(VSL_VERSION_MAJOR,VSL_VERSION_MINOR,VSL_VERSION_PATCH)


/* Common Standard Headers */
#include <cassert>
#include <cstdarg>
#include <cstddef>
#include <cstdint>
#include <cstdlib>
#include <cstring>
#include <exception>
#include <functional>
#include <memory>
#include <optional>
#include <stdexcept>
#include <string>
#include <string_view>
#include <type_traits>
#include <utility>
#include <variant>


/* Common Types and Functions */
namespace vsl
{

/* Integer Types */
//! @brief Unsigned 8-bit integer.
using uint8  = std::uint8_t;
//! @brief Unsigned 16-bit integer.
using uint16 = std::uint16_t;
//! @brief Unsigned 32-bit integer.
using uint32 = std::uint32_t;
//! @brief Unsigned 64-bit integer.
using uint64 = std::uint64_t;
//! @brief Signed 8-bit integer.
using int8   = std::int8_t;
//! @brief Signed 16-bit integer.
using int16  = std::int16_t;
//! @brief Signed 32-bit integer.
using int32  = std::int32_t;
//! @brief Signed 64-bit integer.
using int64  = std::int64_t;

/* Strings */
//! @brief std::string
using String = std::string;
//! @brief std::string_view
using StrView = std::string_view;

/* Pointer Types */
//! @brief std::unique_ptr<T, Deleter>
template<typename T, typename Deleter = std::default_delete<T>>
using UPtr = std::unique_ptr<T, Deleter>;
//! @brief std::shared_ptr<T>
template<typename T>
using SPtr = std::shared_ptr<T>;
//! @brief std::weak_ptr<T>
template<typename T>
using WPtr = std::weak_ptr<T>;

/* Other Utility Types */
//! @brief std::optional<T>
template<typename T>
using Opt = std::optional<T>;
//! @brief std::variant<Types...>
template<typename... Types>
using Var = std::variant<Types...>;

/* String Formatting */
//! @brief Creates a formatted string from the given arguments.
//! @tparam ...Args The types for the format arguments.
//! @param fmt The format string.
//! @param ...args The format arguments.
//! @return A formatted string.
template<typename... Args>
inline String FmtStr(const char* const fmt, Args&&... args)
{
	static constexpr size_t BUFSIZE{ 512 };
	char buf[BUFSIZE];
	int len = snprintf(buf, BUFSIZE, fmt, std::forward<Args>(args)...);
	return (len < 0) ? String() : String(buf, (len < BUFSIZE) ? len : BUFSIZE);
}

} // namespace vsl



/* Class Macros */
//! @brief Use within a type declaration to make the type non-copyable and default non-moveable.
#define VSL_NO_COPY(cName) public: cName(const cName&) = delete; cName& operator = (const cName&) = delete;
//! @brief Use within a type declaration to make the type non-moveable and default non-copyable.
#define VSL_NO_MOVE(cName) public: cName(cName&&) = delete; cName& operator = (cName&&) = delete;
//! @brief Use within a type declaration to make the type default copyable.
#define VSL_DEFAULT_COPY(cName) public: cName(const cName&) = default; cName& operator = (const cName&) = default;
//! @brief Use within a type declaration to make the type default moveable.
#define VSL_DEFAULT_MOVE(cName) public: cName(cName&&) = default; cName& operator = (cName&&) = default;
//! @brief Use within a type declaration to make the type non-initializable (static).
#define VSL_NO_INIT(cName) \
	public: cName() = delete; \
		void* operator new (size_t) = delete; void* operator new[] (size_t) = delete; \
		void* operator new (size_t, const std::nothrow_t&) = delete; \
		void* operator new[] (size_t, const std::nothrow_t&) = delete; \
		void* operator new (size_t, void*) = delete; void* operator new[] (size_t, void*) = delete;
