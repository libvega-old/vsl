/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

#include "./Common.hpp"


namespace vsl
{

// Mask-like shader stages enum.
enum class ShaderStages : uint32
{
	None = 0,
	Vertex =   1 << 0,
	TControl = 1 << 1,
	TEval =    1 << 2,
	Geometry = 1 << 3,
	Fragment = 1 << 4,
	AllGraphics = Vertex | TControl | TEval | Geometry | Fragment
}; // enum class ShaderStages

// ShaderStages ToString
String ToString(ShaderStages stages);
// 4-letter stage code name to stage
ShaderStages FromStageCodeName(const String& name);

inline ShaderStages operator | (ShaderStages l, ShaderStages r) { return ShaderStages(uint32(l) | uint32(r)); }
inline ShaderStages operator & (ShaderStages l, ShaderStages r) { return ShaderStages(uint32(l) & uint32(r)); }
inline ShaderStages& operator |= (ShaderStages& l, ShaderStages r) { l = l | r; return l; }
inline ShaderStages& operator &= (ShaderStages& l, ShaderStages r) { l = l & r; return l; }


// Vertex usage semantics
enum class InputSemantic
{
	POS0     = 0,
	POS1     = 1,
	COLOR0   = 2,
	COLOR1   = 3,
	COLOR2   = 4,
	COLOR3   = 5,
	NORM0    = 6,
	NORM1    = 7,
	TAN0     = 8,
	TAN1     = 9,
	BINORM0  = 10,
	BINORM1  = 11,
	UV0      = 12,
	UV1      = 13,
	UV2      = 14,
	UV3      = 15,
	BWEIGHT0 = 16,
	BWEIGHT1 = 17,
	BWEIGHT2 = 18,
	BWEIGHT3 = 19,
	BINDEX0  = 20,
	BINDEX1  = 21,
	BINDEX2  = 22,
	BINDEX3  = 23,
	USER0    = 24,
	USER1    = 25,
	USER2    = 26,
	USER3    = 27,
	INST0    = 28,
	INST1    = 29,
	INST2    = 30,
	INST3    = 31
}; // enum class InputSemantic

// InputSemantic ToString
const String& ToString(InputSemantic semantic);
// InputSemantic from string (without '#', case insensitive)
std::optional<InputSemantic> FromSemanticString(const String& str);


// The supported fragment output blending modes.
struct Blending final
{
	// The multiplicative blending factors for blend modes.
	enum class Factor
	{
		Zero     = 0,
		One      = 1,
		SrcC     = 2,
		ISrcC    = 3,
		DstC     = 4,
		IDstC    = 5,
		SrcA     = 6,
		ISrcA    = 7,
		DstA     = 8,
		IDstA    = 9,
		RefC     = 10,
		IRefC    = 11,
		RefA     = 12,
		IRefA    = 13,
		Saturate = 14
	}; // enum class Factor
	// The blending operations for blend modes.
	enum class Op
	{
		Add    = 0,
		Sub    = 1,
		RevSub = 2,
		Min    = 3,
		Max    = 4
	}; // enum class Op

public:
	constexpr Blending() : srcColor{ }, dstColor{ }, srcAlpha{ }, dstAlpha{ }, colorOp{ }, alphaOp{ } { }
	constexpr Blending(Factor srcColor, Factor dstColor, Op colorOp, Factor srcAlpha, Factor dstAlpha, Op alphaOp)
		: srcColor{ srcColor }, dstColor{ dstColor }, colorOp{ colorOp }, srcAlpha{ srcAlpha }, dstAlpha{ dstAlpha }
		, alphaOp{ alphaOp }
	{ }

	// Attempts to parse the factor from the case insensitive string.
	static std::optional<Factor> ParseFactor(const String& factor);
	// Attempts to parse the op from the case insensitive string.
	static std::optional<Op> ParseOp(const String& op);
	// Attempts to parse the predefined blending mode from the given case insensitive name.
	static std::optional<Blending> ParseBlendingName(const String& name);

public:
	Factor srcColor;
	Factor dstColor;
	Op colorOp;
	Factor srcAlpha;
	Factor dstAlpha;
	Op alphaOp;

public:
	static const Blending None;
	static const Blending Alpha;
	static const Blending Add;
	static const Blending Mul;
	static const Blending Max;
	static const Blending Min;
}; // struct Blending

// Disabled blending - One Add Zero, One Add Zero
inline constexpr Blending Blending::None{ 
	Blending::Factor::One, Blending::Factor::Zero, Blending::Op::Add,
	Blending::Factor::One, Blending::Factor::Zero, Blending::Op::Add
};
// Traditional alpha blending - SrcA Add ISrcA, One Add ISrcA
inline constexpr Blending Blending::Alpha{ 
	Blending::Factor::SrcA, Blending::Factor::ISrcA, Blending::Op::Add,
	Blending::Factor::One, Blending::Factor::ISrcA, Blending::Op::Add
};
// Alpha-aware additive blending - SrcA Add One, One Add One
inline constexpr Blending Blending::Add{ 
	Blending::Factor::SrcA, Blending::Factor::One, Blending::Op::Add,
	Blending::Factor::One, Blending::Factor::One, Blending::Op::Add
};
// Direct multiplicitive blending - DstC Add Zero, DstA Add Zero
inline constexpr Blending Blending::Mul{ 
	Blending::Factor::DstC, Blending::Factor::Zero, Blending::Op::Add,
	Blending::Factor::DstA, Blending::Factor::Zero, Blending::Op::Add
};
// Maximum value replacement blending - One Max One, One Max One
inline constexpr Blending Blending::Max{
	Blending::Factor::One, Blending::Factor::One, Blending::Op::Max,
	Blending::Factor::One, Blending::Factor::One, Blending::Op::Max
};
// Minimum value replacement blending - One Min One, One Min One
inline constexpr Blending Blending::Min{
	Blending::Factor::One, Blending::Factor::One, Blending::Op::Min,
	Blending::Factor::One, Blending::Factor::One, Blending::Op::Min
};


#pragma pack(push, 1)

// Describes a vertex input in a shader ABI.
struct InputRecord final
{
	uint8 semantic{ 0 };     // The usage semantic for the input.
	uint8 location{ 0 };     // The calculated slot index for the input.
	uint8 baseType{ 0 };     // Base type index.
	uint8 dims[2]{ 0, 0 };   // Numeric dimensions.
	uint8 arraySize{ 0 };    // Array size, or 1 for no array.
	uint8 _pad0_[2]{ 0, 0 }; // Reserved padding space.
}; // struct InputRecord
static_assert(sizeof(InputRecord) == 8);


// Describes a fragment output in a shader ABI.
struct OutputRecord final
{
	uint8 baseType;       // Base type index.
	uint8 dim0;           // Number of components in the output type.
	uint8 srcColorFactor; // Source color blend factor.
	uint8 dstColorFactor; // Dst color blend factor.
	uint8 colorOp;        // Color blend op.
	uint8 srcAlphaFactor; // Source alpha blend factor.
	uint8 dstAlphaFactor; // Dst alpha blend factor.
	uint8 alphaOp;        // Alpha blend op.
}; // struct OutputRecord
static_assert(sizeof(OutputRecord) == 8);


// Describes a binding variable field in a shader ABI.
struct BindingRecord final
{
	BindingRecord() : slot{ }, baseType{ }, stageMask{ }, buffer{ 0 } { }

	uint8 slot{ 0 };       // Binding slot index.
	uint8 baseType{ 0 };   // Base type index.
	uint16 stageMask{ 0 }; // Mask of stages that use the binding variable.
	union {
		struct {
			uint8 rank;      // Image rank.
			uint8 format;    // Image texel format.
		} image;
		struct {
			uint16 size;     // Buffer struct type size.
		} buffer;
	};
	uint8 _pad0_[2]{ 0, 0 }; // Reserved padding space.
}; // struct BindingRecord
static_assert(sizeof(BindingRecord) == 8);


// Describes a subpass input attachment in a shader ABI.
struct SubpassInputRecord final
{
	uint8 index{ 0 };        // Subpass input attachment index.
	uint8 format{ 0 };       // Input texel format.
	uint8 _pad0_[2]{ 0, 0 }; // Reserved padding space.
}; // struct SubpassInputRecord
static_assert(sizeof(SubpassInputRecord) == 4);


// Describes the uniform data in a shader ABI.
struct UniformRecord final
{
	uint16 size{ 0 };   // The size of the uniform, in bytes.
	uint16 stages{ 0 }; // The mask for stages using the uniform.
}; // struct UniformRecord


// Describes a member of the shader uniform ABI.
struct UniformMemberRecord final
{
	uint8 baseType{ 0 };     // Base type index.
	uint8 dims[2]{ 0, 0 };   // Numeric dimensions.
	uint8 arraySize{ 0 };    // Array size, or 1 for no array.
	uint16 offset{ 0 };      // Offset of the member into the uniform, in bytes.
	uint8 _pad0_[2]{ 0, 0 }; // Reserved padding space.
}; // struct UniformMemberRecord
static_assert(sizeof(UniformMemberRecord) == 8);

#pragma pack(pop)


// Represents a single shader file, either compiled from source or loaded from a bytecode file.
class Shader final
{
	friend class Compiler;

public:
	// Typedef for a vector of SPIR-V instructions.
	using Bytecode = std::vector<uint32>;

	inline static constexpr uint32 MAX_NAME_LENGTH{ 32u }; // Type names and identifiers
	inline static constexpr uint32 MAX_UNIFORM_SIZE{ 256u }; // Uniform struct size (bytes)
	inline static constexpr uint32 MAX_STRUCT_SIZE{ 512u }; // Struct size (bytes)
	inline static constexpr uint32 MAX_ARRAY_SIZE{ 32u }; // Array element length
	inline static constexpr uint32 MAX_VERTEX_ATTRIBS{ 16u }; // Vertex attribs
	inline static constexpr uint32 MAX_FRAGMENT_OUTPUTS{ 4u }; // Fragment outputs
	inline static constexpr uint32 MAX_BINDINGS{ 32u }; // Binding count
	inline static constexpr uint32 MAX_SUBPASS_INPUTS{ 4u }; // Subpass inputs

	~Shader();

	inline ShaderStages Stages() const { return stages_; }
	inline const std::vector<InputRecord>& Inputs() const { return inputs_; }
	inline const std::vector<OutputRecord>& Outputs() const { return outputs_; }
	inline const std::vector<BindingRecord>& Bindings() const { return bindings_; }
	inline const std::optional<UniformRecord>& Uniform() const { return uniform_; }
	inline const std::vector<std::pair<String, UniformMemberRecord>>& UniformMembers() const { return uniformMembers_; }
	inline const std::vector<SubpassInputRecord>& SubpassInputs() const { return subpassInputs_; }

	inline const std::unordered_map<ShaderStages, Bytecode>& Bytecodes() const { return bytecodes_; }

	// Loads a compiled shader (.vbc) file. Returns a tuple of [Shader, error].
	static std::tuple<UPtr<Shader>, String> LoadFromFile(const String& filePath);

private:
	Shader();

	// Save the shader as a binary VBC file.
	void WriteToFile(const String& filePath);

private:
	ShaderStages stages_;
	std::vector<InputRecord> inputs_;
	std::vector<OutputRecord> outputs_;
	std::vector<BindingRecord> bindings_;
	std::optional<UniformRecord> uniform_;
	std::vector<std::pair<String, UniformMemberRecord>> uniformMembers_;
	std::vector<SubpassInputRecord> subpassInputs_;
	std::unordered_map<ShaderStages, Bytecode> bytecodes_;

	VSL_NO_COPY(Shader)
	VSL_NO_MOVE(Shader)
}; // class Shader

} // namespace vsl
