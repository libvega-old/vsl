#!/bin/bash
#
# Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
# This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in 
# the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
#

# VSL ANTLR syntax parser/lexer generator script for *nix.

java -jar ../tools/antlr-4.9.2-complete.jar \
    -no-listener                            \
    -visitor                                \
    -o ../src/VSL/Grammar                   \
    -package grammar                        \
    -Xexact-output-dir                      \
    -Dlanguage=Cpp                          \
    VSLLexer.g4

java -jar ../tools/antlr-4.9.2-complete.jar \
    -no-listener                            \
    -visitor                                \
    -o ../src/VSL/Grammar                   \
    -package grammar                        \
    -Xexact-output-dir                      \
    -Dlanguage=Cpp                          \
    VSL.g4
