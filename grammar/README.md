## VSL Grammar

This directory contains the grammar files and generation scripts for the Vega Shader Language (VSL). The grammar is written in and generated with [ANTLR4](https://www.antlr.org). The generated language lexer and visitor is placed into the [Grammar directory](../src/VSL/Grammar) of the VSL project.

The grammar is split into two files: `VSLLexer.g4` which contains the lexical rules of the language, and `VSL.g4` which contains the structural rules of the language. Together, they fully describe VSL and are the single source of truth for the syntax and structure.

The grammar generation scripts are also in this directory, while the generator itself is in the tools folder in the repo root. The system generating the grammar needs the Java Runtime installed. Any changes to the grammar file should be generated, with the new files checked in so that others may use the updated grammar without needing to necessarily install Java themselves.
