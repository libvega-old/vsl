/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include <VSL/Common.hpp>
#include "./Args.hpp"

#include <iostream>


int main(int argc, char* argv[])
{
	using namespace vsl;

	// No-args check
	if (argc < 2) {
		std::cerr << "Usage: " << argv[0] << " [options] <file>" << std::endl;
		return 1;
	}

	// Parse command line
	vslc::ParseResult parsed{};
	if (!vslc::ParseCommandLine(uint32(argc), (const char**)argv, parsed)) {
		return 1;
	}
	if (parsed.help) {
		vslc::PrintHelp(argv[0]);
		return 0;
	}

	// Perform the compilation
#if !defined(VSL_DEBUG)
	try {
#endif
		// Initial compilation
		Compiler compiler{ parsed.compilerOpts };
		if (!compiler.SetSourceFile({ argv[argc - 1] })) {
			std::cerr << "Error: " << compiler.Error()->Message() << std::endl;
			return 2;
		}
		const auto shader = compiler.Compile();

		// Check initial compilation
		if (!shader) {
			const auto error = compiler.Error();
			std::cerr << "Compiler error at [" << error->Line() << ':' << error->Character() << "]";
			if (!error->BadText().empty()) {
				std::cerr << " ('" << error->BadText() << "')";
			}
			std::cerr << " - " << error->Message() << std::endl;
			return 3;
		}
#if !defined(VSL_DEBUG)
	}
	catch (const std::exception& ex) {
		std::cerr << "Unhandled exception: " << ex.what() << std::endl;
		return -1;
	}
#endif // !defined(VSL_DEBUG)

	return 0;
}
