/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include <VSL/Common.hpp>
#include <VSL/Compiler.hpp>


namespace vslc
{

// Contains the result of a command line parsing
struct ParseResult final
{
public:
	vsl::CompilerOptions compilerOpts;
	bool help;
}; // struct ParseResult


// Parse the command line, returning if successful
bool ParseCommandLine(vsl::uint32 argc, const char* argv[], ParseResult& result);

// Print the help text
void PrintHelp(const char* const arg0);

} // namespace vslc
