/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "./Args.hpp"

#include <filesystem>
#include <iostream>
namespace fs = std::filesystem;


namespace vslc
{

// ====================================================================================================================
// Return is [isFlag, argName, paramName, paramValue]
static std::tuple<bool, std::string, std::string, std::string> _NormalizeArg(const std::string& arg)
{
	if (arg[0] == '-') {
		const auto isLong = (arg.length() > 1) && (arg[1] == '-');
		const auto valIndex = arg.find('=');
		const auto hasValue = valIndex != std::string::npos;
		if (arg.length() == (isLong ? 2 : 1)) {
			return { false, "", "", "" };
		}

		if (hasValue) {
			return isLong
				? std::make_tuple(true, arg.substr(2, valIndex - 2), "", arg.substr(valIndex + 1))
				: std::make_tuple(true, std::string{ arg[1] }, arg.substr(2, valIndex - 2), arg.substr(valIndex + 1));
		}
		else {
			return isLong
				? std::make_tuple(true, arg.substr(2), "", "")
				: std::make_tuple(true, std::string{ arg[1] }, arg.substr(2), "");
		}
	}
	else {
		return { false, arg, "", "" };
	}
}

// ====================================================================================================================
#define ERROR(msg) { std::cerr << msg << std::endl; return false; }
bool ParseCommandLine(vsl::uint32 argc, const char* argv[], ParseResult& result)
{
	using namespace vsl;

	auto& options = result.compilerOpts = {};
	result.help = false;

	// Default output file
	const auto inputPath{ fs::absolute(fs::path{ argv[argc - 1] }) };
	const String defaultOutput{ (inputPath.parent_path() / inputPath.stem()).string() + ".vbc" };
	options.OutputFile(defaultOutput);

	// Loop over the arguments
	for (uint32 ai = 0; ai < argc; ++ai) {
		const auto [isFlag, argName, paramName, paramValue] = _NormalizeArg({ argv[ai] });
		if (argName.empty()) {
			continue;
		}

		if ((argName == "h") || (argName == "?") || (argName == "help")) {
			result.help = true;
			return true;
		}
		else if (argName == "S") { // Saving intermediate artifacts
			if (paramName == "all") {
				options.SaveGLSL(true);
				options.SaveSpirv(true);
				options.SaveAsm(true);
			}
			else if (paramName == "glsl") {
				options.SaveGLSL(true);
			}
			else if (paramName == "spirv") {
				options.SaveSpirv(true);
			}
			else if (paramName == "asm") {
				options.SaveAsm(true);
			}
			else {
				ERROR(FmtStr("Unknown intermediate format '%s'", paramName.c_str()));
			}
		}
		else if (argName == "O") { // Optimization
			if (paramName == "s") {
				options.DisableOptimization(false);
			}
			else if (paramName == "d") {
				options.DisableOptimization(true);
			}
			else {
				ERROR(FmtStr("Unknown optimization level '%s'", paramName.c_str()));
			}
		}
		else if (argName == "no-compile") {
			options.NoCompile(true);
		}
	}

	return true;
}
#undef ERROR

// ====================================================================================================================
void PrintHelp(const char* const arg0)
{
	std::cout
		<< "Vega Shader Language Compiler (vslc)\n"
		<< "Usage: " << arg0 << " [options] <file>\n"
		<< "Options:\n"
		<< "  -h;-?;--help        - Show the help text (this text)\n"
		<< "  -S<format>          - Save intermediate compilation artifacts, supported <format>:\n"
		<< "                          glsl:  Save intermediate GLSL code\n"
		<< "                          spirv: Save intermediate SPIR-V bytecode\n"
		<< "                          asm:   Save intermediate SPIR-V text assembly\n"
		<< "                          all:   Save GLSL code, SPIR-V bytecode, and SPIR-V assembly\n"
		<< "  -O<level>           - Set the optimization level for bytecode, supported <level>:\n"
		<< "                          s: Optimization for speed (default)\n"
		<< "                          d: Disable optimization\n"
		<< "  --no-compile        - Skip bytecode compilation, will only validate the shader\n"
		<< std::endl;
}

} // namespace vslc
