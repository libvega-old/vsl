/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "./Common.Impl.hpp"
#include "./Parser/Parser.hpp"
#include "./Generator/FunctionGenerator.hpp"
#include "./Generator/StageGenerator.hpp"
#include "./SPIRV/SpirvCompiler.hpp"

#include <filesystem>
#include <fstream>
namespace fs = std::filesystem;

#define ERR_RETURN(msg) { error_.emplace((msg), 0, 0, ""); return *this; }


namespace vsl
{

// ====================================================================================================================
Compiler::Compiler(const CompilerOptions& options)
	: options_{ options }
	, error_{ }
	, sourceFile_{ }
	, sourceText_{ }
{

}

// ====================================================================================================================
Compiler::~Compiler()
{

}

// ====================================================================================================================
Compiler& Compiler::SetSourceFile(const String& path)
{
	if (HasError()) {
		return *this;
	}

	// Check and open the file
	std::error_code ec{ };
	fs::path filePath{ path };
	if (!fs::exists(filePath, ec) || !fs::is_regular_file(filePath, ec)) {
		ERR_RETURN(FmtStr("Unknown source file '%s'", path.c_str()));
	}
	std::ifstream stream{ filePath };
	if (!stream.is_open()) {
		ERR_RETURN(FmtStr("Failed to open source file '%s'", path.c_str()));
	}

	// Read in all source text
	const auto sourceLen = fs::file_size(filePath, ec);
	sourceText_.resize(sourceLen);
	stream.read(sourceText_.data(), sourceLen);
	sourceFile_ = path;

	return *this;
}

// ====================================================================================================================
Compiler& Compiler::SetSourceCode(const String& source)
{
	if (HasError()) {
		return *this;
	}

	// Store the source and clear any file
	sourceText_ = source;
	sourceFile_.clear();

	return *this;
}

// ====================================================================================================================
UPtr<Shader> Compiler::Compile()
{
	static const auto memberRecord = [](const StructType::Member& mem, UniformMemberRecord& rec) {
		rec.baseType = uint8(mem.type->baseType);
		rec.dims[0] = uint8(mem.type->numeric.dims[0]);
		rec.dims[1] = uint8(mem.type->numeric.dims[1]);
		rec.arraySize = uint8(mem.arraySize);
		rec.offset = uint16(mem.offset);
	};

	// Check state
	if (HasError()) {
		return { nullptr };
	}
	if (!HasSource()) {
		error_.emplace("Cannot compile without specifying shader source");
		return { nullptr };
	}

	// Perform the parsing
	Parser parser{ options_ };
	UPtr<ShaderInfo> shaderInfo{ new ShaderInfo };
	std::vector<UPtr<FunctionGenerator>> functions{ };
	if (!parser.Parse(sourceText_, *shaderInfo, functions)) {
		const auto perr = parser.Error();
		error_.emplace(perr->Message(), perr->Line(), perr->Character(), perr->BadText());
		return { nullptr };
	}

	// Perform per-stage generation
	std::vector<UPtr<StageGenerator>> stages{ };
	for (const auto& funcgen : functions) {
		if (funcgen->Stage() == ShaderStages::None) {
			continue;
		}
		const auto& stageGen = stages.emplace_back(new StageGenerator(&options_, funcgen->Stage()));
		stageGen->Generate(*funcgen, *shaderInfo);
		if (options_.SaveGLSL()) {
			stageGen->Save();
		}
	}

	// Perform compilation if not disabled
	std::vector<UPtr<SpirvCompiler>> spirvs{ };
	if (!options_.NoCompile()) {
		for (const auto& stage : stages) {
			const auto& spirv = spirvs.emplace_back(new SpirvCompiler(&options_));
			spirv->Compile(*stage, *shaderInfo);
			if (options_.SaveSpirv()) {
				spirv->SaveBytecode(*stage, *shaderInfo);
			}
			if (options_.SaveAsm()) {
				spirv->SaveAssembly(*stage, *shaderInfo);
			}
		}
	}

	// Create the shader object
	UPtr<Shader> shader{ new Shader() };
	shader->stages_ = shaderInfo->Stages();
	for (const auto& input : shaderInfo->Inputs()) {
		auto& refinput = shader->inputs_.emplace_back();
		input.ToRecord(&refinput);
	}
	for (const auto& output : shaderInfo->Outputs()) {
		auto& refoutput = shader->outputs_.emplace_back();
		output.ToRecord(&refoutput);
	}
	for (const auto& binding : shaderInfo->Bindings()) {
		auto& refbinding = shader->bindings_.emplace_back();
		binding.ToRecord(&refbinding);
	}
	if (shaderInfo->Uniform().has_value()) {
		auto& refuniform = shader->uniform_.emplace();
		refuniform.size = uint16(shaderInfo->Uniform().value().type->userStruct.type->Size());
		refuniform.stages = uint16(shaderInfo->Uniform().value().stages);
		for (const auto& member : shaderInfo->Uniform().value().type->userStruct.type->Members()) {
			auto& refmember = shader->uniformMembers_.emplace_back();
			refmember.first = member.name;
			memberRecord(member, refmember.second);
		}
	}
	for (const auto& spinput : shaderInfo->SubpassInputs()) {
		auto& refspinput = shader->subpassInputs_.emplace_back();
		spinput.ToRecord(&refspinput);
	}
	if (!options_.NoCompile()) {
		for (uint32 si = 0; si < stages.size(); ++si) {
			shader->bytecodes_.insert({ stages[si]->Stage(), spirvs[si]->Bytecode() });
		}
	}

	// Save the shader if requested
	if (!options_.NoCompile()) {
		shader->WriteToFile(options_.OutputFile());
	}

	return shader;
}

} // namespace vsl
