/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

//// !!!!!!!!!!
//// It is IMPORTANT not to include any third-party headers in this file. This file is used as the sole source for the
//// lightweight loader library, and it absolutely should not have any additional dependencies. If there comes a time
//// where it cannot be reasonable avoided to include those headers in this file, we will need to rethink the linking.
//// !!!!!!!!!!

#include <VSL/Type.hpp>
#include "./Types.Builtin.hpp"

#include <array>


namespace vsl
{

// ====================================================================================================================
const String& ToString(BaseType type)
{
	static const std::array<String, uint32(BaseType::Struct) + 1> NAMES{
		"Void", "Boolean", "Signed", "Unsigned", "Float", "Sampler", "Image", "ROBuffer", "RWBuffer", "ROTexels",
		"RWTexels", "SubpassInput", "Struct"
	};
	return NAMES.at(uint32(type));
}


// ====================================================================================================================
// ====================================================================================================================
const String& TexelRank::ToString() const
{
	static const std::array<String, TexelRank::Buffer.Value() + 1> NAMES{
		"E1D", "E2D", "E3D", "E1DArray", "E2DArray", "Cube", "Buffer"
	};
	return NAMES.at(value_);
}

// ====================================================================================================================
uint32 TexelRank::DimensionCount() const
{
	static const std::array<uint32, TexelRank::Buffer.Value() + 1> VALUES{
		1u, 2u, 3u, 2u, 3u, 3u, 1u
	};
	return VALUES.at(value_);
}

// ====================================================================================================================
const String& TexelRank::TypeSuffix() const
{
	static const std::array<String, TexelRank::Buffer.Value() + 1> VALUES{
		"1D", "2D", "3D", "1DArray", "2DArray", "Cube", "Buffer"
	};
	return VALUES.at(value_);
}


// ====================================================================================================================
// ====================================================================================================================
const String& ToString(TexelType type)
{
	static const std::array<String, uint32(TexelType::SNorm) + 1> NAMES{
		"Signed", "Unsigned", "Float", "UNorm", "SNorm"
	};
	return NAMES.at(uint32(type));
}


// ====================================================================================================================
// ====================================================================================================================
const String& TexelFormat::ToString() const
{
	static const std::array<String, TexelFormat::Float4.Value() + 1> NAMES{
		"U8Norm", "U8Norm2", "U8Norm4", "S8Norm", "S8Norm2", "S8Norm4",
		"U16Norm", "U16Norm2", "U16Norm4", "S16Norm", "S16Norm2", "S16Norm4",
		"Int", "Int2", "Int4", "UInt", "UInt2", "UInt4",
		"Float", "Float2", "Float4"
	};
	return NAMES.at(value_);
}

// ====================================================================================================================
const TexelFormat::TexelInfo& TexelFormat::info() const
{
	static const std::array<TexelInfo, TexelFormat::Float4.Value() + 1> VALUES{
		TexelInfo{ TexelType::UNorm, 1, 1 }, { TexelType::UNorm, 2, 1 }, { TexelType::UNorm, 4, 1 },
		{ TexelType::SNorm, 1, 1 }, { TexelType::SNorm, 2, 1 }, { TexelType::SNorm, 4, 1 },
		{ TexelType::UNorm, 1, 2 }, { TexelType::UNorm, 2, 2 }, { TexelType::UNorm, 4, 2 },
		{ TexelType::SNorm, 1, 2 }, { TexelType::SNorm, 2, 2 }, { TexelType::SNorm, 4, 2 },
		{ TexelType::Signed,   1, 4 }, { TexelType::Signed,   2, 4 }, { TexelType::Signed,   4, 4 },
		{ TexelType::Unsigned, 1, 4 }, { TexelType::Unsigned, 2, 4 }, { TexelType::Unsigned, 4, 4 },
		{ TexelType::Float,    1, 4 }, { TexelType::Float,    2, 4 }, { TexelType::Float,    4, 4 }
	};
	return VALUES.at(value_);
}

// ====================================================================================================================
const ShaderType* TexelFormat::AsShaderType() const
{
#define FORMAT_CASE(fmtName, typestr) case fmtName.Value(): { return ShaderType::Builtin::GetType(typestr); }

	switch (value_)
	{
		FORMAT_CASE(U8Norm, "float");
		FORMAT_CASE(U8Norm2, "float2");
		FORMAT_CASE(U8Norm4, "float4");
		FORMAT_CASE(S8Norm, "float");
		FORMAT_CASE(S8Norm2, "float2");
		FORMAT_CASE(S8Norm4, "float4");
		FORMAT_CASE(U16Norm, "float");
		FORMAT_CASE(U16Norm2, "float2");
		FORMAT_CASE(U16Norm4, "float4");
		FORMAT_CASE(S16Norm, "float");
		FORMAT_CASE(S16Norm2, "float2");
		FORMAT_CASE(S16Norm4, "float4");
		FORMAT_CASE(Int, "int");
		FORMAT_CASE(Int2, "int2");
		FORMAT_CASE(Int4, "int4");
		FORMAT_CASE(UInt, "uint");
		FORMAT_CASE(UInt2, "uint2");
		FORMAT_CASE(UInt4, "uint4");
		FORMAT_CASE(Float, "float");
		FORMAT_CASE(Float2, "float2");
		FORMAT_CASE(Float4, "float4");
	default: throw std::runtime_error("Invalid TexelFormat value");
	}

#undef FORMAT_CASE
}

// ====================================================================================================================
const String& TexelFormat::GetVSLDecorator() const
{
	static const std::array<String, 21> NAMES{
		"u8norm", "u8norm2", "u8norm4",
		"s8norm", "s8norm2", "s8norm4",
		"u16norm", "u16norm2", "u16norm4",
		"s16norm", "s16norm2", "s16norm4",
		"int", "int2", "int4",
		"uint", "uint2", "uint4",
		"float", "float2", "float4"
	};
	return NAMES.at(value_);
}

// ====================================================================================================================
std::optional<TexelFormat> TexelFormat::FromVSLFormat(const String& format)
{
	static const std::unordered_map<String, TexelFormat> FORMATS{
		{ "u8norm", U8Norm }, { "u8norm2", U8Norm2 }, { "u8norm4", U8Norm4 },
		{ "s8norm", S8Norm }, { "s8norm2", S8Norm2 }, { "s8norm4", S8Norm4 },
		{ "u16norm", U16Norm }, { "u16norm2", U16Norm2 }, { "u16norm4", U16Norm4 },
		{ "s16norm", S16Norm }, { "s16norm2", S16Norm2 }, { "s16norm4", S16Norm4 },
		{ "int", Int }, { "int2", Int2 }, { "int4", Int4 },
		{ "uint", UInt }, { "uint2", UInt2 }, { "uint4", UInt4 },
		{ "float", Float }, { "float2", Float2 }, { "float4", Float4 }
	};
	const auto it = FORMATS.find(format);
	return (it != FORMATS.end()) ? it->second : std::optional<TexelFormat>{ };
}


// ====================================================================================================================
// ====================================================================================================================
StructType::StructType(const String& name, const std::vector<Member>& members)
	: name_{ name }
	, members_{ }
	, size_{ 0 }
	, alignment_{ 0 }
{
	// These alignment calculations assume and require the scalarLayout feature
	// If it is ever changed that scalarLayout is no longer required, this needs to be reworked
	for (const auto& member : members) {
		// Get alignment
		const auto memAlign = member.type->numeric.size;
		const auto memSize = member.type->numeric.size * member.type->numeric.dims[0]
			* member.type->numeric.dims[1] * member.arraySize;
		if (size_ % memAlign) {
			size_ += (memAlign - (size_ % memAlign)); // Round size to next member alignment requirement
		}
		if (memAlign > alignment_) {
			alignment_ = memAlign; // Struct alignment is equal to largest alignment requirement of members
		}

		// Add member
		Member newMem{};
		newMem.name = member.name;
		newMem.type = member.type;
		newMem.arraySize = member.arraySize;
		newMem.offset = size_;
		members_.push_back(newMem);
		size_ += memSize;
	}

	// Final size alignment
	if (size_ % alignment_) {
		size_ += (alignment_ - (size_ % alignment_));
	}
}

// ====================================================================================================================
const StructType::Member* StructType::GetMember(const String& name) const
{
	const auto it = std::find_if(members_.begin(), members_.end(), [&name](const Member& mem) {
		return mem.name == name;
	});
	return (it != members_.end()) ? &(*it) : nullptr;
}


// ====================================================================================================================
// ====================================================================================================================
bool ShaderType::IsSame(const ShaderType& otherType) const
{
	// Base type check
	if (baseType != otherType.baseType) {
		return false;
	}

	// Type-specific checks
	switch (baseType)
	{
	case BaseType::Void: return true;
	case BaseType::Boolean:
	case BaseType::Signed:
	case BaseType::Unsigned:
	case BaseType::Float:
		return (numeric.size == otherType.numeric.size) && (numeric.dims[0] == otherType.numeric.dims[0]) &&
			(numeric.dims[1] == otherType.numeric.dims[1]);
	case BaseType::Sampler:
	case BaseType::Image: return (texel.rank == otherType.texel.rank) && (texel.format == otherType.texel.format);
	case BaseType::ROBuffer:
	case BaseType::RWBuffer: return (buffer.structType == otherType.buffer.structType);
	case BaseType::ROTexels:
	case BaseType::RWTexels:
	case BaseType::SubpassInput: return (texel.format == otherType.texel.format);
	case BaseType::Struct: return (userStruct.type->Name() == otherType.userStruct.type->Name());
	default: return false;
	}
}

// ====================================================================================================================
bool ShaderType::HasImplicitCast(const ShaderType& targetType) const
{
	// Check for same-ness
	if ((this == &targetType) || IsSame(targetType)) {
		return true;
	}

	// Only numerics can cast
	if (!IsNumericType() || !targetType.IsNumericType()) {
		return false;
	}

	// Must be same dimensions
	if ((numeric.dims[0] != targetType.numeric.dims[0]) || (numeric.dims[1] != targetType.numeric.dims[1])) {
		return false;
	}

	// Anything can cast to float at the same or greater size
	if (targetType.baseType == BaseType::Float) {
		return (numeric.size <= targetType.numeric.size);
	}
	if (baseType == BaseType::Float) {
		return false; // Cannot go from float -> int
	}

	// Signed/Unsigned -> Unsigned for bigger or same size works
	if (targetType.baseType == BaseType::Unsigned) {
		return (numeric.size <= targetType.numeric.size);
	}
	if (baseType == BaseType::Unsigned) {
		return false; // Cannot go from uint -> int
	}

	// Self-cast, bigger or same size
	return (numeric.size <= targetType.numeric.size);
}

// ====================================================================================================================
uint32 ShaderType::BindingCount() const
{
	if (!IsNumericType()) {
		return 0;
	}

	const uint32 slotCount = ((numeric.size * numeric.dims[0]) > 16) ? 2 : 1;
	return slotCount * numeric.dims[1];
}

// ====================================================================================================================
String ShaderType::GetVSLName() const
{
	static const auto prefix = [](TexelFormat texelFormat) -> String {
		return texelFormat.IsSigned() ? "I" : texelFormat.IsUnsigned() ? "U" : "";
	};

	switch (baseType)
	{
	case BaseType::Void: return "void";
	case BaseType::Boolean: return
		(numeric.dims[0] == 1) ? "bool" : (numeric.dims[0] == 2) ? "bool2" :
		(numeric.dims[0] == 3) ? "bool3" : "bool4";
	case BaseType::Signed: {
		assert((numeric.size == 4) && "Bad signed type size");
		return
			(numeric.dims[0] == 1) ? "int" : (numeric.dims[0] == 2) ? "int2" :
			(numeric.dims[0] == 3) ? "int3" : "int4";
	}
	case BaseType::Unsigned: {
		assert((numeric.size == 4) && "Bad unsigned type size");
		return
			(numeric.dims[0] == 1) ? "uint" : (numeric.dims[0] == 2) ? "uint2" :
			(numeric.dims[0] == 3) ? "uint3" : "uint4";
	}
	case BaseType::Float: {
		assert((numeric.size == 4) && "Bad floating type size");
		switch (numeric.dims[1])
		{
		case 1: return
			(numeric.dims[0] == 1) ? "float" : (numeric.dims[0] == 2) ? "float2" :
			(numeric.dims[0] == 3) ? "float3" : "float4";
		case 2: return (numeric.dims[0] == 2) ? "float2x2" : (numeric.dims[0] == 3) ? "float2x3" : "float2x4";
		case 3: return (numeric.dims[0] == 2) ? "float3x2" : (numeric.dims[0] == 3) ? "float3x3" : "float3x4";
		default: return (numeric.dims[0] == 2) ? "float4x2" : (numeric.dims[0] == 3) ? "float4x3" : "float4x4";
		}
	} break;
	case BaseType::Sampler: return prefix(texel.format) + "Sampler" + texel.rank.TypeSuffix();
	case BaseType::Image: return prefix(texel.format) + "Image" + texel.rank.TypeSuffix();
	case BaseType::ROBuffer: return "ROBuffer<" + buffer.structType->userStruct.type->Name() + ">";
	case BaseType::RWBuffer: return "RWBuffer<" + buffer.structType->userStruct.type->Name() + ">";
	case BaseType::ROTexels: return "RO" + prefix(texel.format) + "Texels";
	case BaseType::RWTexels: return "RWTexels<" + texel.format.GetVSLDecorator() + ">";
	case BaseType::SubpassInput: return texel.format.GetVSLDecorator();
	case BaseType::Struct: return userStruct.type->Name();
	default: assert(false && "Bad base type"); return { };
	}
}

} // namespace vsl
