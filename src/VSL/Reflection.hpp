/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

#include "./Common.Impl.hpp"
#include <VSL/Type.hpp>
#include "./Types.Builtin.hpp"
#include <VSL/Shader.hpp>


namespace vsl
{

// Describes a vertex input variable
class InputVariable final
{
	friend class Parser;

public:
	inline uint32 BindingSize() const { return type->BindingCount() * arraySize; }

	inline void ToRecord(InputRecord* rec) const {
		rec->semantic = uint8(semantic);
		rec->location = uint8(location);
		rec->baseType = uint8(type->baseType);
		rec->dims[0] = type->numeric.dims[0];
		rec->dims[1] = type->numeric.dims[1];
		rec->arraySize = uint8(arraySize);
	}

private:
	InputVariable() = default;
	InputVariable(const String& name, InputSemantic semantic, const ShaderType* type, uint32 arraySize, uint32 location)
		: name{ name }, semantic{ semantic }, type{ type }, arraySize{ arraySize }, location{ location }
	{ }

public:
	String name;
	InputSemantic semantic;
	const ShaderType* type;
	uint32 arraySize;
	uint32 location;
}; // class InputVariable


// Describes a fragment output variable
class OutputVariable final
{
	friend class Parser;

public:
	inline void ToRecord(OutputRecord* rec) const {
		rec->baseType = uint8(type->baseType);
		rec->dim0 = uint8(type->numeric.dims[0]);
		rec->srcColorFactor = uint8(blending.srcColor);
		rec->dstColorFactor = uint8(blending.dstColor);
		rec->colorOp = uint8(blending.colorOp);
		rec->srcAlphaFactor = uint8(blending.srcAlpha);
		rec->dstAlphaFactor = uint8(blending.dstAlpha);
		rec->alphaOp = uint8(blending.alphaOp);
	}

private:
	OutputVariable() = default;
	OutputVariable(const String& name, const ShaderType* type, uint32 index, Blending blending)
		: name{ name }, type{ type }, index{ index }, blending{ blending }
	{ }

public:
	String name;
	const ShaderType* type;
	uint32 index;
	Blending blending;
}; // class OutputVariable


// Describes a binding variable or uniform.
class BindingVariable final
{
	friend class Parser;

private:
	BindingVariable() = default;
	BindingVariable(const String& name, const ShaderType* type, uint32 slot, ShaderStages stages)
		: name{ name }, type{ type }, slot{ slot }, stages{ stages }
	{ }

public:
	inline void ToRecord(BindingRecord* rec) const {
		rec->slot = uint8(slot);
		rec->baseType = uint8(type->baseType);
		rec->stageMask = uint16(stages);
		if (type->HasStructType()) {
			rec->buffer.size = uint16(type->userStruct.type->Size());
		}
		else {
			rec->image.rank = uint8(type->texel.rank.Value());
			rec->image.format = uint8(type->texel.format.Value());
		}
	}

public:
	String name;
	const ShaderType* type;
	uint32 slot;
	ShaderStages stages;
}; // class BindingVariable


// Describes a subpass input attachment.
class SubpassInputVariable final
{
	friend class Parser;

private:
	SubpassInputVariable() = default;
	SubpassInputVariable(const String& name, TexelFormat format, uint32 index)
		: name{ name }, format{ format }, index{ index }
	{ }

public:
	inline void ToRecord(SubpassInputRecord* rec) const {
		rec->index = uint8(index);
		rec->format = uint8(format.Value());
	}

public:
	String name;
	TexelFormat format;
	uint32 index;
}; // class SubpassInputVariable


// Describes a shader local (inter-stage) variable.
class LocalVariable final
{
	friend class Parser;

private:
	LocalVariable() = default;
	LocalVariable(const String& name, const ShaderType* type, ShaderStages stage, bool isFlat)
		: name{ name }, type{ type }, stage{ stage }, isFlat{ isFlat }
	{ }

public:
	String name;
	const ShaderType* type;
	ShaderStages stage;
	bool isFlat;
}; // class LocalVariable


// Contains the set of the object and struct types defined and utilized by a shader program.
class TypeSet final
{
	friend class ShaderInfo;

	using TypeMap = ShaderType::Builtin::TypeMap;
	using StructMap = ShaderType::Builtin::StructMap;

public:
	~TypeSet();

	inline const String& Error() const { return error_; }

	const ShaderType* AddType(const String& name, const ShaderType& type);
	const ShaderType* AddStructType(const String& name, const StructType& type);
	const ShaderType* ParseOrGetType(const String& name);

	const ShaderType* GetType(const String& name) const;

private:
	TypeSet();

private:
	TypeMap types_;
	StructMap structs_;
	mutable String error_;

	VSL_NO_COPY(TypeSet)
	VSL_NO_MOVE(TypeSet)
}; // class TypeSet


// Contains information about the public ABI and structure of a shader program.
class ShaderInfo final
{
	friend class Shader;
	friend class Compiler;
	friend class Parser;

public:
	~ShaderInfo();

	inline const TypeSet* Types() const { return types_.get(); }
	inline ShaderStages Stages() const { return stages_; }

	inline const std::vector<InputVariable>& Inputs() const { return inputs_; }
	inline uint32 SemanticMask() const { return semanticMask_; }
	inline const std::vector<OutputVariable>& Outputs() const { return outputs_; }
	inline const std::vector<LocalVariable>& Locals() const { return locals_; }
	inline const std::vector<BindingVariable>& Bindings() const { return bindings_; }
	inline const std::optional<BindingVariable>& Uniform() const { return uniform_; }
	inline const std::vector<SubpassInputVariable>& SubpassInputs() const { return subpassInputs_; }

	const InputVariable* GetInput(const String& name) const;
	const InputVariable* GetInput(uint32 location) const; // Is aware of inputs larger than one binding
	const OutputVariable* GetOutput(const String& name) const;
	const OutputVariable* GetOutput(uint32 location) const;
	const SubpassInputVariable* GetSubpassInput(const String& name) const;
	const SubpassInputVariable* GetSubpassInput(uint32 index) const;
	const BindingVariable* GetBinding(const String& name) const;
	const BindingVariable* GetBinding(uint32 slotIndex) const;

private:
	ShaderInfo();

private:
	UPtr<TypeSet> types_;
	ShaderStages stages_;
	std::vector<InputVariable> inputs_;
	uint32 semanticMask_;
	std::vector<OutputVariable> outputs_;
	std::vector<LocalVariable> locals_;
	std::vector<BindingVariable> bindings_;
	std::optional<BindingVariable> uniform_;
	std::vector<SubpassInputVariable> subpassInputs_;

	VSL_NO_COPY(ShaderInfo)
	VSL_NO_MOVE(ShaderInfo)
}; // class ShaderInfo

} // namespace vsl
