
// Generated from VSL.g4 by ANTLR 4.9.2


#include "VSLVisitor.h"

#include "VSL.h"


using namespace antlrcpp;
using namespace grammar;
using namespace antlr4;

VSL::VSL(TokenStream *input) : Parser(input) {
  _interpreter = new atn::ParserATNSimulator(this, _atn, _decisionToDFA, _sharedContextCache);
}

VSL::~VSL() {
  delete _interpreter;
}

std::string VSL::getGrammarFileName() const {
  return "VSL.g4";
}

const std::vector<std::string>& VSL::getRuleNames() const {
  return _ruleNames;
}

dfa::Vocabulary& VSL::getVocabulary() const {
  return _vocabulary;
}


//----------------- FileContext ------------------------------------------------------------------

VSL::FileContext::FileContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

VSL::ShaderTypeStatementContext* VSL::FileContext::shaderTypeStatement() {
  return getRuleContext<VSL::ShaderTypeStatementContext>(0);
}

tree::TerminalNode* VSL::FileContext::EOF() {
  return getToken(VSL::EOF, 0);
}

std::vector<VSL::TopLevelStatementContext *> VSL::FileContext::topLevelStatement() {
  return getRuleContexts<VSL::TopLevelStatementContext>();
}

VSL::TopLevelStatementContext* VSL::FileContext::topLevelStatement(size_t i) {
  return getRuleContext<VSL::TopLevelStatementContext>(i);
}


size_t VSL::FileContext::getRuleIndex() const {
  return VSL::RuleFile;
}


antlrcpp::Any VSL::FileContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<VSLVisitor*>(visitor))
    return parserVisitor->visitFile(this);
  else
    return visitor->visitChildren(this);
}

VSL::FileContext* VSL::file() {
  FileContext *_localctx = _tracker.createInstance<FileContext>(_ctx, getState());
  enterRule(_localctx, 0, VSL::RuleFile);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(54);
    shaderTypeStatement();
    setState(58);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while ((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << VSL::KW_BIND)
      | (1ULL << VSL::KW_FRAG)
      | (1ULL << VSL::KW_LOCAL)
      | (1ULL << VSL::KW_PASSINPUT)
      | (1ULL << VSL::KW_STRUCT)
      | (1ULL << VSL::KW_UNIFORM)
      | (1ULL << VSL::KW_VERT))) != 0)) {
      setState(55);
      topLevelStatement();
      setState(60);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
    setState(61);
    match(VSL::EOF);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ShaderTypeStatementContext ------------------------------------------------------------------

VSL::ShaderTypeStatementContext::ShaderTypeStatementContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* VSL::ShaderTypeStatementContext::KW_SHADER() {
  return getToken(VSL::KW_SHADER, 0);
}

tree::TerminalNode* VSL::ShaderTypeStatementContext::SEMICOLON() {
  return getToken(VSL::SEMICOLON, 0);
}

tree::TerminalNode* VSL::ShaderTypeStatementContext::IDENTIFIER() {
  return getToken(VSL::IDENTIFIER, 0);
}


size_t VSL::ShaderTypeStatementContext::getRuleIndex() const {
  return VSL::RuleShaderTypeStatement;
}


antlrcpp::Any VSL::ShaderTypeStatementContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<VSLVisitor*>(visitor))
    return parserVisitor->visitShaderTypeStatement(this);
  else
    return visitor->visitChildren(this);
}

VSL::ShaderTypeStatementContext* VSL::shaderTypeStatement() {
  ShaderTypeStatementContext *_localctx = _tracker.createInstance<ShaderTypeStatementContext>(_ctx, getState());
  enterRule(_localctx, 2, VSL::RuleShaderTypeStatement);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(63);
    match(VSL::KW_SHADER);
    setState(64);
    dynamic_cast<ShaderTypeStatementContext *>(_localctx)->type = match(VSL::IDENTIFIER);
    setState(65);
    match(VSL::SEMICOLON);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- TopLevelStatementContext ------------------------------------------------------------------

VSL::TopLevelStatementContext::TopLevelStatementContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

VSL::ShaderStructDefinitionContext* VSL::TopLevelStatementContext::shaderStructDefinition() {
  return getRuleContext<VSL::ShaderStructDefinitionContext>(0);
}

VSL::ShaderUniformStatementContext* VSL::TopLevelStatementContext::shaderUniformStatement() {
  return getRuleContext<VSL::ShaderUniformStatementContext>(0);
}

VSL::ShaderBindingStatementContext* VSL::TopLevelStatementContext::shaderBindingStatement() {
  return getRuleContext<VSL::ShaderBindingStatementContext>(0);
}

VSL::ShaderLocalStatementContext* VSL::TopLevelStatementContext::shaderLocalStatement() {
  return getRuleContext<VSL::ShaderLocalStatementContext>(0);
}

VSL::ShaderSubpassInputStatementContext* VSL::TopLevelStatementContext::shaderSubpassInputStatement() {
  return getRuleContext<VSL::ShaderSubpassInputStatementContext>(0);
}

VSL::ShaderStageFunctionContext* VSL::TopLevelStatementContext::shaderStageFunction() {
  return getRuleContext<VSL::ShaderStageFunctionContext>(0);
}


size_t VSL::TopLevelStatementContext::getRuleIndex() const {
  return VSL::RuleTopLevelStatement;
}


antlrcpp::Any VSL::TopLevelStatementContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<VSLVisitor*>(visitor))
    return parserVisitor->visitTopLevelStatement(this);
  else
    return visitor->visitChildren(this);
}

VSL::TopLevelStatementContext* VSL::topLevelStatement() {
  TopLevelStatementContext *_localctx = _tracker.createInstance<TopLevelStatementContext>(_ctx, getState());
  enterRule(_localctx, 4, VSL::RuleTopLevelStatement);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    setState(73);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case VSL::KW_STRUCT: {
        enterOuterAlt(_localctx, 1);
        setState(67);
        shaderStructDefinition();
        break;
      }

      case VSL::KW_UNIFORM: {
        enterOuterAlt(_localctx, 2);
        setState(68);
        shaderUniformStatement();
        break;
      }

      case VSL::KW_BIND: {
        enterOuterAlt(_localctx, 3);
        setState(69);
        shaderBindingStatement();
        break;
      }

      case VSL::KW_LOCAL: {
        enterOuterAlt(_localctx, 4);
        setState(70);
        shaderLocalStatement();
        break;
      }

      case VSL::KW_PASSINPUT: {
        enterOuterAlt(_localctx, 5);
        setState(71);
        shaderSubpassInputStatement();
        break;
      }

      case VSL::KW_FRAG:
      case VSL::KW_VERT: {
        enterOuterAlt(_localctx, 6);
        setState(72);
        shaderStageFunction();
        break;
      }

    default:
      throw NoViableAltException(this);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ShaderStructDefinitionContext ------------------------------------------------------------------

VSL::ShaderStructDefinitionContext::ShaderStructDefinitionContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* VSL::ShaderStructDefinitionContext::KW_STRUCT() {
  return getToken(VSL::KW_STRUCT, 0);
}

tree::TerminalNode* VSL::ShaderStructDefinitionContext::LBRACE() {
  return getToken(VSL::LBRACE, 0);
}

tree::TerminalNode* VSL::ShaderStructDefinitionContext::RBRACE() {
  return getToken(VSL::RBRACE, 0);
}

std::vector<tree::TerminalNode *> VSL::ShaderStructDefinitionContext::SEMICOLON() {
  return getTokens(VSL::SEMICOLON);
}

tree::TerminalNode* VSL::ShaderStructDefinitionContext::SEMICOLON(size_t i) {
  return getToken(VSL::SEMICOLON, i);
}

tree::TerminalNode* VSL::ShaderStructDefinitionContext::IDENTIFIER() {
  return getToken(VSL::IDENTIFIER, 0);
}

std::vector<VSL::VariableDeclarationContext *> VSL::ShaderStructDefinitionContext::variableDeclaration() {
  return getRuleContexts<VSL::VariableDeclarationContext>();
}

VSL::VariableDeclarationContext* VSL::ShaderStructDefinitionContext::variableDeclaration(size_t i) {
  return getRuleContext<VSL::VariableDeclarationContext>(i);
}


size_t VSL::ShaderStructDefinitionContext::getRuleIndex() const {
  return VSL::RuleShaderStructDefinition;
}


antlrcpp::Any VSL::ShaderStructDefinitionContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<VSLVisitor*>(visitor))
    return parserVisitor->visitShaderStructDefinition(this);
  else
    return visitor->visitChildren(this);
}

VSL::ShaderStructDefinitionContext* VSL::shaderStructDefinition() {
  ShaderStructDefinitionContext *_localctx = _tracker.createInstance<ShaderStructDefinitionContext>(_ctx, getState());
  enterRule(_localctx, 6, VSL::RuleShaderStructDefinition);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(75);
    match(VSL::KW_STRUCT);
    setState(76);
    dynamic_cast<ShaderStructDefinitionContext *>(_localctx)->name = match(VSL::IDENTIFIER);
    setState(77);
    match(VSL::LBRACE);
    setState(81); 
    _errHandler->sync(this);
    _la = _input->LA(1);
    do {
      setState(78);
      variableDeclaration();
      setState(79);
      match(VSL::SEMICOLON);
      setState(83); 
      _errHandler->sync(this);
      _la = _input->LA(1);
    } while (_la == VSL::IDENTIFIER);
    setState(85);
    match(VSL::RBRACE);
    setState(86);
    match(VSL::SEMICOLON);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ShaderUniformStatementContext ------------------------------------------------------------------

VSL::ShaderUniformStatementContext::ShaderUniformStatementContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* VSL::ShaderUniformStatementContext::KW_UNIFORM() {
  return getToken(VSL::KW_UNIFORM, 0);
}

tree::TerminalNode* VSL::ShaderUniformStatementContext::LBRACE() {
  return getToken(VSL::LBRACE, 0);
}

tree::TerminalNode* VSL::ShaderUniformStatementContext::RBRACE() {
  return getToken(VSL::RBRACE, 0);
}

std::vector<tree::TerminalNode *> VSL::ShaderUniformStatementContext::SEMICOLON() {
  return getTokens(VSL::SEMICOLON);
}

tree::TerminalNode* VSL::ShaderUniformStatementContext::SEMICOLON(size_t i) {
  return getToken(VSL::SEMICOLON, i);
}

tree::TerminalNode* VSL::ShaderUniformStatementContext::IDENTIFIER() {
  return getToken(VSL::IDENTIFIER, 0);
}

std::vector<VSL::VariableDeclarationContext *> VSL::ShaderUniformStatementContext::variableDeclaration() {
  return getRuleContexts<VSL::VariableDeclarationContext>();
}

VSL::VariableDeclarationContext* VSL::ShaderUniformStatementContext::variableDeclaration(size_t i) {
  return getRuleContext<VSL::VariableDeclarationContext>(i);
}


size_t VSL::ShaderUniformStatementContext::getRuleIndex() const {
  return VSL::RuleShaderUniformStatement;
}


antlrcpp::Any VSL::ShaderUniformStatementContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<VSLVisitor*>(visitor))
    return parserVisitor->visitShaderUniformStatement(this);
  else
    return visitor->visitChildren(this);
}

VSL::ShaderUniformStatementContext* VSL::shaderUniformStatement() {
  ShaderUniformStatementContext *_localctx = _tracker.createInstance<ShaderUniformStatementContext>(_ctx, getState());
  enterRule(_localctx, 8, VSL::RuleShaderUniformStatement);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(88);
    match(VSL::KW_UNIFORM);
    setState(89);
    dynamic_cast<ShaderUniformStatementContext *>(_localctx)->name = match(VSL::IDENTIFIER);
    setState(90);
    match(VSL::LBRACE);
    setState(94); 
    _errHandler->sync(this);
    _la = _input->LA(1);
    do {
      setState(91);
      variableDeclaration();
      setState(92);
      match(VSL::SEMICOLON);
      setState(96); 
      _errHandler->sync(this);
      _la = _input->LA(1);
    } while (_la == VSL::IDENTIFIER);
    setState(98);
    match(VSL::RBRACE);
    setState(99);
    match(VSL::SEMICOLON);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ShaderBindingStatementContext ------------------------------------------------------------------

VSL::ShaderBindingStatementContext::ShaderBindingStatementContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* VSL::ShaderBindingStatementContext::KW_BIND() {
  return getToken(VSL::KW_BIND, 0);
}

tree::TerminalNode* VSL::ShaderBindingStatementContext::LPAREN() {
  return getToken(VSL::LPAREN, 0);
}

tree::TerminalNode* VSL::ShaderBindingStatementContext::RPAREN() {
  return getToken(VSL::RPAREN, 0);
}

VSL::VariableDeclarationContext* VSL::ShaderBindingStatementContext::variableDeclaration() {
  return getRuleContext<VSL::VariableDeclarationContext>(0);
}

tree::TerminalNode* VSL::ShaderBindingStatementContext::SEMICOLON() {
  return getToken(VSL::SEMICOLON, 0);
}

tree::TerminalNode* VSL::ShaderBindingStatementContext::INTEGER_LITERAL() {
  return getToken(VSL::INTEGER_LITERAL, 0);
}


size_t VSL::ShaderBindingStatementContext::getRuleIndex() const {
  return VSL::RuleShaderBindingStatement;
}


antlrcpp::Any VSL::ShaderBindingStatementContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<VSLVisitor*>(visitor))
    return parserVisitor->visitShaderBindingStatement(this);
  else
    return visitor->visitChildren(this);
}

VSL::ShaderBindingStatementContext* VSL::shaderBindingStatement() {
  ShaderBindingStatementContext *_localctx = _tracker.createInstance<ShaderBindingStatementContext>(_ctx, getState());
  enterRule(_localctx, 10, VSL::RuleShaderBindingStatement);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(101);
    match(VSL::KW_BIND);
    setState(102);
    match(VSL::LPAREN);
    setState(103);
    dynamic_cast<ShaderBindingStatementContext *>(_localctx)->slot = match(VSL::INTEGER_LITERAL);
    setState(104);
    match(VSL::RPAREN);
    setState(105);
    variableDeclaration();
    setState(106);
    match(VSL::SEMICOLON);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ShaderLocalStatementContext ------------------------------------------------------------------

VSL::ShaderLocalStatementContext::ShaderLocalStatementContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* VSL::ShaderLocalStatementContext::KW_LOCAL() {
  return getToken(VSL::KW_LOCAL, 0);
}

tree::TerminalNode* VSL::ShaderLocalStatementContext::LPAREN() {
  return getToken(VSL::LPAREN, 0);
}

tree::TerminalNode* VSL::ShaderLocalStatementContext::RPAREN() {
  return getToken(VSL::RPAREN, 0);
}

VSL::VariableDeclarationContext* VSL::ShaderLocalStatementContext::variableDeclaration() {
  return getRuleContext<VSL::VariableDeclarationContext>(0);
}

tree::TerminalNode* VSL::ShaderLocalStatementContext::SEMICOLON() {
  return getToken(VSL::SEMICOLON, 0);
}

tree::TerminalNode* VSL::ShaderLocalStatementContext::IDENTIFIER() {
  return getToken(VSL::IDENTIFIER, 0);
}

tree::TerminalNode* VSL::ShaderLocalStatementContext::KW_FLAT() {
  return getToken(VSL::KW_FLAT, 0);
}


size_t VSL::ShaderLocalStatementContext::getRuleIndex() const {
  return VSL::RuleShaderLocalStatement;
}


antlrcpp::Any VSL::ShaderLocalStatementContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<VSLVisitor*>(visitor))
    return parserVisitor->visitShaderLocalStatement(this);
  else
    return visitor->visitChildren(this);
}

VSL::ShaderLocalStatementContext* VSL::shaderLocalStatement() {
  ShaderLocalStatementContext *_localctx = _tracker.createInstance<ShaderLocalStatementContext>(_ctx, getState());
  enterRule(_localctx, 12, VSL::RuleShaderLocalStatement);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(108);
    match(VSL::KW_LOCAL);
    setState(109);
    match(VSL::LPAREN);
    setState(110);
    dynamic_cast<ShaderLocalStatementContext *>(_localctx)->pstage = match(VSL::IDENTIFIER);
    setState(111);
    match(VSL::RPAREN);
    setState(113);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == VSL::KW_FLAT) {
      setState(112);
      match(VSL::KW_FLAT);
    }
    setState(115);
    variableDeclaration();
    setState(116);
    match(VSL::SEMICOLON);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ShaderSubpassInputStatementContext ------------------------------------------------------------------

VSL::ShaderSubpassInputStatementContext::ShaderSubpassInputStatementContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* VSL::ShaderSubpassInputStatementContext::KW_PASSINPUT() {
  return getToken(VSL::KW_PASSINPUT, 0);
}

tree::TerminalNode* VSL::ShaderSubpassInputStatementContext::LPAREN() {
  return getToken(VSL::LPAREN, 0);
}

tree::TerminalNode* VSL::ShaderSubpassInputStatementContext::RPAREN() {
  return getToken(VSL::RPAREN, 0);
}

tree::TerminalNode* VSL::ShaderSubpassInputStatementContext::SEMICOLON() {
  return getToken(VSL::SEMICOLON, 0);
}

tree::TerminalNode* VSL::ShaderSubpassInputStatementContext::INTEGER_LITERAL() {
  return getToken(VSL::INTEGER_LITERAL, 0);
}

std::vector<tree::TerminalNode *> VSL::ShaderSubpassInputStatementContext::IDENTIFIER() {
  return getTokens(VSL::IDENTIFIER);
}

tree::TerminalNode* VSL::ShaderSubpassInputStatementContext::IDENTIFIER(size_t i) {
  return getToken(VSL::IDENTIFIER, i);
}


size_t VSL::ShaderSubpassInputStatementContext::getRuleIndex() const {
  return VSL::RuleShaderSubpassInputStatement;
}


antlrcpp::Any VSL::ShaderSubpassInputStatementContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<VSLVisitor*>(visitor))
    return parserVisitor->visitShaderSubpassInputStatement(this);
  else
    return visitor->visitChildren(this);
}

VSL::ShaderSubpassInputStatementContext* VSL::shaderSubpassInputStatement() {
  ShaderSubpassInputStatementContext *_localctx = _tracker.createInstance<ShaderSubpassInputStatementContext>(_ctx, getState());
  enterRule(_localctx, 14, VSL::RuleShaderSubpassInputStatement);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(118);
    match(VSL::KW_PASSINPUT);
    setState(119);
    match(VSL::LPAREN);
    setState(120);
    dynamic_cast<ShaderSubpassInputStatementContext *>(_localctx)->index = match(VSL::INTEGER_LITERAL);
    setState(121);
    match(VSL::RPAREN);
    setState(122);
    dynamic_cast<ShaderSubpassInputStatementContext *>(_localctx)->format = match(VSL::IDENTIFIER);
    setState(123);
    dynamic_cast<ShaderSubpassInputStatementContext *>(_localctx)->name = match(VSL::IDENTIFIER);
    setState(124);
    match(VSL::SEMICOLON);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ShaderStageFunctionContext ------------------------------------------------------------------

VSL::ShaderStageFunctionContext::ShaderStageFunctionContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t VSL::ShaderStageFunctionContext::getRuleIndex() const {
  return VSL::RuleShaderStageFunction;
}

void VSL::ShaderStageFunctionContext::copyFrom(ShaderStageFunctionContext *ctx) {
  ParserRuleContext::copyFrom(ctx);
}

//----------------- FragmentFunctionContext ------------------------------------------------------------------

tree::TerminalNode* VSL::FragmentFunctionContext::KW_FRAG() {
  return getToken(VSL::KW_FRAG, 0);
}

tree::TerminalNode* VSL::FragmentFunctionContext::LPAREN() {
  return getToken(VSL::LPAREN, 0);
}

tree::TerminalNode* VSL::FragmentFunctionContext::RPAREN() {
  return getToken(VSL::RPAREN, 0);
}

VSL::StatementBlockContext* VSL::FragmentFunctionContext::statementBlock() {
  return getRuleContext<VSL::StatementBlockContext>(0);
}

std::vector<VSL::ShaderFragmentOutputContext *> VSL::FragmentFunctionContext::shaderFragmentOutput() {
  return getRuleContexts<VSL::ShaderFragmentOutputContext>();
}

VSL::ShaderFragmentOutputContext* VSL::FragmentFunctionContext::shaderFragmentOutput(size_t i) {
  return getRuleContext<VSL::ShaderFragmentOutputContext>(i);
}

VSL::FragmentFunctionContext::FragmentFunctionContext(ShaderStageFunctionContext *ctx) { copyFrom(ctx); }


antlrcpp::Any VSL::FragmentFunctionContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<VSLVisitor*>(visitor))
    return parserVisitor->visitFragmentFunction(this);
  else
    return visitor->visitChildren(this);
}
//----------------- VertexFunctionContext ------------------------------------------------------------------

tree::TerminalNode* VSL::VertexFunctionContext::KW_VERT() {
  return getToken(VSL::KW_VERT, 0);
}

tree::TerminalNode* VSL::VertexFunctionContext::LPAREN() {
  return getToken(VSL::LPAREN, 0);
}

tree::TerminalNode* VSL::VertexFunctionContext::RPAREN() {
  return getToken(VSL::RPAREN, 0);
}

VSL::StatementBlockContext* VSL::VertexFunctionContext::statementBlock() {
  return getRuleContext<VSL::StatementBlockContext>(0);
}

std::vector<VSL::ShaderVertexInputContext *> VSL::VertexFunctionContext::shaderVertexInput() {
  return getRuleContexts<VSL::ShaderVertexInputContext>();
}

VSL::ShaderVertexInputContext* VSL::VertexFunctionContext::shaderVertexInput(size_t i) {
  return getRuleContext<VSL::ShaderVertexInputContext>(i);
}

VSL::VertexFunctionContext::VertexFunctionContext(ShaderStageFunctionContext *ctx) { copyFrom(ctx); }


antlrcpp::Any VSL::VertexFunctionContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<VSLVisitor*>(visitor))
    return parserVisitor->visitVertexFunction(this);
  else
    return visitor->visitChildren(this);
}
VSL::ShaderStageFunctionContext* VSL::shaderStageFunction() {
  ShaderStageFunctionContext *_localctx = _tracker.createInstance<ShaderStageFunctionContext>(_ctx, getState());
  enterRule(_localctx, 16, VSL::RuleShaderStageFunction);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    setState(146);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case VSL::KW_VERT: {
        _localctx = dynamic_cast<ShaderStageFunctionContext *>(_tracker.createInstance<VSL::VertexFunctionContext>(_localctx));
        enterOuterAlt(_localctx, 1);
        setState(126);
        match(VSL::KW_VERT);
        setState(127);
        match(VSL::LPAREN);
        setState(131);
        _errHandler->sync(this);
        _la = _input->LA(1);
        while (_la == VSL::LBRACKET) {
          setState(128);
          shaderVertexInput();
          setState(133);
          _errHandler->sync(this);
          _la = _input->LA(1);
        }
        setState(134);
        match(VSL::RPAREN);
        setState(135);
        statementBlock();
        break;
      }

      case VSL::KW_FRAG: {
        _localctx = dynamic_cast<ShaderStageFunctionContext *>(_tracker.createInstance<VSL::FragmentFunctionContext>(_localctx));
        enterOuterAlt(_localctx, 2);
        setState(136);
        match(VSL::KW_FRAG);
        setState(137);
        match(VSL::LPAREN);
        setState(141);
        _errHandler->sync(this);
        _la = _input->LA(1);
        while (_la == VSL::LBRACKET) {
          setState(138);
          shaderFragmentOutput();
          setState(143);
          _errHandler->sync(this);
          _la = _input->LA(1);
        }
        setState(144);
        match(VSL::RPAREN);
        setState(145);
        statementBlock();
        break;
      }

    default:
      throw NoViableAltException(this);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ShaderVertexInputContext ------------------------------------------------------------------

VSL::ShaderVertexInputContext::ShaderVertexInputContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* VSL::ShaderVertexInputContext::LBRACKET() {
  return getToken(VSL::LBRACKET, 0);
}

tree::TerminalNode* VSL::ShaderVertexInputContext::RBRACKET() {
  return getToken(VSL::RBRACKET, 0);
}

VSL::VariableDeclarationContext* VSL::ShaderVertexInputContext::variableDeclaration() {
  return getRuleContext<VSL::VariableDeclarationContext>(0);
}

tree::TerminalNode* VSL::ShaderVertexInputContext::SEMICOLON() {
  return getToken(VSL::SEMICOLON, 0);
}

tree::TerminalNode* VSL::ShaderVertexInputContext::VERTEX_SEMANTIC() {
  return getToken(VSL::VERTEX_SEMANTIC, 0);
}


size_t VSL::ShaderVertexInputContext::getRuleIndex() const {
  return VSL::RuleShaderVertexInput;
}


antlrcpp::Any VSL::ShaderVertexInputContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<VSLVisitor*>(visitor))
    return parserVisitor->visitShaderVertexInput(this);
  else
    return visitor->visitChildren(this);
}

VSL::ShaderVertexInputContext* VSL::shaderVertexInput() {
  ShaderVertexInputContext *_localctx = _tracker.createInstance<ShaderVertexInputContext>(_ctx, getState());
  enterRule(_localctx, 18, VSL::RuleShaderVertexInput);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(148);
    match(VSL::LBRACKET);
    setState(149);
    dynamic_cast<ShaderVertexInputContext *>(_localctx)->semantic = match(VSL::VERTEX_SEMANTIC);
    setState(150);
    match(VSL::RBRACKET);
    setState(151);
    variableDeclaration();
    setState(152);
    match(VSL::SEMICOLON);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ShaderFragmentOutputContext ------------------------------------------------------------------

VSL::ShaderFragmentOutputContext::ShaderFragmentOutputContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* VSL::ShaderFragmentOutputContext::LBRACKET() {
  return getToken(VSL::LBRACKET, 0);
}

tree::TerminalNode* VSL::ShaderFragmentOutputContext::RBRACKET() {
  return getToken(VSL::RBRACKET, 0);
}

VSL::VariableDeclarationContext* VSL::ShaderFragmentOutputContext::variableDeclaration() {
  return getRuleContext<VSL::VariableDeclarationContext>(0);
}

tree::TerminalNode* VSL::ShaderFragmentOutputContext::SEMICOLON() {
  return getToken(VSL::SEMICOLON, 0);
}

VSL::BlendStatementContext* VSL::ShaderFragmentOutputContext::blendStatement() {
  return getRuleContext<VSL::BlendStatementContext>(0);
}

tree::TerminalNode* VSL::ShaderFragmentOutputContext::IDENTIFIER() {
  return getToken(VSL::IDENTIFIER, 0);
}


size_t VSL::ShaderFragmentOutputContext::getRuleIndex() const {
  return VSL::RuleShaderFragmentOutput;
}


antlrcpp::Any VSL::ShaderFragmentOutputContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<VSLVisitor*>(visitor))
    return parserVisitor->visitShaderFragmentOutput(this);
  else
    return visitor->visitChildren(this);
}

VSL::ShaderFragmentOutputContext* VSL::shaderFragmentOutput() {
  ShaderFragmentOutputContext *_localctx = _tracker.createInstance<ShaderFragmentOutputContext>(_ctx, getState());
  enterRule(_localctx, 20, VSL::RuleShaderFragmentOutput);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(154);
    match(VSL::LBRACKET);
    setState(157);
    _errHandler->sync(this);
    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 8, _ctx)) {
    case 1: {
      setState(155);
      dynamic_cast<ShaderFragmentOutputContext *>(_localctx)->blendName = match(VSL::IDENTIFIER);
      break;
    }

    case 2: {
      setState(156);
      blendStatement();
      break;
    }

    default:
      break;
    }
    setState(159);
    match(VSL::RBRACKET);
    setState(160);
    variableDeclaration();
    setState(161);
    match(VSL::SEMICOLON);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- BlendStatementContext ------------------------------------------------------------------

VSL::BlendStatementContext::BlendStatementContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<tree::TerminalNode *> VSL::BlendStatementContext::IDENTIFIER() {
  return getTokens(VSL::IDENTIFIER);
}

tree::TerminalNode* VSL::BlendStatementContext::IDENTIFIER(size_t i) {
  return getToken(VSL::IDENTIFIER, i);
}

tree::TerminalNode* VSL::BlendStatementContext::COMMA() {
  return getToken(VSL::COMMA, 0);
}


size_t VSL::BlendStatementContext::getRuleIndex() const {
  return VSL::RuleBlendStatement;
}


antlrcpp::Any VSL::BlendStatementContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<VSLVisitor*>(visitor))
    return parserVisitor->visitBlendStatement(this);
  else
    return visitor->visitChildren(this);
}

VSL::BlendStatementContext* VSL::blendStatement() {
  BlendStatementContext *_localctx = _tracker.createInstance<BlendStatementContext>(_ctx, getState());
  enterRule(_localctx, 22, VSL::RuleBlendStatement);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(163);
    dynamic_cast<BlendStatementContext *>(_localctx)->srcColor = match(VSL::IDENTIFIER);
    setState(165);
    _errHandler->sync(this);

    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 9, _ctx)) {
    case 1: {
      setState(164);
      dynamic_cast<BlendStatementContext *>(_localctx)->colorOp = match(VSL::IDENTIFIER);
      break;
    }

    default:
      break;
    }
    setState(167);
    dynamic_cast<BlendStatementContext *>(_localctx)->dstColor = match(VSL::IDENTIFIER);
    setState(174);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == VSL::COMMA) {
      setState(168);
      match(VSL::COMMA);
      setState(169);
      dynamic_cast<BlendStatementContext *>(_localctx)->srcAlpha = match(VSL::IDENTIFIER);
      setState(171);
      _errHandler->sync(this);

      switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 10, _ctx)) {
      case 1: {
        setState(170);
        dynamic_cast<BlendStatementContext *>(_localctx)->alphaOp = match(VSL::IDENTIFIER);
        break;
      }

      default:
        break;
      }
      setState(173);
      dynamic_cast<BlendStatementContext *>(_localctx)->dstAlpha = match(VSL::IDENTIFIER);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- StatementContext ------------------------------------------------------------------

VSL::StatementContext::StatementContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

VSL::VariableDefinitionContext* VSL::StatementContext::variableDefinition() {
  return getRuleContext<VSL::VariableDefinitionContext>(0);
}

tree::TerminalNode* VSL::StatementContext::SEMICOLON() {
  return getToken(VSL::SEMICOLON, 0);
}

VSL::VariableDeclarationContext* VSL::StatementContext::variableDeclaration() {
  return getRuleContext<VSL::VariableDeclarationContext>(0);
}

VSL::AssignmentContext* VSL::StatementContext::assignment() {
  return getRuleContext<VSL::AssignmentContext>(0);
}

VSL::IfStatementContext* VSL::StatementContext::ifStatement() {
  return getRuleContext<VSL::IfStatementContext>(0);
}

VSL::ForLoopStatementContext* VSL::StatementContext::forLoopStatement() {
  return getRuleContext<VSL::ForLoopStatementContext>(0);
}

VSL::ControlStatementContext* VSL::StatementContext::controlStatement() {
  return getRuleContext<VSL::ControlStatementContext>(0);
}


size_t VSL::StatementContext::getRuleIndex() const {
  return VSL::RuleStatement;
}


antlrcpp::Any VSL::StatementContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<VSLVisitor*>(visitor))
    return parserVisitor->visitStatement(this);
  else
    return visitor->visitChildren(this);
}

VSL::StatementContext* VSL::statement() {
  StatementContext *_localctx = _tracker.createInstance<StatementContext>(_ctx, getState());
  enterRule(_localctx, 24, VSL::RuleStatement);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    setState(190);
    _errHandler->sync(this);
    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 12, _ctx)) {
    case 1: {
      enterOuterAlt(_localctx, 1);
      setState(176);
      variableDefinition();
      setState(177);
      match(VSL::SEMICOLON);
      break;
    }

    case 2: {
      enterOuterAlt(_localctx, 2);
      setState(179);
      variableDeclaration();
      setState(180);
      match(VSL::SEMICOLON);
      break;
    }

    case 3: {
      enterOuterAlt(_localctx, 3);
      setState(182);
      assignment();
      setState(183);
      match(VSL::SEMICOLON);
      break;
    }

    case 4: {
      enterOuterAlt(_localctx, 4);
      setState(185);
      ifStatement();
      break;
    }

    case 5: {
      enterOuterAlt(_localctx, 5);
      setState(186);
      forLoopStatement();
      break;
    }

    case 6: {
      enterOuterAlt(_localctx, 6);
      setState(187);
      controlStatement();
      setState(188);
      match(VSL::SEMICOLON);
      break;
    }

    default:
      break;
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- StatementBlockContext ------------------------------------------------------------------

VSL::StatementBlockContext::StatementBlockContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* VSL::StatementBlockContext::LBRACE() {
  return getToken(VSL::LBRACE, 0);
}

tree::TerminalNode* VSL::StatementBlockContext::RBRACE() {
  return getToken(VSL::RBRACE, 0);
}

std::vector<VSL::StatementContext *> VSL::StatementBlockContext::statement() {
  return getRuleContexts<VSL::StatementContext>();
}

VSL::StatementContext* VSL::StatementBlockContext::statement(size_t i) {
  return getRuleContext<VSL::StatementContext>(i);
}


size_t VSL::StatementBlockContext::getRuleIndex() const {
  return VSL::RuleStatementBlock;
}


antlrcpp::Any VSL::StatementBlockContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<VSLVisitor*>(visitor))
    return parserVisitor->visitStatementBlock(this);
  else
    return visitor->visitChildren(this);
}

VSL::StatementBlockContext* VSL::statementBlock() {
  StatementBlockContext *_localctx = _tracker.createInstance<StatementBlockContext>(_ctx, getState());
  enterRule(_localctx, 26, VSL::RuleStatementBlock);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(192);
    match(VSL::LBRACE);
    setState(196);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while ((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << VSL::KW_BREAK)
      | (1ULL << VSL::KW_CONTINUE)
      | (1ULL << VSL::KW_DISCARD)
      | (1ULL << VSL::KW_FOR)
      | (1ULL << VSL::KW_IF)
      | (1ULL << VSL::KW_RETURN)
      | (1ULL << VSL::IDENTIFIER))) != 0)) {
      setState(193);
      statement();
      setState(198);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
    setState(199);
    match(VSL::RBRACE);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- VariableDeclarationContext ------------------------------------------------------------------

VSL::VariableDeclarationContext::VariableDeclarationContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<tree::TerminalNode *> VSL::VariableDeclarationContext::IDENTIFIER() {
  return getTokens(VSL::IDENTIFIER);
}

tree::TerminalNode* VSL::VariableDeclarationContext::IDENTIFIER(size_t i) {
  return getToken(VSL::IDENTIFIER, i);
}

tree::TerminalNode* VSL::VariableDeclarationContext::OP_LESS() {
  return getToken(VSL::OP_LESS, 0);
}

tree::TerminalNode* VSL::VariableDeclarationContext::OP_GREATER() {
  return getToken(VSL::OP_GREATER, 0);
}

tree::TerminalNode* VSL::VariableDeclarationContext::LBRACKET() {
  return getToken(VSL::LBRACKET, 0);
}

tree::TerminalNode* VSL::VariableDeclarationContext::RBRACKET() {
  return getToken(VSL::RBRACKET, 0);
}

tree::TerminalNode* VSL::VariableDeclarationContext::INTEGER_LITERAL() {
  return getToken(VSL::INTEGER_LITERAL, 0);
}


size_t VSL::VariableDeclarationContext::getRuleIndex() const {
  return VSL::RuleVariableDeclaration;
}


antlrcpp::Any VSL::VariableDeclarationContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<VSLVisitor*>(visitor))
    return parserVisitor->visitVariableDeclaration(this);
  else
    return visitor->visitChildren(this);
}

VSL::VariableDeclarationContext* VSL::variableDeclaration() {
  VariableDeclarationContext *_localctx = _tracker.createInstance<VariableDeclarationContext>(_ctx, getState());
  enterRule(_localctx, 28, VSL::RuleVariableDeclaration);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(201);
    dynamic_cast<VariableDeclarationContext *>(_localctx)->baseType = match(VSL::IDENTIFIER);
    setState(205);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == VSL::OP_LESS) {
      setState(202);
      match(VSL::OP_LESS);
      setState(203);
      dynamic_cast<VariableDeclarationContext *>(_localctx)->subType = match(VSL::IDENTIFIER);
      setState(204);
      match(VSL::OP_GREATER);
    }
    setState(207);
    dynamic_cast<VariableDeclarationContext *>(_localctx)->name = match(VSL::IDENTIFIER);
    setState(211);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == VSL::LBRACKET) {
      setState(208);
      match(VSL::LBRACKET);
      setState(209);
      dynamic_cast<VariableDeclarationContext *>(_localctx)->arraySize = match(VSL::INTEGER_LITERAL);
      setState(210);
      match(VSL::RBRACKET);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- VariableDefinitionContext ------------------------------------------------------------------

VSL::VariableDefinitionContext::VariableDefinitionContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* VSL::VariableDefinitionContext::OP_ASSIGN() {
  return getToken(VSL::OP_ASSIGN, 0);
}

VSL::VariableDeclarationContext* VSL::VariableDefinitionContext::variableDeclaration() {
  return getRuleContext<VSL::VariableDeclarationContext>(0);
}

VSL::ExpressionContext* VSL::VariableDefinitionContext::expression() {
  return getRuleContext<VSL::ExpressionContext>(0);
}


size_t VSL::VariableDefinitionContext::getRuleIndex() const {
  return VSL::RuleVariableDefinition;
}


antlrcpp::Any VSL::VariableDefinitionContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<VSLVisitor*>(visitor))
    return parserVisitor->visitVariableDefinition(this);
  else
    return visitor->visitChildren(this);
}

VSL::VariableDefinitionContext* VSL::variableDefinition() {
  VariableDefinitionContext *_localctx = _tracker.createInstance<VariableDefinitionContext>(_ctx, getState());
  enterRule(_localctx, 30, VSL::RuleVariableDefinition);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(213);
    dynamic_cast<VariableDefinitionContext *>(_localctx)->decl = variableDeclaration();
    setState(214);
    match(VSL::OP_ASSIGN);
    setState(215);
    dynamic_cast<VariableDefinitionContext *>(_localctx)->value = expression(0);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- AssignmentContext ------------------------------------------------------------------

VSL::AssignmentContext::AssignmentContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

VSL::LvalueContext* VSL::AssignmentContext::lvalue() {
  return getRuleContext<VSL::LvalueContext>(0);
}

VSL::ExpressionContext* VSL::AssignmentContext::expression() {
  return getRuleContext<VSL::ExpressionContext>(0);
}

tree::TerminalNode* VSL::AssignmentContext::OP_ASSIGN() {
  return getToken(VSL::OP_ASSIGN, 0);
}

tree::TerminalNode* VSL::AssignmentContext::OP_ASN_ADD() {
  return getToken(VSL::OP_ASN_ADD, 0);
}

tree::TerminalNode* VSL::AssignmentContext::OP_ASN_SUB() {
  return getToken(VSL::OP_ASN_SUB, 0);
}

tree::TerminalNode* VSL::AssignmentContext::OP_ASN_MUL() {
  return getToken(VSL::OP_ASN_MUL, 0);
}

tree::TerminalNode* VSL::AssignmentContext::OP_ASN_DIV() {
  return getToken(VSL::OP_ASN_DIV, 0);
}

tree::TerminalNode* VSL::AssignmentContext::OP_ASN_MOD() {
  return getToken(VSL::OP_ASN_MOD, 0);
}

tree::TerminalNode* VSL::AssignmentContext::OP_ASN_LSH() {
  return getToken(VSL::OP_ASN_LSH, 0);
}

tree::TerminalNode* VSL::AssignmentContext::OP_ASN_RSH() {
  return getToken(VSL::OP_ASN_RSH, 0);
}

tree::TerminalNode* VSL::AssignmentContext::OP_ASN_BAND() {
  return getToken(VSL::OP_ASN_BAND, 0);
}

tree::TerminalNode* VSL::AssignmentContext::OP_ASN_BOR() {
  return getToken(VSL::OP_ASN_BOR, 0);
}

tree::TerminalNode* VSL::AssignmentContext::OP_ASN_BXOR() {
  return getToken(VSL::OP_ASN_BXOR, 0);
}


size_t VSL::AssignmentContext::getRuleIndex() const {
  return VSL::RuleAssignment;
}


antlrcpp::Any VSL::AssignmentContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<VSLVisitor*>(visitor))
    return parserVisitor->visitAssignment(this);
  else
    return visitor->visitChildren(this);
}

VSL::AssignmentContext* VSL::assignment() {
  AssignmentContext *_localctx = _tracker.createInstance<AssignmentContext>(_ctx, getState());
  enterRule(_localctx, 32, VSL::RuleAssignment);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(217);
    dynamic_cast<AssignmentContext *>(_localctx)->lval = lvalue(0);
    setState(218);
    dynamic_cast<AssignmentContext *>(_localctx)->op = _input->LT(1);
    _la = _input->LA(1);
    if (!((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << VSL::OP_ASSIGN)
      | (1ULL << VSL::OP_ASN_ADD)
      | (1ULL << VSL::OP_ASN_BAND)
      | (1ULL << VSL::OP_ASN_BOR)
      | (1ULL << VSL::OP_ASN_BXOR)
      | (1ULL << VSL::OP_ASN_DIV)
      | (1ULL << VSL::OP_ASN_LSH)
      | (1ULL << VSL::OP_ASN_MOD)
      | (1ULL << VSL::OP_ASN_MUL)
      | (1ULL << VSL::OP_ASN_RSH)
      | (1ULL << VSL::OP_ASN_SUB))) != 0))) {
      dynamic_cast<AssignmentContext *>(_localctx)->op = _errHandler->recoverInline(this);
    }
    else {
      _errHandler->reportMatch(this);
      consume();
    }
    setState(219);
    dynamic_cast<AssignmentContext *>(_localctx)->value = expression(0);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- LvalueContext ------------------------------------------------------------------

VSL::LvalueContext::LvalueContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* VSL::LvalueContext::IDENTIFIER() {
  return getToken(VSL::IDENTIFIER, 0);
}

tree::TerminalNode* VSL::LvalueContext::LBRACKET() {
  return getToken(VSL::LBRACKET, 0);
}

tree::TerminalNode* VSL::LvalueContext::RBRACKET() {
  return getToken(VSL::RBRACKET, 0);
}

VSL::LvalueContext* VSL::LvalueContext::lvalue() {
  return getRuleContext<VSL::LvalueContext>(0);
}

VSL::ExpressionContext* VSL::LvalueContext::expression() {
  return getRuleContext<VSL::ExpressionContext>(0);
}

tree::TerminalNode* VSL::LvalueContext::PERIOD() {
  return getToken(VSL::PERIOD, 0);
}


size_t VSL::LvalueContext::getRuleIndex() const {
  return VSL::RuleLvalue;
}


antlrcpp::Any VSL::LvalueContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<VSLVisitor*>(visitor))
    return parserVisitor->visitLvalue(this);
  else
    return visitor->visitChildren(this);
}


VSL::LvalueContext* VSL::lvalue() {
   return lvalue(0);
}

VSL::LvalueContext* VSL::lvalue(int precedence) {
  ParserRuleContext *parentContext = _ctx;
  size_t parentState = getState();
  VSL::LvalueContext *_localctx = _tracker.createInstance<LvalueContext>(_ctx, parentState);
  VSL::LvalueContext *previousContext = _localctx;
  (void)previousContext; // Silence compiler, in case the context is not used by generated code.
  size_t startState = 34;
  enterRecursionRule(_localctx, 34, VSL::RuleLvalue, precedence);

    

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    unrollRecursionContexts(parentContext);
  });
  try {
    size_t alt;
    enterOuterAlt(_localctx, 1);
    setState(222);
    dynamic_cast<LvalueContext *>(_localctx)->name = match(VSL::IDENTIFIER);
    _ctx->stop = _input->LT(-1);
    setState(234);
    _errHandler->sync(this);
    alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 17, _ctx);
    while (alt != 2 && alt != atn::ATN::INVALID_ALT_NUMBER) {
      if (alt == 1) {
        if (!_parseListeners.empty())
          triggerExitRuleEvent();
        previousContext = _localctx;
        setState(232);
        _errHandler->sync(this);
        switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 16, _ctx)) {
        case 1: {
          _localctx = _tracker.createInstance<LvalueContext>(parentContext, parentState);
          _localctx->val = previousContext;
          pushNewRecursionContext(_localctx, startState, RuleLvalue);
          setState(224);

          if (!(precpred(_ctx, 2))) throw FailedPredicateException(this, "precpred(_ctx, 2)");
          setState(225);
          match(VSL::LBRACKET);
          setState(226);
          dynamic_cast<LvalueContext *>(_localctx)->index = expression(0);
          setState(227);
          match(VSL::RBRACKET);
          break;
        }

        case 2: {
          _localctx = _tracker.createInstance<LvalueContext>(parentContext, parentState);
          _localctx->val = previousContext;
          pushNewRecursionContext(_localctx, startState, RuleLvalue);
          setState(229);

          if (!(precpred(_ctx, 1))) throw FailedPredicateException(this, "precpred(_ctx, 1)");
          setState(230);
          match(VSL::PERIOD);
          setState(231);
          match(VSL::IDENTIFIER);
          break;
        }

        default:
          break;
        } 
      }
      setState(236);
      _errHandler->sync(this);
      alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 17, _ctx);
    }
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }
  return _localctx;
}

//----------------- IfStatementContext ------------------------------------------------------------------

VSL::IfStatementContext::IfStatementContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* VSL::IfStatementContext::KW_IF() {
  return getToken(VSL::KW_IF, 0);
}

tree::TerminalNode* VSL::IfStatementContext::LPAREN() {
  return getToken(VSL::LPAREN, 0);
}

tree::TerminalNode* VSL::IfStatementContext::RPAREN() {
  return getToken(VSL::RPAREN, 0);
}

VSL::ExpressionContext* VSL::IfStatementContext::expression() {
  return getRuleContext<VSL::ExpressionContext>(0);
}

VSL::StatementContext* VSL::IfStatementContext::statement() {
  return getRuleContext<VSL::StatementContext>(0);
}

VSL::StatementBlockContext* VSL::IfStatementContext::statementBlock() {
  return getRuleContext<VSL::StatementBlockContext>(0);
}

std::vector<VSL::ElifStatementContext *> VSL::IfStatementContext::elifStatement() {
  return getRuleContexts<VSL::ElifStatementContext>();
}

VSL::ElifStatementContext* VSL::IfStatementContext::elifStatement(size_t i) {
  return getRuleContext<VSL::ElifStatementContext>(i);
}

VSL::ElseStatementContext* VSL::IfStatementContext::elseStatement() {
  return getRuleContext<VSL::ElseStatementContext>(0);
}


size_t VSL::IfStatementContext::getRuleIndex() const {
  return VSL::RuleIfStatement;
}


antlrcpp::Any VSL::IfStatementContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<VSLVisitor*>(visitor))
    return parserVisitor->visitIfStatement(this);
  else
    return visitor->visitChildren(this);
}

VSL::IfStatementContext* VSL::ifStatement() {
  IfStatementContext *_localctx = _tracker.createInstance<IfStatementContext>(_ctx, getState());
  enterRule(_localctx, 36, VSL::RuleIfStatement);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    size_t alt;
    enterOuterAlt(_localctx, 1);
    setState(237);
    match(VSL::KW_IF);
    setState(238);
    match(VSL::LPAREN);
    setState(239);
    dynamic_cast<IfStatementContext *>(_localctx)->cond = expression(0);
    setState(240);
    match(VSL::RPAREN);
    setState(243);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case VSL::KW_BREAK:
      case VSL::KW_CONTINUE:
      case VSL::KW_DISCARD:
      case VSL::KW_FOR:
      case VSL::KW_IF:
      case VSL::KW_RETURN:
      case VSL::IDENTIFIER: {
        setState(241);
        statement();
        break;
      }

      case VSL::LBRACE: {
        setState(242);
        statementBlock();
        break;
      }

    default:
      throw NoViableAltException(this);
    }
    setState(248);
    _errHandler->sync(this);
    alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 19, _ctx);
    while (alt != 2 && alt != atn::ATN::INVALID_ALT_NUMBER) {
      if (alt == 1) {
        setState(245);
        elifStatement(); 
      }
      setState(250);
      _errHandler->sync(this);
      alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 19, _ctx);
    }
    setState(252);
    _errHandler->sync(this);

    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 20, _ctx)) {
    case 1: {
      setState(251);
      elseStatement();
      break;
    }

    default:
      break;
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ElifStatementContext ------------------------------------------------------------------

VSL::ElifStatementContext::ElifStatementContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* VSL::ElifStatementContext::KW_ELIF() {
  return getToken(VSL::KW_ELIF, 0);
}

tree::TerminalNode* VSL::ElifStatementContext::LPAREN() {
  return getToken(VSL::LPAREN, 0);
}

tree::TerminalNode* VSL::ElifStatementContext::RPAREN() {
  return getToken(VSL::RPAREN, 0);
}

VSL::ExpressionContext* VSL::ElifStatementContext::expression() {
  return getRuleContext<VSL::ExpressionContext>(0);
}

VSL::StatementContext* VSL::ElifStatementContext::statement() {
  return getRuleContext<VSL::StatementContext>(0);
}

VSL::StatementBlockContext* VSL::ElifStatementContext::statementBlock() {
  return getRuleContext<VSL::StatementBlockContext>(0);
}


size_t VSL::ElifStatementContext::getRuleIndex() const {
  return VSL::RuleElifStatement;
}


antlrcpp::Any VSL::ElifStatementContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<VSLVisitor*>(visitor))
    return parserVisitor->visitElifStatement(this);
  else
    return visitor->visitChildren(this);
}

VSL::ElifStatementContext* VSL::elifStatement() {
  ElifStatementContext *_localctx = _tracker.createInstance<ElifStatementContext>(_ctx, getState());
  enterRule(_localctx, 38, VSL::RuleElifStatement);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(254);
    match(VSL::KW_ELIF);
    setState(255);
    match(VSL::LPAREN);
    setState(256);
    dynamic_cast<ElifStatementContext *>(_localctx)->cond = expression(0);
    setState(257);
    match(VSL::RPAREN);
    setState(260);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case VSL::KW_BREAK:
      case VSL::KW_CONTINUE:
      case VSL::KW_DISCARD:
      case VSL::KW_FOR:
      case VSL::KW_IF:
      case VSL::KW_RETURN:
      case VSL::IDENTIFIER: {
        setState(258);
        statement();
        break;
      }

      case VSL::LBRACE: {
        setState(259);
        statementBlock();
        break;
      }

    default:
      throw NoViableAltException(this);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ElseStatementContext ------------------------------------------------------------------

VSL::ElseStatementContext::ElseStatementContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* VSL::ElseStatementContext::KW_ELSE() {
  return getToken(VSL::KW_ELSE, 0);
}

VSL::StatementContext* VSL::ElseStatementContext::statement() {
  return getRuleContext<VSL::StatementContext>(0);
}

VSL::StatementBlockContext* VSL::ElseStatementContext::statementBlock() {
  return getRuleContext<VSL::StatementBlockContext>(0);
}


size_t VSL::ElseStatementContext::getRuleIndex() const {
  return VSL::RuleElseStatement;
}


antlrcpp::Any VSL::ElseStatementContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<VSLVisitor*>(visitor))
    return parserVisitor->visitElseStatement(this);
  else
    return visitor->visitChildren(this);
}

VSL::ElseStatementContext* VSL::elseStatement() {
  ElseStatementContext *_localctx = _tracker.createInstance<ElseStatementContext>(_ctx, getState());
  enterRule(_localctx, 40, VSL::RuleElseStatement);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(262);
    match(VSL::KW_ELSE);
    setState(265);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case VSL::KW_BREAK:
      case VSL::KW_CONTINUE:
      case VSL::KW_DISCARD:
      case VSL::KW_FOR:
      case VSL::KW_IF:
      case VSL::KW_RETURN:
      case VSL::IDENTIFIER: {
        setState(263);
        statement();
        break;
      }

      case VSL::LBRACE: {
        setState(264);
        statementBlock();
        break;
      }

    default:
      throw NoViableAltException(this);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ForLoopStatementContext ------------------------------------------------------------------

VSL::ForLoopStatementContext::ForLoopStatementContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* VSL::ForLoopStatementContext::KW_FOR() {
  return getToken(VSL::KW_FOR, 0);
}

tree::TerminalNode* VSL::ForLoopStatementContext::LPAREN() {
  return getToken(VSL::LPAREN, 0);
}

tree::TerminalNode* VSL::ForLoopStatementContext::SEMICOLON() {
  return getToken(VSL::SEMICOLON, 0);
}

std::vector<tree::TerminalNode *> VSL::ForLoopStatementContext::COLON() {
  return getTokens(VSL::COLON);
}

tree::TerminalNode* VSL::ForLoopStatementContext::COLON(size_t i) {
  return getToken(VSL::COLON, i);
}

tree::TerminalNode* VSL::ForLoopStatementContext::RPAREN() {
  return getToken(VSL::RPAREN, 0);
}

VSL::StatementBlockContext* VSL::ForLoopStatementContext::statementBlock() {
  return getRuleContext<VSL::StatementBlockContext>(0);
}

tree::TerminalNode* VSL::ForLoopStatementContext::IDENTIFIER() {
  return getToken(VSL::IDENTIFIER, 0);
}

std::vector<tree::TerminalNode *> VSL::ForLoopStatementContext::INTEGER_LITERAL() {
  return getTokens(VSL::INTEGER_LITERAL);
}

tree::TerminalNode* VSL::ForLoopStatementContext::INTEGER_LITERAL(size_t i) {
  return getToken(VSL::INTEGER_LITERAL, i);
}


size_t VSL::ForLoopStatementContext::getRuleIndex() const {
  return VSL::RuleForLoopStatement;
}


antlrcpp::Any VSL::ForLoopStatementContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<VSLVisitor*>(visitor))
    return parserVisitor->visitForLoopStatement(this);
  else
    return visitor->visitChildren(this);
}

VSL::ForLoopStatementContext* VSL::forLoopStatement() {
  ForLoopStatementContext *_localctx = _tracker.createInstance<ForLoopStatementContext>(_ctx, getState());
  enterRule(_localctx, 42, VSL::RuleForLoopStatement);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(267);
    match(VSL::KW_FOR);
    setState(268);
    match(VSL::LPAREN);
    setState(269);
    dynamic_cast<ForLoopStatementContext *>(_localctx)->counter = match(VSL::IDENTIFIER);
    setState(270);
    match(VSL::SEMICOLON);
    setState(271);
    dynamic_cast<ForLoopStatementContext *>(_localctx)->start = match(VSL::INTEGER_LITERAL);
    setState(272);
    match(VSL::COLON);
    setState(273);
    dynamic_cast<ForLoopStatementContext *>(_localctx)->end = match(VSL::INTEGER_LITERAL);
    setState(276);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == VSL::COLON) {
      setState(274);
      match(VSL::COLON);
      setState(275);
      dynamic_cast<ForLoopStatementContext *>(_localctx)->step = match(VSL::INTEGER_LITERAL);
    }
    setState(278);
    match(VSL::RPAREN);
    setState(279);
    statementBlock();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ControlStatementContext ------------------------------------------------------------------

VSL::ControlStatementContext::ControlStatementContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* VSL::ControlStatementContext::KW_BREAK() {
  return getToken(VSL::KW_BREAK, 0);
}

tree::TerminalNode* VSL::ControlStatementContext::KW_CONTINUE() {
  return getToken(VSL::KW_CONTINUE, 0);
}

tree::TerminalNode* VSL::ControlStatementContext::KW_RETURN() {
  return getToken(VSL::KW_RETURN, 0);
}

tree::TerminalNode* VSL::ControlStatementContext::KW_DISCARD() {
  return getToken(VSL::KW_DISCARD, 0);
}


size_t VSL::ControlStatementContext::getRuleIndex() const {
  return VSL::RuleControlStatement;
}


antlrcpp::Any VSL::ControlStatementContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<VSLVisitor*>(visitor))
    return parserVisitor->visitControlStatement(this);
  else
    return visitor->visitChildren(this);
}

VSL::ControlStatementContext* VSL::controlStatement() {
  ControlStatementContext *_localctx = _tracker.createInstance<ControlStatementContext>(_ctx, getState());
  enterRule(_localctx, 44, VSL::RuleControlStatement);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(281);
    _la = _input->LA(1);
    if (!((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << VSL::KW_BREAK)
      | (1ULL << VSL::KW_CONTINUE)
      | (1ULL << VSL::KW_DISCARD)
      | (1ULL << VSL::KW_RETURN))) != 0))) {
    _errHandler->recoverInline(this);
    }
    else {
      _errHandler->reportMatch(this);
      consume();
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ExpressionContext ------------------------------------------------------------------

VSL::ExpressionContext::ExpressionContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t VSL::ExpressionContext::getRuleIndex() const {
  return VSL::RuleExpression;
}

void VSL::ExpressionContext::copyFrom(ExpressionContext *ctx) {
  ParserRuleContext::copyFrom(ctx);
}

//----------------- EqualityExprContext ------------------------------------------------------------------

std::vector<VSL::ExpressionContext *> VSL::EqualityExprContext::expression() {
  return getRuleContexts<VSL::ExpressionContext>();
}

VSL::ExpressionContext* VSL::EqualityExprContext::expression(size_t i) {
  return getRuleContext<VSL::ExpressionContext>(i);
}

tree::TerminalNode* VSL::EqualityExprContext::OP_EQUAL() {
  return getToken(VSL::OP_EQUAL, 0);
}

tree::TerminalNode* VSL::EqualityExprContext::OP_NEQUAL() {
  return getToken(VSL::OP_NEQUAL, 0);
}

VSL::EqualityExprContext::EqualityExprContext(ExpressionContext *ctx) { copyFrom(ctx); }


antlrcpp::Any VSL::EqualityExprContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<VSLVisitor*>(visitor))
    return parserVisitor->visitEqualityExpr(this);
  else
    return visitor->visitChildren(this);
}
//----------------- MulDivModExprContext ------------------------------------------------------------------

std::vector<VSL::ExpressionContext *> VSL::MulDivModExprContext::expression() {
  return getRuleContexts<VSL::ExpressionContext>();
}

VSL::ExpressionContext* VSL::MulDivModExprContext::expression(size_t i) {
  return getRuleContext<VSL::ExpressionContext>(i);
}

tree::TerminalNode* VSL::MulDivModExprContext::OP_MUL() {
  return getToken(VSL::OP_MUL, 0);
}

tree::TerminalNode* VSL::MulDivModExprContext::OP_DIV() {
  return getToken(VSL::OP_DIV, 0);
}

tree::TerminalNode* VSL::MulDivModExprContext::OP_MOD() {
  return getToken(VSL::OP_MOD, 0);
}

VSL::MulDivModExprContext::MulDivModExprContext(ExpressionContext *ctx) { copyFrom(ctx); }


antlrcpp::Any VSL::MulDivModExprContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<VSLVisitor*>(visitor))
    return parserVisitor->visitMulDivModExpr(this);
  else
    return visitor->visitChildren(this);
}
//----------------- BitwiseExprContext ------------------------------------------------------------------

std::vector<VSL::ExpressionContext *> VSL::BitwiseExprContext::expression() {
  return getRuleContexts<VSL::ExpressionContext>();
}

VSL::ExpressionContext* VSL::BitwiseExprContext::expression(size_t i) {
  return getRuleContext<VSL::ExpressionContext>(i);
}

tree::TerminalNode* VSL::BitwiseExprContext::OP_BAND() {
  return getToken(VSL::OP_BAND, 0);
}

tree::TerminalNode* VSL::BitwiseExprContext::OP_BOR() {
  return getToken(VSL::OP_BOR, 0);
}

tree::TerminalNode* VSL::BitwiseExprContext::OP_BXOR() {
  return getToken(VSL::OP_BXOR, 0);
}

VSL::BitwiseExprContext::BitwiseExprContext(ExpressionContext *ctx) { copyFrom(ctx); }


antlrcpp::Any VSL::BitwiseExprContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<VSLVisitor*>(visitor))
    return parserVisitor->visitBitwiseExpr(this);
  else
    return visitor->visitChildren(this);
}
//----------------- RelationalExprContext ------------------------------------------------------------------

std::vector<VSL::ExpressionContext *> VSL::RelationalExprContext::expression() {
  return getRuleContexts<VSL::ExpressionContext>();
}

VSL::ExpressionContext* VSL::RelationalExprContext::expression(size_t i) {
  return getRuleContext<VSL::ExpressionContext>(i);
}

tree::TerminalNode* VSL::RelationalExprContext::OP_LESS() {
  return getToken(VSL::OP_LESS, 0);
}

tree::TerminalNode* VSL::RelationalExprContext::OP_GREATER() {
  return getToken(VSL::OP_GREATER, 0);
}

tree::TerminalNode* VSL::RelationalExprContext::OP_LEQUAL() {
  return getToken(VSL::OP_LEQUAL, 0);
}

tree::TerminalNode* VSL::RelationalExprContext::OP_GEQUAL() {
  return getToken(VSL::OP_GEQUAL, 0);
}

VSL::RelationalExprContext::RelationalExprContext(ExpressionContext *ctx) { copyFrom(ctx); }


antlrcpp::Any VSL::RelationalExprContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<VSLVisitor*>(visitor))
    return parserVisitor->visitRelationalExpr(this);
  else
    return visitor->visitChildren(this);
}
//----------------- AtomExprContext ------------------------------------------------------------------

VSL::AtomContext* VSL::AtomExprContext::atom() {
  return getRuleContext<VSL::AtomContext>(0);
}

VSL::AtomExprContext::AtomExprContext(ExpressionContext *ctx) { copyFrom(ctx); }


antlrcpp::Any VSL::AtomExprContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<VSLVisitor*>(visitor))
    return parserVisitor->visitAtomExpr(this);
  else
    return visitor->visitChildren(this);
}
//----------------- AddSubExprContext ------------------------------------------------------------------

std::vector<VSL::ExpressionContext *> VSL::AddSubExprContext::expression() {
  return getRuleContexts<VSL::ExpressionContext>();
}

VSL::ExpressionContext* VSL::AddSubExprContext::expression(size_t i) {
  return getRuleContext<VSL::ExpressionContext>(i);
}

tree::TerminalNode* VSL::AddSubExprContext::OP_ADD() {
  return getToken(VSL::OP_ADD, 0);
}

tree::TerminalNode* VSL::AddSubExprContext::OP_SUB() {
  return getToken(VSL::OP_SUB, 0);
}

VSL::AddSubExprContext::AddSubExprContext(ExpressionContext *ctx) { copyFrom(ctx); }


antlrcpp::Any VSL::AddSubExprContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<VSLVisitor*>(visitor))
    return parserVisitor->visitAddSubExpr(this);
  else
    return visitor->visitChildren(this);
}
//----------------- ShiftExprContext ------------------------------------------------------------------

std::vector<VSL::ExpressionContext *> VSL::ShiftExprContext::expression() {
  return getRuleContexts<VSL::ExpressionContext>();
}

VSL::ExpressionContext* VSL::ShiftExprContext::expression(size_t i) {
  return getRuleContext<VSL::ExpressionContext>(i);
}

tree::TerminalNode* VSL::ShiftExprContext::OP_LSHIFT() {
  return getToken(VSL::OP_LSHIFT, 0);
}

tree::TerminalNode* VSL::ShiftExprContext::OP_RSHIFT() {
  return getToken(VSL::OP_RSHIFT, 0);
}

VSL::ShiftExprContext::ShiftExprContext(ExpressionContext *ctx) { copyFrom(ctx); }


antlrcpp::Any VSL::ShiftExprContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<VSLVisitor*>(visitor))
    return parserVisitor->visitShiftExpr(this);
  else
    return visitor->visitChildren(this);
}
//----------------- LogicalExprContext ------------------------------------------------------------------

std::vector<VSL::ExpressionContext *> VSL::LogicalExprContext::expression() {
  return getRuleContexts<VSL::ExpressionContext>();
}

VSL::ExpressionContext* VSL::LogicalExprContext::expression(size_t i) {
  return getRuleContext<VSL::ExpressionContext>(i);
}

tree::TerminalNode* VSL::LogicalExprContext::OP_LAND() {
  return getToken(VSL::OP_LAND, 0);
}

tree::TerminalNode* VSL::LogicalExprContext::OP_LOR() {
  return getToken(VSL::OP_LOR, 0);
}

VSL::LogicalExprContext::LogicalExprContext(ExpressionContext *ctx) { copyFrom(ctx); }


antlrcpp::Any VSL::LogicalExprContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<VSLVisitor*>(visitor))
    return parserVisitor->visitLogicalExpr(this);
  else
    return visitor->visitChildren(this);
}
//----------------- TernaryExprContext ------------------------------------------------------------------

tree::TerminalNode* VSL::TernaryExprContext::QMARK() {
  return getToken(VSL::QMARK, 0);
}

tree::TerminalNode* VSL::TernaryExprContext::COLON() {
  return getToken(VSL::COLON, 0);
}

std::vector<VSL::ExpressionContext *> VSL::TernaryExprContext::expression() {
  return getRuleContexts<VSL::ExpressionContext>();
}

VSL::ExpressionContext* VSL::TernaryExprContext::expression(size_t i) {
  return getRuleContext<VSL::ExpressionContext>(i);
}

VSL::TernaryExprContext::TernaryExprContext(ExpressionContext *ctx) { copyFrom(ctx); }


antlrcpp::Any VSL::TernaryExprContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<VSLVisitor*>(visitor))
    return parserVisitor->visitTernaryExpr(this);
  else
    return visitor->visitChildren(this);
}
//----------------- FactorExprContext ------------------------------------------------------------------

VSL::ExpressionContext* VSL::FactorExprContext::expression() {
  return getRuleContext<VSL::ExpressionContext>(0);
}

tree::TerminalNode* VSL::FactorExprContext::OP_ADD() {
  return getToken(VSL::OP_ADD, 0);
}

tree::TerminalNode* VSL::FactorExprContext::OP_SUB() {
  return getToken(VSL::OP_SUB, 0);
}

VSL::FactorExprContext::FactorExprContext(ExpressionContext *ctx) { copyFrom(ctx); }


antlrcpp::Any VSL::FactorExprContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<VSLVisitor*>(visitor))
    return parserVisitor->visitFactorExpr(this);
  else
    return visitor->visitChildren(this);
}
//----------------- NegateExprContext ------------------------------------------------------------------

VSL::ExpressionContext* VSL::NegateExprContext::expression() {
  return getRuleContext<VSL::ExpressionContext>(0);
}

tree::TerminalNode* VSL::NegateExprContext::OP_LNOT() {
  return getToken(VSL::OP_LNOT, 0);
}

tree::TerminalNode* VSL::NegateExprContext::OP_BNOT() {
  return getToken(VSL::OP_BNOT, 0);
}

VSL::NegateExprContext::NegateExprContext(ExpressionContext *ctx) { copyFrom(ctx); }


antlrcpp::Any VSL::NegateExprContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<VSLVisitor*>(visitor))
    return parserVisitor->visitNegateExpr(this);
  else
    return visitor->visitChildren(this);
}

VSL::ExpressionContext* VSL::expression() {
   return expression(0);
}

VSL::ExpressionContext* VSL::expression(int precedence) {
  ParserRuleContext *parentContext = _ctx;
  size_t parentState = getState();
  VSL::ExpressionContext *_localctx = _tracker.createInstance<ExpressionContext>(_ctx, parentState);
  VSL::ExpressionContext *previousContext = _localctx;
  (void)previousContext; // Silence compiler, in case the context is not used by generated code.
  size_t startState = 46;
  enterRecursionRule(_localctx, 46, VSL::RuleExpression, precedence);

    size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    unrollRecursionContexts(parentContext);
  });
  try {
    size_t alt;
    enterOuterAlt(_localctx, 1);
    setState(289);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case VSL::BOOLEAN_LITERAL:
      case VSL::INTEGER_LITERAL:
      case VSL::FLOAT_LITERAL:
      case VSL::IDENTIFIER:
      case VSL::LPAREN: {
        _localctx = _tracker.createInstance<AtomExprContext>(_localctx);
        _ctx = _localctx;
        previousContext = _localctx;

        setState(284);
        atom(0);
        break;
      }

      case VSL::OP_ADD:
      case VSL::OP_SUB: {
        _localctx = _tracker.createInstance<FactorExprContext>(_localctx);
        _ctx = _localctx;
        previousContext = _localctx;
        setState(285);
        dynamic_cast<FactorExprContext *>(_localctx)->op = _input->LT(1);
        _la = _input->LA(1);
        if (!(_la == VSL::OP_ADD

        || _la == VSL::OP_SUB)) {
          dynamic_cast<FactorExprContext *>(_localctx)->op = _errHandler->recoverInline(this);
        }
        else {
          _errHandler->reportMatch(this);
          consume();
        }
        setState(286);
        dynamic_cast<FactorExprContext *>(_localctx)->val = expression(10);
        break;
      }

      case VSL::OP_BNOT:
      case VSL::OP_LNOT: {
        _localctx = _tracker.createInstance<NegateExprContext>(_localctx);
        _ctx = _localctx;
        previousContext = _localctx;
        setState(287);
        dynamic_cast<NegateExprContext *>(_localctx)->op = _input->LT(1);
        _la = _input->LA(1);
        if (!(_la == VSL::OP_BNOT

        || _la == VSL::OP_LNOT)) {
          dynamic_cast<NegateExprContext *>(_localctx)->op = _errHandler->recoverInline(this);
        }
        else {
          _errHandler->reportMatch(this);
          consume();
        }
        setState(288);
        dynamic_cast<NegateExprContext *>(_localctx)->val = expression(9);
        break;
      }

    default:
      throw NoViableAltException(this);
    }
    _ctx->stop = _input->LT(-1);
    setState(320);
    _errHandler->sync(this);
    alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 26, _ctx);
    while (alt != 2 && alt != atn::ATN::INVALID_ALT_NUMBER) {
      if (alt == 1) {
        if (!_parseListeners.empty())
          triggerExitRuleEvent();
        previousContext = _localctx;
        setState(318);
        _errHandler->sync(this);
        switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 25, _ctx)) {
        case 1: {
          auto newContext = _tracker.createInstance<MulDivModExprContext>(_tracker.createInstance<ExpressionContext>(parentContext, parentState));
          _localctx = newContext;
          newContext->left = previousContext;
          pushNewRecursionContext(newContext, startState, RuleExpression);
          setState(291);

          if (!(precpred(_ctx, 8))) throw FailedPredicateException(this, "precpred(_ctx, 8)");
          setState(292);
          dynamic_cast<MulDivModExprContext *>(_localctx)->op = _input->LT(1);
          _la = _input->LA(1);
          if (!((((_la & ~ 0x3fULL) == 0) &&
            ((1ULL << _la) & ((1ULL << VSL::OP_DIV)
            | (1ULL << VSL::OP_MOD)
            | (1ULL << VSL::OP_MUL))) != 0))) {
            dynamic_cast<MulDivModExprContext *>(_localctx)->op = _errHandler->recoverInline(this);
          }
          else {
            _errHandler->reportMatch(this);
            consume();
          }
          setState(293);
          dynamic_cast<MulDivModExprContext *>(_localctx)->right = expression(9);
          break;
        }

        case 2: {
          auto newContext = _tracker.createInstance<AddSubExprContext>(_tracker.createInstance<ExpressionContext>(parentContext, parentState));
          _localctx = newContext;
          newContext->left = previousContext;
          pushNewRecursionContext(newContext, startState, RuleExpression);
          setState(294);

          if (!(precpred(_ctx, 7))) throw FailedPredicateException(this, "precpred(_ctx, 7)");
          setState(295);
          dynamic_cast<AddSubExprContext *>(_localctx)->op = _input->LT(1);
          _la = _input->LA(1);
          if (!(_la == VSL::OP_ADD

          || _la == VSL::OP_SUB)) {
            dynamic_cast<AddSubExprContext *>(_localctx)->op = _errHandler->recoverInline(this);
          }
          else {
            _errHandler->reportMatch(this);
            consume();
          }
          setState(296);
          dynamic_cast<AddSubExprContext *>(_localctx)->right = expression(8);
          break;
        }

        case 3: {
          auto newContext = _tracker.createInstance<ShiftExprContext>(_tracker.createInstance<ExpressionContext>(parentContext, parentState));
          _localctx = newContext;
          newContext->left = previousContext;
          pushNewRecursionContext(newContext, startState, RuleExpression);
          setState(297);

          if (!(precpred(_ctx, 6))) throw FailedPredicateException(this, "precpred(_ctx, 6)");
          setState(298);
          dynamic_cast<ShiftExprContext *>(_localctx)->op = _input->LT(1);
          _la = _input->LA(1);
          if (!(_la == VSL::OP_LSHIFT

          || _la == VSL::OP_RSHIFT)) {
            dynamic_cast<ShiftExprContext *>(_localctx)->op = _errHandler->recoverInline(this);
          }
          else {
            _errHandler->reportMatch(this);
            consume();
          }
          setState(299);
          dynamic_cast<ShiftExprContext *>(_localctx)->right = expression(7);
          break;
        }

        case 4: {
          auto newContext = _tracker.createInstance<RelationalExprContext>(_tracker.createInstance<ExpressionContext>(parentContext, parentState));
          _localctx = newContext;
          newContext->left = previousContext;
          pushNewRecursionContext(newContext, startState, RuleExpression);
          setState(300);

          if (!(precpred(_ctx, 5))) throw FailedPredicateException(this, "precpred(_ctx, 5)");
          setState(301);
          dynamic_cast<RelationalExprContext *>(_localctx)->op = _input->LT(1);
          _la = _input->LA(1);
          if (!((((_la & ~ 0x3fULL) == 0) &&
            ((1ULL << _la) & ((1ULL << VSL::OP_GEQUAL)
            | (1ULL << VSL::OP_GREATER)
            | (1ULL << VSL::OP_LEQUAL)
            | (1ULL << VSL::OP_LESS))) != 0))) {
            dynamic_cast<RelationalExprContext *>(_localctx)->op = _errHandler->recoverInline(this);
          }
          else {
            _errHandler->reportMatch(this);
            consume();
          }
          setState(302);
          dynamic_cast<RelationalExprContext *>(_localctx)->right = expression(6);
          break;
        }

        case 5: {
          auto newContext = _tracker.createInstance<EqualityExprContext>(_tracker.createInstance<ExpressionContext>(parentContext, parentState));
          _localctx = newContext;
          newContext->left = previousContext;
          pushNewRecursionContext(newContext, startState, RuleExpression);
          setState(303);

          if (!(precpred(_ctx, 4))) throw FailedPredicateException(this, "precpred(_ctx, 4)");
          setState(304);
          dynamic_cast<EqualityExprContext *>(_localctx)->op = _input->LT(1);
          _la = _input->LA(1);
          if (!(_la == VSL::OP_EQUAL

          || _la == VSL::OP_NEQUAL)) {
            dynamic_cast<EqualityExprContext *>(_localctx)->op = _errHandler->recoverInline(this);
          }
          else {
            _errHandler->reportMatch(this);
            consume();
          }
          setState(305);
          dynamic_cast<EqualityExprContext *>(_localctx)->right = expression(5);
          break;
        }

        case 6: {
          auto newContext = _tracker.createInstance<BitwiseExprContext>(_tracker.createInstance<ExpressionContext>(parentContext, parentState));
          _localctx = newContext;
          newContext->left = previousContext;
          pushNewRecursionContext(newContext, startState, RuleExpression);
          setState(306);

          if (!(precpred(_ctx, 3))) throw FailedPredicateException(this, "precpred(_ctx, 3)");
          setState(307);
          dynamic_cast<BitwiseExprContext *>(_localctx)->op = _input->LT(1);
          _la = _input->LA(1);
          if (!((((_la & ~ 0x3fULL) == 0) &&
            ((1ULL << _la) & ((1ULL << VSL::OP_BAND)
            | (1ULL << VSL::OP_BOR)
            | (1ULL << VSL::OP_BXOR))) != 0))) {
            dynamic_cast<BitwiseExprContext *>(_localctx)->op = _errHandler->recoverInline(this);
          }
          else {
            _errHandler->reportMatch(this);
            consume();
          }
          setState(308);
          dynamic_cast<BitwiseExprContext *>(_localctx)->right = expression(4);
          break;
        }

        case 7: {
          auto newContext = _tracker.createInstance<LogicalExprContext>(_tracker.createInstance<ExpressionContext>(parentContext, parentState));
          _localctx = newContext;
          newContext->left = previousContext;
          pushNewRecursionContext(newContext, startState, RuleExpression);
          setState(309);

          if (!(precpred(_ctx, 2))) throw FailedPredicateException(this, "precpred(_ctx, 2)");
          setState(310);
          dynamic_cast<LogicalExprContext *>(_localctx)->op = _input->LT(1);
          _la = _input->LA(1);
          if (!(_la == VSL::OP_LAND

          || _la == VSL::OP_LOR)) {
            dynamic_cast<LogicalExprContext *>(_localctx)->op = _errHandler->recoverInline(this);
          }
          else {
            _errHandler->reportMatch(this);
            consume();
          }
          setState(311);
          dynamic_cast<LogicalExprContext *>(_localctx)->right = expression(3);
          break;
        }

        case 8: {
          auto newContext = _tracker.createInstance<TernaryExprContext>(_tracker.createInstance<ExpressionContext>(parentContext, parentState));
          _localctx = newContext;
          newContext->cond = previousContext;
          pushNewRecursionContext(newContext, startState, RuleExpression);
          setState(312);

          if (!(precpred(_ctx, 1))) throw FailedPredicateException(this, "precpred(_ctx, 1)");
          setState(313);
          match(VSL::QMARK);
          setState(314);
          dynamic_cast<TernaryExprContext *>(_localctx)->texpr = expression(0);
          setState(315);
          match(VSL::COLON);
          setState(316);
          dynamic_cast<TernaryExprContext *>(_localctx)->fexpr = expression(2);
          break;
        }

        default:
          break;
        } 
      }
      setState(322);
      _errHandler->sync(this);
      alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 26, _ctx);
    }
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }
  return _localctx;
}

//----------------- AtomContext ------------------------------------------------------------------

VSL::AtomContext::AtomContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t VSL::AtomContext::getRuleIndex() const {
  return VSL::RuleAtom;
}

void VSL::AtomContext::copyFrom(AtomContext *ctx) {
  ParserRuleContext::copyFrom(ctx);
}

//----------------- MemberAtomContext ------------------------------------------------------------------

VSL::AtomContext* VSL::MemberAtomContext::atom() {
  return getRuleContext<VSL::AtomContext>(0);
}

tree::TerminalNode* VSL::MemberAtomContext::PERIOD() {
  return getToken(VSL::PERIOD, 0);
}

tree::TerminalNode* VSL::MemberAtomContext::IDENTIFIER() {
  return getToken(VSL::IDENTIFIER, 0);
}

VSL::MemberAtomContext::MemberAtomContext(AtomContext *ctx) { copyFrom(ctx); }


antlrcpp::Any VSL::MemberAtomContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<VSLVisitor*>(visitor))
    return parserVisitor->visitMemberAtom(this);
  else
    return visitor->visitChildren(this);
}
//----------------- IndexAtomContext ------------------------------------------------------------------

VSL::AtomContext* VSL::IndexAtomContext::atom() {
  return getRuleContext<VSL::AtomContext>(0);
}

tree::TerminalNode* VSL::IndexAtomContext::LBRACKET() {
  return getToken(VSL::LBRACKET, 0);
}

tree::TerminalNode* VSL::IndexAtomContext::RBRACKET() {
  return getToken(VSL::RBRACKET, 0);
}

std::vector<VSL::ExpressionContext *> VSL::IndexAtomContext::expression() {
  return getRuleContexts<VSL::ExpressionContext>();
}

VSL::ExpressionContext* VSL::IndexAtomContext::expression(size_t i) {
  return getRuleContext<VSL::ExpressionContext>(i);
}

tree::TerminalNode* VSL::IndexAtomContext::COMMA() {
  return getToken(VSL::COMMA, 0);
}

VSL::IndexAtomContext::IndexAtomContext(AtomContext *ctx) { copyFrom(ctx); }


antlrcpp::Any VSL::IndexAtomContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<VSLVisitor*>(visitor))
    return parserVisitor->visitIndexAtom(this);
  else
    return visitor->visitChildren(this);
}
//----------------- GroupAtomContext ------------------------------------------------------------------

tree::TerminalNode* VSL::GroupAtomContext::LPAREN() {
  return getToken(VSL::LPAREN, 0);
}

VSL::ExpressionContext* VSL::GroupAtomContext::expression() {
  return getRuleContext<VSL::ExpressionContext>(0);
}

tree::TerminalNode* VSL::GroupAtomContext::RPAREN() {
  return getToken(VSL::RPAREN, 0);
}

VSL::GroupAtomContext::GroupAtomContext(AtomContext *ctx) { copyFrom(ctx); }


antlrcpp::Any VSL::GroupAtomContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<VSLVisitor*>(visitor))
    return parserVisitor->visitGroupAtom(this);
  else
    return visitor->visitChildren(this);
}
//----------------- CallAtomContext ------------------------------------------------------------------

VSL::FunctionCallContext* VSL::CallAtomContext::functionCall() {
  return getRuleContext<VSL::FunctionCallContext>(0);
}

VSL::CallAtomContext::CallAtomContext(AtomContext *ctx) { copyFrom(ctx); }


antlrcpp::Any VSL::CallAtomContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<VSLVisitor*>(visitor))
    return parserVisitor->visitCallAtom(this);
  else
    return visitor->visitChildren(this);
}
//----------------- NameAtomContext ------------------------------------------------------------------

tree::TerminalNode* VSL::NameAtomContext::IDENTIFIER() {
  return getToken(VSL::IDENTIFIER, 0);
}

VSL::NameAtomContext::NameAtomContext(AtomContext *ctx) { copyFrom(ctx); }


antlrcpp::Any VSL::NameAtomContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<VSLVisitor*>(visitor))
    return parserVisitor->visitNameAtom(this);
  else
    return visitor->visitChildren(this);
}
//----------------- LiteralAtomContext ------------------------------------------------------------------

VSL::ScalarLiteralContext* VSL::LiteralAtomContext::scalarLiteral() {
  return getRuleContext<VSL::ScalarLiteralContext>(0);
}

VSL::LiteralAtomContext::LiteralAtomContext(AtomContext *ctx) { copyFrom(ctx); }


antlrcpp::Any VSL::LiteralAtomContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<VSLVisitor*>(visitor))
    return parserVisitor->visitLiteralAtom(this);
  else
    return visitor->visitChildren(this);
}

VSL::AtomContext* VSL::atom() {
   return atom(0);
}

VSL::AtomContext* VSL::atom(int precedence) {
  ParserRuleContext *parentContext = _ctx;
  size_t parentState = getState();
  VSL::AtomContext *_localctx = _tracker.createInstance<AtomContext>(_ctx, parentState);
  VSL::AtomContext *previousContext = _localctx;
  (void)previousContext; // Silence compiler, in case the context is not used by generated code.
  size_t startState = 48;
  enterRecursionRule(_localctx, 48, VSL::RuleAtom, precedence);

    size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    unrollRecursionContexts(parentContext);
  });
  try {
    size_t alt;
    enterOuterAlt(_localctx, 1);
    setState(331);
    _errHandler->sync(this);
    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 27, _ctx)) {
    case 1: {
      _localctx = _tracker.createInstance<GroupAtomContext>(_localctx);
      _ctx = _localctx;
      previousContext = _localctx;

      setState(324);
      match(VSL::LPAREN);
      setState(325);
      expression(0);
      setState(326);
      match(VSL::RPAREN);
      break;
    }

    case 2: {
      _localctx = _tracker.createInstance<CallAtomContext>(_localctx);
      _ctx = _localctx;
      previousContext = _localctx;
      setState(328);
      functionCall();
      break;
    }

    case 3: {
      _localctx = _tracker.createInstance<LiteralAtomContext>(_localctx);
      _ctx = _localctx;
      previousContext = _localctx;
      setState(329);
      scalarLiteral();
      break;
    }

    case 4: {
      _localctx = _tracker.createInstance<NameAtomContext>(_localctx);
      _ctx = _localctx;
      previousContext = _localctx;
      setState(330);
      match(VSL::IDENTIFIER);
      break;
    }

    default:
      break;
    }
    _ctx->stop = _input->LT(-1);
    setState(347);
    _errHandler->sync(this);
    alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 30, _ctx);
    while (alt != 2 && alt != atn::ATN::INVALID_ALT_NUMBER) {
      if (alt == 1) {
        if (!_parseListeners.empty())
          triggerExitRuleEvent();
        previousContext = _localctx;
        setState(345);
        _errHandler->sync(this);
        switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 29, _ctx)) {
        case 1: {
          auto newContext = _tracker.createInstance<IndexAtomContext>(_tracker.createInstance<AtomContext>(parentContext, parentState));
          _localctx = newContext;
          pushNewRecursionContext(newContext, startState, RuleAtom);
          setState(333);

          if (!(precpred(_ctx, 5))) throw FailedPredicateException(this, "precpred(_ctx, 5)");
          setState(334);
          match(VSL::LBRACKET);
          setState(335);
          dynamic_cast<IndexAtomContext *>(_localctx)->index = expression(0);
          setState(338);
          _errHandler->sync(this);

          _la = _input->LA(1);
          if (_la == VSL::COMMA) {
            setState(336);
            match(VSL::COMMA);
            setState(337);
            dynamic_cast<IndexAtomContext *>(_localctx)->index2 = expression(0);
          }
          setState(340);
          match(VSL::RBRACKET);
          break;
        }

        case 2: {
          auto newContext = _tracker.createInstance<MemberAtomContext>(_tracker.createInstance<AtomContext>(parentContext, parentState));
          _localctx = newContext;
          pushNewRecursionContext(newContext, startState, RuleAtom);
          setState(342);

          if (!(precpred(_ctx, 4))) throw FailedPredicateException(this, "precpred(_ctx, 4)");
          setState(343);
          match(VSL::PERIOD);
          setState(344);
          match(VSL::IDENTIFIER);
          break;
        }

        default:
          break;
        } 
      }
      setState(349);
      _errHandler->sync(this);
      alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 30, _ctx);
    }
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }
  return _localctx;
}

//----------------- FunctionCallContext ------------------------------------------------------------------

VSL::FunctionCallContext::FunctionCallContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* VSL::FunctionCallContext::LPAREN() {
  return getToken(VSL::LPAREN, 0);
}

tree::TerminalNode* VSL::FunctionCallContext::RPAREN() {
  return getToken(VSL::RPAREN, 0);
}

tree::TerminalNode* VSL::FunctionCallContext::IDENTIFIER() {
  return getToken(VSL::IDENTIFIER, 0);
}

std::vector<VSL::ExpressionContext *> VSL::FunctionCallContext::expression() {
  return getRuleContexts<VSL::ExpressionContext>();
}

VSL::ExpressionContext* VSL::FunctionCallContext::expression(size_t i) {
  return getRuleContext<VSL::ExpressionContext>(i);
}

std::vector<tree::TerminalNode *> VSL::FunctionCallContext::COMMA() {
  return getTokens(VSL::COMMA);
}

tree::TerminalNode* VSL::FunctionCallContext::COMMA(size_t i) {
  return getToken(VSL::COMMA, i);
}


size_t VSL::FunctionCallContext::getRuleIndex() const {
  return VSL::RuleFunctionCall;
}


antlrcpp::Any VSL::FunctionCallContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<VSLVisitor*>(visitor))
    return parserVisitor->visitFunctionCall(this);
  else
    return visitor->visitChildren(this);
}

VSL::FunctionCallContext* VSL::functionCall() {
  FunctionCallContext *_localctx = _tracker.createInstance<FunctionCallContext>(_ctx, getState());
  enterRule(_localctx, 50, VSL::RuleFunctionCall);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(350);
    dynamic_cast<FunctionCallContext *>(_localctx)->name = match(VSL::IDENTIFIER);
    setState(351);
    match(VSL::LPAREN);
    setState(352);
    dynamic_cast<FunctionCallContext *>(_localctx)->expressionContext = expression(0);
    dynamic_cast<FunctionCallContext *>(_localctx)->args.push_back(dynamic_cast<FunctionCallContext *>(_localctx)->expressionContext);
    setState(357);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while (_la == VSL::COMMA) {
      setState(353);
      match(VSL::COMMA);
      setState(354);
      dynamic_cast<FunctionCallContext *>(_localctx)->expressionContext = expression(0);
      dynamic_cast<FunctionCallContext *>(_localctx)->args.push_back(dynamic_cast<FunctionCallContext *>(_localctx)->expressionContext);
      setState(359);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
    setState(360);
    match(VSL::RPAREN);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ScalarLiteralContext ------------------------------------------------------------------

VSL::ScalarLiteralContext::ScalarLiteralContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* VSL::ScalarLiteralContext::INTEGER_LITERAL() {
  return getToken(VSL::INTEGER_LITERAL, 0);
}

tree::TerminalNode* VSL::ScalarLiteralContext::FLOAT_LITERAL() {
  return getToken(VSL::FLOAT_LITERAL, 0);
}

tree::TerminalNode* VSL::ScalarLiteralContext::BOOLEAN_LITERAL() {
  return getToken(VSL::BOOLEAN_LITERAL, 0);
}


size_t VSL::ScalarLiteralContext::getRuleIndex() const {
  return VSL::RuleScalarLiteral;
}


antlrcpp::Any VSL::ScalarLiteralContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<VSLVisitor*>(visitor))
    return parserVisitor->visitScalarLiteral(this);
  else
    return visitor->visitChildren(this);
}

VSL::ScalarLiteralContext* VSL::scalarLiteral() {
  ScalarLiteralContext *_localctx = _tracker.createInstance<ScalarLiteralContext>(_ctx, getState());
  enterRule(_localctx, 52, VSL::RuleScalarLiteral);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(362);
    _la = _input->LA(1);
    if (!((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << VSL::BOOLEAN_LITERAL)
      | (1ULL << VSL::INTEGER_LITERAL)
      | (1ULL << VSL::FLOAT_LITERAL))) != 0))) {
    _errHandler->recoverInline(this);
    }
    else {
      _errHandler->reportMatch(this);
      consume();
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

bool VSL::sempred(RuleContext *context, size_t ruleIndex, size_t predicateIndex) {
  switch (ruleIndex) {
    case 17: return lvalueSempred(dynamic_cast<LvalueContext *>(context), predicateIndex);
    case 23: return expressionSempred(dynamic_cast<ExpressionContext *>(context), predicateIndex);
    case 24: return atomSempred(dynamic_cast<AtomContext *>(context), predicateIndex);

  default:
    break;
  }
  return true;
}

bool VSL::lvalueSempred(LvalueContext *_localctx, size_t predicateIndex) {
  switch (predicateIndex) {
    case 0: return precpred(_ctx, 2);
    case 1: return precpred(_ctx, 1);

  default:
    break;
  }
  return true;
}

bool VSL::expressionSempred(ExpressionContext *_localctx, size_t predicateIndex) {
  switch (predicateIndex) {
    case 2: return precpred(_ctx, 8);
    case 3: return precpred(_ctx, 7);
    case 4: return precpred(_ctx, 6);
    case 5: return precpred(_ctx, 5);
    case 6: return precpred(_ctx, 4);
    case 7: return precpred(_ctx, 3);
    case 8: return precpred(_ctx, 2);
    case 9: return precpred(_ctx, 1);

  default:
    break;
  }
  return true;
}

bool VSL::atomSempred(AtomContext *_localctx, size_t predicateIndex) {
  switch (predicateIndex) {
    case 10: return precpred(_ctx, 5);
    case 11: return precpred(_ctx, 4);

  default:
    break;
  }
  return true;
}

// Static vars and initialization.
std::vector<dfa::DFA> VSL::_decisionToDFA;
atn::PredictionContextCache VSL::_sharedContextCache;

// We own the ATN which in turn owns the ATN states.
atn::ATN VSL::_atn;
std::vector<uint16_t> VSL::_serializedATN;

std::vector<std::string> VSL::_ruleNames = {
  "file", "shaderTypeStatement", "topLevelStatement", "shaderStructDefinition", 
  "shaderUniformStatement", "shaderBindingStatement", "shaderLocalStatement", 
  "shaderSubpassInputStatement", "shaderStageFunction", "shaderVertexInput", 
  "shaderFragmentOutput", "blendStatement", "statement", "statementBlock", 
  "variableDeclaration", "variableDefinition", "assignment", "lvalue", "ifStatement", 
  "elifStatement", "elseStatement", "forLoopStatement", "controlStatement", 
  "expression", "atom", "functionCall", "scalarLiteral"
};

std::vector<std::string> VSL::_literalNames = {
  "", "", "'bind'", "'break'", "'continue'", "'discard'", "'elif'", "'else'", 
  "'flat'", "'for'", "'@frag'", "'if'", "'inst'", "'local'", "'return'", 
  "'@shader'", "'subpassinput'", "'@struct'", "'@uniform'", "'@vert'", "", 
  "", "", "", "'@'", "':'", "','", "'{'", "'['", "'('", "'.'", "'\u003F'", 
  "'}'", "']'", "')'", "';'", "'+'", "'='", "'+='", "'&='", "'|='", "'^='", 
  "'/='", "'<<='", "'%='", "'*='", "'>>='", "'-='", "'&'", "'~'", "'|'", 
  "'^'", "'/'", "'=='", "'>='", "'>'", "'&&'", "'<='", "'<'", "'!'", "'||'", 
  "'<<'", "'%'", "'*'", "'!='", "'>>'", "'-'"
};

std::vector<std::string> VSL::_symbolicNames = {
  "", "BOOLEAN_LITERAL", "KW_BIND", "KW_BREAK", "KW_CONTINUE", "KW_DISCARD", 
  "KW_ELIF", "KW_ELSE", "KW_FLAT", "KW_FOR", "KW_FRAG", "KW_IF", "KW_INST", 
  "KW_LOCAL", "KW_RETURN", "KW_SHADER", "KW_PASSINPUT", "KW_STRUCT", "KW_UNIFORM", 
  "KW_VERT", "INTEGER_LITERAL", "FLOAT_LITERAL", "VERTEX_SEMANTIC", "IDENTIFIER", 
  "ATSIGN", "COLON", "COMMA", "LBRACE", "LBRACKET", "LPAREN", "PERIOD", 
  "QMARK", "RBRACE", "RBRACKET", "RPAREN", "SEMICOLON", "OP_ADD", "OP_ASSIGN", 
  "OP_ASN_ADD", "OP_ASN_BAND", "OP_ASN_BOR", "OP_ASN_BXOR", "OP_ASN_DIV", 
  "OP_ASN_LSH", "OP_ASN_MOD", "OP_ASN_MUL", "OP_ASN_RSH", "OP_ASN_SUB", 
  "OP_BAND", "OP_BNOT", "OP_BOR", "OP_BXOR", "OP_DIV", "OP_EQUAL", "OP_GEQUAL", 
  "OP_GREATER", "OP_LAND", "OP_LEQUAL", "OP_LESS", "OP_LNOT", "OP_LOR", 
  "OP_LSHIFT", "OP_MOD", "OP_MUL", "OP_NEQUAL", "OP_RSHIFT", "OP_SUB", "WS", 
  "COMMENT", "LINE_COMMENT"
};

dfa::Vocabulary VSL::_vocabulary(_literalNames, _symbolicNames);

std::vector<std::string> VSL::_tokenNames;

VSL::Initializer::Initializer() {
	for (size_t i = 0; i < _symbolicNames.size(); ++i) {
		std::string name = _vocabulary.getLiteralName(i);
		if (name.empty()) {
			name = _vocabulary.getSymbolicName(i);
		}

		if (name.empty()) {
			_tokenNames.push_back("<INVALID>");
		} else {
      _tokenNames.push_back(name);
    }
	}

  static const uint16_t serializedATNSegment0[] = {
    0x3, 0x608b, 0xa72a, 0x8133, 0xb9ed, 0x417c, 0x3be7, 0x7786, 0x5964, 
       0x3, 0x47, 0x16f, 0x4, 0x2, 0x9, 0x2, 0x4, 0x3, 0x9, 0x3, 0x4, 0x4, 
       0x9, 0x4, 0x4, 0x5, 0x9, 0x5, 0x4, 0x6, 0x9, 0x6, 0x4, 0x7, 0x9, 
       0x7, 0x4, 0x8, 0x9, 0x8, 0x4, 0x9, 0x9, 0x9, 0x4, 0xa, 0x9, 0xa, 
       0x4, 0xb, 0x9, 0xb, 0x4, 0xc, 0x9, 0xc, 0x4, 0xd, 0x9, 0xd, 0x4, 
       0xe, 0x9, 0xe, 0x4, 0xf, 0x9, 0xf, 0x4, 0x10, 0x9, 0x10, 0x4, 0x11, 
       0x9, 0x11, 0x4, 0x12, 0x9, 0x12, 0x4, 0x13, 0x9, 0x13, 0x4, 0x14, 
       0x9, 0x14, 0x4, 0x15, 0x9, 0x15, 0x4, 0x16, 0x9, 0x16, 0x4, 0x17, 
       0x9, 0x17, 0x4, 0x18, 0x9, 0x18, 0x4, 0x19, 0x9, 0x19, 0x4, 0x1a, 
       0x9, 0x1a, 0x4, 0x1b, 0x9, 0x1b, 0x4, 0x1c, 0x9, 0x1c, 0x3, 0x2, 
       0x3, 0x2, 0x7, 0x2, 0x3b, 0xa, 0x2, 0xc, 0x2, 0xe, 0x2, 0x3e, 0xb, 
       0x2, 0x3, 0x2, 0x3, 0x2, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 
       0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x5, 
       0x4, 0x4c, 0xa, 0x4, 0x3, 0x5, 0x3, 0x5, 0x3, 0x5, 0x3, 0x5, 0x3, 
       0x5, 0x3, 0x5, 0x6, 0x5, 0x54, 0xa, 0x5, 0xd, 0x5, 0xe, 0x5, 0x55, 
       0x3, 0x5, 0x3, 0x5, 0x3, 0x5, 0x3, 0x6, 0x3, 0x6, 0x3, 0x6, 0x3, 
       0x6, 0x3, 0x6, 0x3, 0x6, 0x6, 0x6, 0x61, 0xa, 0x6, 0xd, 0x6, 0xe, 
       0x6, 0x62, 0x3, 0x6, 0x3, 0x6, 0x3, 0x6, 0x3, 0x7, 0x3, 0x7, 0x3, 
       0x7, 0x3, 0x7, 0x3, 0x7, 0x3, 0x7, 0x3, 0x7, 0x3, 0x8, 0x3, 0x8, 
       0x3, 0x8, 0x3, 0x8, 0x3, 0x8, 0x5, 0x8, 0x74, 0xa, 0x8, 0x3, 0x8, 
       0x3, 0x8, 0x3, 0x8, 0x3, 0x9, 0x3, 0x9, 0x3, 0x9, 0x3, 0x9, 0x3, 
       0x9, 0x3, 0x9, 0x3, 0x9, 0x3, 0x9, 0x3, 0xa, 0x3, 0xa, 0x3, 0xa, 
       0x7, 0xa, 0x84, 0xa, 0xa, 0xc, 0xa, 0xe, 0xa, 0x87, 0xb, 0xa, 0x3, 
       0xa, 0x3, 0xa, 0x3, 0xa, 0x3, 0xa, 0x3, 0xa, 0x7, 0xa, 0x8e, 0xa, 
       0xa, 0xc, 0xa, 0xe, 0xa, 0x91, 0xb, 0xa, 0x3, 0xa, 0x3, 0xa, 0x5, 
       0xa, 0x95, 0xa, 0xa, 0x3, 0xb, 0x3, 0xb, 0x3, 0xb, 0x3, 0xb, 0x3, 
       0xb, 0x3, 0xb, 0x3, 0xc, 0x3, 0xc, 0x3, 0xc, 0x5, 0xc, 0xa0, 0xa, 
       0xc, 0x3, 0xc, 0x3, 0xc, 0x3, 0xc, 0x3, 0xc, 0x3, 0xd, 0x3, 0xd, 
       0x5, 0xd, 0xa8, 0xa, 0xd, 0x3, 0xd, 0x3, 0xd, 0x3, 0xd, 0x3, 0xd, 
       0x5, 0xd, 0xae, 0xa, 0xd, 0x3, 0xd, 0x5, 0xd, 0xb1, 0xa, 0xd, 0x3, 
       0xe, 0x3, 0xe, 0x3, 0xe, 0x3, 0xe, 0x3, 0xe, 0x3, 0xe, 0x3, 0xe, 
       0x3, 0xe, 0x3, 0xe, 0x3, 0xe, 0x3, 0xe, 0x3, 0xe, 0x3, 0xe, 0x3, 
       0xe, 0x5, 0xe, 0xc1, 0xa, 0xe, 0x3, 0xf, 0x3, 0xf, 0x7, 0xf, 0xc5, 
       0xa, 0xf, 0xc, 0xf, 0xe, 0xf, 0xc8, 0xb, 0xf, 0x3, 0xf, 0x3, 0xf, 
       0x3, 0x10, 0x3, 0x10, 0x3, 0x10, 0x3, 0x10, 0x5, 0x10, 0xd0, 0xa, 
       0x10, 0x3, 0x10, 0x3, 0x10, 0x3, 0x10, 0x3, 0x10, 0x5, 0x10, 0xd6, 
       0xa, 0x10, 0x3, 0x11, 0x3, 0x11, 0x3, 0x11, 0x3, 0x11, 0x3, 0x12, 
       0x3, 0x12, 0x3, 0x12, 0x3, 0x12, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 
       0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 
       0x3, 0x13, 0x3, 0x13, 0x7, 0x13, 0xeb, 0xa, 0x13, 0xc, 0x13, 0xe, 
       0x13, 0xee, 0xb, 0x13, 0x3, 0x14, 0x3, 0x14, 0x3, 0x14, 0x3, 0x14, 
       0x3, 0x14, 0x3, 0x14, 0x5, 0x14, 0xf6, 0xa, 0x14, 0x3, 0x14, 0x7, 
       0x14, 0xf9, 0xa, 0x14, 0xc, 0x14, 0xe, 0x14, 0xfc, 0xb, 0x14, 0x3, 
       0x14, 0x5, 0x14, 0xff, 0xa, 0x14, 0x3, 0x15, 0x3, 0x15, 0x3, 0x15, 
       0x3, 0x15, 0x3, 0x15, 0x3, 0x15, 0x5, 0x15, 0x107, 0xa, 0x15, 0x3, 
       0x16, 0x3, 0x16, 0x3, 0x16, 0x5, 0x16, 0x10c, 0xa, 0x16, 0x3, 0x17, 
       0x3, 0x17, 0x3, 0x17, 0x3, 0x17, 0x3, 0x17, 0x3, 0x17, 0x3, 0x17, 
       0x3, 0x17, 0x3, 0x17, 0x5, 0x17, 0x117, 0xa, 0x17, 0x3, 0x17, 0x3, 
       0x17, 0x3, 0x17, 0x3, 0x18, 0x3, 0x18, 0x3, 0x19, 0x3, 0x19, 0x3, 
       0x19, 0x3, 0x19, 0x3, 0x19, 0x3, 0x19, 0x5, 0x19, 0x124, 0xa, 0x19, 
       0x3, 0x19, 0x3, 0x19, 0x3, 0x19, 0x3, 0x19, 0x3, 0x19, 0x3, 0x19, 
       0x3, 0x19, 0x3, 0x19, 0x3, 0x19, 0x3, 0x19, 0x3, 0x19, 0x3, 0x19, 
       0x3, 0x19, 0x3, 0x19, 0x3, 0x19, 0x3, 0x19, 0x3, 0x19, 0x3, 0x19, 
       0x3, 0x19, 0x3, 0x19, 0x3, 0x19, 0x3, 0x19, 0x3, 0x19, 0x3, 0x19, 
       0x3, 0x19, 0x3, 0x19, 0x3, 0x19, 0x7, 0x19, 0x141, 0xa, 0x19, 0xc, 
       0x19, 0xe, 0x19, 0x144, 0xb, 0x19, 0x3, 0x1a, 0x3, 0x1a, 0x3, 0x1a, 
       0x3, 0x1a, 0x3, 0x1a, 0x3, 0x1a, 0x3, 0x1a, 0x3, 0x1a, 0x5, 0x1a, 
       0x14e, 0xa, 0x1a, 0x3, 0x1a, 0x3, 0x1a, 0x3, 0x1a, 0x3, 0x1a, 0x3, 
       0x1a, 0x5, 0x1a, 0x155, 0xa, 0x1a, 0x3, 0x1a, 0x3, 0x1a, 0x3, 0x1a, 
       0x3, 0x1a, 0x3, 0x1a, 0x7, 0x1a, 0x15c, 0xa, 0x1a, 0xc, 0x1a, 0xe, 
       0x1a, 0x15f, 0xb, 0x1a, 0x3, 0x1b, 0x3, 0x1b, 0x3, 0x1b, 0x3, 0x1b, 
       0x3, 0x1b, 0x7, 0x1b, 0x166, 0xa, 0x1b, 0xc, 0x1b, 0xe, 0x1b, 0x169, 
       0xb, 0x1b, 0x3, 0x1b, 0x3, 0x1b, 0x3, 0x1c, 0x3, 0x1c, 0x3, 0x1c, 
       0x2, 0x5, 0x24, 0x30, 0x32, 0x1d, 0x2, 0x4, 0x6, 0x8, 0xa, 0xc, 0xe, 
       0x10, 0x12, 0x14, 0x16, 0x18, 0x1a, 0x1c, 0x1e, 0x20, 0x22, 0x24, 
       0x26, 0x28, 0x2a, 0x2c, 0x2e, 0x30, 0x32, 0x34, 0x36, 0x2, 0xd, 0x3, 
       0x2, 0x27, 0x31, 0x4, 0x2, 0x5, 0x7, 0x10, 0x10, 0x4, 0x2, 0x26, 
       0x26, 0x44, 0x44, 0x4, 0x2, 0x33, 0x33, 0x3d, 0x3d, 0x4, 0x2, 0x36, 
       0x36, 0x40, 0x41, 0x4, 0x2, 0x3f, 0x3f, 0x43, 0x43, 0x4, 0x2, 0x38, 
       0x39, 0x3b, 0x3c, 0x4, 0x2, 0x37, 0x37, 0x42, 0x42, 0x4, 0x2, 0x32, 
       0x32, 0x34, 0x35, 0x4, 0x2, 0x3a, 0x3a, 0x3e, 0x3e, 0x4, 0x2, 0x3, 
       0x3, 0x16, 0x17, 0x2, 0x184, 0x2, 0x38, 0x3, 0x2, 0x2, 0x2, 0x4, 
       0x41, 0x3, 0x2, 0x2, 0x2, 0x6, 0x4b, 0x3, 0x2, 0x2, 0x2, 0x8, 0x4d, 
       0x3, 0x2, 0x2, 0x2, 0xa, 0x5a, 0x3, 0x2, 0x2, 0x2, 0xc, 0x67, 0x3, 
       0x2, 0x2, 0x2, 0xe, 0x6e, 0x3, 0x2, 0x2, 0x2, 0x10, 0x78, 0x3, 0x2, 
       0x2, 0x2, 0x12, 0x94, 0x3, 0x2, 0x2, 0x2, 0x14, 0x96, 0x3, 0x2, 0x2, 
       0x2, 0x16, 0x9c, 0x3, 0x2, 0x2, 0x2, 0x18, 0xa5, 0x3, 0x2, 0x2, 0x2, 
       0x1a, 0xc0, 0x3, 0x2, 0x2, 0x2, 0x1c, 0xc2, 0x3, 0x2, 0x2, 0x2, 0x1e, 
       0xcb, 0x3, 0x2, 0x2, 0x2, 0x20, 0xd7, 0x3, 0x2, 0x2, 0x2, 0x22, 0xdb, 
       0x3, 0x2, 0x2, 0x2, 0x24, 0xdf, 0x3, 0x2, 0x2, 0x2, 0x26, 0xef, 0x3, 
       0x2, 0x2, 0x2, 0x28, 0x100, 0x3, 0x2, 0x2, 0x2, 0x2a, 0x108, 0x3, 
       0x2, 0x2, 0x2, 0x2c, 0x10d, 0x3, 0x2, 0x2, 0x2, 0x2e, 0x11b, 0x3, 
       0x2, 0x2, 0x2, 0x30, 0x123, 0x3, 0x2, 0x2, 0x2, 0x32, 0x14d, 0x3, 
       0x2, 0x2, 0x2, 0x34, 0x160, 0x3, 0x2, 0x2, 0x2, 0x36, 0x16c, 0x3, 
       0x2, 0x2, 0x2, 0x38, 0x3c, 0x5, 0x4, 0x3, 0x2, 0x39, 0x3b, 0x5, 0x6, 
       0x4, 0x2, 0x3a, 0x39, 0x3, 0x2, 0x2, 0x2, 0x3b, 0x3e, 0x3, 0x2, 0x2, 
       0x2, 0x3c, 0x3a, 0x3, 0x2, 0x2, 0x2, 0x3c, 0x3d, 0x3, 0x2, 0x2, 0x2, 
       0x3d, 0x3f, 0x3, 0x2, 0x2, 0x2, 0x3e, 0x3c, 0x3, 0x2, 0x2, 0x2, 0x3f, 
       0x40, 0x7, 0x2, 0x2, 0x3, 0x40, 0x3, 0x3, 0x2, 0x2, 0x2, 0x41, 0x42, 
       0x7, 0x11, 0x2, 0x2, 0x42, 0x43, 0x7, 0x19, 0x2, 0x2, 0x43, 0x44, 
       0x7, 0x25, 0x2, 0x2, 0x44, 0x5, 0x3, 0x2, 0x2, 0x2, 0x45, 0x4c, 0x5, 
       0x8, 0x5, 0x2, 0x46, 0x4c, 0x5, 0xa, 0x6, 0x2, 0x47, 0x4c, 0x5, 0xc, 
       0x7, 0x2, 0x48, 0x4c, 0x5, 0xe, 0x8, 0x2, 0x49, 0x4c, 0x5, 0x10, 
       0x9, 0x2, 0x4a, 0x4c, 0x5, 0x12, 0xa, 0x2, 0x4b, 0x45, 0x3, 0x2, 
       0x2, 0x2, 0x4b, 0x46, 0x3, 0x2, 0x2, 0x2, 0x4b, 0x47, 0x3, 0x2, 0x2, 
       0x2, 0x4b, 0x48, 0x3, 0x2, 0x2, 0x2, 0x4b, 0x49, 0x3, 0x2, 0x2, 0x2, 
       0x4b, 0x4a, 0x3, 0x2, 0x2, 0x2, 0x4c, 0x7, 0x3, 0x2, 0x2, 0x2, 0x4d, 
       0x4e, 0x7, 0x13, 0x2, 0x2, 0x4e, 0x4f, 0x7, 0x19, 0x2, 0x2, 0x4f, 
       0x53, 0x7, 0x1d, 0x2, 0x2, 0x50, 0x51, 0x5, 0x1e, 0x10, 0x2, 0x51, 
       0x52, 0x7, 0x25, 0x2, 0x2, 0x52, 0x54, 0x3, 0x2, 0x2, 0x2, 0x53, 
       0x50, 0x3, 0x2, 0x2, 0x2, 0x54, 0x55, 0x3, 0x2, 0x2, 0x2, 0x55, 0x53, 
       0x3, 0x2, 0x2, 0x2, 0x55, 0x56, 0x3, 0x2, 0x2, 0x2, 0x56, 0x57, 0x3, 
       0x2, 0x2, 0x2, 0x57, 0x58, 0x7, 0x22, 0x2, 0x2, 0x58, 0x59, 0x7, 
       0x25, 0x2, 0x2, 0x59, 0x9, 0x3, 0x2, 0x2, 0x2, 0x5a, 0x5b, 0x7, 0x14, 
       0x2, 0x2, 0x5b, 0x5c, 0x7, 0x19, 0x2, 0x2, 0x5c, 0x60, 0x7, 0x1d, 
       0x2, 0x2, 0x5d, 0x5e, 0x5, 0x1e, 0x10, 0x2, 0x5e, 0x5f, 0x7, 0x25, 
       0x2, 0x2, 0x5f, 0x61, 0x3, 0x2, 0x2, 0x2, 0x60, 0x5d, 0x3, 0x2, 0x2, 
       0x2, 0x61, 0x62, 0x3, 0x2, 0x2, 0x2, 0x62, 0x60, 0x3, 0x2, 0x2, 0x2, 
       0x62, 0x63, 0x3, 0x2, 0x2, 0x2, 0x63, 0x64, 0x3, 0x2, 0x2, 0x2, 0x64, 
       0x65, 0x7, 0x22, 0x2, 0x2, 0x65, 0x66, 0x7, 0x25, 0x2, 0x2, 0x66, 
       0xb, 0x3, 0x2, 0x2, 0x2, 0x67, 0x68, 0x7, 0x4, 0x2, 0x2, 0x68, 0x69, 
       0x7, 0x1f, 0x2, 0x2, 0x69, 0x6a, 0x7, 0x16, 0x2, 0x2, 0x6a, 0x6b, 
       0x7, 0x24, 0x2, 0x2, 0x6b, 0x6c, 0x5, 0x1e, 0x10, 0x2, 0x6c, 0x6d, 
       0x7, 0x25, 0x2, 0x2, 0x6d, 0xd, 0x3, 0x2, 0x2, 0x2, 0x6e, 0x6f, 0x7, 
       0xf, 0x2, 0x2, 0x6f, 0x70, 0x7, 0x1f, 0x2, 0x2, 0x70, 0x71, 0x7, 
       0x19, 0x2, 0x2, 0x71, 0x73, 0x7, 0x24, 0x2, 0x2, 0x72, 0x74, 0x7, 
       0xa, 0x2, 0x2, 0x73, 0x72, 0x3, 0x2, 0x2, 0x2, 0x73, 0x74, 0x3, 0x2, 
       0x2, 0x2, 0x74, 0x75, 0x3, 0x2, 0x2, 0x2, 0x75, 0x76, 0x5, 0x1e, 
       0x10, 0x2, 0x76, 0x77, 0x7, 0x25, 0x2, 0x2, 0x77, 0xf, 0x3, 0x2, 
       0x2, 0x2, 0x78, 0x79, 0x7, 0x12, 0x2, 0x2, 0x79, 0x7a, 0x7, 0x1f, 
       0x2, 0x2, 0x7a, 0x7b, 0x7, 0x16, 0x2, 0x2, 0x7b, 0x7c, 0x7, 0x24, 
       0x2, 0x2, 0x7c, 0x7d, 0x7, 0x19, 0x2, 0x2, 0x7d, 0x7e, 0x7, 0x19, 
       0x2, 0x2, 0x7e, 0x7f, 0x7, 0x25, 0x2, 0x2, 0x7f, 0x11, 0x3, 0x2, 
       0x2, 0x2, 0x80, 0x81, 0x7, 0x15, 0x2, 0x2, 0x81, 0x85, 0x7, 0x1f, 
       0x2, 0x2, 0x82, 0x84, 0x5, 0x14, 0xb, 0x2, 0x83, 0x82, 0x3, 0x2, 
       0x2, 0x2, 0x84, 0x87, 0x3, 0x2, 0x2, 0x2, 0x85, 0x83, 0x3, 0x2, 0x2, 
       0x2, 0x85, 0x86, 0x3, 0x2, 0x2, 0x2, 0x86, 0x88, 0x3, 0x2, 0x2, 0x2, 
       0x87, 0x85, 0x3, 0x2, 0x2, 0x2, 0x88, 0x89, 0x7, 0x24, 0x2, 0x2, 
       0x89, 0x95, 0x5, 0x1c, 0xf, 0x2, 0x8a, 0x8b, 0x7, 0xc, 0x2, 0x2, 
       0x8b, 0x8f, 0x7, 0x1f, 0x2, 0x2, 0x8c, 0x8e, 0x5, 0x16, 0xc, 0x2, 
       0x8d, 0x8c, 0x3, 0x2, 0x2, 0x2, 0x8e, 0x91, 0x3, 0x2, 0x2, 0x2, 0x8f, 
       0x8d, 0x3, 0x2, 0x2, 0x2, 0x8f, 0x90, 0x3, 0x2, 0x2, 0x2, 0x90, 0x92, 
       0x3, 0x2, 0x2, 0x2, 0x91, 0x8f, 0x3, 0x2, 0x2, 0x2, 0x92, 0x93, 0x7, 
       0x24, 0x2, 0x2, 0x93, 0x95, 0x5, 0x1c, 0xf, 0x2, 0x94, 0x80, 0x3, 
       0x2, 0x2, 0x2, 0x94, 0x8a, 0x3, 0x2, 0x2, 0x2, 0x95, 0x13, 0x3, 0x2, 
       0x2, 0x2, 0x96, 0x97, 0x7, 0x1e, 0x2, 0x2, 0x97, 0x98, 0x7, 0x18, 
       0x2, 0x2, 0x98, 0x99, 0x7, 0x23, 0x2, 0x2, 0x99, 0x9a, 0x5, 0x1e, 
       0x10, 0x2, 0x9a, 0x9b, 0x7, 0x25, 0x2, 0x2, 0x9b, 0x15, 0x3, 0x2, 
       0x2, 0x2, 0x9c, 0x9f, 0x7, 0x1e, 0x2, 0x2, 0x9d, 0xa0, 0x7, 0x19, 
       0x2, 0x2, 0x9e, 0xa0, 0x5, 0x18, 0xd, 0x2, 0x9f, 0x9d, 0x3, 0x2, 
       0x2, 0x2, 0x9f, 0x9e, 0x3, 0x2, 0x2, 0x2, 0xa0, 0xa1, 0x3, 0x2, 0x2, 
       0x2, 0xa1, 0xa2, 0x7, 0x23, 0x2, 0x2, 0xa2, 0xa3, 0x5, 0x1e, 0x10, 
       0x2, 0xa3, 0xa4, 0x7, 0x25, 0x2, 0x2, 0xa4, 0x17, 0x3, 0x2, 0x2, 
       0x2, 0xa5, 0xa7, 0x7, 0x19, 0x2, 0x2, 0xa6, 0xa8, 0x7, 0x19, 0x2, 
       0x2, 0xa7, 0xa6, 0x3, 0x2, 0x2, 0x2, 0xa7, 0xa8, 0x3, 0x2, 0x2, 0x2, 
       0xa8, 0xa9, 0x3, 0x2, 0x2, 0x2, 0xa9, 0xb0, 0x7, 0x19, 0x2, 0x2, 
       0xaa, 0xab, 0x7, 0x1c, 0x2, 0x2, 0xab, 0xad, 0x7, 0x19, 0x2, 0x2, 
       0xac, 0xae, 0x7, 0x19, 0x2, 0x2, 0xad, 0xac, 0x3, 0x2, 0x2, 0x2, 
       0xad, 0xae, 0x3, 0x2, 0x2, 0x2, 0xae, 0xaf, 0x3, 0x2, 0x2, 0x2, 0xaf, 
       0xb1, 0x7, 0x19, 0x2, 0x2, 0xb0, 0xaa, 0x3, 0x2, 0x2, 0x2, 0xb0, 
       0xb1, 0x3, 0x2, 0x2, 0x2, 0xb1, 0x19, 0x3, 0x2, 0x2, 0x2, 0xb2, 0xb3, 
       0x5, 0x20, 0x11, 0x2, 0xb3, 0xb4, 0x7, 0x25, 0x2, 0x2, 0xb4, 0xc1, 
       0x3, 0x2, 0x2, 0x2, 0xb5, 0xb6, 0x5, 0x1e, 0x10, 0x2, 0xb6, 0xb7, 
       0x7, 0x25, 0x2, 0x2, 0xb7, 0xc1, 0x3, 0x2, 0x2, 0x2, 0xb8, 0xb9, 
       0x5, 0x22, 0x12, 0x2, 0xb9, 0xba, 0x7, 0x25, 0x2, 0x2, 0xba, 0xc1, 
       0x3, 0x2, 0x2, 0x2, 0xbb, 0xc1, 0x5, 0x26, 0x14, 0x2, 0xbc, 0xc1, 
       0x5, 0x2c, 0x17, 0x2, 0xbd, 0xbe, 0x5, 0x2e, 0x18, 0x2, 0xbe, 0xbf, 
       0x7, 0x25, 0x2, 0x2, 0xbf, 0xc1, 0x3, 0x2, 0x2, 0x2, 0xc0, 0xb2, 
       0x3, 0x2, 0x2, 0x2, 0xc0, 0xb5, 0x3, 0x2, 0x2, 0x2, 0xc0, 0xb8, 0x3, 
       0x2, 0x2, 0x2, 0xc0, 0xbb, 0x3, 0x2, 0x2, 0x2, 0xc0, 0xbc, 0x3, 0x2, 
       0x2, 0x2, 0xc0, 0xbd, 0x3, 0x2, 0x2, 0x2, 0xc1, 0x1b, 0x3, 0x2, 0x2, 
       0x2, 0xc2, 0xc6, 0x7, 0x1d, 0x2, 0x2, 0xc3, 0xc5, 0x5, 0x1a, 0xe, 
       0x2, 0xc4, 0xc3, 0x3, 0x2, 0x2, 0x2, 0xc5, 0xc8, 0x3, 0x2, 0x2, 0x2, 
       0xc6, 0xc4, 0x3, 0x2, 0x2, 0x2, 0xc6, 0xc7, 0x3, 0x2, 0x2, 0x2, 0xc7, 
       0xc9, 0x3, 0x2, 0x2, 0x2, 0xc8, 0xc6, 0x3, 0x2, 0x2, 0x2, 0xc9, 0xca, 
       0x7, 0x22, 0x2, 0x2, 0xca, 0x1d, 0x3, 0x2, 0x2, 0x2, 0xcb, 0xcf, 
       0x7, 0x19, 0x2, 0x2, 0xcc, 0xcd, 0x7, 0x3c, 0x2, 0x2, 0xcd, 0xce, 
       0x7, 0x19, 0x2, 0x2, 0xce, 0xd0, 0x7, 0x39, 0x2, 0x2, 0xcf, 0xcc, 
       0x3, 0x2, 0x2, 0x2, 0xcf, 0xd0, 0x3, 0x2, 0x2, 0x2, 0xd0, 0xd1, 0x3, 
       0x2, 0x2, 0x2, 0xd1, 0xd5, 0x7, 0x19, 0x2, 0x2, 0xd2, 0xd3, 0x7, 
       0x1e, 0x2, 0x2, 0xd3, 0xd4, 0x7, 0x16, 0x2, 0x2, 0xd4, 0xd6, 0x7, 
       0x23, 0x2, 0x2, 0xd5, 0xd2, 0x3, 0x2, 0x2, 0x2, 0xd5, 0xd6, 0x3, 
       0x2, 0x2, 0x2, 0xd6, 0x1f, 0x3, 0x2, 0x2, 0x2, 0xd7, 0xd8, 0x5, 0x1e, 
       0x10, 0x2, 0xd8, 0xd9, 0x7, 0x27, 0x2, 0x2, 0xd9, 0xda, 0x5, 0x30, 
       0x19, 0x2, 0xda, 0x21, 0x3, 0x2, 0x2, 0x2, 0xdb, 0xdc, 0x5, 0x24, 
       0x13, 0x2, 0xdc, 0xdd, 0x9, 0x2, 0x2, 0x2, 0xdd, 0xde, 0x5, 0x30, 
       0x19, 0x2, 0xde, 0x23, 0x3, 0x2, 0x2, 0x2, 0xdf, 0xe0, 0x8, 0x13, 
       0x1, 0x2, 0xe0, 0xe1, 0x7, 0x19, 0x2, 0x2, 0xe1, 0xec, 0x3, 0x2, 
       0x2, 0x2, 0xe2, 0xe3, 0xc, 0x4, 0x2, 0x2, 0xe3, 0xe4, 0x7, 0x1e, 
       0x2, 0x2, 0xe4, 0xe5, 0x5, 0x30, 0x19, 0x2, 0xe5, 0xe6, 0x7, 0x23, 
       0x2, 0x2, 0xe6, 0xeb, 0x3, 0x2, 0x2, 0x2, 0xe7, 0xe8, 0xc, 0x3, 0x2, 
       0x2, 0xe8, 0xe9, 0x7, 0x20, 0x2, 0x2, 0xe9, 0xeb, 0x7, 0x19, 0x2, 
       0x2, 0xea, 0xe2, 0x3, 0x2, 0x2, 0x2, 0xea, 0xe7, 0x3, 0x2, 0x2, 0x2, 
       0xeb, 0xee, 0x3, 0x2, 0x2, 0x2, 0xec, 0xea, 0x3, 0x2, 0x2, 0x2, 0xec, 
       0xed, 0x3, 0x2, 0x2, 0x2, 0xed, 0x25, 0x3, 0x2, 0x2, 0x2, 0xee, 0xec, 
       0x3, 0x2, 0x2, 0x2, 0xef, 0xf0, 0x7, 0xd, 0x2, 0x2, 0xf0, 0xf1, 0x7, 
       0x1f, 0x2, 0x2, 0xf1, 0xf2, 0x5, 0x30, 0x19, 0x2, 0xf2, 0xf5, 0x7, 
       0x24, 0x2, 0x2, 0xf3, 0xf6, 0x5, 0x1a, 0xe, 0x2, 0xf4, 0xf6, 0x5, 
       0x1c, 0xf, 0x2, 0xf5, 0xf3, 0x3, 0x2, 0x2, 0x2, 0xf5, 0xf4, 0x3, 
       0x2, 0x2, 0x2, 0xf6, 0xfa, 0x3, 0x2, 0x2, 0x2, 0xf7, 0xf9, 0x5, 0x28, 
       0x15, 0x2, 0xf8, 0xf7, 0x3, 0x2, 0x2, 0x2, 0xf9, 0xfc, 0x3, 0x2, 
       0x2, 0x2, 0xfa, 0xf8, 0x3, 0x2, 0x2, 0x2, 0xfa, 0xfb, 0x3, 0x2, 0x2, 
       0x2, 0xfb, 0xfe, 0x3, 0x2, 0x2, 0x2, 0xfc, 0xfa, 0x3, 0x2, 0x2, 0x2, 
       0xfd, 0xff, 0x5, 0x2a, 0x16, 0x2, 0xfe, 0xfd, 0x3, 0x2, 0x2, 0x2, 
       0xfe, 0xff, 0x3, 0x2, 0x2, 0x2, 0xff, 0x27, 0x3, 0x2, 0x2, 0x2, 0x100, 
       0x101, 0x7, 0x8, 0x2, 0x2, 0x101, 0x102, 0x7, 0x1f, 0x2, 0x2, 0x102, 
       0x103, 0x5, 0x30, 0x19, 0x2, 0x103, 0x106, 0x7, 0x24, 0x2, 0x2, 0x104, 
       0x107, 0x5, 0x1a, 0xe, 0x2, 0x105, 0x107, 0x5, 0x1c, 0xf, 0x2, 0x106, 
       0x104, 0x3, 0x2, 0x2, 0x2, 0x106, 0x105, 0x3, 0x2, 0x2, 0x2, 0x107, 
       0x29, 0x3, 0x2, 0x2, 0x2, 0x108, 0x10b, 0x7, 0x9, 0x2, 0x2, 0x109, 
       0x10c, 0x5, 0x1a, 0xe, 0x2, 0x10a, 0x10c, 0x5, 0x1c, 0xf, 0x2, 0x10b, 
       0x109, 0x3, 0x2, 0x2, 0x2, 0x10b, 0x10a, 0x3, 0x2, 0x2, 0x2, 0x10c, 
       0x2b, 0x3, 0x2, 0x2, 0x2, 0x10d, 0x10e, 0x7, 0xb, 0x2, 0x2, 0x10e, 
       0x10f, 0x7, 0x1f, 0x2, 0x2, 0x10f, 0x110, 0x7, 0x19, 0x2, 0x2, 0x110, 
       0x111, 0x7, 0x25, 0x2, 0x2, 0x111, 0x112, 0x7, 0x16, 0x2, 0x2, 0x112, 
       0x113, 0x7, 0x1b, 0x2, 0x2, 0x113, 0x116, 0x7, 0x16, 0x2, 0x2, 0x114, 
       0x115, 0x7, 0x1b, 0x2, 0x2, 0x115, 0x117, 0x7, 0x16, 0x2, 0x2, 0x116, 
       0x114, 0x3, 0x2, 0x2, 0x2, 0x116, 0x117, 0x3, 0x2, 0x2, 0x2, 0x117, 
       0x118, 0x3, 0x2, 0x2, 0x2, 0x118, 0x119, 0x7, 0x24, 0x2, 0x2, 0x119, 
       0x11a, 0x5, 0x1c, 0xf, 0x2, 0x11a, 0x2d, 0x3, 0x2, 0x2, 0x2, 0x11b, 
       0x11c, 0x9, 0x3, 0x2, 0x2, 0x11c, 0x2f, 0x3, 0x2, 0x2, 0x2, 0x11d, 
       0x11e, 0x8, 0x19, 0x1, 0x2, 0x11e, 0x124, 0x5, 0x32, 0x1a, 0x2, 0x11f, 
       0x120, 0x9, 0x4, 0x2, 0x2, 0x120, 0x124, 0x5, 0x30, 0x19, 0xc, 0x121, 
       0x122, 0x9, 0x5, 0x2, 0x2, 0x122, 0x124, 0x5, 0x30, 0x19, 0xb, 0x123, 
       0x11d, 0x3, 0x2, 0x2, 0x2, 0x123, 0x11f, 0x3, 0x2, 0x2, 0x2, 0x123, 
       0x121, 0x3, 0x2, 0x2, 0x2, 0x124, 0x142, 0x3, 0x2, 0x2, 0x2, 0x125, 
       0x126, 0xc, 0xa, 0x2, 0x2, 0x126, 0x127, 0x9, 0x6, 0x2, 0x2, 0x127, 
       0x141, 0x5, 0x30, 0x19, 0xb, 0x128, 0x129, 0xc, 0x9, 0x2, 0x2, 0x129, 
       0x12a, 0x9, 0x4, 0x2, 0x2, 0x12a, 0x141, 0x5, 0x30, 0x19, 0xa, 0x12b, 
       0x12c, 0xc, 0x8, 0x2, 0x2, 0x12c, 0x12d, 0x9, 0x7, 0x2, 0x2, 0x12d, 
       0x141, 0x5, 0x30, 0x19, 0x9, 0x12e, 0x12f, 0xc, 0x7, 0x2, 0x2, 0x12f, 
       0x130, 0x9, 0x8, 0x2, 0x2, 0x130, 0x141, 0x5, 0x30, 0x19, 0x8, 0x131, 
       0x132, 0xc, 0x6, 0x2, 0x2, 0x132, 0x133, 0x9, 0x9, 0x2, 0x2, 0x133, 
       0x141, 0x5, 0x30, 0x19, 0x7, 0x134, 0x135, 0xc, 0x5, 0x2, 0x2, 0x135, 
       0x136, 0x9, 0xa, 0x2, 0x2, 0x136, 0x141, 0x5, 0x30, 0x19, 0x6, 0x137, 
       0x138, 0xc, 0x4, 0x2, 0x2, 0x138, 0x139, 0x9, 0xb, 0x2, 0x2, 0x139, 
       0x141, 0x5, 0x30, 0x19, 0x5, 0x13a, 0x13b, 0xc, 0x3, 0x2, 0x2, 0x13b, 
       0x13c, 0x7, 0x21, 0x2, 0x2, 0x13c, 0x13d, 0x5, 0x30, 0x19, 0x2, 0x13d, 
       0x13e, 0x7, 0x1b, 0x2, 0x2, 0x13e, 0x13f, 0x5, 0x30, 0x19, 0x4, 0x13f, 
       0x141, 0x3, 0x2, 0x2, 0x2, 0x140, 0x125, 0x3, 0x2, 0x2, 0x2, 0x140, 
       0x128, 0x3, 0x2, 0x2, 0x2, 0x140, 0x12b, 0x3, 0x2, 0x2, 0x2, 0x140, 
       0x12e, 0x3, 0x2, 0x2, 0x2, 0x140, 0x131, 0x3, 0x2, 0x2, 0x2, 0x140, 
       0x134, 0x3, 0x2, 0x2, 0x2, 0x140, 0x137, 0x3, 0x2, 0x2, 0x2, 0x140, 
       0x13a, 0x3, 0x2, 0x2, 0x2, 0x141, 0x144, 0x3, 0x2, 0x2, 0x2, 0x142, 
       0x140, 0x3, 0x2, 0x2, 0x2, 0x142, 0x143, 0x3, 0x2, 0x2, 0x2, 0x143, 
       0x31, 0x3, 0x2, 0x2, 0x2, 0x144, 0x142, 0x3, 0x2, 0x2, 0x2, 0x145, 
       0x146, 0x8, 0x1a, 0x1, 0x2, 0x146, 0x147, 0x7, 0x1f, 0x2, 0x2, 0x147, 
       0x148, 0x5, 0x30, 0x19, 0x2, 0x148, 0x149, 0x7, 0x24, 0x2, 0x2, 0x149, 
       0x14e, 0x3, 0x2, 0x2, 0x2, 0x14a, 0x14e, 0x5, 0x34, 0x1b, 0x2, 0x14b, 
       0x14e, 0x5, 0x36, 0x1c, 0x2, 0x14c, 0x14e, 0x7, 0x19, 0x2, 0x2, 0x14d, 
       0x145, 0x3, 0x2, 0x2, 0x2, 0x14d, 0x14a, 0x3, 0x2, 0x2, 0x2, 0x14d, 
       0x14b, 0x3, 0x2, 0x2, 0x2, 0x14d, 0x14c, 0x3, 0x2, 0x2, 0x2, 0x14e, 
       0x15d, 0x3, 0x2, 0x2, 0x2, 0x14f, 0x150, 0xc, 0x7, 0x2, 0x2, 0x150, 
       0x151, 0x7, 0x1e, 0x2, 0x2, 0x151, 0x154, 0x5, 0x30, 0x19, 0x2, 0x152, 
       0x153, 0x7, 0x1c, 0x2, 0x2, 0x153, 0x155, 0x5, 0x30, 0x19, 0x2, 0x154, 
       0x152, 0x3, 0x2, 0x2, 0x2, 0x154, 0x155, 0x3, 0x2, 0x2, 0x2, 0x155, 
       0x156, 0x3, 0x2, 0x2, 0x2, 0x156, 0x157, 0x7, 0x23, 0x2, 0x2, 0x157, 
       0x15c, 0x3, 0x2, 0x2, 0x2, 0x158, 0x159, 0xc, 0x6, 0x2, 0x2, 0x159, 
       0x15a, 0x7, 0x20, 0x2, 0x2, 0x15a, 0x15c, 0x7, 0x19, 0x2, 0x2, 0x15b, 
       0x14f, 0x3, 0x2, 0x2, 0x2, 0x15b, 0x158, 0x3, 0x2, 0x2, 0x2, 0x15c, 
       0x15f, 0x3, 0x2, 0x2, 0x2, 0x15d, 0x15b, 0x3, 0x2, 0x2, 0x2, 0x15d, 
       0x15e, 0x3, 0x2, 0x2, 0x2, 0x15e, 0x33, 0x3, 0x2, 0x2, 0x2, 0x15f, 
       0x15d, 0x3, 0x2, 0x2, 0x2, 0x160, 0x161, 0x7, 0x19, 0x2, 0x2, 0x161, 
       0x162, 0x7, 0x1f, 0x2, 0x2, 0x162, 0x167, 0x5, 0x30, 0x19, 0x2, 0x163, 
       0x164, 0x7, 0x1c, 0x2, 0x2, 0x164, 0x166, 0x5, 0x30, 0x19, 0x2, 0x165, 
       0x163, 0x3, 0x2, 0x2, 0x2, 0x166, 0x169, 0x3, 0x2, 0x2, 0x2, 0x167, 
       0x165, 0x3, 0x2, 0x2, 0x2, 0x167, 0x168, 0x3, 0x2, 0x2, 0x2, 0x168, 
       0x16a, 0x3, 0x2, 0x2, 0x2, 0x169, 0x167, 0x3, 0x2, 0x2, 0x2, 0x16a, 
       0x16b, 0x7, 0x24, 0x2, 0x2, 0x16b, 0x35, 0x3, 0x2, 0x2, 0x2, 0x16c, 
       0x16d, 0x9, 0xc, 0x2, 0x2, 0x16d, 0x37, 0x3, 0x2, 0x2, 0x2, 0x22, 
       0x3c, 0x4b, 0x55, 0x62, 0x73, 0x85, 0x8f, 0x94, 0x9f, 0xa7, 0xad, 
       0xb0, 0xc0, 0xc6, 0xcf, 0xd5, 0xea, 0xec, 0xf5, 0xfa, 0xfe, 0x106, 
       0x10b, 0x116, 0x123, 0x140, 0x142, 0x14d, 0x154, 0x15b, 0x15d, 0x167, 
  };

  _serializedATN.insert(_serializedATN.end(), serializedATNSegment0,
    serializedATNSegment0 + sizeof(serializedATNSegment0) / sizeof(serializedATNSegment0[0]));


  atn::ATNDeserializer deserializer;
  _atn = deserializer.deserialize(_serializedATN);

  size_t count = _atn.getNumberOfDecisions();
  _decisionToDFA.reserve(count);
  for (size_t i = 0; i < count; i++) { 
    _decisionToDFA.emplace_back(_atn.getDecisionState(i), i);
  }
}

VSL::Initializer VSL::_init;
