
// Generated from VSL.g4 by ANTLR 4.9.2

#pragma once


#include "antlr4-runtime.h"
#include "VSL.h"


namespace grammar {

/**
 * This class defines an abstract visitor for a parse tree
 * produced by VSL.
 */
class  VSLVisitor : public antlr4::tree::AbstractParseTreeVisitor {
public:

  /**
   * Visit parse trees produced by VSL.
   */
    virtual antlrcpp::Any visitFile(VSL::FileContext *context) = 0;

    virtual antlrcpp::Any visitShaderTypeStatement(VSL::ShaderTypeStatementContext *context) = 0;

    virtual antlrcpp::Any visitTopLevelStatement(VSL::TopLevelStatementContext *context) = 0;

    virtual antlrcpp::Any visitShaderStructDefinition(VSL::ShaderStructDefinitionContext *context) = 0;

    virtual antlrcpp::Any visitShaderUniformStatement(VSL::ShaderUniformStatementContext *context) = 0;

    virtual antlrcpp::Any visitShaderBindingStatement(VSL::ShaderBindingStatementContext *context) = 0;

    virtual antlrcpp::Any visitShaderLocalStatement(VSL::ShaderLocalStatementContext *context) = 0;

    virtual antlrcpp::Any visitShaderSubpassInputStatement(VSL::ShaderSubpassInputStatementContext *context) = 0;

    virtual antlrcpp::Any visitVertexFunction(VSL::VertexFunctionContext *context) = 0;

    virtual antlrcpp::Any visitFragmentFunction(VSL::FragmentFunctionContext *context) = 0;

    virtual antlrcpp::Any visitShaderVertexInput(VSL::ShaderVertexInputContext *context) = 0;

    virtual antlrcpp::Any visitShaderFragmentOutput(VSL::ShaderFragmentOutputContext *context) = 0;

    virtual antlrcpp::Any visitBlendStatement(VSL::BlendStatementContext *context) = 0;

    virtual antlrcpp::Any visitStatement(VSL::StatementContext *context) = 0;

    virtual antlrcpp::Any visitStatementBlock(VSL::StatementBlockContext *context) = 0;

    virtual antlrcpp::Any visitVariableDeclaration(VSL::VariableDeclarationContext *context) = 0;

    virtual antlrcpp::Any visitVariableDefinition(VSL::VariableDefinitionContext *context) = 0;

    virtual antlrcpp::Any visitAssignment(VSL::AssignmentContext *context) = 0;

    virtual antlrcpp::Any visitLvalue(VSL::LvalueContext *context) = 0;

    virtual antlrcpp::Any visitIfStatement(VSL::IfStatementContext *context) = 0;

    virtual antlrcpp::Any visitElifStatement(VSL::ElifStatementContext *context) = 0;

    virtual antlrcpp::Any visitElseStatement(VSL::ElseStatementContext *context) = 0;

    virtual antlrcpp::Any visitForLoopStatement(VSL::ForLoopStatementContext *context) = 0;

    virtual antlrcpp::Any visitControlStatement(VSL::ControlStatementContext *context) = 0;

    virtual antlrcpp::Any visitEqualityExpr(VSL::EqualityExprContext *context) = 0;

    virtual antlrcpp::Any visitMulDivModExpr(VSL::MulDivModExprContext *context) = 0;

    virtual antlrcpp::Any visitBitwiseExpr(VSL::BitwiseExprContext *context) = 0;

    virtual antlrcpp::Any visitRelationalExpr(VSL::RelationalExprContext *context) = 0;

    virtual antlrcpp::Any visitAtomExpr(VSL::AtomExprContext *context) = 0;

    virtual antlrcpp::Any visitAddSubExpr(VSL::AddSubExprContext *context) = 0;

    virtual antlrcpp::Any visitShiftExpr(VSL::ShiftExprContext *context) = 0;

    virtual antlrcpp::Any visitLogicalExpr(VSL::LogicalExprContext *context) = 0;

    virtual antlrcpp::Any visitTernaryExpr(VSL::TernaryExprContext *context) = 0;

    virtual antlrcpp::Any visitFactorExpr(VSL::FactorExprContext *context) = 0;

    virtual antlrcpp::Any visitNegateExpr(VSL::NegateExprContext *context) = 0;

    virtual antlrcpp::Any visitMemberAtom(VSL::MemberAtomContext *context) = 0;

    virtual antlrcpp::Any visitIndexAtom(VSL::IndexAtomContext *context) = 0;

    virtual antlrcpp::Any visitGroupAtom(VSL::GroupAtomContext *context) = 0;

    virtual antlrcpp::Any visitCallAtom(VSL::CallAtomContext *context) = 0;

    virtual antlrcpp::Any visitNameAtom(VSL::NameAtomContext *context) = 0;

    virtual antlrcpp::Any visitLiteralAtom(VSL::LiteralAtomContext *context) = 0;

    virtual antlrcpp::Any visitFunctionCall(VSL::FunctionCallContext *context) = 0;

    virtual antlrcpp::Any visitScalarLiteral(VSL::ScalarLiteralContext *context) = 0;


};

}  // namespace grammar
