
// Generated from VSLLexer.g4 by ANTLR 4.9.2

#pragma once


#include "antlr4-runtime.h"


namespace grammar {


class  VSLLexer : public antlr4::Lexer {
public:
  enum {
    BOOLEAN_LITERAL = 1, KW_BIND = 2, KW_BREAK = 3, KW_CONTINUE = 4, KW_DISCARD = 5, 
    KW_ELIF = 6, KW_ELSE = 7, KW_FLAT = 8, KW_FOR = 9, KW_FRAG = 10, KW_IF = 11, 
    KW_INST = 12, KW_LOCAL = 13, KW_RETURN = 14, KW_SHADER = 15, KW_PASSINPUT = 16, 
    KW_STRUCT = 17, KW_UNIFORM = 18, KW_VERT = 19, INTEGER_LITERAL = 20, 
    FLOAT_LITERAL = 21, VERTEX_SEMANTIC = 22, IDENTIFIER = 23, ATSIGN = 24, 
    COLON = 25, COMMA = 26, LBRACE = 27, LBRACKET = 28, LPAREN = 29, PERIOD = 30, 
    QMARK = 31, RBRACE = 32, RBRACKET = 33, RPAREN = 34, SEMICOLON = 35, 
    OP_ADD = 36, OP_ASSIGN = 37, OP_ASN_ADD = 38, OP_ASN_BAND = 39, OP_ASN_BOR = 40, 
    OP_ASN_BXOR = 41, OP_ASN_DIV = 42, OP_ASN_LSH = 43, OP_ASN_MOD = 44, 
    OP_ASN_MUL = 45, OP_ASN_RSH = 46, OP_ASN_SUB = 47, OP_BAND = 48, OP_BNOT = 49, 
    OP_BOR = 50, OP_BXOR = 51, OP_DIV = 52, OP_EQUAL = 53, OP_GEQUAL = 54, 
    OP_GREATER = 55, OP_LAND = 56, OP_LEQUAL = 57, OP_LESS = 58, OP_LNOT = 59, 
    OP_LOR = 60, OP_LSHIFT = 61, OP_MOD = 62, OP_MUL = 63, OP_NEQUAL = 64, 
    OP_RSHIFT = 65, OP_SUB = 66, WS = 67, COMMENT = 68, LINE_COMMENT = 69
  };

  explicit VSLLexer(antlr4::CharStream *input);
  ~VSLLexer();

  virtual std::string getGrammarFileName() const override;
  virtual const std::vector<std::string>& getRuleNames() const override;

  virtual const std::vector<std::string>& getChannelNames() const override;
  virtual const std::vector<std::string>& getModeNames() const override;
  virtual const std::vector<std::string>& getTokenNames() const override; // deprecated, use vocabulary instead
  virtual antlr4::dfa::Vocabulary& getVocabulary() const override;

  virtual const std::vector<uint16_t> getSerializedATN() const override;
  virtual const antlr4::atn::ATN& getATN() const override;

private:
  static std::vector<antlr4::dfa::DFA> _decisionToDFA;
  static antlr4::atn::PredictionContextCache _sharedContextCache;
  static std::vector<std::string> _ruleNames;
  static std::vector<std::string> _tokenNames;
  static std::vector<std::string> _channelNames;
  static std::vector<std::string> _modeNames;

  static std::vector<std::string> _literalNames;
  static std::vector<std::string> _symbolicNames;
  static antlr4::dfa::Vocabulary _vocabulary;
  static antlr4::atn::ATN _atn;
  static std::vector<uint16_t> _serializedATN;


  // Individual action functions triggered by action() above.

  // Individual semantic predicate functions triggered by sempred() above.

  struct Initializer {
    Initializer();
  };
  static Initializer _init;
};

}  // namespace grammar
