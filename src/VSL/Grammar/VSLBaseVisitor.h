
// Generated from VSL.g4 by ANTLR 4.9.2

#pragma once


#include "antlr4-runtime.h"
#include "VSLVisitor.h"


namespace grammar {

/**
 * This class provides an empty implementation of VSLVisitor, which can be
 * extended to create a visitor which only needs to handle a subset of the available methods.
 */
class  VSLBaseVisitor : public VSLVisitor {
public:

  virtual antlrcpp::Any visitFile(VSL::FileContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitShaderTypeStatement(VSL::ShaderTypeStatementContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitTopLevelStatement(VSL::TopLevelStatementContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitShaderStructDefinition(VSL::ShaderStructDefinitionContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitShaderUniformStatement(VSL::ShaderUniformStatementContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitShaderBindingStatement(VSL::ShaderBindingStatementContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitShaderLocalStatement(VSL::ShaderLocalStatementContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitShaderSubpassInputStatement(VSL::ShaderSubpassInputStatementContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitVertexFunction(VSL::VertexFunctionContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitFragmentFunction(VSL::FragmentFunctionContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitShaderVertexInput(VSL::ShaderVertexInputContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitShaderFragmentOutput(VSL::ShaderFragmentOutputContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitBlendStatement(VSL::BlendStatementContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitStatement(VSL::StatementContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitStatementBlock(VSL::StatementBlockContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitVariableDeclaration(VSL::VariableDeclarationContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitVariableDefinition(VSL::VariableDefinitionContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitAssignment(VSL::AssignmentContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitLvalue(VSL::LvalueContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitIfStatement(VSL::IfStatementContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitElifStatement(VSL::ElifStatementContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitElseStatement(VSL::ElseStatementContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitForLoopStatement(VSL::ForLoopStatementContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitControlStatement(VSL::ControlStatementContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitEqualityExpr(VSL::EqualityExprContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitMulDivModExpr(VSL::MulDivModExprContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitBitwiseExpr(VSL::BitwiseExprContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitRelationalExpr(VSL::RelationalExprContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitAtomExpr(VSL::AtomExprContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitAddSubExpr(VSL::AddSubExprContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitShiftExpr(VSL::ShiftExprContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitLogicalExpr(VSL::LogicalExprContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitTernaryExpr(VSL::TernaryExprContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitFactorExpr(VSL::FactorExprContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitNegateExpr(VSL::NegateExprContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitMemberAtom(VSL::MemberAtomContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitIndexAtom(VSL::IndexAtomContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitGroupAtom(VSL::GroupAtomContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitCallAtom(VSL::CallAtomContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitNameAtom(VSL::NameAtomContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitLiteralAtom(VSL::LiteralAtomContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitFunctionCall(VSL::FunctionCallContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitScalarLiteral(VSL::ScalarLiteralContext *ctx) override {
    return visitChildren(ctx);
  }


};

}  // namespace grammar
