
// Generated from VSL.g4 by ANTLR 4.9.2

#pragma once


#include "antlr4-runtime.h"


namespace grammar {


class  VSL : public antlr4::Parser {
public:
  enum {
    BOOLEAN_LITERAL = 1, KW_BIND = 2, KW_BREAK = 3, KW_CONTINUE = 4, KW_DISCARD = 5, 
    KW_ELIF = 6, KW_ELSE = 7, KW_FLAT = 8, KW_FOR = 9, KW_FRAG = 10, KW_IF = 11, 
    KW_INST = 12, KW_LOCAL = 13, KW_RETURN = 14, KW_SHADER = 15, KW_PASSINPUT = 16, 
    KW_STRUCT = 17, KW_UNIFORM = 18, KW_VERT = 19, INTEGER_LITERAL = 20, 
    FLOAT_LITERAL = 21, VERTEX_SEMANTIC = 22, IDENTIFIER = 23, ATSIGN = 24, 
    COLON = 25, COMMA = 26, LBRACE = 27, LBRACKET = 28, LPAREN = 29, PERIOD = 30, 
    QMARK = 31, RBRACE = 32, RBRACKET = 33, RPAREN = 34, SEMICOLON = 35, 
    OP_ADD = 36, OP_ASSIGN = 37, OP_ASN_ADD = 38, OP_ASN_BAND = 39, OP_ASN_BOR = 40, 
    OP_ASN_BXOR = 41, OP_ASN_DIV = 42, OP_ASN_LSH = 43, OP_ASN_MOD = 44, 
    OP_ASN_MUL = 45, OP_ASN_RSH = 46, OP_ASN_SUB = 47, OP_BAND = 48, OP_BNOT = 49, 
    OP_BOR = 50, OP_BXOR = 51, OP_DIV = 52, OP_EQUAL = 53, OP_GEQUAL = 54, 
    OP_GREATER = 55, OP_LAND = 56, OP_LEQUAL = 57, OP_LESS = 58, OP_LNOT = 59, 
    OP_LOR = 60, OP_LSHIFT = 61, OP_MOD = 62, OP_MUL = 63, OP_NEQUAL = 64, 
    OP_RSHIFT = 65, OP_SUB = 66, WS = 67, COMMENT = 68, LINE_COMMENT = 69
  };

  enum {
    RuleFile = 0, RuleShaderTypeStatement = 1, RuleTopLevelStatement = 2, 
    RuleShaderStructDefinition = 3, RuleShaderUniformStatement = 4, RuleShaderBindingStatement = 5, 
    RuleShaderLocalStatement = 6, RuleShaderSubpassInputStatement = 7, RuleShaderStageFunction = 8, 
    RuleShaderVertexInput = 9, RuleShaderFragmentOutput = 10, RuleBlendStatement = 11, 
    RuleStatement = 12, RuleStatementBlock = 13, RuleVariableDeclaration = 14, 
    RuleVariableDefinition = 15, RuleAssignment = 16, RuleLvalue = 17, RuleIfStatement = 18, 
    RuleElifStatement = 19, RuleElseStatement = 20, RuleForLoopStatement = 21, 
    RuleControlStatement = 22, RuleExpression = 23, RuleAtom = 24, RuleFunctionCall = 25, 
    RuleScalarLiteral = 26
  };

  explicit VSL(antlr4::TokenStream *input);
  ~VSL();

  virtual std::string getGrammarFileName() const override;
  virtual const antlr4::atn::ATN& getATN() const override { return _atn; };
  virtual const std::vector<std::string>& getTokenNames() const override { return _tokenNames; }; // deprecated: use vocabulary instead.
  virtual const std::vector<std::string>& getRuleNames() const override;
  virtual antlr4::dfa::Vocabulary& getVocabulary() const override;


  class FileContext;
  class ShaderTypeStatementContext;
  class TopLevelStatementContext;
  class ShaderStructDefinitionContext;
  class ShaderUniformStatementContext;
  class ShaderBindingStatementContext;
  class ShaderLocalStatementContext;
  class ShaderSubpassInputStatementContext;
  class ShaderStageFunctionContext;
  class ShaderVertexInputContext;
  class ShaderFragmentOutputContext;
  class BlendStatementContext;
  class StatementContext;
  class StatementBlockContext;
  class VariableDeclarationContext;
  class VariableDefinitionContext;
  class AssignmentContext;
  class LvalueContext;
  class IfStatementContext;
  class ElifStatementContext;
  class ElseStatementContext;
  class ForLoopStatementContext;
  class ControlStatementContext;
  class ExpressionContext;
  class AtomContext;
  class FunctionCallContext;
  class ScalarLiteralContext; 

  class  FileContext : public antlr4::ParserRuleContext {
  public:
    FileContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    ShaderTypeStatementContext *shaderTypeStatement();
    antlr4::tree::TerminalNode *EOF();
    std::vector<TopLevelStatementContext *> topLevelStatement();
    TopLevelStatementContext* topLevelStatement(size_t i);


    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  FileContext* file();

  class  ShaderTypeStatementContext : public antlr4::ParserRuleContext {
  public:
    antlr4::Token *type = nullptr;
    ShaderTypeStatementContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *KW_SHADER();
    antlr4::tree::TerminalNode *SEMICOLON();
    antlr4::tree::TerminalNode *IDENTIFIER();


    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  ShaderTypeStatementContext* shaderTypeStatement();

  class  TopLevelStatementContext : public antlr4::ParserRuleContext {
  public:
    TopLevelStatementContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    ShaderStructDefinitionContext *shaderStructDefinition();
    ShaderUniformStatementContext *shaderUniformStatement();
    ShaderBindingStatementContext *shaderBindingStatement();
    ShaderLocalStatementContext *shaderLocalStatement();
    ShaderSubpassInputStatementContext *shaderSubpassInputStatement();
    ShaderStageFunctionContext *shaderStageFunction();


    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  TopLevelStatementContext* topLevelStatement();

  class  ShaderStructDefinitionContext : public antlr4::ParserRuleContext {
  public:
    antlr4::Token *name = nullptr;
    ShaderStructDefinitionContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *KW_STRUCT();
    antlr4::tree::TerminalNode *LBRACE();
    antlr4::tree::TerminalNode *RBRACE();
    std::vector<antlr4::tree::TerminalNode *> SEMICOLON();
    antlr4::tree::TerminalNode* SEMICOLON(size_t i);
    antlr4::tree::TerminalNode *IDENTIFIER();
    std::vector<VariableDeclarationContext *> variableDeclaration();
    VariableDeclarationContext* variableDeclaration(size_t i);


    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  ShaderStructDefinitionContext* shaderStructDefinition();

  class  ShaderUniformStatementContext : public antlr4::ParserRuleContext {
  public:
    antlr4::Token *name = nullptr;
    ShaderUniformStatementContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *KW_UNIFORM();
    antlr4::tree::TerminalNode *LBRACE();
    antlr4::tree::TerminalNode *RBRACE();
    std::vector<antlr4::tree::TerminalNode *> SEMICOLON();
    antlr4::tree::TerminalNode* SEMICOLON(size_t i);
    antlr4::tree::TerminalNode *IDENTIFIER();
    std::vector<VariableDeclarationContext *> variableDeclaration();
    VariableDeclarationContext* variableDeclaration(size_t i);


    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  ShaderUniformStatementContext* shaderUniformStatement();

  class  ShaderBindingStatementContext : public antlr4::ParserRuleContext {
  public:
    antlr4::Token *slot = nullptr;
    ShaderBindingStatementContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *KW_BIND();
    antlr4::tree::TerminalNode *LPAREN();
    antlr4::tree::TerminalNode *RPAREN();
    VariableDeclarationContext *variableDeclaration();
    antlr4::tree::TerminalNode *SEMICOLON();
    antlr4::tree::TerminalNode *INTEGER_LITERAL();


    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  ShaderBindingStatementContext* shaderBindingStatement();

  class  ShaderLocalStatementContext : public antlr4::ParserRuleContext {
  public:
    antlr4::Token *pstage = nullptr;
    ShaderLocalStatementContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *KW_LOCAL();
    antlr4::tree::TerminalNode *LPAREN();
    antlr4::tree::TerminalNode *RPAREN();
    VariableDeclarationContext *variableDeclaration();
    antlr4::tree::TerminalNode *SEMICOLON();
    antlr4::tree::TerminalNode *IDENTIFIER();
    antlr4::tree::TerminalNode *KW_FLAT();


    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  ShaderLocalStatementContext* shaderLocalStatement();

  class  ShaderSubpassInputStatementContext : public antlr4::ParserRuleContext {
  public:
    antlr4::Token *index = nullptr;
    antlr4::Token *format = nullptr;
    antlr4::Token *name = nullptr;
    ShaderSubpassInputStatementContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *KW_PASSINPUT();
    antlr4::tree::TerminalNode *LPAREN();
    antlr4::tree::TerminalNode *RPAREN();
    antlr4::tree::TerminalNode *SEMICOLON();
    antlr4::tree::TerminalNode *INTEGER_LITERAL();
    std::vector<antlr4::tree::TerminalNode *> IDENTIFIER();
    antlr4::tree::TerminalNode* IDENTIFIER(size_t i);


    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  ShaderSubpassInputStatementContext* shaderSubpassInputStatement();

  class  ShaderStageFunctionContext : public antlr4::ParserRuleContext {
  public:
    ShaderStageFunctionContext(antlr4::ParserRuleContext *parent, size_t invokingState);
   
    ShaderStageFunctionContext() = default;
    void copyFrom(ShaderStageFunctionContext *context);
    using antlr4::ParserRuleContext::copyFrom;

    virtual size_t getRuleIndex() const override;

   
  };

  class  FragmentFunctionContext : public ShaderStageFunctionContext {
  public:
    FragmentFunctionContext(ShaderStageFunctionContext *ctx);

    antlr4::tree::TerminalNode *KW_FRAG();
    antlr4::tree::TerminalNode *LPAREN();
    antlr4::tree::TerminalNode *RPAREN();
    StatementBlockContext *statementBlock();
    std::vector<ShaderFragmentOutputContext *> shaderFragmentOutput();
    ShaderFragmentOutputContext* shaderFragmentOutput(size_t i);

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  VertexFunctionContext : public ShaderStageFunctionContext {
  public:
    VertexFunctionContext(ShaderStageFunctionContext *ctx);

    antlr4::tree::TerminalNode *KW_VERT();
    antlr4::tree::TerminalNode *LPAREN();
    antlr4::tree::TerminalNode *RPAREN();
    StatementBlockContext *statementBlock();
    std::vector<ShaderVertexInputContext *> shaderVertexInput();
    ShaderVertexInputContext* shaderVertexInput(size_t i);

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  ShaderStageFunctionContext* shaderStageFunction();

  class  ShaderVertexInputContext : public antlr4::ParserRuleContext {
  public:
    antlr4::Token *semantic = nullptr;
    ShaderVertexInputContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *LBRACKET();
    antlr4::tree::TerminalNode *RBRACKET();
    VariableDeclarationContext *variableDeclaration();
    antlr4::tree::TerminalNode *SEMICOLON();
    antlr4::tree::TerminalNode *VERTEX_SEMANTIC();


    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  ShaderVertexInputContext* shaderVertexInput();

  class  ShaderFragmentOutputContext : public antlr4::ParserRuleContext {
  public:
    antlr4::Token *blendName = nullptr;
    ShaderFragmentOutputContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *LBRACKET();
    antlr4::tree::TerminalNode *RBRACKET();
    VariableDeclarationContext *variableDeclaration();
    antlr4::tree::TerminalNode *SEMICOLON();
    BlendStatementContext *blendStatement();
    antlr4::tree::TerminalNode *IDENTIFIER();


    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  ShaderFragmentOutputContext* shaderFragmentOutput();

  class  BlendStatementContext : public antlr4::ParserRuleContext {
  public:
    antlr4::Token *srcColor = nullptr;
    antlr4::Token *colorOp = nullptr;
    antlr4::Token *dstColor = nullptr;
    antlr4::Token *srcAlpha = nullptr;
    antlr4::Token *alphaOp = nullptr;
    antlr4::Token *dstAlpha = nullptr;
    BlendStatementContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    std::vector<antlr4::tree::TerminalNode *> IDENTIFIER();
    antlr4::tree::TerminalNode* IDENTIFIER(size_t i);
    antlr4::tree::TerminalNode *COMMA();


    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  BlendStatementContext* blendStatement();

  class  StatementContext : public antlr4::ParserRuleContext {
  public:
    StatementContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    VariableDefinitionContext *variableDefinition();
    antlr4::tree::TerminalNode *SEMICOLON();
    VariableDeclarationContext *variableDeclaration();
    AssignmentContext *assignment();
    IfStatementContext *ifStatement();
    ForLoopStatementContext *forLoopStatement();
    ControlStatementContext *controlStatement();


    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  StatementContext* statement();

  class  StatementBlockContext : public antlr4::ParserRuleContext {
  public:
    StatementBlockContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *LBRACE();
    antlr4::tree::TerminalNode *RBRACE();
    std::vector<StatementContext *> statement();
    StatementContext* statement(size_t i);


    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  StatementBlockContext* statementBlock();

  class  VariableDeclarationContext : public antlr4::ParserRuleContext {
  public:
    antlr4::Token *baseType = nullptr;
    antlr4::Token *subType = nullptr;
    antlr4::Token *name = nullptr;
    antlr4::Token *arraySize = nullptr;
    VariableDeclarationContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    std::vector<antlr4::tree::TerminalNode *> IDENTIFIER();
    antlr4::tree::TerminalNode* IDENTIFIER(size_t i);
    antlr4::tree::TerminalNode *OP_LESS();
    antlr4::tree::TerminalNode *OP_GREATER();
    antlr4::tree::TerminalNode *LBRACKET();
    antlr4::tree::TerminalNode *RBRACKET();
    antlr4::tree::TerminalNode *INTEGER_LITERAL();


    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  VariableDeclarationContext* variableDeclaration();

  class  VariableDefinitionContext : public antlr4::ParserRuleContext {
  public:
    VSL::VariableDeclarationContext *decl = nullptr;
    VSL::ExpressionContext *value = nullptr;
    VariableDefinitionContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *OP_ASSIGN();
    VariableDeclarationContext *variableDeclaration();
    ExpressionContext *expression();


    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  VariableDefinitionContext* variableDefinition();

  class  AssignmentContext : public antlr4::ParserRuleContext {
  public:
    VSL::LvalueContext *lval = nullptr;
    antlr4::Token *op = nullptr;
    VSL::ExpressionContext *value = nullptr;
    AssignmentContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    LvalueContext *lvalue();
    ExpressionContext *expression();
    antlr4::tree::TerminalNode *OP_ASSIGN();
    antlr4::tree::TerminalNode *OP_ASN_ADD();
    antlr4::tree::TerminalNode *OP_ASN_SUB();
    antlr4::tree::TerminalNode *OP_ASN_MUL();
    antlr4::tree::TerminalNode *OP_ASN_DIV();
    antlr4::tree::TerminalNode *OP_ASN_MOD();
    antlr4::tree::TerminalNode *OP_ASN_LSH();
    antlr4::tree::TerminalNode *OP_ASN_RSH();
    antlr4::tree::TerminalNode *OP_ASN_BAND();
    antlr4::tree::TerminalNode *OP_ASN_BOR();
    antlr4::tree::TerminalNode *OP_ASN_BXOR();


    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  AssignmentContext* assignment();

  class  LvalueContext : public antlr4::ParserRuleContext {
  public:
    VSL::LvalueContext *val = nullptr;
    antlr4::Token *name = nullptr;
    VSL::ExpressionContext *index = nullptr;
    LvalueContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *IDENTIFIER();
    antlr4::tree::TerminalNode *LBRACKET();
    antlr4::tree::TerminalNode *RBRACKET();
    LvalueContext *lvalue();
    ExpressionContext *expression();
    antlr4::tree::TerminalNode *PERIOD();


    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  LvalueContext* lvalue();
  LvalueContext* lvalue(int precedence);
  class  IfStatementContext : public antlr4::ParserRuleContext {
  public:
    VSL::ExpressionContext *cond = nullptr;
    IfStatementContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *KW_IF();
    antlr4::tree::TerminalNode *LPAREN();
    antlr4::tree::TerminalNode *RPAREN();
    ExpressionContext *expression();
    StatementContext *statement();
    StatementBlockContext *statementBlock();
    std::vector<ElifStatementContext *> elifStatement();
    ElifStatementContext* elifStatement(size_t i);
    ElseStatementContext *elseStatement();


    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  IfStatementContext* ifStatement();

  class  ElifStatementContext : public antlr4::ParserRuleContext {
  public:
    VSL::ExpressionContext *cond = nullptr;
    ElifStatementContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *KW_ELIF();
    antlr4::tree::TerminalNode *LPAREN();
    antlr4::tree::TerminalNode *RPAREN();
    ExpressionContext *expression();
    StatementContext *statement();
    StatementBlockContext *statementBlock();


    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  ElifStatementContext* elifStatement();

  class  ElseStatementContext : public antlr4::ParserRuleContext {
  public:
    ElseStatementContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *KW_ELSE();
    StatementContext *statement();
    StatementBlockContext *statementBlock();


    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  ElseStatementContext* elseStatement();

  class  ForLoopStatementContext : public antlr4::ParserRuleContext {
  public:
    antlr4::Token *counter = nullptr;
    antlr4::Token *start = nullptr;
    antlr4::Token *end = nullptr;
    antlr4::Token *step = nullptr;
    ForLoopStatementContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *KW_FOR();
    antlr4::tree::TerminalNode *LPAREN();
    antlr4::tree::TerminalNode *SEMICOLON();
    std::vector<antlr4::tree::TerminalNode *> COLON();
    antlr4::tree::TerminalNode* COLON(size_t i);
    antlr4::tree::TerminalNode *RPAREN();
    StatementBlockContext *statementBlock();
    antlr4::tree::TerminalNode *IDENTIFIER();
    std::vector<antlr4::tree::TerminalNode *> INTEGER_LITERAL();
    antlr4::tree::TerminalNode* INTEGER_LITERAL(size_t i);


    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  ForLoopStatementContext* forLoopStatement();

  class  ControlStatementContext : public antlr4::ParserRuleContext {
  public:
    ControlStatementContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *KW_BREAK();
    antlr4::tree::TerminalNode *KW_CONTINUE();
    antlr4::tree::TerminalNode *KW_RETURN();
    antlr4::tree::TerminalNode *KW_DISCARD();


    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  ControlStatementContext* controlStatement();

  class  ExpressionContext : public antlr4::ParserRuleContext {
  public:
    ExpressionContext(antlr4::ParserRuleContext *parent, size_t invokingState);
   
    ExpressionContext() = default;
    void copyFrom(ExpressionContext *context);
    using antlr4::ParserRuleContext::copyFrom;

    virtual size_t getRuleIndex() const override;

   
  };

  class  EqualityExprContext : public ExpressionContext {
  public:
    EqualityExprContext(ExpressionContext *ctx);

    VSL::ExpressionContext *left = nullptr;
    antlr4::Token *op = nullptr;
    VSL::ExpressionContext *right = nullptr;
    std::vector<ExpressionContext *> expression();
    ExpressionContext* expression(size_t i);
    antlr4::tree::TerminalNode *OP_EQUAL();
    antlr4::tree::TerminalNode *OP_NEQUAL();

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  MulDivModExprContext : public ExpressionContext {
  public:
    MulDivModExprContext(ExpressionContext *ctx);

    VSL::ExpressionContext *left = nullptr;
    antlr4::Token *op = nullptr;
    VSL::ExpressionContext *right = nullptr;
    std::vector<ExpressionContext *> expression();
    ExpressionContext* expression(size_t i);
    antlr4::tree::TerminalNode *OP_MUL();
    antlr4::tree::TerminalNode *OP_DIV();
    antlr4::tree::TerminalNode *OP_MOD();

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  BitwiseExprContext : public ExpressionContext {
  public:
    BitwiseExprContext(ExpressionContext *ctx);

    VSL::ExpressionContext *left = nullptr;
    antlr4::Token *op = nullptr;
    VSL::ExpressionContext *right = nullptr;
    std::vector<ExpressionContext *> expression();
    ExpressionContext* expression(size_t i);
    antlr4::tree::TerminalNode *OP_BAND();
    antlr4::tree::TerminalNode *OP_BOR();
    antlr4::tree::TerminalNode *OP_BXOR();

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  RelationalExprContext : public ExpressionContext {
  public:
    RelationalExprContext(ExpressionContext *ctx);

    VSL::ExpressionContext *left = nullptr;
    antlr4::Token *op = nullptr;
    VSL::ExpressionContext *right = nullptr;
    std::vector<ExpressionContext *> expression();
    ExpressionContext* expression(size_t i);
    antlr4::tree::TerminalNode *OP_LESS();
    antlr4::tree::TerminalNode *OP_GREATER();
    antlr4::tree::TerminalNode *OP_LEQUAL();
    antlr4::tree::TerminalNode *OP_GEQUAL();

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  AtomExprContext : public ExpressionContext {
  public:
    AtomExprContext(ExpressionContext *ctx);

    AtomContext *atom();

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  AddSubExprContext : public ExpressionContext {
  public:
    AddSubExprContext(ExpressionContext *ctx);

    VSL::ExpressionContext *left = nullptr;
    antlr4::Token *op = nullptr;
    VSL::ExpressionContext *right = nullptr;
    std::vector<ExpressionContext *> expression();
    ExpressionContext* expression(size_t i);
    antlr4::tree::TerminalNode *OP_ADD();
    antlr4::tree::TerminalNode *OP_SUB();

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  ShiftExprContext : public ExpressionContext {
  public:
    ShiftExprContext(ExpressionContext *ctx);

    VSL::ExpressionContext *left = nullptr;
    antlr4::Token *op = nullptr;
    VSL::ExpressionContext *right = nullptr;
    std::vector<ExpressionContext *> expression();
    ExpressionContext* expression(size_t i);
    antlr4::tree::TerminalNode *OP_LSHIFT();
    antlr4::tree::TerminalNode *OP_RSHIFT();

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  LogicalExprContext : public ExpressionContext {
  public:
    LogicalExprContext(ExpressionContext *ctx);

    VSL::ExpressionContext *left = nullptr;
    antlr4::Token *op = nullptr;
    VSL::ExpressionContext *right = nullptr;
    std::vector<ExpressionContext *> expression();
    ExpressionContext* expression(size_t i);
    antlr4::tree::TerminalNode *OP_LAND();
    antlr4::tree::TerminalNode *OP_LOR();

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  TernaryExprContext : public ExpressionContext {
  public:
    TernaryExprContext(ExpressionContext *ctx);

    VSL::ExpressionContext *cond = nullptr;
    VSL::ExpressionContext *texpr = nullptr;
    VSL::ExpressionContext *fexpr = nullptr;
    antlr4::tree::TerminalNode *QMARK();
    antlr4::tree::TerminalNode *COLON();
    std::vector<ExpressionContext *> expression();
    ExpressionContext* expression(size_t i);

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  FactorExprContext : public ExpressionContext {
  public:
    FactorExprContext(ExpressionContext *ctx);

    antlr4::Token *op = nullptr;
    VSL::ExpressionContext *val = nullptr;
    ExpressionContext *expression();
    antlr4::tree::TerminalNode *OP_ADD();
    antlr4::tree::TerminalNode *OP_SUB();

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  NegateExprContext : public ExpressionContext {
  public:
    NegateExprContext(ExpressionContext *ctx);

    antlr4::Token *op = nullptr;
    VSL::ExpressionContext *val = nullptr;
    ExpressionContext *expression();
    antlr4::tree::TerminalNode *OP_LNOT();
    antlr4::tree::TerminalNode *OP_BNOT();

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  ExpressionContext* expression();
  ExpressionContext* expression(int precedence);
  class  AtomContext : public antlr4::ParserRuleContext {
  public:
    AtomContext(antlr4::ParserRuleContext *parent, size_t invokingState);
   
    AtomContext() = default;
    void copyFrom(AtomContext *context);
    using antlr4::ParserRuleContext::copyFrom;

    virtual size_t getRuleIndex() const override;

   
  };

  class  MemberAtomContext : public AtomContext {
  public:
    MemberAtomContext(AtomContext *ctx);

    AtomContext *atom();
    antlr4::tree::TerminalNode *PERIOD();
    antlr4::tree::TerminalNode *IDENTIFIER();

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  IndexAtomContext : public AtomContext {
  public:
    IndexAtomContext(AtomContext *ctx);

    VSL::ExpressionContext *index = nullptr;
    VSL::ExpressionContext *index2 = nullptr;
    AtomContext *atom();
    antlr4::tree::TerminalNode *LBRACKET();
    antlr4::tree::TerminalNode *RBRACKET();
    std::vector<ExpressionContext *> expression();
    ExpressionContext* expression(size_t i);
    antlr4::tree::TerminalNode *COMMA();

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  GroupAtomContext : public AtomContext {
  public:
    GroupAtomContext(AtomContext *ctx);

    antlr4::tree::TerminalNode *LPAREN();
    ExpressionContext *expression();
    antlr4::tree::TerminalNode *RPAREN();

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  CallAtomContext : public AtomContext {
  public:
    CallAtomContext(AtomContext *ctx);

    FunctionCallContext *functionCall();

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  NameAtomContext : public AtomContext {
  public:
    NameAtomContext(AtomContext *ctx);

    antlr4::tree::TerminalNode *IDENTIFIER();

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  LiteralAtomContext : public AtomContext {
  public:
    LiteralAtomContext(AtomContext *ctx);

    ScalarLiteralContext *scalarLiteral();

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  AtomContext* atom();
  AtomContext* atom(int precedence);
  class  FunctionCallContext : public antlr4::ParserRuleContext {
  public:
    antlr4::Token *name = nullptr;
    VSL::ExpressionContext *expressionContext = nullptr;
    std::vector<ExpressionContext *> args;
    FunctionCallContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *LPAREN();
    antlr4::tree::TerminalNode *RPAREN();
    antlr4::tree::TerminalNode *IDENTIFIER();
    std::vector<ExpressionContext *> expression();
    ExpressionContext* expression(size_t i);
    std::vector<antlr4::tree::TerminalNode *> COMMA();
    antlr4::tree::TerminalNode* COMMA(size_t i);


    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  FunctionCallContext* functionCall();

  class  ScalarLiteralContext : public antlr4::ParserRuleContext {
  public:
    ScalarLiteralContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *INTEGER_LITERAL();
    antlr4::tree::TerminalNode *FLOAT_LITERAL();
    antlr4::tree::TerminalNode *BOOLEAN_LITERAL();


    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  ScalarLiteralContext* scalarLiteral();


  virtual bool sempred(antlr4::RuleContext *_localctx, size_t ruleIndex, size_t predicateIndex) override;
  bool lvalueSempred(LvalueContext *_localctx, size_t predicateIndex);
  bool expressionSempred(ExpressionContext *_localctx, size_t predicateIndex);
  bool atomSempred(AtomContext *_localctx, size_t predicateIndex);

private:
  static std::vector<antlr4::dfa::DFA> _decisionToDFA;
  static antlr4::atn::PredictionContextCache _sharedContextCache;
  static std::vector<std::string> _ruleNames;
  static std::vector<std::string> _tokenNames;

  static std::vector<std::string> _literalNames;
  static std::vector<std::string> _symbolicNames;
  static antlr4::dfa::Vocabulary _vocabulary;
  static antlr4::atn::ATN _atn;
  static std::vector<uint16_t> _serializedATN;


  struct Initializer {
    Initializer();
  };
  static Initializer _init;
};

}  // namespace grammar
