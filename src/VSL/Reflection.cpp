/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "./Reflection.hpp"

#define NULL_ERROR(msg) { error_ = (msg); return nullptr; }


namespace vsl
{

// ====================================================================================================================
// ====================================================================================================================
TypeSet::TypeSet()
	: types_{ }
	, structs_{ }
{

}

// ====================================================================================================================
TypeSet::~TypeSet()
{

}

// ====================================================================================================================
const ShaderType* TypeSet::AddType(const String& name, const ShaderType& type)
{
	const auto it = types_.find(name);
	if (it == types_.end()) {
		const auto [iter, _] = types_.insert({ name, type });
		return &(iter->second);
	}
	NULL_ERROR(FmtStr("type name '%s' already exists", name.c_str()));
}

// ====================================================================================================================
const ShaderType* TypeSet::AddStructType(const String& name, const StructType& type)
{
	const auto tit = types_.find(name);
	if (tit != types_.end()) {
		NULL_ERROR(FmtStr("type name '%s' already exists", name.c_str()));
	}
	const auto sit = structs_.find(name);
	if (sit != structs_.end()) {
		NULL_ERROR(FmtStr("struct type '%s' already exists", name.c_str()));
	}
	const auto [siter, _] = structs_.emplace(name, type);
	const auto [iter, __] = types_.emplace(name, ShaderType{ &(siter->second) });
	return &(iter->second);
}

// ====================================================================================================================
const ShaderType* TypeSet::ParseOrGetType(const String& name)
{
	// Normalize type name
	auto realName = name;
	realName.erase(std::remove_if(realName.begin(), realName.end(), ::isspace), realName.end());

	// Check if it exists already
	if (const auto type = GetType(realName); type) {
		return type;
	}

	// Parse the base generic type
	const auto stIndex = realName.find('<');
	if (stIndex == String::npos) {
		// All non-generic types are known as builtins, so this is an incorrect type
		NULL_ERROR(FmtStr("unknown type '%s'", realName.c_str()));
	}
	const auto genTypeName = realName.substr(0, stIndex);
	ShaderType genType{ };
	if (!ShaderType::Builtin::ParseGenericType(genTypeName, genType)) {
		NULL_ERROR(FmtStr("unknown generic type '%s'", genTypeName.c_str()));
	}

	// Parse subtype
	const auto subTypeName = realName.substr(stIndex + 1, realName.length() - stIndex - 2);
	if ((genType.baseType == BaseType::Image) || (genType.baseType == BaseType::RWTexels)) {
		const auto format = TexelFormat::FromVSLFormat(subTypeName);
		if (!format.has_value()) {
			NULL_ERROR(FmtStr("invalid texel format '%s'", subTypeName.c_str()));
		}
		genType.texel.format = format.value();
	}
	else { // Buffer types
		const auto structType = GetType(subTypeName);
		if (!structType) {
			NULL_ERROR(FmtStr("unknown type '%s'", subTypeName.c_str()));
		}
		if (!structType->IsStruct()) {
			NULL_ERROR(FmtStr("buffer types cannot use non-struct type '%s'", subTypeName.c_str()));
		}
		genType.buffer.structType = structType;
	}

	// Add and return
	const auto [iter, _] = types_.insert({ realName, genType });
	return &(iter->second);
}

// ====================================================================================================================
const ShaderType* TypeSet::GetType(const String& name) const
{
	const auto it = types_.find(name);
	if (it != types_.end()) {
		return &(it->second);
	}
	const auto btype = ShaderType::Builtin::GetType(name);
	if (btype) {
		return btype;
	}
	NULL_ERROR(FmtStr("no type with name '%s' found", name.c_str()));
}


// ====================================================================================================================
// ====================================================================================================================
ShaderInfo::ShaderInfo()
	: types_{ new TypeSet }
	, stages_{ }
	, inputs_{ }
	, semanticMask_{ 0 }
	, outputs_{ }
	, locals_{ }
	, bindings_{ }
	, uniform_{ std::nullopt }
	, subpassInputs_{ }
{

}

// ====================================================================================================================
ShaderInfo::~ShaderInfo()
{
	types_.reset();
}

// ====================================================================================================================
const InputVariable* ShaderInfo::GetInput(const String& name) const
{
	const auto it = std::find_if(inputs_.begin(), inputs_.end(), [&name](const InputVariable& var) {
		return var.name == name;
		});
	return (it != inputs_.end()) ? &(*it) : nullptr;
}

// ====================================================================================================================
const InputVariable* ShaderInfo::GetInput(uint32 location) const
{
	const auto it = std::find_if(inputs_.begin(), inputs_.end(), [location](const InputVariable& var) {
		return (var.location == location) || (location < (var.location + var.BindingSize()));
		});
	return (it != inputs_.end()) ? &(*it) : nullptr;
}

// ====================================================================================================================
const OutputVariable* ShaderInfo::GetOutput(const String& name) const
{
	const auto it = std::find_if(outputs_.begin(), outputs_.end(), [&name](const OutputVariable& var) {
		return var.name == name;
		});
	return (it != outputs_.end()) ? &(*it) : nullptr;
}

// ====================================================================================================================
const OutputVariable* ShaderInfo::GetOutput(uint32 location) const
{
	return (location < outputs_.size()) ? &(outputs_[location]) : nullptr;
}

// ====================================================================================================================
const SubpassInputVariable* ShaderInfo::GetSubpassInput(const String& name) const
{
	const auto it = std::find_if(subpassInputs_.begin(), subpassInputs_.end(), [&name](const SubpassInputVariable& si) {
		return si.name == name;
		});
	return (it != subpassInputs_.end()) ? &(*it) : nullptr;
}

// ====================================================================================================================
const SubpassInputVariable* ShaderInfo::GetSubpassInput(uint32 index) const
{
	const auto it = std::find_if(subpassInputs_.begin(), subpassInputs_.end(), [index](const SubpassInputVariable& si) {
		return si.index == index;
		});
	return (it != subpassInputs_.end()) ? &(*it) : nullptr;
}

// ====================================================================================================================
const BindingVariable* ShaderInfo::GetBinding(const String& name) const
{
	const auto it = std::find_if(bindings_.begin(), bindings_.end(), [&name](const BindingVariable& bind) {
		return bind.name == name;
		});
	return (it != bindings_.end()) ? &(*it) : nullptr;
}

// ====================================================================================================================
const BindingVariable* ShaderInfo::GetBinding(uint32 slotIndex) const
{
	const auto it = std::find_if(bindings_.begin(), bindings_.end(), [slotIndex](const BindingVariable& bind) {
		return bind.slot == slotIndex;
		});
	return (it != bindings_.end()) ? &(*it) : nullptr;
}

} // namespace vsl
