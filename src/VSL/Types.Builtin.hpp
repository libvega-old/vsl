/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

#include "./Common.Impl.hpp"
#include <VSL/Type.hpp>


namespace vsl
{

// Manages the storage and access to the builtin VSL types.
class ShaderType::Builtin final
{
public:
	using TypeMap = std::unordered_map<String, ShaderType>;
	using StructMap = std::unordered_map<String, StructType>;

	static const ShaderType* GetType(const String& name);
	static const ShaderType* GetNumericType(BaseType baseType, uint32 size, uint32 dim0, uint32 dim1);
	static bool ParseGenericType(const String& baseType, ShaderType& genericType);

	static const StructType* GetStructType(const String& name);

	inline static const TypeMap& Types() { return Types_; }
	inline static const StructMap& StructTypes() { return Structs_; }

private:
	static TypeMap Types_;
	static StructMap Structs_;

	VSL_NO_COPY(Builtin)
	VSL_NO_MOVE(Builtin)
	VSL_NO_INIT(Builtin)
}; // class ShaderType::Builtin

} // namespace vsl
