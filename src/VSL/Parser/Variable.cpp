/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "./Variable.hpp"

#include <algorithm>


namespace vsl
{

// ====================================================================================================================
ScopeManager::Scope::Scope(ScopeType type, Scope* parent)
	: type{ type }
	, parent{ parent }
	, children{ }
	, variables{ }
	, needsAssignment{ }
	, hasAssignment{ }
{

}

// ====================================================================================================================
ScopeManager::Scope::~Scope()
{
	children.clear();
}

// ====================================================================================================================
bool ScopeManager::Scope::HasVariable(const String& name) const
{
	return variables.find(name) != variables.end();
}

// ====================================================================================================================
bool ScopeManager::Scope::HasAssignment(const String& name) const
{
	// Check self
	const auto it = std::find(hasAssignment.begin(), hasAssignment.end(), name);
	if (it != hasAssignment.end()) {
		return true;
	}

	// Check children
	const bool allChildren = std::all_of(children.begin(), children.end(), 
		[&name](const UPtr<Scope>& child) { return child->HasAssignment(name); });
	return allChildren;
}

// ====================================================================================================================
std::optional<String> ScopeManager::Scope::GetMissingAssignment() const
{
	for (const auto& required : needsAssignment) {
		if (!HasAssignment(required)) {
			return required;
		}
	}
	return std::nullopt;
}


// ====================================================================================================================
// ====================================================================================================================
ScopeManager::ScopeManager()
	: rootScope_{ new Scope(ScopeType::Root, nullptr) }
	, currentScope_{ rootScope_.get() }
	, globals_{ }
{
	Variable glob{ 
		"$G", Variable::Use::Builtin, Variable::Access::ReadOnly, ShaderType::Builtin::GetType("_GLOB_"), 1
	};
	globals_.insert({ "$G", glob });
}

// ====================================================================================================================
ScopeManager::~ScopeManager()
{
	rootScope_.reset();
}

// ====================================================================================================================
void ScopeManager::PushFunctionScope(ShaderStages stage)
{
	UPtr<Scope> scope{ new Scope(ScopeType::Function, currentScope_) };

	const auto& bit = ShaderType::Builtin::Types();

	// Per-stage builtin variables
	if (stage == ShaderStages::Vertex) {
		// Readonly Builtins
		scope->variables.insert({ "$VertexIndex",
			{ "$VertexIndex", Variable::Use::Builtin, Variable::Access::ReadOnly, &bit.at("int"), 1 } });
		scope->variables.insert({ "$InstanceIndex",
			{ "$InstanceIndex", Variable::Use::Builtin, Variable::Access::ReadOnly, &bit.at("int"), 1 } });
		scope->variables.insert({ "$DrawIndex",
			{ "$DrawIndex", Variable::Use::Builtin, Variable::Access::ReadOnly, &bit.at("int"), 1 } });
		scope->variables.insert({ "$VertexBase",
			{ "$VertexBase", Variable::Use::Builtin, Variable::Access::ReadOnly, &bit.at("int"), 1 } });
		scope->variables.insert({ "$InstanceBase",
			{ "$InstanceBase", Variable::Use::Builtin, Variable::Access::ReadOnly, &bit.at("int"), 1 } });

		// Writeonly Builtins
		scope->variables.insert({ "$Position",
			{ "$Position", Variable::Use::Builtin, Variable::Access::WriteOnly, &bit.at("float4"), 1 } });
		scope->variables.insert({ "$PointSize",
			{ "$PointSize", Variable::Use::Builtin, Variable::Access::WriteOnly, &bit.at("float"), 1 } });

		// User-Declared
		for (const auto& var : globals_) {
			if ((var.second.use == Variable::Use::Local) &&
				(var.second.extra.local.sourceStage == ShaderStages::Vertex)) {
				scope->variables.insert({ var.second.name, var.second });
			}
		}
	}
	else if (stage == ShaderStages::Fragment) {
		// Readonly Builtins
		scope->variables.insert({ "$FragCoord",
			{ "$FragCoord", Variable::Use::Builtin, Variable::Access::ReadOnly, &bit.at("float4"), 1 } });
		scope->variables.insert({ "$FrontFacing",
			{ "$FrontFacing", Variable::Use::Builtin, Variable::Access::ReadOnly, &bit.at("bool"), 1 } });
		scope->variables.insert({ "$PointCoord",
			{ "$PointCoord", Variable::Use::Builtin, Variable::Access::ReadOnly, &bit.at("float2"), 1 } });
		scope->variables.insert({ "$PrimitiveID",
			{ "$PrimitiveID", Variable::Use::Builtin, Variable::Access::ReadOnly, &bit.at("int"), 1 } });

		// User-Declared
		for (const auto& var : globals_) {
			if ((var.second.use == Variable::Use::Local) &&
				(var.second.extra.local.sourceStage < ShaderStages::Fragment)) {
				scope->variables.insert({ var.second.name, var.second });
			}
			if (var.second.type->IsSubpassInput()) {
				scope->variables.insert({ var.second.name, var.second });
			}
		}
	}

	// Add the globals that are always present for all shader stages
	if (stage != ShaderStages::None) {
		for (const auto& var : globals_) {
			if (var.second.type->baseType == BaseType::Struct) { // Uniforms and uniform globals
				scope->variables.insert({ var.second.name, var.second });
			}
			if (var.second.use == Variable::Use::Binding) { // Bindings
				scope->variables.insert({ var.second.name, var.second });
			}
		}
	}

	currentScope_ = currentScope_->children.emplace_back(scope.release()).get();
}

// ====================================================================================================================
void ScopeManager::PushConditionalScope()
{
	currentScope_ = currentScope_->children.emplace_back(new Scope(ScopeType::Conditional, currentScope_)).get();
}

// ====================================================================================================================
void ScopeManager::PushLoopScope()
{
	currentScope_ = currentScope_->children.emplace_back(new Scope(ScopeType::Loop, currentScope_)).get();
}

// ====================================================================================================================
std::optional<String> ScopeManager::PopScope()
{
	assert(currentScope_ && currentScope_->parent && "Popped the global scope from ScopeManager");
	const auto missing = currentScope_->GetMissingAssignment();
	currentScope_ = currentScope_->parent;
	return missing;
}

// ====================================================================================================================
bool ScopeManager::HasGlobal(const String& name) const
{
	const auto it = globals_.find(name);
	return (it != globals_.end());
}

// ====================================================================================================================
const Variable* ScopeManager::AddGlobal(const Variable& var)
{
	const auto [iter, _] = globals_.insert({ var.name, var });
	return &(iter->second);
}

// ====================================================================================================================
bool ScopeManager::HasName(const String& name) const
{
	Scope* curr{ currentScope_ };
	while (curr) {
		if (curr->HasVariable(name)) {
			return true;
		}
		curr = curr->parent;
	}
	return false;
}

// ====================================================================================================================
const Variable* ScopeManager::GetVariable(const String& name) const
{
	Scope* curr{ currentScope_ };
	while (curr) {
		const auto it = curr->variables.find(name);
		if (it != curr->variables.end()) {
			return &(it->second);
		}
		curr = curr->parent;
	}
	return nullptr;
}

// ====================================================================================================================
const Variable* ScopeManager::AddVariable(const Variable& var)
{
	const auto [iter, _] = currentScope_->variables.insert({ var.name, var });
	return &(iter->second);
}

// ====================================================================================================================
void ScopeManager::MarkRequiredAssignment(const String& name)
{
	currentScope_->needsAssignment.push_back(name);
}

// ====================================================================================================================
bool ScopeManager::InLoop() const
{
	Scope* curr{ currentScope_ };
	while (curr) {
		if (curr->type == ScopeType::Loop) {
			return true;
		}
		curr = curr->parent;
	}
	return false;
}

} // namespace vsl
