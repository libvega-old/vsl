/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "./Expr.hpp"

#include <cmath>


namespace vsl
{

// ====================================================================================================================
std::optional<Literal> Literal::Parse(const String& text, String* error)
{
#define PARSE_ERROR(msg) { if (error) { *error = (msg); } return std::nullopt; }

	const char* beg = text.data();
	char* end{ nullptr };

	// Basic checks
	if (text.empty()) {
		PARSE_ERROR("Cannot parse empty literal");
	}

	// Try to parse a float first
	const bool isFlt = (text.find_first_of(".eE", 0) != String::npos);
	if (isFlt) {
		const auto val = std::strtod(beg, &end);
		if (errno == ERANGE) {
			PARSE_ERROR("Floating point literal is outside representable range");
		}
		else if (std::isnan(val) || std::isinf(val)) {
			PARSE_ERROR("Floating point literal cannot be NaN or inf");
		}
		else if (end == beg) {
			PARSE_ERROR("Invalid floating point literal");
		}
		else {
			return { val };
		}
	}

	// Check integer components
	const bool isNeg = (text[0] == '-');
	const bool isHex = (text[0] == '0') && (text.length() > 1) && (std::tolower(text[1]) == 'x');
	const bool isU = std::tolower(*text.rbegin()) == 'u';

	// Parse integers
	if (isHex || isU) {
		if (isNeg) {
			PARSE_ERROR("Cannot negate hex or unsigned integer literal");
		}
		const auto val = std::strtoull(beg, &end, isHex ? 16 : 10);
		if (errno == ERANGE) {
			PARSE_ERROR("Unsigned integer literal is outside representable range");
		}
		else if (end == beg) {
			PARSE_ERROR("Invalid unsigned integer literal");
		}
		else {
			return { uint64(val) };
		}
	}
	else {
		const auto val = std::strtoll(beg, &end, 10);
		if (errno == ERANGE) {
			PARSE_ERROR("Signed integer literal is outside representable range");
		}
		else if (end == beg) {
			PARSE_ERROR("Invalid signed integer literal");
		}
		else {
			return { int64(val) };
		}
	}

#undef PARSE_ERROR
}

} // namespace vsl
