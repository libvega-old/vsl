/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "./Parser.hpp"
#include "./Function.hpp"
#include "./Operator.hpp"
#include "../Generator/GeneratorUtils.hpp"

#define VISIT_FUNC(type) antlrcpp::Any Parser::visit##type(grammar::VSL::type##Context* ctx)
#define VISIT_EXPR(context) (std::move(visit(context).as<std::shared_ptr<vsl::Expr>>()))
#define MAKE_EXPR(name,type,arrSize) (std::make_shared<vsl::Expr>(name,type,arrSize))
#define SINFO (parse_.info)
#define STYPES (parse_.info->types_)
#define SCOPES (parse_.scopes)
#define FUNCGEN (parse_.currentFunction)


namespace vsl
{

// ====================================================================================================================
VISIT_FUNC(FactorExpr)
{
	const auto expr = VISIT_EXPR(ctx->expression());
	const auto [resType, refStr] = Operators::Check(ctx->op->getText(), { expr.get() });
	if (!resType) {
		ERROR(ctx, refStr);
	}
	return MAKE_EXPR(refStr, resType, 1);
}

// ====================================================================================================================
VISIT_FUNC(NegateExpr)
{
	const auto expr = VISIT_EXPR(ctx->expression());
	const auto [resType, refStr] = Operators::Check(ctx->op->getText(), { expr.get() });
	if (!resType) {
		ERROR(ctx, refStr);
	}
	return MAKE_EXPR(refStr, resType, 1);
}

// ====================================================================================================================
VISIT_FUNC(MulDivModExpr)
{
	const auto left = VISIT_EXPR(ctx->left);
	const auto right = VISIT_EXPR(ctx->right);
	const auto [resType, refStr] = Operators::Check(ctx->op->getText(), { left.get(), right.get() });
	if (!resType) {
		ERROR(ctx, refStr);
	}
	return MAKE_EXPR(refStr, resType, 1);
}

// ====================================================================================================================
VISIT_FUNC(AddSubExpr)
{
	const auto left = VISIT_EXPR(ctx->left);
	const auto right = VISIT_EXPR(ctx->right);
	const auto [resType, refStr] = Operators::Check(ctx->op->getText(), { left.get(), right.get() });
	if (!resType) {
		ERROR(ctx, refStr);
	}
	return MAKE_EXPR(refStr, resType, 1);
}

// ====================================================================================================================
VISIT_FUNC(ShiftExpr)
{
	const auto left = VISIT_EXPR(ctx->left);
	const auto right = VISIT_EXPR(ctx->right);
	const auto [resType, refStr] = Operators::Check(ctx->op->getText(), { left.get(), right.get() });
	if (!resType) {
		ERROR(ctx, refStr);
	}
	return MAKE_EXPR(refStr, resType, 1);
}

// ====================================================================================================================
VISIT_FUNC(RelationalExpr)
{
	const auto left = VISIT_EXPR(ctx->left);
	const auto right = VISIT_EXPR(ctx->right);
	const auto [resType, refStr] = Operators::Check(ctx->op->getText(), { left.get(), right.get() });
	if (!resType) {
		ERROR(ctx, refStr);
	}
	return MAKE_EXPR(refStr, resType, 1);
}

// ====================================================================================================================
VISIT_FUNC(EqualityExpr)
{
	const auto left = VISIT_EXPR(ctx->left);
	const auto right = VISIT_EXPR(ctx->right);
	const auto [resType, refStr] = Operators::Check(ctx->op->getText(), { left.get(), right.get() });
	if (!resType) {
		ERROR(ctx, refStr);
	}
	return MAKE_EXPR(refStr, resType, 1);
}

// ====================================================================================================================
VISIT_FUNC(BitwiseExpr)
{
	const auto left = VISIT_EXPR(ctx->left);
	const auto right = VISIT_EXPR(ctx->right);
	const auto [resType, refStr] = Operators::Check(ctx->op->getText(), { left.get(), right.get() });
	if (!resType) {
		ERROR(ctx, refStr);
	}
	return MAKE_EXPR(refStr, resType, 1);
}

// ====================================================================================================================
VISIT_FUNC(LogicalExpr)
{
	const auto left = VISIT_EXPR(ctx->left);
	const auto right = VISIT_EXPR(ctx->right);
	const auto [resType, refStr] = Operators::Check(ctx->op->getText(), { left.get(), right.get() });
	if (!resType) {
		ERROR(ctx, refStr);
	}
	return MAKE_EXPR(refStr, resType, 1);
}

// ====================================================================================================================
VISIT_FUNC(TernaryExpr)
{
	const auto cond = VISIT_EXPR(ctx->cond);
	const auto tval = VISIT_EXPR(ctx->texpr);
	const auto fval = VISIT_EXPR(ctx->fexpr);
	const auto [resType, refStr] = Operators::Check("?:", { cond.get(), tval.get(), fval.get() });
	if (!resType) {
		ERROR(ctx, refStr);
	}
	return MAKE_EXPR(refStr, resType, 1);
}

// ====================================================================================================================
VISIT_FUNC(GroupAtom)
{
	auto expr = VISIT_EXPR(ctx->expression());
	expr->refString = "(" + expr->refString + ")";
	return expr;
}

// ====================================================================================================================
VISIT_FUNC(IndexAtom)
{
	// Visit components
	const auto left = VISIT_EXPR(ctx->atom());
	const auto& leftStr = left->refString;
	const auto index = VISIT_EXPR(ctx->index);
	const auto& indexStr = index->refString;
	const auto index2 = ctx->index2 ? VISIT_EXPR(ctx->index2) : nullptr;
	const auto& index2Str = index2 ? index2->refString : "";

	// General checks
	if (index2 && !left->type->IsMatrix() && !left->type->IsSampler()) {
		ERROR(ctx->index2, FmtStr("Second indexer is invalid for type '%s'", left->type->GetVSLName().c_str()));
	}

	// Switch based on type
	if (left->arraySize != 1) {
		if (index2) {
			ERROR(ctx->index2, "Second indexer not valid for arrays");
		}
		return MAKE_EXPR(FmtStr("%s[%s]", leftStr.c_str(), indexStr.c_str()), left->type, 1);
	}
	else if (left->type->IsScalar()) {
		ERROR(ctx->atom(), "Indexing is not valid for scalar types");
	}
	else if (left->type->IsVector()) {
		if (!index->type->IsInteger() || !index->type->IsScalar()) {
			ERROR(ctx->index, "Vector indexer must have scalar integer type");
		}
		return MAKE_EXPR(FmtStr("%s[%s]", leftStr.c_str(), indexStr.c_str()),
			ShaderType::Builtin::GetNumericType(left->type->baseType, left->type->numeric.size, 1, 1), 1);
	}
	else if (left->type->IsMatrix()) {
		if (!index->type->IsInteger() || !index->type->IsScalar()) {
			ERROR(ctx->index, "Matrix indexer must have scalar integer type");
		}
		if (index2 && (!index2->type->IsInteger() || !index2->type->IsScalar())) {
			ERROR(ctx->index2, "Second matrix indexer must have scalar integer type");
		}

		if (index2) {
			return MAKE_EXPR(FmtStr("%s[%s][%s]", leftStr.c_str(), indexStr.c_str(), index2Str.c_str()),
				ShaderType::Builtin::GetNumericType(left->type->baseType, left->type->numeric.size, 1, 1), 1);
		}
		else {
			return MAKE_EXPR(FmtStr("%s[%s]", leftStr.c_str(), indexStr.c_str()),
				ShaderType::Builtin::GetNumericType(left->type->baseType, left->type->numeric.size,
					left->type->numeric.dims[0], 1), 1);
		}
	}
	else if (left->type->IsSampler()) {
		const auto compCount = left->type->texel.rank.DimensionCount();
		if (!index->type->IsFloat() || (index->type->numeric.dims[0] != compCount)) {
			ERROR(ctx->index, FmtStr("Invalid coordinates, %s expects float%u", left->type->GetVSLName().c_str(),
				compCount));
		}
		if (index2 && (!index2->type->IsInteger() || !index2->type->IsScalar())) {
			ERROR(ctx->index2, "Second sampler indexer must have scalar integer type");
		}

		if (index2) {
			return MAKE_EXPR(FmtStr("texture(%s, %s, %s)", leftStr.c_str(), indexStr.c_str(), index2Str.c_str()),
				left->type->texel.format.AsShaderType(), 1);
		}
		else {
			return MAKE_EXPR(FmtStr("texture(%s, %s)", leftStr.c_str(), indexStr.c_str()),
				left->type->texel.format.AsShaderType(), 1);
		}
	}
	else if (left->type->IsImage()) {
		const auto compCount = left->type->texel.rank.DimensionCount();
		if (!index->type->IsInteger() || (index->type->numeric.dims[0] != compCount)) {
			ERROR(ctx->index, FmtStr("Invalid coordinates, %s expects int%u or uint%u",
				left->type->GetVSLName().c_str(), compCount, compCount));
		}

		const auto loadStr =
			(left->type->texel.format.ComponentCount() == 1) ? "(imageLoad(%s, %s).x)" :
			(left->type->texel.format.ComponentCount() == 2) ? "(imageLoad(%s, %s).xy)" : "imageLoad(%s, %s)";
		return MAKE_EXPR(FmtStr(loadStr, leftStr.c_str(), indexStr.c_str()),
			left->type->texel.format.AsShaderType(), 1);
	}
	else if (left->type->IsROBuffer() || left->type->IsRWBuffer()) {
		if (!index->type->IsInteger() || !index->type->IsScalar()) {
			ERROR(ctx->index, "Buffer indexer must have scalar integer type");
		}

		return MAKE_EXPR(FmtStr("%s[%s]", leftStr.c_str(), indexStr.c_str()), left->type->buffer.structType, 1);
	}
	else if (left->type->IsROTexels()) {
		if (!index->type->IsInteger() || !index->type->IsScalar()) {
			ERROR(ctx->index, "ROTexels indexer must have scalar integer type");
		}

		return MAKE_EXPR(FmtStr("texelFetch(%s, %s)", leftStr.c_str(), indexStr.c_str()),
			left->type->texel.format.AsShaderType(), 1);
	}
	else if (left->type->IsRWTexels()) {
		if (!index->type->IsInteger() || !index->type->IsScalar()) {
			ERROR(ctx->index, "RWTexels indexer must have scalar integer type");
		}

		const auto loadStr =
			(left->type->texel.format.ComponentCount() == 1) ? "(imageLoad(%s, %s).x)" :
			(left->type->texel.format.ComponentCount() == 2) ? "(imageLoad(%s, %s).xy)" : "imageLoad(%s, %s)";
		return MAKE_EXPR(FmtStr(loadStr, leftStr.c_str(), indexStr.c_str()),
			left->type->texel.format.AsShaderType(), 1);
	}
	else {
		ERROR(ctx->atom(), "Invalid type for indexing operations");
	}
}

// ====================================================================================================================
VISIT_FUNC(MemberAtom)
{
	// Visit left atom
	const auto left = VISIT_EXPR(ctx->atom());
	const auto ltype = left->type;
	const auto memberName = ctx->IDENTIFIER()->getText();

	// Switch on type
	if (ltype->IsStruct()) {
		// Get the member and type
		const auto member = ltype->userStruct.type->GetMember(memberName);
		if (!member) {
			ERROR(ctx->IDENTIFIER(), FmtStr("Type '%s' has no member '%s'",
				ltype->GetVSLName().c_str(), memberName.c_str()));
		}

		return MAKE_EXPR(FmtStr("%s.%s", left->refString.c_str(), memberName.c_str()), member->type, member->arraySize);
	}
	else if (ltype->IsVector()) {
		// Validate the swizzle
		ValidateSwizzle(ltype->numeric.dims[0], ctx->IDENTIFIER());

		return MAKE_EXPR(FmtStr("%s.%s", left->refString.c_str(), memberName.c_str()),
			ShaderType::Builtin::GetNumericType(ltype->baseType, ltype->numeric.size, uint32(memberName.length()), 1), 1);
	}
	else {
		ERROR(ctx->atom(), "Operator '.' is only valid for structs (members) or vectors (swizzles)");
	}

	return nullptr;
}

// ====================================================================================================================
VISIT_FUNC(CallAtom)
{
	// Visit the argument expressions
	std::vector<SPtr<Expr>> arguments{};
	for (const auto arg : ctx->functionCall()->args) {
		arguments.push_back(VISIT_EXPR(arg));
	}
	std::vector<const Expr*> argPtrs{};
	for (const auto& arg : arguments) {
		argPtrs.push_back(arg.get());
	}

	// Validate the constructor/function
	const auto fnName = ctx->functionCall()->name->getText();
	const auto [callType, callName] = Functions::CheckFunction(fnName, argPtrs);
	if (!callType) {
		ERROR(ctx->functionCall()->name, callName);
	}

	// Create the call string
	std::stringstream ss{ std::stringstream::out };
	ss << callName << "( ";
	for (const auto& arg : arguments) {
		ss << arg->refString << ", ";
	}
	ss.seekp(-2, std::stringstream::cur);
	ss << " )"; // Overwrite last ", " with function close " )"

	// Emit temp and return
	return MAKE_EXPR(ss.str(), callType, 1);
}

// ====================================================================================================================
VISIT_FUNC(LiteralAtom)
{
	const auto litptr = ctx->scalarLiteral();

	String errStr{};
	if (litptr->INTEGER_LITERAL()) {
		const auto literal = Literal::Parse(litptr->INTEGER_LITERAL()->getSymbol()->getText(), &errStr);
		if (!literal.has_value()) {
			ERROR(litptr, errStr);
		}
		const auto valstr = (literal.value().type == Literal::Unsigned) 
			? FmtStr("%llu", literal.value().u) 
			: FmtStr("%lld", literal.value().i);
		const auto type = STYPES->GetType((literal.value().type == Literal::Unsigned) ? "uint" : "int");

		return MAKE_EXPR(valstr, type, 1);
	}
	else if (litptr->FLOAT_LITERAL()) {
		const auto literal = Literal::Parse(litptr->FLOAT_LITERAL()->getSymbol()->getText(), &errStr);
		if (!literal.has_value()) {
			ERROR(litptr, errStr);
		}
		return MAKE_EXPR(FmtStr("%f", literal.value().f), STYPES->GetType("float"), 1);
	}
	else { // litptr->BOOLEAN_LITERAL()
		const auto value = litptr->BOOLEAN_LITERAL()->getText() == "true";
		return MAKE_EXPR(value ? "true" : "false", STYPES->GetType("bool"), 1);
	}
}

// ====================================================================================================================
VISIT_FUNC(NameAtom)
{
	// Find and validate the variable
	const auto varName = ctx->IDENTIFIER()->getText();
	const auto var = SCOPES->GetVariable(varName);
	if (!var) {
		ERROR(ctx->IDENTIFIER(), FmtStr("Could not find variable with name '%s'", varName.c_str()));
	}
	if (!var->CanRead(parse_.currentStage)) {
		ERROR(ctx->IDENTIFIER(), FmtStr("The variable '%s' is write-only in this context", varName.c_str()));
	}

	// Calculate the correct expression type and ref string
	const ShaderType* type{};
	String refStr{};
	uint32 arraySize{ 1 };
	if (var->type->IsNumericType() || var->type->IsBoolean() || var->type->IsStruct()) {
		type = var->type;
		refStr = var->name;
		arraySize = var->arraySize;
		if ((var->use == Variable::Use::Binding) && SINFO->uniform_.has_value()) { // Must be the uniform
			SINFO->uniform_.value().stages |= parse_.currentStage;
		}
	}
	else if (var->type->IsSampler() || var->type->IsImage() || var->type->IsROTexels()
		|| var->type->IsRWTexels()) {
		// Get type/refstr
		type = var->type;
		const auto table = GeneratorUtils::BindingTableName(*var->type);
		refStr = FmtStr("%s[_bidx%u_]", table.c_str(), var->extra.binding.slot);

		// Update binding
		FUNCGEN->EmitBindingIndex(var->extra.binding.slot);
		const_cast<BindingVariable*>(SINFO->GetBinding(var->extra.binding.slot))->stages |= parse_.currentStage;
	}
	else if (var->type->IsROBuffer() || var->type->IsRWBuffer()) {
		// Get type/refstr
		type = var->type;
		refStr = FmtStr("%s[_bidx%u_]._data_", var->name.c_str(), var->extra.binding.slot);

		// Update binding
		FUNCGEN->EmitBindingIndex(var->extra.binding.slot);
		const_cast<BindingVariable*>(SINFO->GetBinding(var->extra.binding.slot))->stages |= parse_.currentStage;
	}
	else if (var->type->IsSubpassInput()) {
		if (parse_.currentStage != ShaderStages::Fragment) {
			ERROR(ctx, "Cannot access subpass inputs outside of fragment shader function");
		}
		type = var->type->texel.format.AsShaderType();
		refStr = FmtStr("subpassLoad(%s)", var->name.c_str());
	}

	// For locals, one more name translation
	if (var->use == Variable::Use::Local) {
		const auto inout = (var->extra.local.sourceStage == parse_.currentStage) ? "out" : "in";
		refStr = FmtStr("_l%s_%s", inout, refStr.c_str());
	}
	else if (var->use == Variable::Use::Builtin) { // Builtins need a name lookup
		refStr = GeneratorUtils::BuiltinName(refStr);
	}

	return MAKE_EXPR(refStr, type, arraySize);
}

} // namespace vsl
