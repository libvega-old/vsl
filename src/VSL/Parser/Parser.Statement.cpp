/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "./Parser.hpp"
#include "./Function.hpp"
#include "./Operator.hpp"
#include "../Generator/GeneratorUtils.hpp"

#define VISIT_FUNC(type) antlrcpp::Any Parser::visit##type(grammar::VSL::type##Context* ctx)
#define VISIT_EXPR(context) (std::move(visit(context).as<std::shared_ptr<vsl::Expr>>()))
#define MAKE_EXPR(name,type,arrSize) (std::make_shared<vsl::Expr>(name,type,arrSize))
#define SINFO (parse_.info)
#define STYPES (parse_.info->types_)
#define SCOPES (parse_.scopes)
#define FUNCGEN (parse_.currentFunction)


namespace vsl
{

// ====================================================================================================================
VISIT_FUNC(Statement)
{
	// Dispatch
	if (ctx->variableDefinition()) {
		visit(ctx->variableDefinition());
	}
	else if (ctx->variableDeclaration()) {
		visit(ctx->variableDeclaration());
	}
	else if (ctx->assignment()) {
		visit(ctx->assignment());
	}
	else if (ctx->ifStatement()) {
		visit(ctx->ifStatement());
	}
	else if (ctx->forLoopStatement()) {
		visit(ctx->forLoopStatement());
	}
	else if (ctx->controlStatement()) {
		visit(ctx->controlStatement());
	}

	return nullptr;
}

// ====================================================================================================================
VISIT_FUNC(VariableDefinition)
{
	// Create the variable definition
	const auto varDecl = ctx->variableDeclaration();
	auto var = ParseVariableDeclaration(varDecl);
	if (var.arraySize != 1) {
		ERROR(varDecl->arraySize, "Function-local variables cannot be arrays");
	}
	if (!var.type->IsNumericType() && !var.type->IsBoolean()) {
		ERROR(varDecl->baseType, "Function-local variable must be numeric or boolean type");
	}

	// Visit and check expression
	const auto expr = VISIT_EXPR(ctx->value);
	const auto etype = expr->type;
	if (!etype->HasImplicitCast(*var.type)) {
		ERROR(ctx->value, FmtStr("No implicit cast from '%s' to '%s'", etype->GetVSLName().c_str(),
			var.type->GetVSLName().c_str()));
	}

	// Add the variable
	var.use = Variable::Use::Private;
	SCOPES->AddVariable(var);

	// Emit declaration and assignment
	const auto typeStr = GeneratorUtils::GLSLTypeName(*var.type);
	FUNCGEN->EmitAssignment(typeStr + " " + var.name, "=", expr->refString);

	return nullptr;
}

// ====================================================================================================================
VISIT_FUNC(VariableDeclaration)
{
	// Create the variable definition
	auto var = ParseVariableDeclaration(ctx);
	if (var.arraySize != 1) {
		ERROR(ctx->arraySize, "Function-local variables cannot be arrays");
	}
	if (!var.type->IsNumericType() && !var.type->IsBoolean()) {
		ERROR(ctx->baseType, "Function-local variable must be numeric or boolean type");
	}

	// Add the variable
	var.use = Variable::Use::Private;
	SCOPES->AddVariable(var);

	// Emit declaration and assignment
	FUNCGEN->EmitDeclaration(*var.type, var.name);

	return nullptr;
}

// ====================================================================================================================
VISIT_FUNC(Assignment)
{
	// Visit the left-hand side
	const auto left = VISIT_EXPR(ctx->lval);
	const auto ltype = left->type;
	const auto isImageStore = (left->refString.find("imageStore") == 0);

	// Visit expression
	const auto expr = VISIT_EXPR(ctx->value);
	const auto etype = expr->type;

	// Validate types
	const auto optxt = ctx->op->getText();
	const auto isCompound = (optxt != "=");
	if (isImageStore) {
		if (isCompound) {
			ERROR(ctx->op, "Compound assignment not allowed on Image or RWTexel types");
		}
		if ((etype->baseType != ltype->baseType) || (etype->numeric.dims[0] != ltype->numeric.dims[0]) ||
			etype->IsMatrix()) {
			ERROR(ctx->op, FmtStr("Cannot store type '%s' in object with texel type '%s'", etype->GetVSLName().c_str(),
				ltype->GetVSLName().c_str()));
		}

		// Get the value (need to promote the stored type to *gvec4* for imageStore(...))
		const auto dims = etype->numeric.dims[0];
		const String prefix = etype->IsSigned() ? "i" : etype->IsUnsigned() ? "u" : "";
		const auto valstr =
			(dims == 1) ? FmtStr("%svec4(%s, 0, 0, 0)", prefix.c_str(), expr->refString.c_str()) :
			(dims == 2) ? FmtStr("%svec4(%s, 0, 0)", prefix.c_str(), expr->refString.c_str()) : expr->refString;

		// Emit image store
		FUNCGEN->EmitImageStore(left->refString, valstr);
	}
	else if (!isCompound) {
		if (!etype->HasImplicitCast(*left->type)) {
			ERROR(ctx->value, FmtStr("No implicit cast from '%s' to '%s'", etype->GetVSLName().c_str(),
				left->type->GetVSLName().c_str()));
		}
		FUNCGEN->EmitAssignment(left->refString, optxt, expr->refString);
	}
	else {
		const auto subop = optxt.substr(0, optxt.length() - 1);
		const auto [resType, refStr] = Operators::Check(subop, { left.get(), expr.get() });
		if (!resType) {
			ERROR(ctx->value, FmtStr("Compound assignment '%s' not possible with types '%s' and '%s'",
				optxt.c_str(), ltype->GetVSLName().c_str(), etype->GetVSLName().c_str()));
		}
		FUNCGEN->EmitAssignment(left->refString, optxt, expr->refString);
	}

	return nullptr;
}

// ====================================================================================================================
VISIT_FUNC(Lvalue)
{
	// Switch on lvalue type
	if (ctx->name) {
		// Find the variable
		const auto varName = ctx->name->getText();
		const auto var = SCOPES->GetVariable(varName);
		if (!var) {
			ERROR(ctx->name, FmtStr("No variable with name '%s' found", varName.c_str()));
		}
		if (!var->CanWrite(parse_.currentStage)) {
			ERROR(ctx->name, FmtStr("The variable '%s' is read-only in this context", varName.c_str()));
		}

		// Get the name of the variable based on the variable type
		String outname{};
		switch (var->use)
		{
		case Variable::Use::Binding: {
			if (var->type->baseType == BaseType::Struct) {
				outname = var->name;
				if (SINFO->Uniform().has_value()) {
					SINFO->uniform_.value().stages |= parse_.currentStage;
				}
			}
			else if (var->type->IsBufferType()) {
				FUNCGEN->EmitBindingIndex(var->extra.binding.slot);
				outname = FmtStr("%s[_bidx%u_]", var->name.c_str(), uint32(var->extra.binding.slot));
				const_cast<BindingVariable*>(SINFO->GetBinding(var->extra.binding.slot))->stages |= parse_.currentStage;
			}
			else {
				const auto table = GeneratorUtils::BindingTableName(*var->type);
				FUNCGEN->EmitBindingIndex(var->extra.binding.slot);
				outname = FmtStr("(%s[_bidx%u_])", table.c_str(), uint32(var->extra.binding.slot));
				const_cast<BindingVariable*>(SINFO->GetBinding(var->extra.binding.slot))->stages |= parse_.currentStage;
			}
		} break;
		case Variable::Use::Builtin: {
			outname = GeneratorUtils::BuiltinName(var->name);
		} break;
		case Variable::Use::Local: {
			const auto inout = (var->extra.local.sourceStage == parse_.currentStage) ? "out" : "in";
			outname = FmtStr("_l%s_%s", inout, varName.c_str());
		} break;
		default: outname = varName;
		}

		return MAKE_EXPR(outname, var->type, var->arraySize);
	}
	else if (ctx->index) {
		// Get the lvalue
		const auto left = VISIT_EXPR(ctx->val);
		const auto ltype = left->type;
		if (left->refString.find("imageStore") == 0) {
			ERROR(ctx->val, "Image or RWTexel stores must be top-level lvalue");
		}

		// Visit the index expression
		const auto index = VISIT_EXPR(ctx->index);
		const auto itype = index->type;
		if (!itype->IsNumericType() || itype->IsMatrix() || (index->arraySize != 1)) {
			ERROR(ctx->index, "Indexer argument must by a non-array numeric scalar or vector");
		}
		if (itype->IsFloat()) {
			ERROR(ctx->index, "Indexer argument must be an integer type");
		}

		// A few different types can be used as arrays
		String refStr{};
		const ShaderType* refType{};
		if (left->arraySize != 1) {
			refStr = FmtStr("%s[%s]", left->refString, index->refString);
			refType = ltype;
		}
		else if (ltype->IsImage()) {
			const auto dimcount = ltype->texel.rank.DimensionCount();
			if (dimcount != itype->numeric.dims[0]) {
				ERROR(ctx->index, FmtStr("Image type expects indexer with %u components", dimcount));
			}
			refStr = FmtStr("imageStore(%s, %s, {})", left->refString.c_str(), index->refString.c_str());
			refType = ltype->texel.format.AsShaderType();
		}
		else if (ltype->baseType == BaseType::RWBuffer) {
			if (!itype->IsScalar()) {
				ERROR(ctx->index, "RWBuffer expects a scalar integer indexer");
			}
			refStr = FmtStr("(%s._data_[%s])", left->refString.c_str(), ctx->index->getText().c_str());
			refType = STYPES->GetType(ltype->GetVSLName());
		}
		else if (ltype->baseType == BaseType::RWTexels) {
			if (!itype->IsScalar()) {
				ERROR(ctx->index, "RWTexels expects a scalar integer indexer");
			}
			refStr = FmtStr("imageStore(%s, %s, {})", left->refString.c_str(), index->refString.c_str());
			refType = ltype->texel.format.AsShaderType();
		}
		else if (ltype->IsNumericType()) {
			if (ltype->IsMatrix()) { // Matrix
				refStr = left->refString;
				refType = ShaderType::Builtin::GetNumericType(
					ltype->baseType, ltype->numeric.size, ltype->numeric.dims[0], 1);
			}
			else if (ltype->IsVector()) { // Vector
				refStr = left->refString;
				refType = ShaderType::Builtin::GetNumericType(ltype->baseType, ltype->numeric.size, 1, 1);
			}
			else {
				ERROR(ctx->index, "Cannot apply indexer to scalar type");
			}
		}
		else {
			ERROR(ctx->index, "Type cannot receive an indexer");
		}

		return MAKE_EXPR(refStr, refType, 1);
	}
	else { // value.member
		const auto ident = ctx->IDENTIFIER()->getText();

		// Get the lvalue
		const auto left = VISIT_EXPR(ctx->val);
		const auto ltype = left->type;
		if (left->refString.find("imageStore") == 0) {
			ERROR(ctx->val, "Image or RWTexel stores must be top-level lvalue");
		}

		// Switch on type (struct=member, vector=swizzle)
		if (ltype->IsStruct()) {
			// Validate member
			const auto memType = ltype->userStruct.type->GetMember(ident);
			if (!memType) {
				ERROR(ctx->IDENTIFIER(), FmtStr("Type '%s' does not have member '%s'",
					ltype->userStruct.type->Name().c_str(), ident.c_str()));
			}

			// Return member
			const auto refStr = FmtStr("(%s.%s)", left->refString.c_str(), ident.c_str());
			const auto refType = ShaderType::Builtin::GetNumericType(memType->type->baseType, 
				memType->type->numeric.size, memType->type->numeric.dims[0], memType->type->numeric.dims[1]);
			return MAKE_EXPR(refStr, refType, memType->arraySize);
		}
		else if (ltype->IsVector()) {
			// Validate data type
			const uint32 compCount = ltype->numeric.dims[0];
			if (!ltype->IsNumericType() && !ltype->IsBoolean()) {
				ERROR(ctx->IDENTIFIER(), "Swizzles can only be applied to numeric types");
			}
			if ((compCount == 1) || (ltype->numeric.dims[1] != 1)) {
				ERROR(ctx->IDENTIFIER(), "Swizzles can only be applied to a vector type");
			}
			ValidateSwizzle(compCount, ctx->IDENTIFIER());

			// Get the new type
			const auto stype = ShaderType::Builtin::GetNumericType(
				ltype->baseType, ltype->numeric.size, uint32(ident.length()), 1);
			return MAKE_EXPR(FmtStr("(%s.%s)", left->refString.c_str(), ident.c_str()), stype, 1);
		}
		else {
			ERROR(ctx->val, "Operator '.' can only be applied to structs or vectors");
		}
	}

	return nullptr;
}

// ====================================================================================================================
VISIT_FUNC(IfStatement)
{
	// Check the condition
	const auto cond = VISIT_EXPR(ctx->cond);
	if (cond->arraySize != 1) {
		ERROR(ctx->cond, "If statement condition cannot be an array");
	}
	if (!cond->type->IsScalar() || !cond->type->IsBoolean()) {
		ERROR(ctx->cond, "If statement condition must be a scalar boolean");
	}

	// Emit and create scope
	FUNCGEN->EmitIf(cond->refString);
	SCOPES->PushConditionalScope();

	// Visit statements
	if (ctx->statement()) {
		visit(ctx->statement());
	}
	else {
		for (const auto& stmt : ctx->statementBlock()->statement()) {
			visit(stmt);
		}
	}

	// Close scope
	FUNCGEN->CloseBlock();
	const auto missing = SCOPES->PopScope();
	if (missing.has_value()) {
		ERROR(ctx->getStart(), FmtStr("Variable '%s' must be assinged before existing the block", 
			missing.value().c_str()));
	}

	// Visit the elif and else statements
	for (const auto& elif : ctx->elifStatement()) {
		visit(elif);
	}
	if (ctx->elseStatement()) {
		visit(ctx->elseStatement());
	}

	return nullptr;
}

// ====================================================================================================================
VISIT_FUNC(ElifStatement)
{
	// Check the condition
	const auto cond = VISIT_EXPR(ctx->cond);
	if (cond->arraySize != 1) {
		ERROR(ctx->cond, "Elif statement condition cannot be an array");
	}
	if (!cond->type->IsScalar() || !cond->type->IsBoolean()) {
		ERROR(ctx->cond, "Elif statement condition must be a scalar boolean");
	}

	// Emit and create scope
	FUNCGEN->EmitElif(cond->refString);
	SCOPES->PushConditionalScope();

	// Visit statements
	if (ctx->statement()) {
		visit(ctx->statement());
	}
	else {
		for (const auto& stmt : ctx->statementBlock()->statement()) {
			visit(stmt);
		}
	}

	// Close scope
	FUNCGEN->CloseBlock();
	const auto missing = SCOPES->PopScope();
	if (missing.has_value()) {
		ERROR(ctx->getStart(), FmtStr("Variable '%s' must be assinged before existing the block",
			missing.value().c_str()));
	}

	return nullptr;
}

// ====================================================================================================================
VISIT_FUNC(ElseStatement)
{
	// Emit and create scope
	FUNCGEN->EmitElse();
	SCOPES->PushConditionalScope();

	// Visit statements
	if (ctx->statement()) {
		visit(ctx->statement());
	}
	else {
		for (const auto& stmt : ctx->statementBlock()->statement()) {
			visit(stmt);
		}
	}

	// Close scope
	FUNCGEN->CloseBlock();
	const auto missing = SCOPES->PopScope();
	if (missing.has_value()) {
		ERROR(ctx->getStart(), FmtStr("Variable '%s' must be assinged before existing the block",
			missing.value().c_str()));
	}

	return nullptr;
}

// ====================================================================================================================
VISIT_FUNC(ForLoopStatement)
{
	static const auto signum = [](int32 val) { return (0 < val) - (val < 0); };

	// Check the variable
	const auto counterName = ctx->counter->getText();
	ValidateNewName(ctx->counter);

	// Check the start variable
	const auto slit = Literal::Parse(ctx->start->getText(), nullptr);
	if (!slit.has_value()) {
		ERROR(ctx->start, "Invalid literal for loop start value");
	}
	if (slit.value().type == Literal::Float) {
		ERROR(ctx->start, "Loop start value must be an integer type");
	}
	if ((slit.value().i < INT32_MIN) || (slit.value().i > INT32_MAX)) {
		ERROR(ctx->start, "Loop end value is out of range");
	}
	const auto startValue = int32(slit.value().i);

	// Check the end variable
	const auto elit = Literal::Parse(ctx->end->getText(), nullptr);
	if (!elit.has_value()) {
		ERROR(ctx->start, "Invalid literal for loop end value");
	}
	if (elit.value().type == Literal::Float) {
		ERROR(ctx->end, "Loop end value must be an integer type");
	}
	if ((elit.value().i < INT32_MIN) || (elit.value().i > INT32_MAX)) {
		ERROR(ctx->end, "Loop end value is out of range");
	}
	const auto endValue = int32(elit.value().i);

	// Check the optional step value
	int32 stepValue{ 1 };
	if (ctx->step) {
		// Parse
		const auto lit = Literal::Parse(ctx->step->getText(), nullptr);
		if (!lit.has_value()) {
			ERROR(ctx->start, "Invalid literal for loop step value");
		}
		if (lit.value().type == Literal::Float) {
			ERROR(ctx->step, "Loop step value must be an integer type");
		}
		if ((lit.value().i < INT32_MIN) || (lit.value().i > INT32_MAX)) {
			ERROR(ctx->step, "Loop step value is out of range");
		}
		stepValue = int32(lit.value().i);

		// Validate
		if (stepValue == 0) {
			ERROR(ctx->step, "Loop step value cannot be zero");
		}
		if (signum(stepValue) != signum(endValue - startValue)) {
			ERROR(ctx->step, "Sign of step is invalid for given start and end values");
		}
	}
	else {
		if (endValue < startValue) {
			stepValue = -1;
		}
	}

	// Emit and push scope, add counter as readonly variable
	FUNCGEN->EmitForLoop(counterName, startValue, endValue, stepValue);
	SCOPES->PushLoopScope();
	Variable counterVar{ 
		counterName, Variable::Use::Private, Variable::Access::ReadOnly, ShaderType::Builtin::GetType("int"), 1
	};
	SCOPES->AddVariable(counterVar);

	// Visit the inner statements
	for (const auto& stmt : ctx->statementBlock()->statement()) {
		visit(stmt);
	}

	// Emit and pop scope
	FUNCGEN->CloseBlock();
	const auto missing = SCOPES->PopScope();
	if (missing.has_value()) {
		ERROR(ctx->getStart(), FmtStr("Variable '%s' must be assinged before existing the block",
			missing.value().c_str()));
	}

	return nullptr;
}

// ====================================================================================================================
VISIT_FUNC(ControlStatement)
{
	const auto keyword = ctx->getText();

	if ((keyword == "break") || (keyword == "continue")) {
		if (!SCOPES->InLoop()) {
			ERROR(ctx, FmtStr("Statement '%s' only allowed in loops", keyword.c_str()));
		}
	}
	else if (keyword == "discard") {
		if (parse_.currentStage != ShaderStages::Fragment) {
			ERROR(ctx, "Statement 'discard' only allowed in fragment stage");
		}
	}

	FUNCGEN->EmitControlStatement(keyword);

	return nullptr;
}

} // namespace vsl
