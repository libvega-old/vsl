/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

#include "../Common.Impl.hpp"
#include "../Types.Builtin.hpp"
#include "./Expr.hpp"

#include <vector>


namespace vsl
{

// Special type object that can represent a function parameter or return type
// Supports the concept of genType/genIType/genUType/genBType, and out variables
struct FunctionType final
{
public:
	FunctionType() : type{ nullptr }, genType{ false }, refType{ false } { }
	FunctionType(const String& typeName);
	FunctionType(const char* const typeName) : FunctionType(String{ typeName }) { }

	bool Match(const Expr* expr) const;

public:
	const ShaderType* type; // The argument type (full type or just base type)
	bool genType;
	bool refType; // If the type is an out or inout argument

private:
	static std::vector<UPtr<ShaderType>> GenericTypes_;
}; // struct FunctionType


// Describes a specific version (arg list) of a specific function
class FunctionEntry final
{
public:
	FunctionEntry() : genName{ "INVALID" }, retType{}, argTypes{} {}
	FunctionEntry(const String& genName, const FunctionType& retType, const std::vector<FunctionType>& args);

	const ShaderType* Match(const std::vector<const Expr*>& params) const;

public:
	String genName; // Output generated name
	FunctionType retType;
	std::vector<FunctionType> argTypes;
}; // class FunctionEntry


// Contains the registry of built-in and user defined functions, and constructors
class Functions final
{
public:
	// Function Checks (tuples return the error message in item2 if item1 is nullptr)
	static bool HasFunction(const String& funcName);
	static std::tuple<const ShaderType*, String> CheckFunction(const String& funcName,
		const std::vector<const Expr*>& args);
	static std::tuple<const ShaderType*, String> CheckConstructor(const String& typeName,
		const std::vector<const Expr*>& args);

private:
	static void Initialize();

private:
	static std::unordered_map<String, std::vector<FunctionEntry>> Builtins_;

	VSL_NO_COPY(Functions)
	VSL_NO_MOVE(Functions)
	VSL_NO_INIT(Functions)
}; // class Functions

} // namespace vsl
