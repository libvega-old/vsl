/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "./Operator.hpp"

#include <atomic>


namespace vsl
{

// ====================================================================================================================
void Operators::Initialize()
{
	static std::atomic_flag Initialized_{ };
	static bool FinishedInit_{ false };
	if (Initialized_.test_and_set()) {
		while (!FinishedInit_) { ; }
		return;
	}

	const OpType GENF{ "genType" };
	const OpType GENU{ "genUType" };
	const OpType GENI{ "genIType" };
	const OpType GENB{ "genBType" };
	const OpType BOOL_T{ "bool" };
	const OpType INT_T{ "int" };
	const OpType UINT_T{ "uint" };
	const OpType FLOAT_T{ "float" };
	const String DEFAULT1{ "$op$1" };
	const String DEFAULT2{ "$1 $op $2" };
	const String DEFAULT3{ "($1 ? ($2) : ($3))" };

	// Unary Ops
	Ops_["!"] = {
		{ DEFAULT1, BOOL_T, { BOOL_T } },
		{ "(not($1))", GENB, { GENB } }
	};
	Ops_["~"] = {
		{ DEFAULT1, GENU, { GENU } }
	};

	// Binary Ops
	Ops_["*"] = {
		// Matrix * Matrix
		{ DEFAULT2, "float2x2", { "float2x2", "float2x2" } },
		{ DEFAULT2, "float3x3", { "float2x3", "float3x2" } },
		{ DEFAULT2, "float4x4", { "float2x4", "float4x2" } },
		{ DEFAULT2, "float2x2", { "float3x2", "float2x3" } },
		{ DEFAULT2, "float3x3", { "float3x3", "float3x3" } },
		{ DEFAULT2, "float4x4", { "float3x4", "float4x3" } },
		{ DEFAULT2, "float2x2", { "float4x2", "float2x4" } },
		{ DEFAULT2, "float3x3", { "float4x3", "float3x4" } },
		{ DEFAULT2, "float4x4", { "float4x4", "float4x4" } },
		
		// Matrix * Vector
		{ DEFAULT2, "float2", { "float2x2", "float2" } },
		{ DEFAULT2, "float3", { "float2x3", "float2" } },
		{ DEFAULT2, "float4", { "float2x4", "float2" } },
		{ DEFAULT2, "float2", { "float3x2", "float3" } },
		{ DEFAULT2, "float3", { "float3x3", "float3" } },
		{ DEFAULT2, "float4", { "float3x4", "float3" } },
		{ DEFAULT2, "float2", { "float4x2", "float4" } },
		{ DEFAULT2, "float3", { "float4x3", "float4" } },
		{ DEFAULT2, "float4", { "float4x4", "float4" } },

		// Matrix * Scalar
		{ DEFAULT2, "float2x2", { "float2x2", FLOAT_T } },
		{ DEFAULT2, "float2x3", { "float2x3", FLOAT_T } },
		{ DEFAULT2, "float2x4", { "float2x4", FLOAT_T } },
		{ DEFAULT2, "float3x2", { "float3x2", FLOAT_T } },
		{ DEFAULT2, "float3x3", { "float3x3", FLOAT_T } },
		{ DEFAULT2, "float3x4", { "float3x4", FLOAT_T } },
		{ DEFAULT2, "float4x2", { "float4x2", FLOAT_T } },
		{ DEFAULT2, "float4x3", { "float4x3", FLOAT_T } },
		{ DEFAULT2, "float4x4", { "float4x4", FLOAT_T } },

		// Vector/Scalar * Vector/Scalar
		{ DEFAULT2, GENU, { GENU, UINT_T } },
		{ DEFAULT2, GENU, { GENU, GENU } },
		{ DEFAULT2, GENI, { GENI, INT_T } },
		{ DEFAULT2, GENI, { GENI, GENI } },
		{ DEFAULT2, GENF, { GENF, FLOAT_T } },
		{ DEFAULT2, GENF, { GENF, GENF } }
	};
	Ops_["/"] = {
		// Matrix / Matrix
		{ DEFAULT2, "float2x2", { "float2x2", "float2x2" } },
		{ DEFAULT2, "float2x3", { "float2x3", "float2x3" } },
		{ DEFAULT2, "float2x4", { "float2x4", "float2x4" } },
		{ DEFAULT2, "float3x2", { "float3x2", "float3x2" } },
		{ DEFAULT2, "float3x3", { "float3x3", "float3x3" } },
		{ DEFAULT2, "float3x4", { "float3x4", "float3x4" } },
		{ DEFAULT2, "float4x2", { "float4x2", "float4x2" } },
		{ DEFAULT2, "float4x3", { "float4x3", "float4x3" } },
		{ DEFAULT2, "float4x4", { "float4x4", "float4x4" } },

		// Matrix / Scalar
		{ DEFAULT2, "float2x2", { "float2x2", FLOAT_T } },
		{ DEFAULT2, "float2x3", { "float2x3", FLOAT_T } },
		{ DEFAULT2, "float2x4", { "float2x4", FLOAT_T } },
		{ DEFAULT2, "float3x2", { "float3x2", FLOAT_T } },
		{ DEFAULT2, "float3x3", { "float3x3", FLOAT_T } },
		{ DEFAULT2, "float3x4", { "float3x4", FLOAT_T } },
		{ DEFAULT2, "float4x2", { "float4x2", FLOAT_T } },
		{ DEFAULT2, "float4x3", { "float4x3", FLOAT_T } },
		{ DEFAULT2, "float4x4", { "float4x4", FLOAT_T } },

		// Scalar/Vector / Scalar/Vector
		{ DEFAULT2, GENU, { GENU, UINT_T } },
		{ DEFAULT2, GENU, { GENU, GENU } },
		{ DEFAULT2, GENI, { GENI, INT_T } },
		{ DEFAULT2, GENI, { GENI, GENI } },
		{ DEFAULT2, GENF, { GENF, FLOAT_T } },
		{ DEFAULT2, GENF, { GENF, GENF } }
	};
	Ops_["+"] = {
		{ "$1", GENU, { GENU } }, // Unary
		{ "$1", GENI, { GENI } }, // Unary
		{ "$1", GENF, { GENF } }, // Unary

		// Matrix / Matrix
		{ DEFAULT2, "float2x2", { "float2x2", "float2x2" } },
		{ DEFAULT2, "float2x3", { "float2x3", "float2x3" } },
		{ DEFAULT2, "float2x4", { "float2x4", "float2x4" } },
		{ DEFAULT2, "float3x2", { "float3x2", "float3x2" } },
		{ DEFAULT2, "float3x3", { "float3x3", "float3x3" } },
		{ DEFAULT2, "float3x4", { "float3x4", "float3x4" } },
		{ DEFAULT2, "float4x2", { "float4x2", "float4x2" } },
		{ DEFAULT2, "float4x3", { "float4x3", "float4x3" } },
		{ DEFAULT2, "float4x4", { "float4x4", "float4x4" } },

		// Scalar/Vector + Scalar/Vector
		{ DEFAULT2, GENU, { GENU, GENU } },
		{ DEFAULT2, GENI, { GENI, GENI } },
		{ DEFAULT2, GENF, { GENF, GENF } }
	};
	Ops_["-"] = {
		{ DEFAULT1, GENI, { GENI } }, // Unary
		{ DEFAULT1, GENF, { GENF } }, // Unary

		// Matrix / Matrix
		{ DEFAULT2, "float2x2", { "float2x2", "float2x2" } },
		{ DEFAULT2, "float2x3", { "float2x3", "float2x3" } },
		{ DEFAULT2, "float2x4", { "float2x4", "float2x4" } },
		{ DEFAULT2, "float3x2", { "float3x2", "float3x2" } },
		{ DEFAULT2, "float3x3", { "float3x3", "float3x3" } },
		{ DEFAULT2, "float3x4", { "float3x4", "float3x4" } },
		{ DEFAULT2, "float4x2", { "float4x2", "float4x2" } },
		{ DEFAULT2, "float4x3", { "float4x3", "float4x3" } },
		{ DEFAULT2, "float4x4", { "float4x4", "float4x4" } },

		// Scalar/Vector + Scalar/Vector
		{ DEFAULT2, GENU, { GENU, GENU } },
		{ DEFAULT2, GENI, { GENI, GENI } },
		{ DEFAULT2, GENF, { GENF, GENF } }
	};
	Ops_["%"] = {
		{ DEFAULT2, GENU, { GENU, UINT_T } },
		{ DEFAULT2, GENU, { GENU, GENU } },
		{ DEFAULT2, GENI, { GENI, INT_T } },
		{ DEFAULT2, GENI, { GENI, GENI } },
		{ "(mod($1, $2))", GENF, { GENF, FLOAT_T } },
		{ "(mod($1, $2))", GENF, { GENF, GENF } }
	};
	Ops_["<<"] = {
		{ DEFAULT2, GENU, { GENU, UINT_T } },
		{ DEFAULT2, GENU, { GENU, GENU } },
		{ DEFAULT2, GENI, { GENI, INT_T } },
		{ DEFAULT2, GENI, { GENI, GENI } }
	};
	Ops_[">>"] = {
		{ DEFAULT2, GENU, { GENU, UINT_T } },
		{ DEFAULT2, GENU, { GENU, GENU } },
		{ DEFAULT2, GENI, { GENI, INT_T } },
		{ DEFAULT2, GENI, { GENI, GENI } }
	};
	Ops_["<"] = {
		{ DEFAULT2, BOOL_T, { UINT_T, UINT_T } },
		{ DEFAULT2, BOOL_T, { INT_T, INT_T } },
		{ DEFAULT2, BOOL_T, { FLOAT_T, FLOAT_T } },
		{ "lessThan($1, $2)", GENB, { GENU, GENU } },
		{ "lessThan($1, $2)", GENB, { GENI, GENI } },
		{ "lessThan($1, $2)", GENB, { GENF, GENF } }
	};
	Ops_[">"] = {
		{ DEFAULT2, BOOL_T, { UINT_T, UINT_T } },
		{ DEFAULT2, BOOL_T, { INT_T, INT_T } },
		{ DEFAULT2, BOOL_T, { FLOAT_T, FLOAT_T } },
		{ "greaterThan($1, $2)", GENB, { GENU, GENU } },
		{ "greaterThan($1, $2)", GENB, { GENI, GENI } },
		{ "greaterThan($1, $2)", GENB, { GENF, GENF } }
	};
	Ops_["<="] = {
		{ DEFAULT2, BOOL_T, { UINT_T, UINT_T } },
		{ DEFAULT2, BOOL_T, { INT_T, INT_T } },
		{ DEFAULT2, BOOL_T, { FLOAT_T, FLOAT_T } },
		{ "lessThanEqual($1, $2)", GENB, { GENU, GENU } },
		{ "lessThanEqual($1, $2)", GENB, { GENI, GENI } },
		{ "lessThanEqual($1, $2)", GENB, { GENF, GENF } }
	};
	Ops_[">="] = {
		{ DEFAULT2, BOOL_T, { UINT_T, UINT_T } },
		{ DEFAULT2, BOOL_T, { INT_T, INT_T } },
		{ DEFAULT2, BOOL_T, { FLOAT_T, FLOAT_T } },
		{ "greaterThanEqual($1, $2)", GENB, { GENU, GENU } },
		{ "greaterThanEqual($1, $2)", GENB, { GENI, GENI } },
		{ "greaterThanEqual($1, $2)", GENB, { GENF, GENF } }
	};
	Ops_["=="] = {
		{ DEFAULT2, BOOL_T, { UINT_T, UINT_T } },
		{ DEFAULT2, BOOL_T, { INT_T, INT_T } },
		{ DEFAULT2, BOOL_T, { FLOAT_T, FLOAT_T } },
		{ "equal($1, $2)", GENB, { GENU, GENU } },
		{ "equal($1, $2)", GENB, { GENI, GENI } },
		{ "equal($1, $2)", GENB, { GENF, GENF } }
	};
	Ops_["!="] = {
		{ DEFAULT2, BOOL_T, { UINT_T, UINT_T } },
		{ DEFAULT2, BOOL_T, { INT_T, INT_T } },
		{ DEFAULT2, BOOL_T, { FLOAT_T, FLOAT_T } },
		{ "notEqual($1, $2)", GENB, { GENU, GENU } },
		{ "notEqual($1, $2)", GENB, { GENI, GENI } },
		{ "notEqual($1, $2)", GENB, { GENF, GENF } }
	};
	Ops_["&"] = {
		{ DEFAULT2, GENU, { GENU, UINT_T } },
		{ DEFAULT2, GENU, { GENU, GENU } },
		{ DEFAULT2, GENI, { GENI, INT_T } },
		{ DEFAULT2, GENI, { GENI, GENI } }
	};
	Ops_["|"] = {
		{ DEFAULT2, GENU, { GENU, UINT_T } },
		{ DEFAULT2, GENU, { GENU, GENU } },
		{ DEFAULT2, GENI, { GENI, INT_T } },
		{ DEFAULT2, GENI, { GENI, GENI } }
	};
	Ops_["^"] = {
		{ DEFAULT2, GENU, { GENU, UINT_T } },
		{ DEFAULT2, GENU, { GENU, GENU } },
		{ DEFAULT2, GENI, { GENI, INT_T } },
		{ DEFAULT2, GENI, { GENI, GENI } }
	};
	Ops_["&&"] = {
		{ DEFAULT2, BOOL_T, { BOOL_T, BOOL_T } }
	};
	Ops_["||"] = {
		{ DEFAULT2, BOOL_T, { BOOL_T, BOOL_T } }
	};

	// Ternary
	Ops_["?:"] = {
		{ DEFAULT3, "float2x2", { BOOL_T, "float2x2", "float2x2" } },
		{ DEFAULT3, "float2x3", { BOOL_T, "float2x3", "float2x3" } },
		{ DEFAULT3, "float2x4", { BOOL_T, "float2x4", "float2x4" } },
		{ DEFAULT3, "float3x2", { BOOL_T, "float3x2", "float3x2" } },
		{ DEFAULT3, "float3x3", { BOOL_T, "float3x3", "float3x3" } },
		{ DEFAULT3, "float3x4", { BOOL_T, "float3x4", "float3x4" } },
		{ DEFAULT3, "float4x2", { BOOL_T, "float4x2", "float4x2" } },
		{ DEFAULT3, "float4x3", { BOOL_T, "float4x3", "float4x3" } },
		{ DEFAULT3, "float4x4", { BOOL_T, "float4x4", "float4x4" } },

		{ DEFAULT3, GENU, { BOOL_T, GENU, GENU } },
		{ DEFAULT3, GENI, { BOOL_T, GENI, GENI } },
		{ DEFAULT3, GENF, { BOOL_T, GENF, GENF } }
	};

	FinishedInit_ = true;
}

} // namespace vsl
