/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

#include "../Common.Impl.hpp"

#include <antlr4/BaseErrorListener.h>


namespace vsl
{

class Parser;

// Error listener for parsing operations in the Parser class
class ErrorListener final
	: public antlr4::BaseErrorListener
{
public:
	ErrorListener(Parser* parser) : parser_{ parser } { }
	~ErrorListener() { }

	void syntaxError(antlr4::Recognizer* recognizer, antlr4::Token* badToken, size_t line, size_t charPosition,
		const std::string& msg, std::exception_ptr e) override;

private:
	Parser* const parser_;

	VSL_NO_COPY(ErrorListener)
	VSL_NO_MOVE(ErrorListener)
}; // class ErrorListener

} // namespace vsl
