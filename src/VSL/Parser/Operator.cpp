/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "./Operator.hpp"


namespace vsl
{

// ====================================================================================================================
OpType::OpType(const String& typeName)
	: type{ nullptr }
	, genType{ false }
{
	// Get the type or gentype
	if (typeName == "genType") {
		type = ShaderType::Builtin::GetType("float");
		genType = true;
	}
	else if (typeName == "genIType") {
		type = ShaderType::Builtin::GetType("int");
		genType = true;
	}
	else if (typeName == "genUType") {
		type = ShaderType::Builtin::GetType("uint");
		genType = true;
	}
	else if (typeName == "genBType") {
		type = ShaderType::Builtin::GetType("bool");
		genType = true;
	}
	else {
		type = ShaderType::Builtin::GetType(typeName);
		assert(type && "Failed to find builtin type for OpType");
		genType = false;
	}
}

// ====================================================================================================================
bool OpType::Match(const Expr& expr) const
{
	const auto etype = expr.type;
	if (expr.arraySize != 1) {
		return false; // No operators take arrays as arguments
	}
	if (genType) {
		const auto casttype = ShaderType::Builtin::GetNumericType(
			type->baseType, etype->numeric.size, etype->numeric.dims[0], etype->numeric.dims[1]
		);
		return etype->HasImplicitCast(*casttype);
	}
	else {
		return (type->IsNumericType() || type->IsBoolean()) && etype->HasImplicitCast(*type);
	}
}


// ====================================================================================================================
// ====================================================================================================================
OpEntry::OpEntry(const String& genStr, const OpType& retType, const std::vector<OpType>& args)
	: genStr{ genStr }, retType{ retType }, argTypes{ args }
{

}

// ====================================================================================================================
const ShaderType* OpEntry::Match(const std::vector<const Expr*>& params) const
{
	// Count check
	if (params.size() != argTypes.size()) {
		return nullptr;
	}

	// Per-arg check
	uint32 genSize{ 0 };
	uint32 genCount{ 0 };
	BaseType genType{ BaseType::Void };
	for (uint32 i = 0; i < argTypes.size(); ++i) {
		const auto& ai = argTypes[i];
		const auto& pi = params[i];
		if (!ai.Match(*pi)) {
			return nullptr;
		}
		if (ai.genType) {
			if (genSize == 0) {
				genSize = pi->type->numeric.size;
				genCount = pi->type->numeric.dims[0];
				genType = pi->type->baseType;
			}
			else if (genCount != pi->type->numeric.dims[0]) {
				return nullptr; // Gen types must match sizes
			}
		}
	}

	// Return the correct type
	const auto retSize =
		(!retType.genType || (genSize == 0)) ? retType.type->numeric.size :
		(retType.type->baseType != genType) ? retType.type->numeric.size : genSize;
	return retType.genType
		? ShaderType::Builtin::GetNumericType(retType.type->baseType, retSize, genCount, 1)
		: retType.type;
}

// ====================================================================================================================
String OpEntry::GenerateString(const String& op, const std::vector<const Expr*>& params) const
{
	static const String STROP{ "$op" };
	static const String STR1{ "$1" };
	static const String STR2{ "$2" };
	static const String STR3{ "$3" };

	String gen{ genStr };
	if (const auto posOp = gen.find(STROP); posOp != String::npos) {
		gen.replace(posOp, 3, op);
	}
	if (const auto pos1 = gen.find(STR1); pos1 != String::npos) {
		gen.replace(pos1, 2, params[0]->refString);
	}
	if (params.size() >= 2) {
		if (const auto pos2 = gen.find(STR2); pos2 != String::npos) {
			gen.replace(pos2, 2, params[1]->refString);
		}
	}
	if (params.size() >= 3) {
		if (const auto pos3 = gen.find(STR3); pos3 != String::npos) {
			gen.replace(pos3, 2, params[2]->refString);
		}
	}
	return gen;
}


// ====================================================================================================================
// ====================================================================================================================
std::unordered_map<String, std::vector<OpEntry>> Operators::Ops_{ };

// ====================================================================================================================
std::tuple<const ShaderType*, String> Operators::Check(const String& op, const std::vector<const Expr*>& args)
{
	Initialize();
	
	const auto it = Ops_.find(op);
	if (it == Ops_.end()) {
		return { nullptr, FmtStr("No operator '%s' found", op.c_str()) };
	}

	for (const auto& entry : it->second) {
		const auto match = entry.Match(args);
		if (match) {
			return { match, entry.GenerateString(op, args) };
		}
	}

	// Prepare types list for error msg
	String types{};
	for (const auto& expr : args) {
		types += (expr->type->GetVSLName() + ", ");
	}
	types = types.substr(0, types.length() - 2);

	return { 
		nullptr, 
		FmtStr("No overload of operator '%s' matched the given argument types (%s)", op.c_str(), types.c_str())
	};
}

} // namespace vsl
