/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

#include "../Common.Impl.hpp"
#include "../Types.Builtin.hpp"

#include <limits>


namespace vsl
{

// Represents a literal numeric value from VSL source
struct Literal final
{
public:
	Literal() : u{ 0 }, type{ Unsigned } { }
	Literal(uint64_t val) : u{ val }, type{ Unsigned } { }
	Literal(int64_t val) : i{ val }, type{ Signed } { }
	Literal(double val) : f{ val }, type{ Float } { }

	inline bool IsNegative() const {
		return (type == Float) ? f < 0 : (type == Signed) ? i < 0 : false;
	}
	inline bool IsZero() const { 
		return u == 0 || (std::abs(f) < std::numeric_limits<double>::epsilon());
	}

	static std::optional<Literal> Parse(const String& text, String* error);

public:
	union {
		uint64 u; // The unsigned parsed literal
		int64  i; // The signed parsed literal
		double f; // The floating point parsed literal
	};
	enum : uint32 {
		Unsigned,
		Signed,
		Float
	} type;
}; // struct Literal


// Describes the result of an expression node in a program AST
class Expr final
{
public:
	Expr() : refString{ }, type{ nullptr }, arraySize{ } { }
	Expr(const String& refString, const ShaderType* type, uint32 arraySize)
		: refString{ refString }, type{ type }, arraySize{ arraySize }
	{ }

public:
	String refString;
	const ShaderType* type;
	uint32 arraySize;
}; // class Expr

} // namespace vsl
