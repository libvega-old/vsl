/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "./Parser.hpp"
#include "./ErrorListener.hpp"
#include "../Grammar/VSLLexer.h"
#include "./Function.hpp"

#include <algorithm>

#define SINFO (parse_.info)
#define STYPES (parse_.info->types_)
#define SCOPES (parse_.scopes)


namespace vsl
{

// ====================================================================================================================
Parser::Parser(const CompilerOptions& options)
	: options_{ &options }
	, error_{ std::nullopt }
	, parse_{ }
{

}

// ====================================================================================================================
Parser::~Parser()
{
	
}

// ====================================================================================================================
bool Parser::Parse(const String& source, ShaderInfo& info, std::vector<UPtr<FunctionGenerator>>& functionGens)
{
	parse_.info = &info;
	parse_.scopes = std::make_unique<ScopeManager>();
	parse_.functions = &functionGens;
	functionGens.clear();

	// Create the lexer and parser objects
	antlr4::ANTLRInputStream inStream{ source };
	grammar::VSLLexer lexer{ &inStream };
	antlr4::CommonTokenStream tokens{ &lexer };
	grammar::VSL parser{ &tokens };
	parse_.tokens = &tokens;

	// Install parser error listener
	ErrorListener listener{ this };
	lexer.removeErrorListeners();
	parser.removeErrorListeners();
	lexer.addErrorListener(&listener);
	parser.addErrorListener(&listener);

	// Perform parsing
	bool result{ false };
	const auto fileCtx = parser.file();
	if (HasError()) {
		goto end_parse;
	}

	// Perform the visitor walk
	try {
		const auto _ = visit(fileCtx);
	}
	catch (const CompilerError& err) {
		error_.emplace(err.Message(), err.Line(), err.Character(), err.BadText());
		goto end_parse;
	}
#if !defined(VSL_DEBUG)
	catch (const std::exception& ex) {
		error_.emplace(FmtStr("Unhandled exception - %s", ex.what()));
		goto end_parse;
	}
#endif // !defined(VSL_DEBUG)

	// Perform sorting of the reflection info based on global binding index
	std::sort(SINFO->inputs_.begin(), SINFO->inputs_.end(), 
		[](const InputVariable& l, const InputVariable& r) { return l.location < r.location; });
	std::sort(SINFO->bindings_.begin(), SINFO->bindings_.end(), 
		[](const BindingVariable& l, const BindingVariable& r) { return l.slot < r.slot; });
	std::sort(SINFO->subpassInputs_.begin(), SINFO->subpassInputs_.end(),
		[](const SubpassInputVariable& l, const SubpassInputVariable& r) { return l.index < r.index; });

	// Cleanup and return
	result = true;
end_parse:
	parse_ = { };
	return result;
}

// ====================================================================================================================
void Parser::ValidateNewName(const antlr4::Token* name)
{
	const auto varName = name->getText();
	if (varName[0] == '$') {
		ERROR(name, "Identifiers starting with '$' are reserved for builtin variables");
	}
	if (varName.length() > Shader::MAX_NAME_LENGTH) {
		ERROR(name, FmtStr("Variable names cannot be longer than %u bytes", Shader::MAX_NAME_LENGTH));
	}
	if ((varName[0] == '_') && (*varName.rbegin() == '_')) {
		ERROR(name, "Names that start and end with '_' are reserved");
	}
	if (STYPES->GetType(varName)) {
		ERROR(name, FmtStr("Variable name '%s' overlaps with type name", varName.c_str()));
	}
	if (SCOPES->HasGlobal(varName) || SCOPES->HasName(varName)) {
		ERROR(name, FmtStr("Duplicate variable name '%s'", varName.c_str()));
	}
	if (Functions::HasFunction(varName)) {
		ERROR(name, FmtStr("Variable name '%s' overlaps with function name", varName.c_str()));
	}
	if (STYPES->GetType(varName)) {
		ERROR(name, FmtStr("Variable name '%s' overlaps with type name", varName.c_str()));
	}
}

// ====================================================================================================================
Variable Parser::ParseVariableDeclaration(const grammar::VSL::VariableDeclarationContext* ctx)
{
	// Perform name validation
	ValidateNewName(ctx->name);

	// Get type
	const auto typeName = ctx->baseType->getText() + (ctx->subType ? '<' + ctx->subType->getText() + '>' : "");
	const auto vType = STYPES->ParseOrGetType(typeName);
	if (!vType) {
		ERROR(ctx->baseType, FmtStr("Unknown type: %s", STYPES->Error().c_str()));
	}

	// Get array size
	uint32 arrSize = 1;
	if (ctx->arraySize) {
		const auto arrSizeLiteral = Literal::Parse(ctx->arraySize->getText(), nullptr);
		if (!arrSizeLiteral.has_value()) {
			ERROR(ctx->arraySize, "Invalid array size literal");
		}
		if (arrSizeLiteral.value().IsNegative() || arrSizeLiteral.value().IsZero()) {
			ERROR(ctx->arraySize, "Array size cannot be zero or negative");
		}
		if (arrSizeLiteral.value().u > Shader::MAX_ARRAY_SIZE) {
			ERROR(ctx->arraySize, FmtStr("Array is larger than max allowed size %u", Shader::MAX_ARRAY_SIZE));
		}
		arrSize = uint32(arrSizeLiteral.value().u);
	}

	// Type-specific checks
	if (!vType->IsNumericType() && !vType->IsBoolean()) { // Handle types
		if (arrSize != 1) {
			ERROR(ctx->arraySize, "Non-numeric types cannot be arrays");
		}
	}

	// Return
	return { ctx->name->getText(), Variable::Use::Unknown, Variable::Access::ReadWrite, vType, arrSize };
}

// ====================================================================================================================
Blending Parser::ParseBlending(const grammar::VSL::BlendStatementContext* ctx)
{
	Blending blending{};

	// Parse the color blending
	const auto srcColorText = ctx->srcColor->getText();
	const auto dstColorText = ctx->dstColor->getText();
	const auto colorOpText = ctx->colorOp ? ctx->colorOp->getText() : "add";
	const auto parseSrcColor = Blending::ParseFactor(srcColorText);
	if (!parseSrcColor.has_value()) {
		ERROR(ctx->srcColor, FmtStr("Unknown blending factor '%s'", srcColorText.c_str()));
	}
	const auto parseDstColor = Blending::ParseFactor(dstColorText);
	if (!parseDstColor.has_value()) {
		ERROR(ctx->dstColor, FmtStr("Unknown blending factor '%s'", dstColorText.c_str()));
	}
	const auto parseColorOp = Blending::ParseOp(colorOpText);
	if (!parseColorOp.has_value()) {
		ERROR(ctx->colorOp, FmtStr("Unknown blending op '%s'", colorOpText.c_str()));
	}
	blending.srcColor = parseSrcColor.value();
	blending.dstColor = parseDstColor.value();
	blending.colorOp = parseColorOp.value();

	// Parse the alpha blending
	if (ctx->srcAlpha) {
		const auto srcAlphaText = ctx->srcAlpha->getText();
		const auto dstAlphaText = ctx->dstAlpha->getText();
		const auto alphaOpText = ctx->alphaOp ? ctx->alphaOp->getText() : "add";
		const auto parseSrcAlpha = Blending::ParseFactor(srcAlphaText);
		if (!parseSrcAlpha.has_value()) {
			ERROR(ctx->srcAlpha, FmtStr("Unknown blending factor '%s'", srcAlphaText.c_str()));
		}
		const auto parseDstAlpha = Blending::ParseFactor(dstAlphaText);
		if (!parseDstAlpha.has_value()) {
			ERROR(ctx->dstAlpha, FmtStr("Unknown blending factor '%s'", dstAlphaText.c_str()));
		}
		const auto parseAlphaOp = Blending::ParseOp(alphaOpText);
		if (!parseAlphaOp.has_value()) {
			ERROR(ctx->alphaOp, FmtStr("Unknown blending op '%s'", alphaOpText.c_str()));
		}
		blending.srcAlpha = parseSrcAlpha.value();
		blending.dstAlpha = parseDstAlpha.value();
		blending.alphaOp = parseAlphaOp.value();
	}
	else {
		blending.srcAlpha = blending.srcColor;
		blending.dstAlpha = blending.dstColor;
		blending.alphaOp = blending.colorOp;
	}
	
	return blending;
}

// ====================================================================================================================
void Parser::ValidateSwizzle(uint32 compCount, antlr4::tree::TerminalNode* swizzle)
{
	// Check length
	const auto swtxt = swizzle->getText();
	if (swtxt.length() > 4) {
		ERROR(swizzle, "Swizzles have a max length of 4");
	}

	// Check each character
	uint32 cclass = UINT32_MAX;
	for (const auto ch : swizzle->getText()) {
		uint32 idx = UINT32_MAX;
		uint32 cc = UINT32_MAX;

		switch (ch)
		{
		case 'x': case 'r': case 's': idx = 1; break;
		case 'y': case 'g': case 't': idx = 2; break;
		case 'z': case 'b': case 'p': idx = 3; break;
		case 'w': case 'a': case 'q': idx = 4; break;
		}
		if (idx > compCount) {
			ERROR(swizzle, FmtStr("Invalid swizzle character '%c' for vector size", ch));
		}

		switch (ch)
		{
		case 'x': case 'y': case 'z': case 'w': cc = 1; break;
		case 'r': case 'g': case 'b': case 'a': cc = 2; break;
		case 's': case 't': case 'p': case 'q': cc = 3; break;
		}
		if (cclass != UINT32_MAX) {
			if (cc != cclass) {
				const auto expect = (cclass == 1) ? "xyzw" : (cclass == 2) ? "rgba" : "stpq";
				ERROR(swizzle, FmtStr("Swizzle class mismatch for character '%c', expected one of '%s'", ch, expect));
			}
		}
		else {
			cclass = cc;
		}
	}
}

} // namespace vsl
