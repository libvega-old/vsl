/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

#include "../Common.Impl.hpp"
#include "../Types.Builtin.hpp"
#include "../Reflection.hpp"

#include <unordered_map>
#include <vector>


namespace vsl
{

// Represents a specific shader variable within a scope
class Variable final
{
public:
	/// The variable use class
	enum class Use : uint32
	{
		Unknown = 0, // Intermediate or error value
		Input,       // Vertex shader input
		Output,      // Fragment shader output
		Binding,     // Uniform resource binding
		Builtin,     // Stage-specific builtin
		Constant,    // Specialization constant
		Local,       // Local value passed between stages
		Parameter,   // Parameter to a function
		Private      // Private within a function scope
	}; // enum class Use
	/// The variable read/write access
	enum class Access : uint32
	{
		ReadOnly,
		WriteOnly,
		ReadWrite
	}; // enum class Access

public:
	Variable() : name{ "INVALID" }, use{ }, access{ }, type{ nullptr }, arraySize{ }, extra{ } { }
	Variable(const String& name, Use use, Access access, const ShaderType* type, uint32 arraySize)
		: name{ name }, use{ use }, access{ access }, type{ type }, arraySize{ arraySize }, extra{ }
	{ }

	inline bool CanRead(ShaderStages stage) const {
		return (use == Use::Local) ? (stage != extra.local.sourceStage) : (access != Access::WriteOnly);
	}
	inline bool CanWrite(ShaderStages stage) const {
		return (use == Use::Local) ? (stage == extra.local.sourceStage) : (access != Access::ReadOnly);
	}

public:
	String name;
	Use use;
	Access access;
	const ShaderType* type;
	uint32 arraySize;
	union 
	{
		struct
		{
			InputSemantic semantic;
		} input;
		struct
		{
			ShaderStages sourceStage;
			bool flat;
		} local;
		struct
		{
			uint32 slot;
		} binding;
	} extra;

	VSL_NO_MOVE(Variable)
	VSL_DEFAULT_COPY(Variable)
}; // class Variable


// Manages the tree of scopes, including the root scope, for a specific Parser operation, as well as global variables
class ScopeManager final
{
public:
	/// Scope types, used to check validity for certain operations
	enum class ScopeType
	{
		Root,        // Special root program scope
		Function,    // Function
		Conditional, // if/elif/else
		Loop         // Looping scope
	}; // enum class ScopeType

private:
	/// Manages the set of variables within a specific program scope
	class Scope final
	{
	public:
		Scope(ScopeType type, Scope* parent);
		~Scope();

		bool HasVariable(const String& name) const;
		bool HasAssignment(const String& name) const;
		std::optional<String> GetMissingAssignment() const;

	public:
		const ScopeType type;
		Scope* const parent;
		std::vector<UPtr<Scope>> children;
		std::unordered_map<String, Variable> variables;
		std::vector<String> needsAssignment;
		std::vector<String> hasAssignment;

		VSL_NO_MOVE(Scope)
		VSL_NO_COPY(Scope)
	}; // class Scope

public:
	ScopeManager();
	~ScopeManager();

	// Scope Push/Pop
	void PushFunctionScope(ShaderStages stage);
	void PushConditionalScope();
	void PushLoopScope();
	[[nodiscard]] std::optional<String> PopScope();

	// Globals
	bool HasGlobal(const String& name) const;
	const Variable* AddGlobal(const Variable& var);

	// Variables
	bool HasName(const String& name) const;
	const Variable* GetVariable(const String& name) const;
	const Variable* AddVariable(const Variable& var);
	void MarkRequiredAssignment(const String& name);

	// Scope Info
	bool InLoop() const;

private:
	UPtr<Scope> rootScope_;
	Scope* currentScope_;
	std::unordered_map<String, Variable> globals_;

	VSL_NO_MOVE(ScopeManager)
	VSL_NO_COPY(ScopeManager)
}; // class ScopeManager

} // namespace vsl
