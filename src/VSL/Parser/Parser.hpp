/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

#include "../Common.Impl.hpp"
#include <VSL/Compiler.hpp>
#include "./Variable.hpp"
#include "./Expr.hpp"
#include "../Generator/FunctionGenerator.hpp"
#include "../Reflection.hpp"

#include "../Grammar/VSLBaseVisitor.h"

#include <antlr4/CommonTokenStream.h>
#include <antlr4/RuleContext.h>
#include <antlr4/Token.h>
#include <antlr4/tree/TerminalNode.h>


namespace vsl
{

// Parser implementation for VSL
class Parser final
	: public grammar::VSLBaseVisitor
{
	friend class ErrorListener;

public:
	Parser(const CompilerOptions& options);
	~Parser();

	inline bool HasError() const { return error_.has_value(); }
	inline const CompilerError* Error() const { return &(error_.value()); }

	/// Performs the full parsing, checking, and generation for the code - does not perform final SPIR-V compilation
	bool Parse(const String& source, ShaderInfo& info, std::vector<UPtr<FunctionGenerator>>& functionGens);

#define VISIT_DECL(type) antlrcpp::Any visit##type(grammar::VSL::type##Context* ctx) override;

	/* Top-Level File Rules */
	VISIT_DECL(File)
	VISIT_DECL(ShaderTypeStatement)
	VISIT_DECL(ShaderStructDefinition)
	VISIT_DECL(ShaderUniformStatement)
	VISIT_DECL(ShaderBindingStatement)
	VISIT_DECL(ShaderLocalStatement)
	VISIT_DECL(ShaderSubpassInputStatement)
	VISIT_DECL(VertexFunction)
	VISIT_DECL(FragmentFunction)

	/* Statement Rules */
	VISIT_DECL(Statement)
	VISIT_DECL(VariableDefinition)
	VISIT_DECL(VariableDeclaration)
	VISIT_DECL(Assignment)
	VISIT_DECL(Lvalue)
	VISIT_DECL(IfStatement)
	VISIT_DECL(ElifStatement)
	VISIT_DECL(ElseStatement)
	VISIT_DECL(ForLoopStatement)
	VISIT_DECL(ControlStatement)

	/* Expression Rules */
	VISIT_DECL(FactorExpr)
	VISIT_DECL(NegateExpr)
	VISIT_DECL(MulDivModExpr)
	VISIT_DECL(AddSubExpr)
	VISIT_DECL(ShiftExpr)
	VISIT_DECL(RelationalExpr)
	VISIT_DECL(EqualityExpr)
	VISIT_DECL(BitwiseExpr)
	VISIT_DECL(LogicalExpr)
	VISIT_DECL(TernaryExpr)
	VISIT_DECL(GroupAtom)
	VISIT_DECL(IndexAtom)
	VISIT_DECL(MemberAtom)
	VISIT_DECL(CallAtom)
	VISIT_DECL(LiteralAtom)
	VISIT_DECL(NameAtom)

#undef VISIT_DECL

	void ValidateNewName(const antlr4::Token* name);
	Variable ParseVariableDeclaration(const grammar::VSL::VariableDeclarationContext* ctx);
	Blending ParseBlending(const grammar::VSL::BlendStatementContext* ctx);
	void ValidateSwizzle(uint32 compCount, antlr4::tree::TerminalNode* swizzle);

private:
	[[noreturn]] inline void ERROR(const antlr4::Token* tk, const String& msg) const {
		CompilerError err{ msg, uint32(tk->getLine()), uint32(tk->getCharPositionInLine()), tk->getText() };
		throw err;
	}
	[[noreturn]] inline void ERROR(antlr4::RuleContext* ctx, const String& msg) const {
		const auto tk = parse_.tokens->get(ctx->getSourceInterval().a);
		CompilerError err{ msg, uint32(tk->getLine()), uint32(tk->getCharPositionInLine()), tk->getText() };
		throw err;
	}
	[[noreturn]] inline void ERROR(antlr4::tree::TerminalNode* node, const String& msg) const {
		const auto tk = parse_.tokens->get(node->getSourceInterval().a);
		CompilerError err{ msg, uint32(tk->getLine()), uint32(tk->getCharPositionInLine()), tk->getText() };
		throw err;
	}

private:
	const CompilerOptions* const options_;
	std::optional<CompilerError> error_;
	struct
	{
		antlr4::CommonTokenStream* tokens{ nullptr };
		ShaderInfo* info{ nullptr };
		UPtr<ScopeManager> scopes{ };
		std::vector<UPtr<FunctionGenerator>>* functions{ nullptr };
		FunctionGenerator* currentFunction{ nullptr };
		ShaderStages currentStage{ ShaderStages::None };
	} parse_; // Fields in this struct are only valid inside the Parse() function

	VSL_NO_COPY(Parser)
	VSL_NO_MOVE(Parser)
};

} // namespace vsl
