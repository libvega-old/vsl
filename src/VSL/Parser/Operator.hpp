/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

#include "../Common.Impl.hpp"
#include "../Types.Builtin.hpp"
#include "./Expr.hpp"

#include <unordered_map>


namespace vsl
{

// Special type describing object that can represent an operation parameter or return type
//    Supports the concept of genType/genIType/genUType/genBType
struct OpType final
{
public:
	inline static const String GENTYPE{ "genType" };
	inline static const String GENITYPE{ "genIType" };
	inline static const String GENUTYPE{ "genUType" };
	inline static const String GENBTYPE{ "genBType" };

	OpType() : type{ nullptr }, genType{ false } { }
	OpType(const String& typeName);
	OpType(const char* const typeName) : OpType(String{ typeName }) { }

	bool Match(const Expr& expr) const;

public:
	const ShaderType* type; // The argument type (full type or just base type)
	bool genType;
}; // struct OpType


/// Describes a specific version (operand type set) of an operator
class OpEntry final
{
public:
	OpEntry() : genStr{ "INVALID" }, retType{}, argTypes{} {}
	OpEntry(const String& genStr, const OpType& retType, const std::vector<OpType>& args);

	const ShaderType* Match(const std::vector<const Expr*>& params) const;
	String GenerateString(const String& op, const std::vector<const Expr*>& params) const;

public:
	String genStr; // Output generated string (with $1, $2, $3 for operands and $op for operator)
	OpType retType;
	std::vector<OpType> argTypes;
}; // class OpEntry


/// Contains the registry of operators and functionality related to operators
class Operators final
{
public:
	/// Operator Checking
	///    On success, return type is [op type, gererated expr string]
	///    On error, return type is [nullptr, error message]
	static std::tuple<const ShaderType*, String> Check(const String& op, const std::vector<const Expr*>& args);

private:
	static void Initialize();

private:
	static std::unordered_map<String, std::vector<OpEntry>> Ops_;

	VSL_NO_COPY(Operators)
	VSL_NO_MOVE(Operators)
	VSL_NO_INIT(Operators)
}; // class Operators

} // namespace vsl
