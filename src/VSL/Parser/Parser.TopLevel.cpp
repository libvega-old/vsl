/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "./Parser.hpp"

#define VISIT_FUNC(type) antlrcpp::Any Parser::visit##type(grammar::VSL::type##Context* ctx)
#define SINFO (parse_.info)
#define STYPES (parse_.info->types_)
#define SCOPES (parse_.scopes)



namespace vsl
{

// ====================================================================================================================
VISIT_FUNC(File)
{
	// Visit the shader type statement
	visit(ctx->shaderTypeStatement());

	// Visit all top level statements
	for (const auto tls : ctx->topLevelStatement()) {
		visit(tls);
	}

	// Perform whole-shader checks
	if (!bool(SINFO->Stages() & ShaderStages::Vertex)) {
		ERROR(ctx, "Shader is missing required vertex stage function");
	}
	if (!bool(SINFO->Stages() & ShaderStages::Fragment)) {
		ERROR(ctx, "Shader is missing required fragment stage function");
	}

	return nullptr;
}

// ====================================================================================================================
VISIT_FUNC(ShaderTypeStatement)
{
	// Validate the shader type
	const auto shaderType = ctx->type->getText();
	if (shaderType == "graphics") {
		return nullptr; // Only graphics supported for now
	}
	else if (shaderType == "compute") {
		ERROR(ctx->type, "Compute shaders are not yet supported");
	}
	else if (shaderType == "ray") {
		ERROR(ctx->type, "Ray shaders are not yet supported");
	}
	else {
		ERROR(ctx->type, FmtStr("Unknown shader type '%s'", shaderType.c_str()));
	}

	return nullptr;
}

// ====================================================================================================================
VISIT_FUNC(ShaderStructDefinition)
{
	// Check parse status
	if (bool(SINFO->Stages())) {
		ERROR(ctx, "All user-defined types must be provided before the first stage function");
	}

	// Get type name and check
	const auto typeName = ctx->name->getText();
	if (SINFO->Types()->GetType(typeName)) {
		ERROR(ctx->name, FmtStr("Duplicate type name '%s'", typeName.c_str()));
	}
	if (typeName.length() > Shader::MAX_NAME_LENGTH) {
		ERROR(ctx->name, FmtStr("Type names cannot be longer than %u characters", Shader::MAX_NAME_LENGTH));
	}
	if (typeName[0] == '_' && *(typeName.rbegin()) == '_') {
		ERROR(ctx->name, "Type names that start and end with '_' are reserved");
	}

	// Parse the field declarations
	std::vector<StructType::Member> members{};
	std::vector<String> names{};
	for (const auto field : ctx->variableDeclaration()) {
		// Create variable
		const auto fVar = ParseVariableDeclaration(field);

		// Check name
		if (std::find(names.begin(), names.end(), fVar.name) != names.end()) {
			ERROR(field->name, FmtStr("Duplicate struct field '%s'", fVar.name.c_str()));
		}

		// Check field type
		if (!fVar.type->IsNumericType() && !fVar.type->IsBoolean()) {
			ERROR(field->baseType, FmtStr("Struct field '%s' must be numeric", fVar.name.c_str()));
		}

		// Add the member
		StructType::Member member{};
		member.name = fVar.name;
		member.arraySize = fVar.arraySize;
		member.type = fVar.type;
		members.push_back(member);
		names.push_back(fVar.name);
	}
	if (members.size() == 0) {
		ERROR(ctx->name, "Empty struct types are not allowed");
	}
	StructType structType{ typeName, members };
	if (structType.Size() > Shader::MAX_STRUCT_SIZE) {
		ERROR(ctx->name, FmtStr("Struct types cannot be larger than %u bytes", Shader::MAX_STRUCT_SIZE));
	}

	// Add the struct type
	STYPES->AddStructType(typeName, structType);

	return nullptr;
}

// ====================================================================================================================
VISIT_FUNC(ShaderUniformStatement)
{
	// Status validation
	if (bool(SINFO->Stages())) {
		ERROR(ctx, "Uniform values must be specified before first stage function");
	}
	if (SINFO->Uniform().has_value()) {
		ERROR(ctx, "Only one uniform block per shader is allowed");
	}

	// Check the name
	const auto uniformName = ctx->name->getText();
	if (SCOPES->HasGlobal(uniformName)) {
		ERROR(ctx->name, FmtStr("The name '%s' is already in use", uniformName.c_str()));
	}

	// Parse the field declarations
	std::vector<StructType::Member> members{};
	std::vector<String> names{};
	for (const auto field : ctx->variableDeclaration()) {
		// Create variable
		const auto fVar = ParseVariableDeclaration(field);

		// Check name
		if (std::find(names.begin(), names.end(), fVar.name) != names.end()) {
			ERROR(field->name, FmtStr("Duplicate uniform field '%s'", fVar.name.c_str()));
		}

		// Check field type
		if (!fVar.type->IsNumericType() && !fVar.type->IsBoolean()) {
			ERROR(field->baseType, FmtStr("Uniform field '%s' must be numeric", fVar.name.c_str()));
		}

		// Add the member
		StructType::Member member{};
		member.name = fVar.name;
		member.arraySize = fVar.arraySize;
		member.type = fVar.type;
		members.push_back(member);
		names.push_back(fVar.name);
	}
	if (members.size() == 0) {
		ERROR(ctx->name, "Empty uniform blocks are not allowed");
	}

	// Add the struct type
	StructType structType{ "_UNIFORM_", members };
	if (structType.Size() > Shader::MAX_UNIFORM_SIZE) {
		ERROR(ctx->name, FmtStr("Shader uniform data cannot be larger than %u bytes", Shader::MAX_UNIFORM_SIZE));
	}
	const auto structPtr = STYPES->AddStructType(structType.Name(), structType);

	// Add to the shader info and scope
	SINFO->uniform_ = BindingVariable{ uniformName, structPtr, 0, ShaderStages::None};
	SCOPES->AddGlobal({ uniformName, Variable::Use::Binding, Variable::Access::ReadOnly, structPtr, 1 });

	return nullptr;
}

// ====================================================================================================================
VISIT_FUNC(ShaderBindingStatement)
{
	// Check parse status
	if (bool(SINFO->Stages())) {
		ERROR(ctx, "All bindings must be provided before the first stage function");
	}

	// Check for binding limit
	if (SINFO->Bindings().size() == Shader::MAX_BINDINGS) {
		ERROR(ctx, FmtStr("Cannot have more than %u bindings in a shader", Shader::MAX_BINDINGS));
	}

	// Parse the variable declaration
	const auto varDecl = ctx->variableDeclaration();
	const auto bVar = ParseVariableDeclaration(varDecl);
	if (bVar.type->IsNumericType() || bVar.type->IsBoolean() || bVar.type->IsStruct()) {
		ERROR(varDecl->baseType, "Bindings cannot be numeric, boolean, or struct types");
	}
	if (bVar.arraySize != 1) {
		ERROR(varDecl->arraySize, "Bindings cannot be arrays");
	}

	// TEMPORARY: Disable the bindings that are unsupported by Vega
	// TODO: Add these in as they are developed within Vega
	if (bVar.type->IsImage()) {
		ERROR(varDecl->baseType, "Read/Write images are not yet supported");
	}
	if (bVar.type->IsRWBuffer()) {
		ERROR(varDecl->baseType, "Read/Write storage buffers are not yet supported");
	}
	if (bVar.type->IsRWTexels()) {
		ERROR(varDecl->baseType, "Read/Write texel buffers are not yet supported");
	}
	if (bVar.type->IsSampler()) {
		if (bVar.type->texel.rank == TexelRank::E1DArray || bVar.type->texel.rank == TexelRank::E2DArray) {
			ERROR(varDecl->baseType, "Arrayed samplers are not yet supported");
		}
		if (bVar.type->texel.rank == TexelRank::Cube) {
			ERROR(varDecl->baseType, "Cube samplers are not yet supported");
		}
	}

	// Get the binding slot
	const auto slotLiteral = Literal::Parse(ctx->slot->getText(), nullptr);
	if (!slotLiteral.has_value()) {
		ERROR(ctx->slot, "Invalid binding slot index literal");
	}
	if (slotLiteral.value().IsNegative() || (slotLiteral.value().type == Literal::Float)) {
		ERROR(ctx->slot, "Binding slot index must be non-negative integer");
	}
	else if (slotLiteral.value().u >= Shader::MAX_BINDINGS) {
		ERROR(ctx->slot, FmtStr("Slot index out of range (max %u)", Shader::MAX_BINDINGS - 1));
	}
	const auto slotIndex = uint32(slotLiteral.value().u);
	const auto existing = SINFO->GetBinding(slotIndex);
	if (existing) {
		ERROR(ctx->slot, FmtStr("Binding slot %u is already populated by '%s'", slotIndex, existing->name.c_str()));
	}

	// Add to info and scope
	SINFO->bindings_.push_back({ bVar.name, bVar.type, slotIndex, ShaderStages::None });
	const auto canWrite =
		(bVar.type->IsImage() || bVar.type->IsRWBuffer() || bVar.type->IsRWTexels());
	Variable var{
		bVar.name, Variable::Use::Binding, canWrite ? Variable::Access::ReadWrite : Variable::Access::ReadOnly, 
		bVar.type, 1,
	};
	var.extra.binding.slot = slotIndex;
	SCOPES->AddGlobal(var);

	return nullptr;
}

// ====================================================================================================================
VISIT_FUNC(ShaderLocalStatement)
{
	// Check parse status
	if (bool(SINFO->Stages())) {
		ERROR(ctx, "All locals must be provided before the first stage function");
	}

	// Parse and validate variable
	const auto isFlat = !!ctx->KW_FLAT();
	const auto varDecl = ctx->variableDeclaration();
	const auto lVar = ParseVariableDeclaration(varDecl);
	if (lVar.arraySize != 1) {
		ERROR(varDecl->arraySize, "Shader locals cannot be arrays");
	}
	if (!lVar.type->IsNumericType() || lVar.type->IsMatrix()) {
		ERROR(varDecl->baseType, "Shader locals must be numeric scalars or vectors");
	}
	if (lVar.type->IsInteger() && !isFlat) {
		ERROR(varDecl->baseType, "Shader locals with integer types must be declared as 'flat'");
	}

	// Parse and validate stages
	const auto pStage = FromStageCodeName(ctx->pstage->getText());
	if (pStage == ShaderStages::None) {
		ERROR(ctx->pstage, FmtStr("Unknown shader stage '%s'", ctx->pstage->getText().c_str()));
	}
	if ((pStage == ShaderStages::TControl) || (pStage == ShaderStages::TEval) ||
		(pStage == ShaderStages::Geometry)) {
		ERROR(ctx->pstage, "Currently only vertex stages can produce locals");
	}

	// Add variable
	Variable var{ lVar.name, Variable::Use::Local, Variable::Access::ReadWrite, lVar.type, 1 };
	var.extra.local.sourceStage = pStage;
	var.extra.local.flat = isFlat;
	SCOPES->AddGlobal(var);

	// Add to info
	SINFO->locals_.push_back({ lVar.name, lVar.type, pStage, isFlat });

	return nullptr;
}

// ====================================================================================================================
VISIT_FUNC(ShaderSubpassInputStatement)
{
	// Check parse status
	if (bool(SINFO->Stages())) {
		ERROR(ctx, "All subpass inputs must be provided before the first stage function");
	}

	// Parse and validate format
	const auto fmtText = ctx->format->getText();
	const auto format = TexelFormat::FromVSLFormat(fmtText);
	if (!format.has_value()) {
		ERROR(ctx->format, FmtStr("No texel format '%s' found", fmtText.c_str()));
	}
	if (format.value().IsNormalizedType() || (format.value().ComponentCount() != 4) || 
			(format.value().ComponentSize() != 4)) {
		ERROR(ctx->format, "Only int4, uint4, and float4 allowed for subpass inputs");
	}

	// Validate name
	ValidateNewName(ctx->name);

	// Get the binding slot
	const auto indexLiteral = Literal::Parse(ctx->index->getText(), nullptr);
	if (!indexLiteral.has_value()) {
		ERROR(ctx->index, "Invalid index literal");
	}
	if (indexLiteral.value().IsNegative() || (indexLiteral.value().type == Literal::Float)) {
		ERROR(ctx->index, "Subpass input index must be non-negative integer");
	}
	else if (indexLiteral.value().u >= Shader::MAX_SUBPASS_INPUTS) {
		ERROR(ctx->index, FmtStr("Slot index out of range (max %u)", Shader::MAX_SUBPASS_INPUTS - 1));
	}
	const auto slotIndex = uint32(indexLiteral.value().u);
	const auto existing = SINFO->GetSubpassInput(slotIndex);
	if (existing) {
		ERROR(ctx->index, FmtStr("Subpass input %u is already populated by '%s'", slotIndex, existing->name.c_str()));
	}
	if ((slotIndex != 0) && !SINFO->GetSubpassInput(slotIndex - 1)) {
		ERROR(ctx->index, "Subpass inputs must have contiguous indices");
	}

	// Add the type
	const auto typeName = FmtStr("Spi<%s>", format.value().GetVSLDecorator());
	auto spiType = STYPES->GetType(typeName);
	if (!spiType) {
		spiType = STYPES->AddType(typeName, { BaseType::SubpassInput, TexelRank::E2D, format.value() });
	}

	// Add to info and scope
	SINFO->subpassInputs_.push_back({ ctx->name->getText(), format.value(), slotIndex });
	Variable var{ ctx->name->getText(), Variable::Use::Binding, Variable::Access::ReadOnly, spiType, 1 };
	var.extra.binding.slot = slotIndex;
	SCOPES->AddGlobal(var);

	return nullptr;
}

// ====================================================================================================================
VISIT_FUNC(VertexFunction)
{
	// Validate state
	if (bool(SINFO->Stages() & ShaderStages::Vertex)) {
		ERROR(ctx, "Cannot have multiple vertex stages in a single shader");
	}

	// Push function scope
	SCOPES->PushFunctionScope(ShaderStages::Vertex);
	parse_.currentFunction = parse_.functions->emplace_back(new FunctionGenerator(ShaderStages::Vertex)).get();
	parse_.currentStage = ShaderStages::Vertex;

	// Visit the vertex inputs
	uint32 locationOffset{ 0 };
	for (const auto input : ctx->shaderVertexInput()) {
		// Get and check the semantic
		const auto semText = input->semantic->getText();
		const auto semantic = FromSemanticString(semText.substr(1));
		if (!semantic.has_value()) {
			ERROR(input->semantic, FmtStr("Unknown vertex semantic '%s'", semText.c_str()));
		}
		const auto semMask = (1u << uint32(semantic.value()));
		if (SINFO->SemanticMask() & semMask) {
			ERROR(input->semantic, FmtStr("Duplicate vertex semantic '%s'", semText.c_str()));
		}
		SINFO->semanticMask_ |= semMask;

		// Parse the vertex input declaration
		const auto varDecl = input->variableDeclaration();
		auto inputVar = ParseVariableDeclaration(varDecl);
		inputVar.use = Variable::Use::Input;
		inputVar.extra.input.semantic = semantic.value();
		if (inputVar.arraySize > Shader::MAX_VERTEX_ATTRIBS) {
			ERROR(varDecl->arraySize, "Vertex arrays cannot be larger than the number of input slots");
		}
		if ((inputVar.arraySize != 1) && inputVar.type->IsMatrix()) {
			ERROR(varDecl->arraySize, "Vertex matrix inputs cannot be arrays");
		}
		SCOPES->AddVariable(inputVar);

		// Add the variable to the shader info (validate location)
		InputVariable infoVar{
			varDecl->name->getText(), semantic.value(), inputVar.type, inputVar.arraySize, locationOffset 
		};
		if ((locationOffset + infoVar.BindingSize()) > Shader::MAX_VERTEX_ATTRIBS) {
			ERROR(varDecl, "Vertex input is outside of max input slot bounds");
		}
		SINFO->inputs_.push_back(infoVar);
		locationOffset += infoVar.BindingSize();
	}

	// Visit the function statements
	for (const auto stmt : ctx->statementBlock()->statement()) {
		visit(stmt);
	}

	// Pop and check function scope
	const auto missing = SCOPES->PopScope();
	if (missing.has_value()) {
		ERROR(ctx->getStart(),
			FmtStr("Variable '%s' must be assigned in all paths before function return", missing.value().c_str()));
	}
	parse_.currentFunction = nullptr;
	parse_.currentStage = ShaderStages::None;

	// Update shader info
	SINFO->stages_ |= ShaderStages::Vertex;

	return nullptr;
}

// ====================================================================================================================
VISIT_FUNC(FragmentFunction)
{
	// Validate state
	if (bool(SINFO->Stages() & ShaderStages::Fragment)) {
		ERROR(ctx, "Cannot have multiple fragment stages in a single shader");
	}

	// Push function scope
	SCOPES->PushFunctionScope(ShaderStages::Fragment);
	parse_.currentFunction = parse_.functions->emplace_back(new FunctionGenerator(ShaderStages::Fragment)).get();
	parse_.currentStage = ShaderStages::Fragment;

	// Visit the fragment outputs
	uint32 outIndex{ 0 };
	for (const auto output : ctx->shaderFragmentOutput()) {
		// State check
		if (outIndex == Shader::MAX_FRAGMENT_OUTPUTS) {
			ERROR(output, "Too many fragment outputs in shader");
		}

		// Parse the variable declaration
		const auto varDecl = output->variableDeclaration();
		auto outputVar = ParseVariableDeclaration(varDecl);
		outputVar.use = Variable::Use::Output;
		if (!outputVar.type->IsNumericType()) {
			ERROR(varDecl->arraySize, "Fragment outputs cannot be non-numeric types");
		}
		if (outputVar.arraySize != 1) {
			ERROR(varDecl->arraySize, "Fragment outputs cannot be arrays");
		}
		if (outputVar.type->IsMatrix()) {
			ERROR(varDecl->arraySize, "Fragment outputs cannot be matrices");
		}
		if (outputVar.type->numeric.dims[0] == 3) {
			ERROR(varDecl->arraySize, "Fragment outputs cannot be 3-component vectors");
		}
		SCOPES->AddVariable(outputVar);

		// Parse the blending mode
		Blending blending{};
		if (output->blendName) {
			const auto blendText = output->blendName->getText();
			const auto blendParse = Blending::ParseBlendingName(blendText);
			if (!blendParse.has_value()) {
				ERROR(output->blendName, FmtStr("Unknown blending mode name '%s'", blendText.c_str()));
			}
			blending = blendParse.value();
		}
		else {
			blending = ParseBlending(output->blendStatement());
		}

		// Add the variable to the shader info
		OutputVariable infoVar{
			varDecl->name->getText(), outputVar.type, outIndex++, blending
		};
		SINFO->outputs_.push_back(infoVar);
	}

	// Visit the function statements
	for (const auto stmt : ctx->statementBlock()->statement()) {
		visit(stmt);
	}

	// Pop and check function scope
	const auto missing = SCOPES->PopScope();
	if (missing.has_value()) {
		ERROR(ctx->getStart(),
			FmtStr("Variable '%s' must be assigned in all paths before function return", missing.value().c_str()));
	}
	parse_.currentFunction = nullptr;
	parse_.currentStage = ShaderStages::None;

	// Update shader info
	SINFO->stages_ |= ShaderStages::Fragment;

	return nullptr;
}

} // namespace vsl
