/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "./Function.hpp"

#include <atomic>


namespace vsl
{

// ====================================================================================================================
void Functions::Initialize()
{
	static std::atomic_flag Initialized_{ };
	static bool FinishedInit_{ false };
	if (Initialized_.test_and_set()) {
		while (!FinishedInit_) { ; }
		return;
	}

	const FunctionType GENF{ "genType" };
	const FunctionType GENU{ "genUType" };
	const FunctionType GENI{ "genIType" };
	const FunctionType GENB{ "genBType" };
	const FunctionType FLOAT_T{ "float" };
	const FunctionType FLOAT2_T{ "float2" };
	const FunctionType FLOAT3_T{ "float3" };
	const FunctionType FLOAT4_T{ "float4" };
	const FunctionType INT_T{ "int" };
	const FunctionType INT2_T{ "int2" };
	const FunctionType INT3_T{ "int3" };
	const FunctionType INT4_T{ "int4" };
	const FunctionType UINT_T{ "uint" };
	const FunctionType UINT4_T{ "uint4" };
	const FunctionType BOOL_T{ "bool" };

	// See http://docs.gl/sl4/degrees for a listing of GLSL 450 functions

	// ===== TRIG FUNCTIONS =====
	Builtins_["acos"] = {
		{ "acos", GENF, { GENF } }
	};
	Builtins_["acosh"] = {
		{ "acosh", GENF, { GENF } }
	};
	Builtins_["asin"] = {
		{ "asin", GENF, { GENF } }
	};
	Builtins_["asinh"] = {
		{ "asinh", GENF, { GENF } }
	};
	Builtins_["atan"] = {
		{ "atan", GENF, { GENF } }
	};
	Builtins_["atan2"] = {
		{ "atan", GENF, { GENF, GENF } }
	};
	Builtins_["atanh"] = {
		{ "atanh", GENF, { GENF } }
	};
	Builtins_["cos"] = {
		{ "cos", GENF, { GENF } }
	};
	Builtins_["cosh"] = {
		{ "cosh", GENF, { GENF } }
	};
	Builtins_["deg2rad"] = {
		{ "radians", GENF, { GENF } }
	};
	Builtins_["rad2deg"] = {
		{ "degrees", GENF, { GENF } }
	};
	Builtins_["sin"] = {
		{ "sin", GENF, { GENF } }
	};
	Builtins_["sinh"] = {
		{ "sinh", GENF, { GENF } }
	};
	Builtins_["tan"] = {
		{ "tan", GENF, { GENF } }
	};
	Builtins_["tanh"] = {
		{ "tanh", GENF, { GENF } }
	};

	// ===== General Mathematics =====
	Builtins_["abs"] = {
		{ "abs", GENI, { GENI } },
		{ "abs", GENF, { GENF } }
	};
	Builtins_["ceil"] = {
		{ "ceil", GENF, { GENF } }
	};
	Builtins_["clamp"] = {
		{ "clamp", GENI, { GENI, INT_T, INT_T } },
		{ "clamp", GENI, { GENI, GENI, GENI } },
		{ "clamp", GENU, { GENU, UINT_T, UINT_T } },
		{ "clamp", GENU, { GENU, GENU, GENU } },
		{ "clamp", GENF, { GENF, FLOAT_T, FLOAT_T } },
		{ "clamp", GENF, { GENF, GENF, GENF } }
	};
	// dFdy, dFdx
	Builtins_["exp"] = {
		{ "exp", GENF, { GENF } }
	};
	Builtins_["exp2"] = {
		{ "exp2", GENF, { GENF } }
	};
	Builtins_["floor"] = {
		{ "floor", GENF, { GENF } }
	};
	Builtins_["fma"] = {
		{ "fma", GENF, { GENF, GENF, GENF } }
	};
	Builtins_["fract"] = {
		{ "fract", GENF, { GENF } }
	};
	// fwidth
	Builtins_["isqrt"] = {
		{ "inverseSqrt", GENF, { GENF } }
	};
	Builtins_["isinf"] = {
		{ "isinf", GENB, { GENF } }
	};
	Builtins_["isnan"] = {
		{ "isnan", GENB, { GENF } }
	};
	Builtins_["log"] = {
		{ "log", GENF, { GENF } }
	};
	Builtins_["log2"] = {
		{ "log2", GENF, { GENF } }
	};
	Builtins_["max"] = {
		{ "max", GENI, { GENI, INT_T } },
		{ "max", GENI, { GENI, GENI } },
		{ "max", GENU, { GENU, UINT_T } },
		{ "max", GENU, { GENU, GENU } },
		{ "max", GENF, { GENF, FLOAT_T } },
		{ "max", GENF, { GENF, GENF } }
	};
	Builtins_["min"] = {
		{ "min", GENI, { GENI, INT_T } },
		{ "min", GENI, { GENI, GENI } },
		{ "min", GENU, { GENU, UINT_T } },
		{ "min", GENU, { GENU, GENU } },
		{ "min", GENF, { GENF, FLOAT_T } },
		{ "min", GENF, { GENF, GENF } }
	};
	Builtins_["mix"] = {
		{ "mix", GENB, { GENB, GENB, GENB } },
		{ "mix", GENI, { GENI, GENI, GENB } },
		{ "mix", GENU, { GENU, GENU, GENB } },
		{ "mix", GENF, { GENF, GENF, GENB } },
		{ "mix", GENF, { GENF, GENF, FLOAT_T } },
		{ "mix", GENF, { GENF, GENF, GENF } }
	};
	Builtins_["mod"] = {
		{ "mod", GENF, { GENF, FLOAT_T } },
		{ "mod", GENF, { GENF, GENF } }
	};
	Builtins_["modf"] = {
		{ "modf", GENF, { GENF, "out genType" } }
	};
	// noise
	Builtins_["pow"] = {
		{ "pow", GENF, { GENF, GENF } }
	};
	Builtins_["round"] = {
		{ "round", GENF, { GENF } }
	};
	Builtins_["roundEven"] = {
		{ "roundEven", GENF, { GENF } }
	};
	Builtins_["sign"] = {
		{ "sign", GENI, { GENI } },
		{ "sign", GENF, { GENF } }
	};
	Builtins_["smoothStep"] = {
		{ "smoothStep", GENF, { FLOAT_T, FLOAT_T, GENF } },
		{ "smoothStep", GENF, { GENF, GENF, GENF } }
	};
	Builtins_["sqrt"] = {
		{ "sqrt", GENF, { GENF } }
	};
	Builtins_["step"] = {
		{ "step", GENF, { FLOAT_T, GENF } },
		{ "step", GENF, { GENF, GENF } }
	};
	Builtins_["trunc"] = {
		{ "trunc", GENF, { GENF } }
	};

	// ===== Floating Point Functions ===== 
	Builtins_["bitCastInt"] = {
		{ "floatBitsToInt", GENI, { GENF } }
	};
	Builtins_["bitCastUint"] = {
		{ "floatBitsToUint", GENU, { GENF } }
	};
	Builtins_["frexp"] = {
		{ "frexp", GENF, { GENF, "out genIType" } }
	};
	Builtins_["bitCastFloat"] = {
		{ "intBitsToFloat", GENF, { GENI } },
		{ "uintBitsToFloat", GENF, { GENU } },
	};
	Builtins_["ldexp"] = {
		{ "ldexp", GENF, { GENF, GENI } }
	};
	// packing and unpacking functions

	// ===== Vector Functions =====
	Builtins_["cross"] = {
		{ "cross", FLOAT3_T, { FLOAT3_T, FLOAT3_T } }
	};
	Builtins_["distance"] = {
		{ "distance", FLOAT_T, { GENF, GENF } }
	};
	Builtins_["dot"] = {
		{ "dot", FLOAT_T, { GENF, GENF } }
	};
	// equal (replaced with `vec == vec` operator)
	Builtins_["faceForward"] = {
		{ "faceForward", GENF, { GENF, GENF, GENF } }
	};
	Builtins_["length"] = {
		{ "length", FLOAT_T, { GENF } }
	};
	Builtins_["normalize"] = {
		{ "normalize", GENF, { GENF } }
	};
	// notEqual (replaced with `vec != vec` operator)
	Builtins_["reflect"] = {
		{ "reflect", GENF, { GENF, GENF } }
	};
	Builtins_["refract"] = {
		{ "refract", GENF, { GENF, GENF, FLOAT_T } }
	};

	// ===== Vector Component Functions =====
	Builtins_["all"] = {
		{ "all", BOOL_T, { GENB } }
	};
	Builtins_["any"] = {
		{ "any", BOOL_T, { GENB } }
	};
	// greaterThan, greaterThanEqual, lessThan, lessThanEqual, not are all replaced with operators

	// ===== Integer Functions =====
	Builtins_["bitCount"] = {
		{ "bitCount", GENI, { GENI } },
		{ "bitCount", GENI, { GENU } },
	};
	// bitfieldExtract, bitfieldInsert, bitfieldReverse
	Builtins_["findLSB"] = {
		{ "findLSB", GENI, { GENI } },
		{ "findLSB", GENI, { GENU } },
	};
	Builtins_["findMSB"] = {
		{ "findMSB", GENI, { GENI } },
		{ "findMSB", GENI, { GENU } },
	};
	// uaddCarry, umulExtent, usubBorrow

	// ===== Matrix Functions =====
	Builtins_["determinant"] = {
		{ "determinant", FLOAT_T, { "float2x2" } },
		{ "determinant", FLOAT_T, { "float3x3" } },
		{ "determinant", FLOAT_T, { "float4x4" } }
	};
	Builtins_["inverse"] = {
		{ "inverse", "float2x2", { "float2x2" } },
		{ "inverse", "float3x3", { "float3x3" } },
		{ "inverse", "float4x4", { "float4x4" } }
	};
	Builtins_["matCompMul"] = {
		{ "matrixCompMult", "float2x2", { "float2x2", "float2x2" } },
		{ "matrixCompMult", "float2x3", { "float2x3", "float2x3" } },
		{ "matrixCompMult", "float2x4", { "float2x4", "float2x4" } },
		{ "matrixCompMult", "float3x2", { "float3x2", "float3x2" } },
		{ "matrixCompMult", "float3x3", { "float3x3", "float3x3" } },
		{ "matrixCompMult", "float3x4", { "float3x4", "float3x4" } },
		{ "matrixCompMult", "float4x2", { "float4x2", "float4x2" } },
		{ "matrixCompMult", "float4x3", { "float4x3", "float4x3" } },
		{ "matrixCompMult", "float4x4", { "float4x4", "float4x4" } }
	};
	Builtins_["outerProd"] = {
		{ "outerProduct", "float2x2", { FLOAT2_T, FLOAT2_T } },
		{ "outerProduct", "float2x3", { FLOAT3_T, FLOAT2_T } },
		{ "outerProduct", "float2x4", { FLOAT4_T, FLOAT2_T } },
		{ "outerProduct", "float3x2", { FLOAT2_T, FLOAT3_T } },
		{ "outerProduct", "float3x3", { FLOAT3_T, FLOAT3_T } },
		{ "outerProduct", "float3x4", { FLOAT4_T, FLOAT3_T } },
		{ "outerProduct", "float4x2", { FLOAT2_T, FLOAT4_T } },
		{ "outerProduct", "float4x3", { FLOAT3_T, FLOAT4_T } },
		{ "outerProduct", "float4x4", { FLOAT4_T, FLOAT4_T } }
	};
	Builtins_["transpose"] = {
		{ "transpose", "float2x2", { "float2x2" } },
		{ "transpose", "float2x3", { "float3x2" } },
		{ "transpose", "float2x4", { "float4x2" } },
		{ "transpose", "float3x2", { "float2x3" } },
		{ "transpose", "float3x3", { "float3x3" } },
		{ "transpose", "float3x4", { "float4x3" } },
		{ "transpose", "float4x2", { "float2x4" } },
		{ "transpose", "float4x3", { "float3x4" } },
		{ "transpose", "float4x4", { "float4x4" } }
	};

	// ===== Texture/Image Functions =====
	Builtins_["texelFetch"] = {
		{ "texelFetch", FLOAT4_T, { "Sampler1D", INT_T, INT_T } },
		{ "texelFetch", FLOAT4_T, { "Sampler2D", INT2_T, INT_T } },
		{ "texelFetch", FLOAT4_T, { "Sampler3D", INT3_T, INT_T } },
		{ "texelFetch", FLOAT4_T, { "Sampler1DArray", INT2_T, INT_T } },
		{ "texelFetch", FLOAT4_T, { "Sampler2DArray", INT3_T, INT_T } },
		{ "texelFetch", INT4_T,   { "ISampler1D", INT_T, INT_T } },
		{ "texelFetch", INT4_T,   { "ISampler2D", INT2_T, INT_T } },
		{ "texelFetch", INT4_T,   { "ISampler3D", INT3_T, INT_T } },
		{ "texelFetch", INT4_T,   { "ISampler1DArray", INT2_T, INT_T } },
		{ "texelFetch", INT4_T,   { "ISampler2DArray", INT3_T, INT_T } },
		{ "texelFetch", UINT4_T,  { "USampler1D", INT_T, INT_T } },
		{ "texelFetch", UINT4_T,  { "USampler2D", INT2_T, INT_T } },
		{ "texelFetch", UINT4_T,  { "USampler3D", INT3_T, INT_T } },
		{ "texelFetch", UINT4_T,  { "USampler1DArray", INT2_T, INT_T } },
		{ "texelFetch", UINT4_T,  { "USampler2DArray", INT3_T, INT_T } }
	};
	Builtins_["levelsOf"] = {
		{ "textureQueryLevels", INT_T, { "Sampler1D" } },
		{ "textureQueryLevels", INT_T, { "Sampler2D" } },
		{ "textureQueryLevels", INT_T, { "Sampler3D" } },
		{ "textureQueryLevels", INT_T, { "Sampler1DArray" } },
		{ "textureQueryLevels", INT_T, { "Sampler2DArray" } },
		{ "textureQueryLevels", INT_T, { "SamplerCube" } },
		{ "textureQueryLevels", INT_T, { "ISampler1D" } },
		{ "textureQueryLevels", INT_T, { "ISampler2D" } },
		{ "textureQueryLevels", INT_T, { "ISampler3D" } },
		{ "textureQueryLevels", INT_T, { "ISampler1DArray" } },
		{ "textureQueryLevels", INT_T, { "ISampler2DArray" } },
		{ "textureQueryLevels", INT_T, { "ISamplerCube" } },
		{ "textureQueryLevels", INT_T, { "USampler1D" } },
		{ "textureQueryLevels", INT_T, { "USampler2D" } },
		{ "textureQueryLevels", INT_T, { "USampler3D" } },
		{ "textureQueryLevels", INT_T, { "USampler1DArray" } },
		{ "textureQueryLevels", INT_T, { "USampler2DArray" } },
		{ "textureQueryLevels", INT_T, { "USamplerCube" } },
	};
	Builtins_["sizeOf"] = {
		{ "textureSize", INT_T,  { "Sampler1D", INT_T } },
		{ "textureSize", INT2_T, { "Sampler2D", INT_T } },
		{ "textureSize", INT3_T, { "Sampler3D", INT_T } },
		{ "textureSize", INT2_T, { "Sampler1DArray", INT_T } },
		{ "textureSize", INT3_T, { "Sampler2DArray", INT_T } },
		{ "textureSize", INT2_T, { "SamplerCube", INT_T } },
		{ "textureSize", INT_T,  { "ISampler1D", INT_T } },
		{ "textureSize", INT2_T, { "ISampler2D", INT_T } },
		{ "textureSize", INT3_T, { "ISampler3D", INT_T } },
		{ "textureSize", INT2_T, { "ISampler1DArray", INT_T } },
		{ "textureSize", INT3_T, { "ISampler2DArray", INT_T } },
		{ "textureSize", INT2_T, { "ISamplerCube", INT_T } },
		{ "textureSize", INT_T,  { "USampler1D", INT_T } },
		{ "textureSize", INT2_T, { "USampler2D", INT_T } },
		{ "textureSize", INT3_T, { "USampler3D", INT_T } },
		{ "textureSize", INT2_T, { "USampler1DArray", INT_T } },
		{ "textureSize", INT3_T, { "USampler2DArray", INT_T } },
		{ "textureSize", INT2_T, { "USamplerCube", INT_T } },
		{ "imageSize", INT_T,  { "Image1D<>" } },
		{ "imageSize", INT2_T, { "Image2D<>" } },
		{ "imageSize", INT3_T, { "Image3D<>" } },
		{ "imageSize", INT2_T, { "Image1DArray<>" } },
		{ "imageSize", INT3_T, { "Image2DArray<>" } }
	};

	FinishedInit_ = true;
}

} // namespace vsl
