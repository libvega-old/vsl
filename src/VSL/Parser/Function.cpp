/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "./Function.hpp"
#include "../Generator/GeneratorUtils.hpp"


namespace vsl
{

// ====================================================================================================================
std::vector<UPtr<ShaderType>> FunctionType::GenericTypes_{ };

// ====================================================================================================================
FunctionType::FunctionType(const String& typeName)
	: type{ nullptr }
	, genType{ false }
	, refType{ false }
{
	// Get out/inout status
	StrView name{ typeName };
	if (typeName.find("out ") == 0) {
		name = name.substr(4);
		refType = true;
	}
	else if (typeName.find("inout ") == 0) {
		name = name.substr(6);
		refType = true;
	}
	else {
		refType = false;
	}

	// Get the type or gentype
	if (name == "genType") {
		type = ShaderType::Builtin::GetType("float");
		genType = true;
	}
	else if (name == "genIType") {
		type = ShaderType::Builtin::GetType("int");
		genType = true;
	}
	else if (name == "genUType") {
		type = ShaderType::Builtin::GetType("uint");
		genType = true;
	}
	else if (name == "genBType") {
		type = ShaderType::Builtin::GetType("bool");
		genType = true;
	}
	else if (name.compare(name.length() - 2, 2, "<>") == 0) {
		const auto genName = name.substr(0, name.length() - 2);
		UPtr<ShaderType> newType{ new ShaderType };
		assert(ShaderType::Builtin::ParseGenericType(String(name.substr(0, name.length() - 2)), *newType) &&
			"Invalid generic type in FunctionType");
		type = GenericTypes_.emplace_back(newType.release()).get();
		genType = true;
	}
	else {
		type = ShaderType::Builtin::GetType(String(name));
		assert(type && "Invalid type name for FunctionType");
		genType = false;
	}
}

// ====================================================================================================================
bool FunctionType::Match(const Expr* expr) const
{
	const auto etype = expr->type;
	if (expr->arraySize != 1) {
		return false; // No functions take arrays as arguments
	}
	if (genType) {
		if (type->IsNumericType()) {
			const auto casttype =
				ShaderType::Builtin::GetNumericType(type->baseType, etype->numeric.size, etype->numeric.dims[0], 1);
			return etype->HasImplicitCast(*casttype);
		}
		else if (type->IsTexelType()) {
			return (type->baseType == expr->type->baseType) && (type->texel.rank == expr->type->texel.rank);
		}
		else if (type->IsBufferType()) {
			return (type->baseType == expr->type->baseType) &&
				(type->buffer.structType == expr->type->buffer.structType);
		}
		else {
			return false;
		}
	}
	else {
		if (type->IsNumericType() || type->IsBoolean()) {
			return etype->HasImplicitCast(*type);
		}
		else if (type->IsSampler() || type->IsImage()) {
			return (type->baseType == etype->baseType) && (type->texel.rank == etype->texel.rank);
		}
		else {
			return false;
		}
	}
}


// ====================================================================================================================
// ====================================================================================================================
FunctionEntry::FunctionEntry(const String& genName, const FunctionType& retType, const std::vector<FunctionType>& args)
	: genName{ genName }
	, retType{ retType }
	, argTypes{ args }
{

}

// ====================================================================================================================
const ShaderType* FunctionEntry::Match(const std::vector<const Expr*>& params) const
{
	// Count check
	if (params.size() != argTypes.size()) {
		return nullptr;
	}

	// Per-arg check
	uint32 genSize{ 0 };
	uint32 genCount{ 0 };
	BaseType genType{ BaseType::Void };
	for (uint32 i = 0; i < argTypes.size(); ++i) {
		const auto& ai = argTypes[i];
		const auto& pi = params[i];
		if (!ai.Match(pi)) {
			return nullptr;
		}
		if (ai.genType) {
			if (genSize == 0) {
				genSize = pi->type->numeric.size;
				genCount = pi->type->numeric.dims[0];
				genType = pi->type->baseType;
			}
			else if (genCount != pi->type->numeric.dims[0]) {
				return nullptr; // Gen types must match sizes
			}
		}
	}

	// Return the correct type
	const auto retSize =
		(!retType.genType || (genSize == 0)) ? retType.type->numeric.size :
		(retType.type->baseType != genType) ? retType.type->numeric.size : genSize;
	return retType.genType
		? ShaderType::Builtin::GetNumericType(retType.type->baseType, retSize, genCount, 1)
		: retType.type;
}


// ====================================================================================================================
// ====================================================================================================================
std::unordered_map<String, std::vector<FunctionEntry>> Functions::Builtins_{ };

// ====================================================================================================================
bool Functions::HasFunction(const String& funcName)
{
	Initialize();

	const auto it = Builtins_.find(funcName);
	return (it != Builtins_.end());
}

// ====================================================================================================================
std::tuple<const ShaderType*, String> Functions::CheckFunction(const String& funcName, 
	const std::vector<const Expr*>& args)
{
	Initialize();

	const auto typeName = ShaderType::Builtin::GetType(funcName);
	if (typeName) {
		return CheckConstructor(funcName, args);
	}
	else {
		const auto it = Builtins_.find(funcName);
		if (it == Builtins_.end()) {
			return { nullptr, FmtStr("No function with name '%s' found", funcName.c_str()) };
		}
		for (const auto& entry : it->second) {
			const auto match = entry.Match(args);
			if (match) {
				return { match, entry.genName };
			}
		}
		return { nullptr, FmtStr("No overload of function '%s' matched the given arguments", funcName.c_str()) };
	}
}

// ====================================================================================================================
std::tuple<const ShaderType*, String> Functions::CheckConstructor(const String& typeName,
	const std::vector<const Expr*>& args)
{
#define ERR_RETURN(msg) return { nullptr, (msg) };
#define GOOD_RETURN(typeObj,callName) return { (typeObj), (callName) };

	Initialize();

	// Get the type
	const auto retType = ShaderType::Builtin::GetType(typeName);
	if (!retType) {
		ERR_RETURN(FmtStr("No such type '%s' for constructor", typeName.c_str()));
	}
	if (!retType->IsNumericType() && !retType->IsBoolean()) {
		ERR_RETURN(FmtStr("Cannot construct type '%s' - only numeric types have constructors", typeName.c_str()));
	}
	const auto callName = GeneratorUtils::GLSLTypeName(*retType);

	// Generic argument checks
	if (args.empty()) {
		ERR_RETURN(FmtStr("Type '%s' does not have a no-args contructor", typeName.c_str())); // Technically none do
	}
	for (uint32 i = 0; i < args.size(); ++i) {
		if (args[i]->arraySize != 1) {
			ERR_RETURN(FmtStr("Constructor argument %u cannot be an array type", i + 1));
		}
		if (!args[i]->type->IsNumericType() && !args[i]->type->IsBoolean()) {
			ERR_RETURN(FmtStr("Constructor argument %u cannot be a non-value type", i + 1));
		}
	}

	// Switch on constructed type
	if (retType->IsScalar()) {  // Scalar -> scalar cast
		if (args.size() != 1) {
			ERR_RETURN("Scalar casts cannot have more than one argument");
		}
		if (!args[0]->type->IsScalar()) {
			ERR_RETURN("Scalar casts must take scalar arguments");
		}
		GOOD_RETURN(retType, callName);
	}
	else if (retType->IsVector()) {
		const auto ctype = ShaderType::Builtin::GetNumericType(retType->baseType, retType->numeric.size, 1, 1);
		const auto ccount = uint32(retType->numeric.dims[0]);
		if (args.size() == 1) {
			const auto a1 = args[0];
			if (a1->type->IsScalar()) {  // Fill vector with scalar
				if (!a1->type->HasImplicitCast(*ctype)) {
					ERR_RETURN(FmtStr("Cannot construct type '%s' with scalar type '%s'",
						retType->GetVSLName().c_str(), a1->type->GetVSLName().c_str()));
				}
				GOOD_RETURN(retType, callName);
			}
			else if (a1->type->IsVector()) {  // Vector -> vector cast
				if (a1->type->numeric.dims[0] != ccount) {
					ERR_RETURN("Cannot cast vector types of different component counts");
				}
				GOOD_RETURN(retType, callName);
			}
			else { // Matrix -> vector (invalid)
				ERR_RETURN("Cannot construct vector type from matrix type");
			}
		}
		else { // Standard vector constructor - match component counts
			uint32 found = 0;
			for (uint32 i = 0; i < args.size(); ++i) {
				const auto arg = args[i];
				const auto argCType = ShaderType::Builtin::GetNumericType(
					arg->type->baseType, arg->type->numeric.size, 1, 1
				);
				if (arg->type->IsMatrix()) {
					ERR_RETURN(FmtStr("Cannot construct vector from matrix argument %u", i + 1));
				}
				if (!argCType->HasImplicitCast(*ctype)) {
					ERR_RETURN(FmtStr("No implicit cast from argument %u type '%s' to component type '%s'", i + 1,
						argCType->GetVSLName().c_str(), ctype->GetVSLName().c_str()));
				}
				found += arg->type->numeric.dims[0];
			}
			if (found < ccount) {
				ERR_RETURN(FmtStr("Not enough components for %s constructor", retType->GetVSLName().c_str()));
			}
			if (found > ccount) {
				ERR_RETURN(FmtStr("Too many components for %s constructor", retType->GetVSLName().c_str()));
			}
			GOOD_RETURN(retType, callName);
		}
	}
	else { // retType->IsMatrix()
		const auto ctype = ShaderType::Builtin::GetNumericType(retType->baseType, retType->numeric.size, 1, 1);
		const auto ccount = uint32(retType->numeric.dims[0]) * retType->numeric.dims[1];
		if (args.size() == 1) {
			const auto a1 = args[1];
			if (a1->type->IsMatrix()) { // Matrix -> matrix (always works for all sizes)
				GOOD_RETURN(retType, callName);
			}
			else if (a1->type->IsVector()) { // Vector -> matrix (never works)
				ERR_RETURN("Cannot construct matrix type from vector type");
			}
			else { // Scalar -> matrix (creates identity matrix with scalar)
				GOOD_RETURN(retType, callName);
			}
		}
		else { // Standard matrix constructor - match component counts
			uint32 found = 0;
			for (uint32 i = 0; i < args.size(); ++i) {
				const auto arg = args[i];
				const auto argCType = ShaderType::Builtin::GetNumericType(
					arg->type->baseType, arg->type->numeric.size, 1, 1
				);
				if (!argCType->HasImplicitCast(*ctype)) {
					ERR_RETURN(FmtStr("No implicit cast from argument %u type '%s' to component type '%s'", i + 1,
						argCType->GetVSLName().c_str(), ctype->GetVSLName().c_str()));
				}
				found += (uint32(arg->type->numeric.dims[0]) * arg->type->numeric.dims[1]);
			}
			if (found < ccount) {
				ERR_RETURN(FmtStr("Not enough components for %s constructor", retType->GetVSLName().c_str()));
			}
			if (found > ccount) {
				ERR_RETURN(FmtStr("Too many components for %s constructor", retType->GetVSLName().c_str()));
			}
			GOOD_RETURN(retType, callName);
		}
	}

#undef ERR_RETURN
#undef GOOD_RETURN
}

} // namespace vsl
