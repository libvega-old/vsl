/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

//// !!!!!!!!!!
//// It is IMPORTANT not to include any third-party headers in this file. This file is used as the sole source for the
//// lightweight loader library, and it absolutely should not have any additional dependencies. If there comes a time
//// where it cannot be reasonable avoided to include those headers in this file, we will need to rethink the linking.
//// !!!!!!!!!!

#include "./Types.Builtin.hpp"

#include <algorithm>


namespace vsl
{

// ====================================================================================================================
ShaderType::Builtin::TypeMap ShaderType::Builtin::Types_{ 
	{ "void", { } },
	// Boolean
	{ "bool",  { BaseType::Boolean, 4, 1, 1 } }, { "bool2", { BaseType::Boolean, 4, 2, 1 } },
	{ "bool3", { BaseType::Boolean, 4, 3, 1 } }, { "bool4", { BaseType::Boolean, 4, 4, 1 } },
	// Integer
	{ "int",   { BaseType::Signed, 4, 1, 1 } }, { "int2",  { BaseType::Signed, 4, 2, 1 } },
	{ "int3",  { BaseType::Signed, 4, 3, 1 } }, { "int4",  { BaseType::Signed, 4, 4, 1 } },
	{ "uint",  { BaseType::Unsigned, 4, 1, 1 } }, { "uint2", { BaseType::Unsigned, 4, 2, 1 } },
	{ "uint3", { BaseType::Unsigned, 4, 3, 1 } }, { "uint4", { BaseType::Unsigned, 4, 4, 1 } },
	// Float
	{ "float",  { BaseType::Float, 4, 1, 1 } }, { "float2", { BaseType::Float, 4, 2, 1 } },
	{ "float3", { BaseType::Float, 4, 3, 1 } }, { "float4", { BaseType::Float, 4, 4, 1 } },
	// Matrices
	{ "float2x2", { BaseType::Float, 4, 2, 2 } },
	{ "float3x3", { BaseType::Float, 4, 3, 3 } },
	{ "float4x4", { BaseType::Float, 4, 4, 4 } },
	{ "float2x3", { BaseType::Float, 4, 3, 2 } }, { "float3x2", { BaseType::Float, 4, 2, 3 } },
	{ "float2x4", { BaseType::Float, 4, 4, 2 } }, { "float4x2", { BaseType::Float, 4, 2, 4 } },
	{ "float3x4", { BaseType::Float, 4, 4, 3 } }, { "float4x3", { BaseType::Float, 4, 3, 4 } },
	
	// Samplers
	{ "Sampler1D",       { BaseType::Sampler, TexelRank::E1D, TexelFormat::Float4 } },
	{ "Sampler2D",       { BaseType::Sampler, TexelRank::E2D, TexelFormat::Float4 } },
	{ "Sampler3D",       { BaseType::Sampler, TexelRank::E3D, TexelFormat::Float4 } },
	{ "Sampler1DArray",  { BaseType::Sampler, TexelRank::E1DArray, TexelFormat::Float4 } },
	{ "Sampler2DArray",  { BaseType::Sampler, TexelRank::E2DArray, TexelFormat::Float4 } },
	{ "SamplerCube",     { BaseType::Sampler, TexelRank::Cube, TexelFormat::Float4 } },
	{ "ISampler1D",      { BaseType::Sampler, TexelRank::E1D, TexelFormat::Int4 } },
	{ "ISampler2D",      { BaseType::Sampler, TexelRank::E2D, TexelFormat::Int4 } },
	{ "ISampler3D",      { BaseType::Sampler, TexelRank::E3D, TexelFormat::Int4 } },
	{ "ISampler1DArray", { BaseType::Sampler, TexelRank::E1DArray, TexelFormat::Int4 } },
	{ "ISampler2DArray", { BaseType::Sampler, TexelRank::E2DArray, TexelFormat::Int4 } },
	{ "ISamplerCube",    { BaseType::Sampler, TexelRank::Cube, TexelFormat::Int4 } },
	{ "USampler1D",      { BaseType::Sampler, TexelRank::E1D, TexelFormat::UInt4 } },
	{ "USampler2D",      { BaseType::Sampler, TexelRank::E2D, TexelFormat::UInt4 } },
	{ "USampler3D",      { BaseType::Sampler, TexelRank::E3D, TexelFormat::UInt4 } },
	{ "USampler1DArray", { BaseType::Sampler, TexelRank::E1DArray, TexelFormat::UInt4 } },
	{ "USampler2DArray", { BaseType::Sampler, TexelRank::E2DArray, TexelFormat::UInt4 } },
	{ "USamplerCube",    { BaseType::Sampler, TexelRank::Cube, TexelFormat::UInt4 } },
	// Readonly Texel Buffers
	{ "ROTexels",  { BaseType::ROTexels, TexelRank::Buffer, TexelFormat::Float4 } },
	{ "ROITexels", { BaseType::ROTexels, TexelRank::Buffer, TexelFormat::Int4 } },
	{ "ROUTexels", { BaseType::ROTexels, TexelRank::Buffer, TexelFormat::UInt4 } },
};
ShaderType::Builtin::StructMap ShaderType::Builtin::Structs_{ 
	{ "_GLOB_", { "_GLOB_", { 
		{ "time",      &ShaderType::Builtin::Types_.at("float"),    1,   0 }, // elapsed time (s)
		{ "deltaTime", &ShaderType::Builtin::Types_.at("float2"),   1,   4 }, // (scaled delta, real delta)
		{ "cameraPos", &ShaderType::Builtin::Types_.at("float3"),   1,  12 }, // camera world pos (x, y, z)
		{ "cameraDir", &ShaderType::Builtin::Types_.at("float3"),   1,  24 }, // camera look dir (x, y, z) normed
		{ "viewProj",  &ShaderType::Builtin::Types_.at("float4x4"), 1,  36 }, // camera view * projection matrix
		{ "target",    &ShaderType::Builtin::Types_.at("float4"),   1, 100 }, // render target (w, h, 1/w, 1/h)
		{ "depth",     &ShaderType::Builtin::Types_.at("float2"),   1, 116 }, // projection (near, far) plane dist
	} } }
};

// ====================================================================================================================
const ShaderType* ShaderType::Builtin::GetType(const String& name)
{
	static bool Initialized_{ false };
	if (!Initialized_) {
		// Add globals struct
		Types_.insert({ "_GLOB_", ShaderType{ &Structs_.at("_GLOB_") } });

		Initialized_ = true;
	}

	const auto it = Types_.find(name);
	return (it != Types_.end()) ? &(it->second) : nullptr;
}

// ====================================================================================================================
const ShaderType* ShaderType::Builtin::GetNumericType(BaseType baseType, uint32 size, uint32 dim0, uint32 dim1)
{
	const auto it = std::find_if(Types_.begin(), Types_.end(),
		[baseType, size, dim0, dim1](const TypeMap::value_type& type) {
			return (type.second.baseType == baseType) && (type.second.numeric.size == size) &&
				(type.second.numeric.dims[0] == dim0) && (type.second.numeric.dims[1] == dim1);
		}
	);
	return (it != Types_.end()) ? &(it->second) : nullptr;
}

// ====================================================================================================================
bool ShaderType::Builtin::ParseGenericType(const String& baseType, ShaderType& genericType)
{
	static const std::unordered_map<String, TexelRank> SUFFIX_RANKS{
		{ "1D", TexelRank::E1D }, { "2D", TexelRank::E2D }, { "3D", TexelRank::E3D },
		{ "1DArray", TexelRank::E1DArray }, { "2DArray", TexelRank::E2DArray }, { "Cube", TexelRank::Cube }
	};

	if (baseType.find("Image") == 0) {
		genericType.baseType = BaseType::Image;
		const auto rank = baseType.substr(5);
		const auto it = SUFFIX_RANKS.find(rank);
		if (it == SUFFIX_RANKS.end()) {
			return false;
		}
		genericType.texel.rank = it->second;
	}
	else if (baseType == "ROBuffer") {
		genericType.baseType = BaseType::ROBuffer;
	}
	else if (baseType == "RWBuffer") {
		genericType.baseType = BaseType::RWBuffer;
	}
	else if (baseType == "RWTexels") {
		genericType.baseType = BaseType::RWTexels;
		genericType.texel.rank = TexelRank::Buffer;
	}
	else {
		return false;
	}

	return true;
}

// ====================================================================================================================
const StructType* ShaderType::Builtin::GetStructType(const String& name)
{
	const auto it = Structs_.find(name);
	return (it != Structs_.end()) ? &(it->second) : nullptr;
}

} // namespace vsl
