/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

#include "../Common.Impl.hpp"

#include <vector>


namespace vsl
{

class CompilerOptions;
class StageGenerator;
class ShaderInfo;

// Manages compiling generated GLSL source into SPIR-V
class SpirvCompiler final
{
public:
	SpirvCompiler(const CompilerOptions* options);
	~SpirvCompiler();

	inline const std::vector<uint32>& Bytecode() const { return bytecode_; }
	inline const std::vector<char>& Assembly() const { return assembly_; }

	void Compile(const StageGenerator& stageGen, const ShaderInfo& info);
	void SaveBytecode(const StageGenerator& stageGen, const ShaderInfo& info);
	void SaveAssembly(const StageGenerator& stageGen, const ShaderInfo& info);

private:
	const CompilerOptions* const options_;
	std::vector<uint32> bytecode_;
	std::vector<char> assembly_;

	VSL_NO_COPY(SpirvCompiler)
	VSL_NO_MOVE(SpirvCompiler)
}; // class SpirvCompiler

} // namespace vsl
