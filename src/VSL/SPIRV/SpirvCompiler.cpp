/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "./SpirvCompiler.hpp"
#include "../Reflection.hpp"
#include <VSL/Compiler.hpp>
#include "../Generator/StageGenerator.hpp"
#include "../Generator/GeneratorUtils.hpp"

#include <shaderc/shaderc.hpp>

#include <iostream>
#include <fstream>


namespace vsl
{

// ====================================================================================================================
SpirvCompiler::SpirvCompiler(const CompilerOptions* options)
	: options_{ options }
	, bytecode_{ }
	, assembly_{ }
{

}

// ====================================================================================================================
SpirvCompiler::~SpirvCompiler()
{

}

// ====================================================================================================================
void SpirvCompiler::Compile(const StageGenerator& stageGen, const ShaderInfo& info)
{
	const auto stage = stageGen.Stage();

	// Create the compiler options
	shaderc::CompileOptions opts{};
	opts.SetOptimizationLevel(options_->DisableOptimization()
		? shaderc_optimization_level_zero
		: shaderc_optimization_level_performance);
	opts.SetTargetSpirv(shaderc_spirv_version_1_5); // Vulkan 1.2 targets SPIR-V 1.5
	opts.SetTargetEnvironment(shaderc_target_env_vulkan, shaderc_env_version_vulkan_1_2);

	// Perform compilation
	const auto sstage =
		(stage == ShaderStages::Vertex) ? shaderc_vertex_shader :
		(stage == ShaderStages::TControl) ? shaderc_tess_control_shader :
		(stage == ShaderStages::TEval) ? shaderc_tess_evaluation_shader :
		(stage == ShaderStages::Geometry) ? shaderc_geometry_shader : shaderc_fragment_shader;
	shaderc::Compiler compiler{ };
	const auto result = compiler.CompileGlslToSpv(
		stageGen.Source().str(),
		sstage,
		"VSLC",
		"main",
		opts
	);

	// Check compile result
	if (result.GetCompilationStatus() != shaderc_compilation_status_success) {
		std::cerr << "Failed to compile shader: " << result.GetErrorMessage() << std::endl;
		assert(false && "SPIR-V compilation failed - generated GLSL is incorrect");
	}

	// Save the bytecode
	bytecode_.assign(result.begin(), result.end());

	// Perform assembly compilation if requested
	if (options_->SaveAsm()) {
		const auto asmResult = compiler.CompileGlslToSpvAssembly(
			stageGen.Source().str(),
			sstage,
			"VSLC",
			"main",
			opts
		);
		assembly_.assign(asmResult.begin(), asmResult.end());
	}
}

// ====================================================================================================================
void SpirvCompiler::SaveBytecode(const StageGenerator& stageGen, const ShaderInfo& info)
{
	// Open file
	const auto stageName = GeneratorUtils::StageName(stageGen.Stage());
	std::ofstream file{ FmtStr("%s.%s.spv", options_->OutputFile().c_str(), stageName.c_str()),
		std::ofstream::trunc | std::ofstream::binary };
	if (!file.is_open()) {
		return;
	}

	// Write the bytecode
	file.write((const char*)bytecode_.data(), bytecode_.size() * sizeof(uint32));
	file.flush();
}

// ====================================================================================================================
void SpirvCompiler::SaveAssembly(const StageGenerator& stageGen, const ShaderInfo& info)
{
	// Open file
	const auto stageName = GeneratorUtils::StageName(stageGen.Stage());
	std::ofstream file{ FmtStr("%s.%s.asm", options_->OutputFile().c_str(), stageName.c_str()),
		std::ofstream::trunc | std::ofstream::binary };
	if (!file.is_open()) {
		return;
	}

	// Write the bytecode
	file.write(assembly_.data(), assembly_.size());
	file.flush();
}

} // namespace vsl
