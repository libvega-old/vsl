/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

//// !!!!!!!!!!
//// It is IMPORTANT not to include any third-party headers in this file. This file is used as the sole source for the
//// lightweight loader library, and it absolutely should not have any additional dependencies. If there comes a time
//// where it cannot be reasonable avoided to include those headers in this file, we will need to rethink the linking.
//// !!!!!!!!!!

#include "./Common.Impl.hpp"
#include <VSL/Shader.hpp>

#include <algorithm>
#include <array>
#include <fstream>
#include <sstream>
#include <unordered_map>


namespace vsl
{

// ====================================================================================================================
template<typename T>
inline static void file_write(std::ostream& stream, const T& val) {
	stream.write(reinterpret_cast<const char*>(&val), sizeof(T));
}
template<typename T>
inline static void file_read(std::istream& stream, T& val) {
	static constexpr auto readSize = sizeof(T);
	stream.read(reinterpret_cast<char*>(&val), readSize);
	if (stream.gcount() != readSize) {
		throw std::runtime_error("Unexpected EOF");
	}
}
inline static void file_read_count(std::istream& stream, void* data, uint64 count) {
	stream.read(reinterpret_cast<char*>(data), count);
	if (stream.gcount() != count) {
		throw std::runtime_error("Unexpected EOF");
	}
}


// ====================================================================================================================
// ====================================================================================================================
String ToString(ShaderStages stages)
{
	std::stringstream ss{};
	if (bool(stages & ShaderStages::Vertex)) {
		ss << "|Vertex";
	}
	if (bool(stages & ShaderStages::TControl)) {
		ss << "|TControl";
	}
	if (bool(stages & ShaderStages::TEval)) {
		ss << "|TEval";
	}
	if (bool(stages & ShaderStages::Geometry)) {
		ss << "|Geometry";
	}
	if (bool(stages & ShaderStages::Fragment)) {
		ss << "|Fragment";
	}
	return (ss.tellp() == 0) ? "None" : ss.str().substr(1);
}

// ====================================================================================================================
ShaderStages FromStageCodeName(const String& name)
{
	static const std::unordered_map<String, ShaderStages> CODES{
		{ "vert", ShaderStages::Vertex }, { "tesc", ShaderStages::TControl }, { "tese", ShaderStages::TEval },
		{ "geom", ShaderStages::Geometry }, { "frag", ShaderStages::Fragment },
	};
	const auto it = CODES.find(name);
	return (it != CODES.end()) ? it->second : ShaderStages::None;
}


// ====================================================================================================================
// ====================================================================================================================
const String& ToString(InputSemantic semantic)
{
	static const std::array<String, uint32(InputSemantic::INST3) + 1> NAMES{
		"POS0", "POS1",
		"COLOR0", "COLOR1", "COLOR2", "COLOR3",
		"NORM0", "NORM1",
		"TAN0", "TAN1",
		"BINORM0", "BINORM1",
		"UV0", "UV1", "UV2", "UV3",
		"BWEIGHT0", "BWEIGHT1", "BWEIGHT2", "BWEIGHT3",
		"BINDEX0", "BINDEX1", "BINDEX2", "BINDEX3",
		"USER0", "USER1", "USER2", "USER3",
		"INST0", "INST1", "INST2", "INST3"
	};
	return NAMES.at(uint32(semantic));
}

// ====================================================================================================================
std::optional<InputSemantic> FromSemanticString(const String& str)
{
	static const std::unordered_map<String, InputSemantic> NAMES{
		{ "POS0", InputSemantic::POS0 },
		{ "POS1", InputSemantic::POS1 },
		{ "COLOR0", InputSemantic::COLOR0 },
		{ "COLOR1", InputSemantic::COLOR1 },
		{ "COLOR2", InputSemantic::COLOR2 },
		{ "COLOR3", InputSemantic::COLOR3 },
		{ "NORM0", InputSemantic::NORM0 },
		{ "NORM1", InputSemantic::NORM1 },
		{ "TAN0", InputSemantic::TAN0 },
		{ "TAN1", InputSemantic::TAN1 },
		{ "BINORM0", InputSemantic::BINORM0 },
		{ "BINORM1", InputSemantic::BINORM1 },
		{ "UV0", InputSemantic::UV0 },
		{ "UV1", InputSemantic::UV1 },
		{ "UV2", InputSemantic::UV2 },
		{ "UV3", InputSemantic::UV3 },
		{ "BWEIGHT0", InputSemantic::BWEIGHT0 },
		{ "BWEIGHT1", InputSemantic::BWEIGHT1 },
		{ "BWEIGHT2", InputSemantic::BWEIGHT2 },
		{ "BWEIGHT3", InputSemantic::BWEIGHT3 },
		{ "BINDEX0", InputSemantic::BINDEX0 },
		{ "BINDEX1", InputSemantic::BINDEX1 },
		{ "BINDEX2", InputSemantic::BINDEX2 },
		{ "BINDEX3", InputSemantic::BINDEX3 },
		{ "USER0", InputSemantic::USER0 },
		{ "USER1", InputSemantic::USER1 },
		{ "USER2", InputSemantic::USER2 },
		{ "USER3", InputSemantic::USER3 },
		{ "INST0", InputSemantic::INST0 },
		{ "INST1", InputSemantic::INST1 },
		{ "INST2", InputSemantic::INST2 },
		{ "INST3", InputSemantic::INST3 }
	};
	String copy{ str };
	std::transform(copy.begin(), copy.end(), copy.begin(), ::toupper);
	const auto it = NAMES.find(copy);
	return (it != NAMES.end()) ? it->second : std::optional<InputSemantic>{ };
}


// ====================================================================================================================
// ====================================================================================================================
std::optional<Blending::Factor> Blending::ParseFactor(const String& factor)
{
	static const std::unordered_map<String, Factor> FACTORS{
		{ "zero",     Factor::Zero },
		{ "one",      Factor::One },
		{ "srcc",     Factor::SrcC },
		{ "isrcc",    Factor::ISrcC },
		{ "dstc",     Factor::DstC },
		{ "idstc",    Factor::IDstC },
		{ "srca",     Factor::SrcA },
		{ "isrca",    Factor::ISrcA },
		{ "dsta",     Factor::DstA },
		{ "idsta",    Factor::IDstA },
		{ "refc",     Factor::RefC },
		{ "irefc",    Factor::IRefC },
		{ "refa",     Factor::RefA },
		{ "irefa",    Factor::IRefA },
		{ "saturate", Factor::Saturate }
	};
	String copy{ factor };
	std::transform(copy.begin(), copy.end(), copy.begin(), ::tolower);
	const auto it = FACTORS.find(copy);
	return (it != FACTORS.end()) ? it->second : std::optional<Factor>{ };
}

// ====================================================================================================================
std::optional<Blending::Op> Blending::ParseOp(const String& op)
{
	static const std::unordered_map<String, Op> OPS{
		{ "add",    Op::Add },
		{ "sub",    Op::Sub },
		{ "revsub", Op::RevSub },
		{ "min",    Op::Min },
		{ "max",    Op::Max }
	};
	String copy{ op };
	std::transform(copy.begin(), copy.end(), copy.begin(), ::tolower);
	const auto it = OPS.find(copy);
	return (it != OPS.end()) ? it->second : std::optional<Op>{ };
}

// ====================================================================================================================
std::optional<Blending> Blending::ParseBlendingName(const String& name)
{
	static const std::unordered_map<String, Blending> BLENDS{
		{ "none",  Blending::None },  { "off", Blending::None }, { "opaque", Blending::None },
		{ "alpha", Blending::Alpha }, { "mix", Blending::Alpha },
		{ "add",   Blending::Add },
		{ "mul",   Blending::Mul },
		{ "max",   Blending::Max },
		{ "min",   Blending::Min }
	};
	String copy{ name };
	std::transform(copy.begin(), copy.end(), copy.begin(), ::tolower);
	const auto it = BLENDS.find(copy);
	return (it != BLENDS.end()) ? it->second : std::optional<Blending>{ };
}


// ====================================================================================================================
// ====================================================================================================================
Shader::Shader()
	: stages_{ }
	, inputs_{ }
	, outputs_{ }
	, bindings_{ }
	, uniform_{ }
	, uniformMembers_{ }
	, subpassInputs_{ }
	, bytecodes_{ }
{

}

// ====================================================================================================================
Shader::~Shader()
{

}

// ====================================================================================================================
void Shader::WriteToFile(const String& filePath)
{
	static const char MAGIC[3]{ 'V', 'B', 'C' };

	// Open file
	std::ofstream file{ filePath, std::ofstream::trunc | std::ofstream::binary };
	if (!file.is_open()) {
		return;
	}

	// Write standard file header
	file_write(file, MAGIC);    // Magic number "VBC"
	file_write(file, uint8(1)); // File version 1
	file_write(file, uint8(1)); // Shader type (1 == graphics)

	// Write the bytecode sizes
	uint16 bytecodeSizes[5]{
		uint16(bool(stages_ & ShaderStages::Vertex)   ? bytecodes_.at(ShaderStages::Vertex).size()   : 0),
		uint16(bool(stages_ & ShaderStages::TControl) ? bytecodes_.at(ShaderStages::TControl).size() : 0),
		uint16(bool(stages_ & ShaderStages::TEval)    ? bytecodes_.at(ShaderStages::TEval).size()    : 0),
		uint16(bool(stages_ & ShaderStages::Geometry) ? bytecodes_.at(ShaderStages::Geometry).size() : 0),
		uint16(bool(stages_ & ShaderStages::Fragment) ? bytecodes_.at(ShaderStages::Fragment).size() : 0)
	};
	file_write(file, bytecodeSizes);

	// Write vertex inputs
	file_write(file, uint8(inputs_.size()));
	for (const auto& input : inputs_) {
		file_write(file, input);
	}

	// Write fragment outputs
	file_write(file, uint8(outputs_.size()));
	for (const auto& output : outputs_) {
		file_write(file, output);
	}

	// Write bindings
	file_write(file, uint8(bindings_.size()));
	for (const auto& binding : bindings_) {
		file_write(file, binding);
	}

	// Write subpass inputs
	file_write(file, uint8(subpassInputs_.size()));
	for (const auto& spinput : subpassInputs_) {
		file_write(file, spinput);
	}

	// Write uniform info
	if (uniform_.has_value()) {
		const auto& uniform = uniform_.value();

		file_write(file, uniform);
		file_write(file, uint16(uniformMembers_.size()));
		for (const auto& member : uniformMembers_) {
			file_write(file, uint8(member.first.length()));
			file.write(member.first.data(), member.first.length());
			file_write(file, member.second);
		}
	}
	else {
		file_write(file, UniformRecord{ });
	}

	// Write bytecodes
	if (bool(stages_ & ShaderStages::Vertex)) {
		const auto& bc = bytecodes_.at(ShaderStages::Vertex);
		file.write(reinterpret_cast<const char*>(bc.data()), bc.size() * sizeof(uint32));
	}
	if (bool(stages_ & ShaderStages::TControl)) {
		const auto& bc = bytecodes_.at(ShaderStages::TControl);
		file.write(reinterpret_cast<const char*>(bc.data()), bc.size() * sizeof(uint32));
	}
	if (bool(stages_ & ShaderStages::TEval)) {
		const auto& bc = bytecodes_.at(ShaderStages::TEval);
		file.write(reinterpret_cast<const char*>(bc.data()), bc.size() * sizeof(uint32));
	}
	if (bool(stages_ & ShaderStages::Geometry)) {
		const auto& bc = bytecodes_.at(ShaderStages::Geometry);
		file.write(reinterpret_cast<const char*>(bc.data()), bc.size() * sizeof(uint32));
	}
	if (bool(stages_ & ShaderStages::Fragment)) {
		const auto& bc = bytecodes_.at(ShaderStages::Fragment);
		file.write(reinterpret_cast<const char*>(bc.data()), bc.size() * sizeof(uint32));
	}

	// Flush bytecode before return
	file.flush();
}

// ====================================================================================================================
std::tuple<UPtr<Shader>, String> Shader::LoadFromFile(const String& filePath)
{
	static const char MAGIC[3]{ 'V', 'B', 'C' };

	try {
		// Open file
		std::ifstream file{ filePath, std::ofstream::binary };
		if (!file.is_open()) {
			throw std::runtime_error("Failed to open file");
		}

		// Create shader object
		UPtr<Shader> shader{ new Shader };

		// Read in and validate the standard header
		char magic[3];
		uint8 fileVersion{}, shaderType{};
		file_read(file, magic);
		file_read(file, fileVersion);
		file_read(file, shaderType);
		if (std::memcmp(MAGIC, magic, sizeof(MAGIC)) != 0) {
			throw std::runtime_error("Invalid shader file (bad magic number)");
		}
		if (fileVersion != 1) {
			throw std::runtime_error("Invalid shader file (unsupported version)");
		}
		if (shaderType != 1) {
			throw std::runtime_error("Invalid shader file (unknown type)");
		}

		// Read in bytecode lengths and calculate shader mask
		uint16 bytecodeLengths[5];
		file_read(file, bytecodeLengths);
		shader->stages_ =
			((bytecodeLengths[0] > 0) ? ShaderStages::Vertex   : ShaderStages::None) |
			((bytecodeLengths[1] > 0) ? ShaderStages::TControl : ShaderStages::None) |
			((bytecodeLengths[2] > 0) ? ShaderStages::TEval    : ShaderStages::None) |
			((bytecodeLengths[3] > 0) ? ShaderStages::Geometry : ShaderStages::None) |
			((bytecodeLengths[4] > 0) ? ShaderStages::Fragment : ShaderStages::None);

		// Read vertex inputs
		uint8 inputCount{ };
		file_read(file, inputCount);
		shader->inputs_.resize(inputCount);
		for (auto& input : shader->inputs_) {
			file_read(file, input);
		}

		// Read fragment outputs
		uint8 outputCount{ };
		file_read(file, outputCount);
		shader->outputs_.resize(outputCount);
		for (auto& output : shader->outputs_) {
			file_read(file, output);
		}

		// Read bindings
		uint8 bindingCount{ };
		file_read(file, bindingCount);
		shader->bindings_.resize(bindingCount);
		for (auto& binding : shader->bindings_) {
			file_read(file, binding);
		}

		// Read subpass inputs
		uint8 spiCount{ };
		file_read(file, spiCount);
		shader->subpassInputs_.resize(spiCount);
		for (auto& spi : shader->subpassInputs_) {
			file_read(file, spi);
		}

		// Read the uniform data
		UniformRecord tmpUniform{ };
		file_read(file, tmpUniform);
		if (tmpUniform.size > 0) {
			shader->uniform_ = tmpUniform;
		}

		// If uniform is present, read the members
		if (shader->uniform_.has_value()) {
			uint16 memberCount{ };
			file_read(file, memberCount);
			shader->uniformMembers_.resize(memberCount);
			for (auto& memberPair : shader->uniformMembers_) {
				uint8 nameSize{ };
				file_read(file, nameSize);
				memberPair.first.resize(nameSize, '\0');
				file_read_count(file, memberPair.first.data(), nameSize);
				file_read(file, memberPair.second);
			}
		}

		// Read bytecodes
		if (bool(shader->stages_ & ShaderStages::Vertex)) {
			auto& bc = shader->bytecodes_.emplace(ShaderStages::Vertex, std::vector<uint32>{ }).first->second;
			bc.resize(bytecodeLengths[0]);
			file_read_count(file, bc.data(), bc.size() * sizeof(uint32));
		}
		if (bool(shader->stages_ & ShaderStages::TControl)) {
			auto& bc = shader->bytecodes_.emplace(ShaderStages::TControl, std::vector<uint32>{ }).first->second;
			bc.resize(bytecodeLengths[1]);
			file_read_count(file, bc.data(), bc.size() * sizeof(uint32));
		}
		if (bool(shader->stages_ & ShaderStages::TEval)) {
			auto& bc = shader->bytecodes_.emplace(ShaderStages::TEval, std::vector<uint32>{ }).first->second;
			bc.resize(bytecodeLengths[2]);
			file_read_count(file, bc.data(), bc.size() * sizeof(uint32));
		}
		if (bool(shader->stages_ & ShaderStages::Geometry)) {
			auto& bc = shader->bytecodes_.emplace(ShaderStages::Geometry, std::vector<uint32>{ }).first->second;
			bc.resize(bytecodeLengths[3]);
			file_read_count(file, bc.data(), bc.size() * sizeof(uint32));
		}
		if (bool(shader->stages_ & ShaderStages::Fragment)) {
			auto& bc = shader->bytecodes_.emplace(ShaderStages::Fragment, std::vector<uint32>{ }).first->second;
			bc.resize(bytecodeLengths[4]);
			file_read_count(file, bc.data(), bc.size() * sizeof(uint32));
		}

		// Return completed shader object
		return { std::move(shader), String{ } };
	}
	catch (const std::exception& ex) {
		return { UPtr<Shader>{ }, String{ ex.what() } };
	}
}

} // namespace vsl
