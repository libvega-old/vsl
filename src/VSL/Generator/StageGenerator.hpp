/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

#include "../Common.Impl.hpp"
#include "../Types.Builtin.hpp"
#include "../Reflection.hpp"
#include "./FunctionGenerator.hpp"

#include <sstream>
#include <vector>


namespace vsl
{

class CompilerOptions;

// Manages the generation of GLSL source code for a specific stage, outside of function generation
class StageGenerator final
{
public:
	StageGenerator(const CompilerOptions* options, ShaderStages stage);
	~StageGenerator();

	inline ShaderStages Stage() const { return stage_; }
	inline const std::stringstream& Source() const { return source_; }

	void Generate(const FunctionGenerator& func, const ShaderInfo& info);
	bool Save();

private:
	void emitStruct(const ShaderType& type);
	void emitInput(const InputVariable& var);
	void emitOutput(const OutputVariable& var);
	void emitBinding(const BindingVariable& bind);
	void emitSubpassInput(const SubpassInputVariable& var);
	void emitBindingIndices(uint32 maxIndex);
	void emitLocal(const LocalVariable& var);

	void getBindingInfo(const ShaderType& type, uint32* set, uint32* binding, String* tableName);

private:
	const CompilerOptions* const options_;
	const ShaderStages stage_;
	std::stringstream source_;
	std::vector<const ShaderType*> structTypes_;
	uint32 uid_;
	uint32 inIdx_;
	uint32 outIdx_;

	VSL_NO_COPY(StageGenerator)
	VSL_NO_MOVE(StageGenerator)
}; // class StageGenerator

} // namespace vsl
