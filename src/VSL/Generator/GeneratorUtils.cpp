/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "./GeneratorUtils.hpp"

#include <array>
#include <algorithm>


namespace vsl
{

// ====================================================================================================================
String GeneratorUtils::BindingTableName(const ShaderType& type)
{
	static const auto prefix = [](TexelFormat texelFormat) -> String {
		return texelFormat.IsSigned() ? "i" : texelFormat.IsUnsigned() ? "u" : "";
	};

	String basename{ };
	switch (type.baseType)
	{
	case BaseType::Sampler: {
		const auto rtxt = type.texel.rank.TypeSuffix();
		const auto ltxt = prefix(type.texel.format);
		basename = ltxt + "sampler" + rtxt;
	} break;
	case BaseType::Image: {
		const auto rtxt = type.texel.rank.TypeSuffix();
		const auto ftxt = type.texel.format.GetVSLDecorator();
		basename = "image" + rtxt + "_" + ftxt;
	} break;
	case BaseType::ROTexels: {
		const auto fix = prefix(type.texel.format);
		basename = fix + "rotexels";
	} break;
	case BaseType::RWTexels: {
		const auto ftxt = type.texel.format.GetVSLDecorator();
		basename = "rwtexels_" + ftxt;
	} break;
	case BaseType::ROBuffer:
	case BaseType::RWBuffer: 
	case BaseType::Struct: {
		return ""; // Uniforms and buffers don't use the table name
	} break;
	}

	// Create final name
	assert(!basename.empty() && "Invalid binding table shader type");
	std::transform(basename.begin(), basename.end(), basename.begin(), ::toupper);
	return "_" + basename + "_TABLE_";
}

// ====================================================================================================================
String GeneratorUtils::BindingIndexLoadString(uint32 index)
{
	return ((index & 1) == 0)
		? FmtStr("(_bidx_.index%u & 0x0000FFFF)", index / 2)
		: FmtStr("(_bidx_.index%u >> 16)", index / 2);
}

// ====================================================================================================================
String GeneratorUtils::BuiltinName(const String& name)
{
	static const std::unordered_map<String, String> NAMES{
		{ "$VertexIndex",   "gl_VertexIndex" },
		{ "$InstanceIndex", "gl_InstanceIndex" },
		{ "$DrawIndex",     "gl_DrawIndex" },
		{ "$VertexBase",    "gl_BaseVertex" },
		{ "$InstanceBase",  "gl_BaseInstance" },

		{ "$Position",  "gl_Position" },
		{ "$PointSize", "gl_PointSize" },

		{ "$FragCoord",   "gl_FragCoord" },
		{ "$FrontFacing", "gl_FrontFacing" },
		{ "$PointCoord",  "gl_PointCoord" },
		{ "$PrimitiveID", "gl_PrimitiveID" },

		{ "$G", "globals_" }
	};

	const auto it = NAMES.find(name);
	assert((it != NAMES.end()) && "Invalid builtin name");
	return it->second;
}

// ====================================================================================================================
String GeneratorUtils::StageName(ShaderStages stage)
{
	if (stage == ShaderStages::Vertex) {
		return "vert";
	}
	if (stage == ShaderStages::TControl) {
		return "tesc";
	}
	if (stage == ShaderStages::TEval) {
		return "tese";
	}
	if (stage == ShaderStages::Geometry) {
		return "geom";
	}
	if (stage == ShaderStages::Fragment) {
		return "frag";
	}

	assert(false && "Invalid shader stage for name");
	return {};
}

// ====================================================================================================================
String GeneratorUtils::GLSLFormatSpecifier(TexelFormat format)
{
	static const std::array<String, 21> NAMES{
		"r8", "rg8", "rgba8",
		"r8_snorm", "rg8_snorm", "rgba8_snorm",
		"r16", "rg16", "rgba16",
		"r16_snorm", "rg16_snorm", "rgba16_snorm",
		"r32i", "rg32i", "rgba32i",
		"r32ui", "rg32ui", "rgba32ui",
		"r32f", "rg32f", "rgba32f"
	};
	return NAMES.at(format.Value());
}

// ====================================================================================================================
String GeneratorUtils::GLSLTypeName(const ShaderType& type)
{
	static const auto prefix = [](TexelFormat texelFormat) -> String {
		return texelFormat.IsSigned() ? "i" : texelFormat.IsUnsigned() ? "u" : "";
	};

	switch (type.baseType)
	{
	case BaseType::Void: return "void";
	case BaseType::Boolean: return
		(type.numeric.dims[0] == 1) ? "bool" : (type.numeric.dims[0] == 2) ? "bvec2" :
		(type.numeric.dims[0] == 3) ? "bvec3" : "bvec4";
	case BaseType::Signed: {
		assert((type.numeric.size == 4) && "Bad signed type size");
		return
			(type.numeric.dims[0] == 1) ? "int" : (type.numeric.dims[0] == 2) ? "ivec2" :
			(type.numeric.dims[0] == 3) ? "ivec3" : "ivec4";
	}
	case BaseType::Unsigned: {
		assert((type.numeric.size == 4) && "Bad unsigned type size");
		return
			(type.numeric.dims[0] == 1) ? "uint" : (type.numeric.dims[0] == 2) ? "uvec2" :
			(type.numeric.dims[0] == 3) ? "uvec3" : "uvec4";
	}
	case BaseType::Float: {
		assert((type.numeric.size == 4) && "Bad floating type size");
		switch (type.numeric.dims[1])
		{
		case 1: return
			(type.numeric.dims[0] == 1) ? "float" : (type.numeric.dims[0] == 2) ? "vec2" :
			(type.numeric.dims[0] == 3) ? "vec3" : "vec4";
		case 2: return (type.numeric.dims[0] == 2) ? "mat2x2" : (type.numeric.dims[0] == 3) ? "mat2x3" : "mat2x4";
		case 3: return (type.numeric.dims[0] == 2) ? "mat3x2" : (type.numeric.dims[0] == 3) ? "mat3x3" : "mat3x4";
		default: return (type.numeric.dims[0] == 2) ? "mat4x2" : (type.numeric.dims[0] == 3) ? "mat4x3" : "mat4x4";
		}
	} break;
	case BaseType::Sampler: return prefix(type.texel.format) + "sampler" + type.texel.rank.TypeSuffix();
	case BaseType::Image: return prefix(type.texel.format) + "image" + type.texel.rank.TypeSuffix();
	case BaseType::ROBuffer:
	case BaseType::RWBuffer: return type.buffer.structType->userStruct.type->Name() + "_t";
	case BaseType::ROTexels: return prefix(type.texel.format) + "samplerBuffer";
	case BaseType::RWTexels: return prefix(type.texel.format) + "imageBuffer";
	case BaseType::SubpassInput: return prefix(type.texel.format) + "subpassInput";
	case BaseType::Struct: return type.userStruct.type->Name() + "_t";
	default: assert(false && "Bad base type"); return { };
	}
}

} // namespace vsl
