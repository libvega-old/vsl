/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "./FunctionGenerator.hpp"
#include "./GeneratorUtils.hpp"


namespace vsl
{

// ====================================================================================================================
static const String CRLF{ "\r\n" };

// ====================================================================================================================
FunctionGenerator::FunctionGenerator(ShaderStages stage)
	: name_{ "main" }
	, stage_{ stage }
	, source_{ }
	, indent_{ '\t' }
	, uid_{ 0 }
	, bindingMask_{ 0 }
	, passInputMask_{ 0 }
{

}

// ====================================================================================================================
FunctionGenerator::~FunctionGenerator()
{

}

// ====================================================================================================================
void FunctionGenerator::EmitDeclaration(const ShaderType& type, const String& name)
{
	source_ << indent_ << GeneratorUtils::GLSLTypeName(type) << " " << name << ";" << CRLF;
}

// ====================================================================================================================
void FunctionGenerator::EmitVariableDefinition(const ShaderType& type, const String& name, const String& value)
{
	source_ << indent_ << GeneratorUtils::GLSLTypeName(type) << " " << name << " = " << value << ";" << CRLF;
}

// ====================================================================================================================
void FunctionGenerator::EmitAssignment(const String& left, const String& op, const String& value)
{
	source_ << indent_ << left << " " << op << " " << value << ";" << CRLF;
}

// ====================================================================================================================
String FunctionGenerator::EmitTempDefinition(const ShaderType& type, const String& value)
{
	source_ << indent_ << GeneratorUtils::GLSLTypeName(type) << " _t" << (uid_++) << "_ = " << value << ";" << CRLF;
	return FmtStr("_t%u_", uid_ - 1);
}

// ====================================================================================================================
void FunctionGenerator::EmitImageStore(const String& imStore, const String& value)
{
	String repl{ imStore };
	const auto repidx = repl.find("{}");
	repl.replace(repidx, 2, value);
	source_ << indent_ << repl << ";" << CRLF;
}

// ====================================================================================================================
void FunctionGenerator::EmitIf(const String& cond)
{
	source_ << indent_ << "if (" << cond << ") {" << CRLF;
	indent_ += '\t';
}

// ====================================================================================================================
void FunctionGenerator::EmitElif(const String& cond)
{
	source_ << indent_ << "else if (" << cond << ") {" << CRLF;
	indent_ += '\t';
}

// ====================================================================================================================
void FunctionGenerator::EmitElse()
{
	source_ << indent_ << "else {" << CRLF;
	indent_ += '\t';
}

// ====================================================================================================================
void FunctionGenerator::EmitForLoop(const String& name, int32 start, int32 end, int32 step)
{
	const char comp = (step > 0) ? '<' : '>';
	const char op = (step > 0) ? '+' : '-';
	source_
		<< indent_
		<< "for (int " << name << " = " << start << "; "
		<< name << ' ' << comp << ' ' << end << "; "
		<< name << ' ' << op << "= " << std::abs(step) << ") {" << CRLF;
	indent_ += '\t';
}

// ====================================================================================================================
void FunctionGenerator::CloseBlock()
{
	indent_ = indent_.substr(0, indent_.size() - 1);
	source_ << indent_ << "}" << CRLF;
}

// ====================================================================================================================
void FunctionGenerator::EmitControlStatement(const String& keyword)
{
	source_ << indent_ << keyword << ";" << CRLF;
}

// ====================================================================================================================
void FunctionGenerator::EmitBindingIndex(uint32 index)
{
	if (bindingMask_ & (1u << index)) {
		return; // Already emitted
	}
	bindingMask_ |= (1u << index);

	// Put at beginning
	const auto load = GeneratorUtils::BindingIndexLoadString(index);
	const auto saved = source_.str();
	source_ = {};
	source_ << "\tuint _bidx" << index << "_ = " << load << ";" << CRLF;
	source_ << saved;
}

} // namespace vsl
