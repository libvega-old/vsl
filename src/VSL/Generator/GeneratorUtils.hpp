/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

#include "../Common.Impl.hpp"
#include "../Types.Builtin.hpp"
#include "../Reflection.hpp"


namespace vsl
{

// Miscellaneous utilities related to code generation
class GeneratorUtils final
{
public:
	// Binding
	static String BindingTableName(const ShaderType& type);
	static String BindingIndexLoadString(uint32 index);

	// Names
	static String BuiltinName(const String& name);
	static String StageName(ShaderStages stage);
	static String GLSLFormatSpecifier(TexelFormat format);
	static String GLSLTypeName(const ShaderType& type);

	VSL_NO_COPY(GeneratorUtils)
	VSL_NO_MOVE(GeneratorUtils)
	VSL_NO_INIT(GeneratorUtils)
}; // class GeneratorUtils

} // namespace vsl
