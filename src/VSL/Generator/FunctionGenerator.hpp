/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

#include "../Common.Impl.hpp"
#include "../Reflection.hpp"

#include <sstream>


namespace vsl
{

// Manages the generation of GLSL source code for function bodies
class FunctionGenerator final
{
public:
	FunctionGenerator(ShaderStages stage);
	~FunctionGenerator();

	inline const String& Name() const { return name_; }
	inline ShaderStages Stage() const { return stage_; }
	inline const std::stringstream& Source() const { return source_; }

	/// Assignment
	void EmitDeclaration(const ShaderType& type, const String& name);
	void EmitVariableDefinition(const ShaderType& type, const String& name, const String& value);
	void EmitAssignment(const String& left, const String& op, const String& value);
	String EmitTempDefinition(const ShaderType& type, const String& value);
	void EmitImageStore(const String& imStore, const String& value);

	/// Blocks
	void EmitIf(const String& cond);
	void EmitElif(const String& cond);
	void EmitElse();
	void EmitForLoop(const String& name, int32 start, int32 end, int32 step);
	void CloseBlock();

	/// Other
	void EmitControlStatement(const String& keyword);
	void EmitBindingIndex(uint32 index);

private:
	const String name_;
	const ShaderStages stage_;
	std::stringstream source_;
	String indent_;
	uint32 uid_;
	uint32 bindingMask_;
	uint32 passInputMask_;

	VSL_NO_COPY(FunctionGenerator)
	VSL_NO_MOVE(FunctionGenerator)
}; // class FunctionGenerator

} // namespace vsl
